package controllerServer;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import model.EColore;
import model.Giocatore;
import model.PedinaPastore;
import model.mosse.IComunicazione;
import model.mosse.Messaggio;
import view.CLCHelper;
import view.Start;

/**
 * CLasse per la gestione delle partite in RMI
 * 
 * @author federico
 * 
 */
public class StanzaRMI extends Stanza {

	/**
	 * rappresenta tutti client associati alla stanza, ad ogni client un solo
	 * giocatore
	 */
	private final Map<Integer, ClientRMI> atClient;
	private Date atTempoMinimoGiocatori;
	private static final Integer AT_TEMPO_TIMEOUT = 15;
	private static final Integer AT_MOSSE = 3;
	private Integer atMosseRimaste;
	private final AtomicBoolean atFineMossePedine;
	AtomicInteger eroGiocatoreAttuale = null;
	private AtomicInteger atGiocatorePedinaAttuale = null;
	private final AtomicBoolean atPrimoTurno;

	/**
	 * Costruttore
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapprsenta l'ID dela parita
	 */
	public StanzaRMI(Integer idPartita) {
		super(idPartita);
		this.atClient = new LinkedHashMap<Integer, ClientRMI>();
		this.atIdPartita = idPartita;
		this.atTempoMinimoGiocatori = null;
		this.atMosseRimaste = AT_MOSSE;
		this.atFineMossePedine = new AtomicBoolean(false);
		this.eroGiocatoreAttuale = new AtomicInteger(-1);
		this.atPrimoTurno = new AtomicBoolean(true);
	}

	/**
	 * Ritorna un valore che indica se sta per iniziare il primo turno della
	 * partita
	 * 
	 * @return {@link AtomicBoolean} che indica se sta per iniziare il primo
	 *         turno della partita
	 */
	public AtomicBoolean getPrimoTurno() {
		return this.atPrimoTurno;
	}

	/**
	 * Ritorna un valore che indica se ? finita la fase di impostazione delle
	 * pedine
	 * 
	 * @return {@link AtomicBoolean} che indica se è finita la fase di
	 *         posizionamento delle pedine
	 */
	public AtomicBoolean getFineMossePedine() {
		return this.atFineMossePedine;
	}

	/**
	 * Ritorna il set di id dei giocatori
	 * 
	 * @return
	 */
	public Set<Integer> getIdGiocatori() {
		return this.atClient.keySet();
	}

	/**
	 * riduce di uno il conteggio delle model.mosse rimaste
	 */
	void riduciMosseRimaste() {
		this.atMosseRimaste--;
	}

	/**
	 * calcola il prossimo giocatore
	 */
	public void calcolaProssimoGiocatore() {
		Integer[] wvIdGiocatori = this.atClient.keySet().toArray(new Integer[0]);
		Integer wvIndice;
		Integer wvNumeroGiocatori = wvIdGiocatori.length;
		Integer wvContatore = -1;
		// CLCHelper.Stampa("§§§§§§§§§§§§§§\nHai ancora " + at_mosse_rimaste
		// + " model.mosse rimanenti, per " + getGiocatoreAttuale().get());
		this.atMosseRimaste--;
		if ((this.atMosseRimaste > 0) && !this.atClient.get((this.getGiocatoreAttuale().get())).getScollegato().get()) {
			// CLCHelper.Stampa("§§§§§§§§§§§§§§\nHai ancora " +
			// at_mosse_rimaste
			// + " model.mosse rimanenti, per " + getGiocatoreAttuale().get());
			return;
		}
		// CLCHelper
		// .Stampa("----------------\nMosse del turno finite...\n---------------------");
		this.atMosseRimaste = AT_MOSSE;
		for (wvIndice = 0; wvIndice < wvNumeroGiocatori; wvIndice++) {
			if (wvIdGiocatori[wvIndice].equals(this.getGiocatoreAttuale().get())) {
				CLCHelper.stampa("Ho trovato il giocatore designato... break");
				break;
			}
		}
		this.getPartitaServer().calcolaFaseFinale();
		do {
			wvIndice++;

			if (wvIndice >= wvNumeroGiocatori) {
				// qui ci entro quando andrei outofbound
				this.getPartitaServer().attivaFaseLupo();
				if (this.getPartitaServer().getFaseFinale()) {
					this.setTerminaPartita(true);
				}
				wvIndice = 0;
			}
			CLCHelper.stampa("++++++\n" + this.getGiocatoreAttuale().get() + "\n++++++");
			this.getGiocatoreAttuale().set(wvIdGiocatori[wvIndice]);
			CLCHelper.stampa("|||||||||\n" + this.getGiocatoreAttuale().get() + "\n||||||");
			this.getPartitaServer().attivaFaseAgnelli();
			wvContatore++;
		} while (this.atClient.get((this.getGiocatoreAttuale().get())).getScollegato().get() && (wvContatore <= wvNumeroGiocatori));
	}

	/**
	 * come get tempo scaduto, ma lo calcola anche
	 * 
	 * @return
	 */
	public synchronized Boolean calcolaTempoScaduto() {
		if (this.getTempoScaduto()) {
			return true;
		}
		if (this.minimoGiocatori() && (this.atTempoMinimoGiocatori == null)) {
			this.atTempoMinimoGiocatori = new Date();
		}
		if (Start.abilitaTest2G) {
			if (this.atTempoMinimoGiocatori != null) {
				if (this.getDateDiff(this.atTempoMinimoGiocatori, new Date(), TimeUnit.SECONDS) > 0.5) {
					this.setTempoScaduto(true);
					return true;
				}
			}
		} else {
			if (this.atTempoMinimoGiocatori != null) {
				if (this.getDateDiff(this.atTempoMinimoGiocatori, new Date(), TimeUnit.SECONDS) > AT_TEMPO_TIMEOUT) {
					this.setTempoScaduto(true);
					return true;
				}
			}
		}

		return false;
	}

	void calcolaPrssimoGiocatoreMercato() {
		Integer[] wvIdGiocatori = this.atClient.keySet().toArray(new Integer[0]);
		Integer wvIndice;
		Integer wvNumeroGiocatori = wvIdGiocatori.length;
		Integer wvContatore = 0;

		switch (this.atFaseMercato) {
		case 0:
			// imposto il primo giocatore per la proposta delle offerte
			this.atFaseMercato++;
			for (wvIndice = 0; wvIndice < wvNumeroGiocatori; wvIndice++) {
				if (!this.atClient.get(wvIdGiocatori[wvIndice]).getScollegato().get()) {
					this.atIdGiocatoreAttualeMercato = wvIdGiocatori[this.atFaseMercato];
					break;
				}
			}
			break;
		case 1:
			// imposto i giocatori successivi per la proposta delle offerte
			wvContatore = 0;
			wvIndice = this.cercaIndiceGiocatore(wvIdGiocatori, this.atIdGiocatoreAttualeMercato);

			do {
				wvIndice++;
				if (wvIndice <= wvNumeroGiocatori) {
					this.atIdGiocatoreAttualeMercato = (wvIdGiocatori[wvIndice]);
					wvContatore++;
				} else {
					this.atFaseMercato++;
					break;
				}
			} while (this.atClient.get(this.atIdGiocatoreAttualeMercato).getScollegato().get() && (wvContatore <= wvNumeroGiocatori));
			break;
		// se la fase è < 2 mando _TURNOOFFERTA
		// se la fase mercato è maggiore di 2 e minore di 4 mando _TURNOACQUISTO
		// indico che è il momento di comprare
		case 2:
			wvContatore = 0;
			wvIndice = new Random().nextInt(wvNumeroGiocatori);
			this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
			while (this.atClient.get(this.atIdGiocatoreAttualeMercato).getScollegato().get() && (wvContatore <= wvNumeroGiocatori)) {
				this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
				this.atIdPrimoGiocatoreAcquisto = this.atIdGiocatoreAttualeMercato;
				wvIndice++;
				if (wvIndice >= wvNumeroGiocatori) {
					wvIndice = 0;
				}
			}
			this.atFaseMercato++;
			break;
		case 3:
			wvContatore = 0;
			wvIndice = this.cercaIndiceGiocatore(wvIdGiocatori, this.atIdGiocatoreAttualeMercato);
			do {
				wvIndice++;
				if (wvIndice >= wvNumeroGiocatori) {
					wvIndice = 0;
				}
				this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
			} while (this.atClient.get(this.atIdGiocatoreAttualeMercato).getScollegato().get() && (wvContatore <= wvNumeroGiocatori));
			if (this.atIdGiocatoreAttualeMercato.equals(this.atIdPrimoGiocatoreAcquisto)) {
				this.atFaseMercato++;
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Ottiene la differenza tra 2 date
	 * 
	 * @param date1
	 *            la data più vecchia
	 * @param date2
	 *            la data più nuova
	 * @param timeUnit
	 *            l'unita di misura in cui vuoi la differenza
	 * @return la differenza tr 2 date nell'unita di misura specificata
	 */
	public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	/**
	 * Rileva la comunicazione dal buffer
	 * 
	 * @param idClient
	 *            {@link Integer} che rappresenta l'id del client a cui leggere
	 *            il buffer
	 * @return oggetto che implementa l'interfaccia {@link IComunicazione}
	 */
	public IComunicazione rilevaComunicazione(Integer idClient) {
		return this.atClient.get(idClient).leggiComunicazione();
	}

	/**
	 * Deposita la comunicazion su tutti i client
	 */
	public void depositaComunicazione(IComunicazione comunicazione) {
		for (Integer wvId : this.atClient.keySet()) {
			this.atClient.get(wvId).depositaComunicazione(comunicazione);
		}
	}

	/**
	 * Deposita la comunicazione su tutti i client meno quello specificato
	 * 
	 * @param idClient
	 */
	public void depositaComunicazione(IComunicazione comunicazione, Integer idClient) {
		for (Integer wvId : this.atClient.keySet()) {
			if (!wvId.equals(idClient)) {
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					CLCHelper.stampaEccezione(e);
				}
				this.atClient.get(wvId).depositaComunicazione(comunicazione);
			}
		}
	}

	/**
	 * Carica la mappa nella stanza
	 */
	public void caricaMappa() {
		this.caricaMappa(this.ottieniGiocatori());
	}

	/**
	 * Riassegna un {@link ClientRMI} alla stanza
	 * 
	 * @param idClient
	 *            {@link Integer} che rappresenta l'ID del client
	 */
	public void riassegnaClient(Integer idClient) {
		this.atClient.get(idClient).getScollegato().set(false);
	}

	/**
	 * Agginge un client nella stanza
	 * 
	 * @param nome
	 *            {@link String} che rappresenta il nome del {@link ClientRMI}
	 * @return Ritorna un {@link Integer} che rappresenta l'ID del gicotore
	 */
	public Integer aggiungiClient(String nome) {
		this.aggiungiGiocatore();
		Integer wvIdGiocatore = this.calcolaIDGocatore();
		CLCHelper.stampaAncheInTest("Client_RMI connesso. ID: " + wvIdGiocatore + " - Nome: " + nome);

		synchronized (this.atClient) {
			this.atClient.put(wvIdGiocatore, new ClientRMI(nome));
		}

		if (this.atClient.size() == 1) {
			this.atIdPrimoGiocatore = wvIdGiocatore;
			this.getGiocatoreAttuale().set(wvIdGiocatore);
			this.atGiocatorePedinaAttuale = new AtomicInteger(wvIdGiocatore);
		}

		return wvIdGiocatore;
	}

	/**
	*
	*/
	@Override
	ArrayList<Giocatore> ottieniGiocatori() {
		ArrayList<Giocatore> wvGiocatori = new ArrayList<Giocatore>();
		Integer wvId = 1;
		Integer wvDenaro;
		PedinaPastore[] wvPedine;
		for (Integer item : this.atClient.keySet()) {
			if (this.atClient.size() == 2) {
				wvDenaro = DENARODUEGIOCATORI;
				wvPedine = new PedinaPastore[2];
				wvPedine[0] = new PedinaPastore(this.calcolaIdPedina(wvId), EColore.values()[wvId / 3]);
				wvId++;
				wvPedine[1] = new PedinaPastore(this.calcolaIdPedina(wvId), EColore.values()[wvId / 3]);
			} else {
				wvDenaro = DENAROQUATTROGIOCATORI;
				wvPedine = new PedinaPastore[1];
				wvPedine[0] = new PedinaPastore(this.calcolaIdPedina(wvId), EColore.values()[wvId - 1]);
			}
			wvGiocatori.add(new Giocatore(item, this.atClient.get(item).getNome(), item / 100, wvPedine, this.calcolaTesseraIniziale(), wvDenaro));
			this.assegnaGiocatore(wvGiocatori.get(wvGiocatori.size() - 1));
			wvId++;
		}
		return wvGiocatori;
	}

	private void assegnaGiocatore(Giocatore giocatore) {
		for (PedinaPastore wvItem : giocatore.getPedinePastore()) {
			wvItem.setGiocatorePossessore(giocatore);
		}
	}

	/**
	 * Deposita un {@link Messaggio} di tipo nack nel buffer di ogni client
	 */
	public void depositaNack() {
		Messaggio wvMessaggio = new Messaggio();
		wvMessaggio.nack();
		this.depositaComunicazione(wvMessaggio);

	}

	/**
	 * Ritorna il gicoatore a cui tocca il turno attuale per l'impostazione
	 * della pedina
	 * 
	 * @return {@link AtomicInteger} che rappresenta l'id del giocatore
	 */
	public AtomicInteger getGiocatorePedinaAttuale() {
		return this.atGiocatorePedinaAttuale;
	}

	/**
	 * Ritorna la lista di {@link ClientRMI} presenti nella stanza. Effettua un
	 * accesso sincornizzato sul {@link Set} della {@link LinkedHashMap} che
	 * contiene i client
	 * 
	 * @return {@link ArrayList} di {@link ClientRMI} che rappresenta i client
	 *         collegati nella stanza
	 */
	public ArrayList<ClientRMI> getClientRMI() {
		ArrayList<ClientRMI> wvClient = new ArrayList<ClientRMI>();
		synchronized (this.atClient) {
			for (Integer wvInteger : this.getIdGiocatori()) {
				wvClient.add(this.atClient.get(wvInteger));
			}
		}
		return wvClient;
	}

	/**
	 * Ritorna il client specificandone l'id
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del client
	 * @return {@link ClientRMI} che rappresenta il client
	 */
	public ClientRMI getClient(Integer idGiocatore) {
		return this.atClient.get(idGiocatore);
	}

}

class ClientRMI {

	private static final Integer AT_MAX_TENTATIVI = 5;
	private static final Integer AT_TEMPO_ATESA = 2;

	private IstanteUltimoPing atIstanteUltimoPing;
	private final Buffer atBuffer;
	// devo impostarlo a false ogni volta che il client fa qualcosa
	private final AtomicBoolean atScollegato;
	/**
	 * Socket associate al client, identificate dall'ID gicoatore
	 */
	private final String atNome;

	/**
	 * Costruttore
	 * 
	 * @param nome
	 *            {@link String} che rappresenta il nome del gicoatore
	 */
	public ClientRMI(String nome) {
		this.atBuffer = new Buffer();
		this.atNome = nome;
		this.atScollegato = new AtomicBoolean(false);
		this.atIstanteUltimoPing = new IstanteUltimoPing();
	}

	/**
	 * Da usare anche per impostare se scollegato
	 * 
	 * @return {@link AtomicBoolean} che indica se il client è scollegato
	 */
	public AtomicBoolean getScollegato() {
		return this.atScollegato;
	}

	/**
	 * Ritorna il nome del client
	 * 
	 * @return {@link String} che rappresenta il nome del client
	 */
	public String getNome() {
		return this.atNome;
	}

	/**
	 * Immete la comunicazione nel buffer, segnandola come da leggere
	 * 
	 * @param comunicazione
	 */
	public void depositaComunicazione(IComunicazione comunicazione) {

		for (int i = 1; (i < AT_MAX_TENTATIVI) && this.atBuffer.getBufferDaLeggere().get(); i++) {
			try {
				Thread.sleep(AT_TEMPO_ATESA);
			} catch (InterruptedException e) {
				CLCHelper.stampaEccezione(e);
			}
		}

		if (this.atBuffer.getBufferDaLeggere().get()) {
			return;
		}
		this.atBuffer.setComunicazione(comunicazione);
		this.atBuffer.getBufferDaLeggere().set(true);
	}

	/**
	 * Leggi la comunicazione dal buffer, se presente
	 * 
	 * @return
	 */
	public IComunicazione leggiComunicazione() {
		IComunicazione wvComunicazione;
		if (this.atBuffer.getBufferDaLeggere().get()) {
			wvComunicazione = this.atBuffer.getComunicazione();
			this.atBuffer.getBufferDaLeggere().set(false);
			return wvComunicazione;
		}
		return null;
	}

	/**
	 * Imposta l'oggetto {@link IstanteUltimoPing} per indicare l'istante
	 * dell'ultimo ping effettuato dal cliet
	 * 
	 * @param istanteUltimoPing
	 */
	public void setIstanteUltimoPing(IstanteUltimoPing istanteUltimoPing) {
		synchronized (this.atIstanteUltimoPing) {
			this.atIstanteUltimoPing = istanteUltimoPing;
		}
	}

	/**
	 * Ritorna un valore che indica se è scaduto il tempo di ping di un client
	 * 
	 * @return {@link Boolean}:<br>
	 *         <code>true</code> indica che è scaduto il tempo di ping del
	 *         client<br>
	 *         <code>false</code> indica che non è scaduto il tempo di ping del
	 *         client
	 */
	public Boolean getTempoScaduto() {
		synchronized (this.atIstanteUltimoPing) {
			return this.atIstanteUltimoPing.tempoScaduto();
		}
	}

}

class Buffer {
	private IComunicazione atComunicazione;
	private final AtomicBoolean atBufferDaLeggere;

	/**
	 * Costruttore, imposta lo stato interno iniziale del buffer
	 */
	public Buffer() {
		this.atComunicazione = null;
		this.atBufferDaLeggere = new AtomicBoolean(false);
	}

	/**
	 * Ritorna un valore che indica se il buffer è da leggere
	 * 
	 * @return {@link AtomicBoolean} che indica se il buffer è da leggere
	 */
	public AtomicBoolean getBufferDaLeggere() {
		return this.atBufferDaLeggere;
	}

	/**
	 * Ritorna la comunicazione depositata nel buffer e imposta come letto il
	 * buffer
	 * 
	 * @return {@link IComunicazione} la comunicazione nel buffer
	 */
	public IComunicazione getComunicazione() {
		IComunicazione wvComunicazione = this.atComunicazione;
		this.atBufferDaLeggere.set(false);
		return wvComunicazione;
	}

	/**
	 * Imposta la comunicazione nel buffer
	 * 
	 * @param comunicazione
	 *            Oggetto che implementa l'interfaccia {@link IComunicazione}
	 */
	public void setComunicazione(IComunicazione comunicazione) {
		this.atComunicazione = comunicazione;
		this.atBufferDaLeggere.set(true);
	}

}