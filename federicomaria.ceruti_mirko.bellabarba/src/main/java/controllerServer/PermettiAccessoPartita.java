package controllerServer;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe che implementa i metodi per gestire l'accesso ad una partia
 */
class PermettiAccessoPartita {
	private AtomicBoolean at_permetti_avvio;
	private AtomicBoolean at_tempo_scaduto;
	private AtomicInteger at_giocatori_presenti;

	/**
	 * Costruttore di {@link PermettiAccessoPartita}
	 */
	public PermettiAccessoPartita() {
		at_permetti_avvio = new AtomicBoolean(false);
		at_tempo_scaduto = new AtomicBoolean(false);
		at_giocatori_presenti = new AtomicInteger(0);
	}

	/**
	 * Ottiene un valore che indica se la partia è avviabile
	 * 
	 * @return Ritorna un {@link Boolean} che indica se la partita è avviabile
	 */
	public Boolean getPermettiAvvio() {
		return at_permetti_avvio.get();
	}

	/**
	 * Ottiene il numero di giocatori assegnati alla partita controllata da
	 * questo oggetto {@link PermettiAccessoPartita}
	 * 
	 * @return Ritorna un'{@link Integer} che rappresenta il numero di giocatori
	 *         assegnati alla partita
	 */
	public Integer getNumeroGiocatori() {
		return at_giocatori_presenti.get();
	}

	/**
	 * Ottiene un valore che indica se il tempo per l'accettazione di altri
	 * giocatori nella partita è scaduto
	 * 
	 * @return Ritorna un {@link Boolean} che indica se il tempo per
	 *         l'accettazione di altri giocatori nella partita è scaduto
	 */
	public Boolean getTempoScaduto() {
		return at_tempo_scaduto.get();
	}

	/**
	 * Imposta la varibile per indicare se il tempo per accettare altri
	 * giocatori è scaduto o meno
	 * 
	 * @param valore
	 *            {@link Boolean}
	 */
	public void setTempoScaduto(Boolean valore) {
		at_tempo_scaduto.set(valore);
		permettiAvvio();
	}

	/**
	 * Aggiunge un giocatore alla partita solo se il chimaante è di tipo
	 * {@link Timer_Avvio_Partita}
	 * 
	 * @param Chiamante
	 *            Oggetto che richiama
	 *            {@link PermettiAccessoPartita.Aggiungi_Giocatore}
	 * @return Ritorna un {@link Boolean} che indica se la somma è andata a buon
	 *         fine o meno.<br>
	 *         La somma fallisce se: sono assegnati più di 4 giocatori alla
	 *         partita, se la partita ha già impostato il permesso di avvio, se
	 *         il chimante non è di tipo {@link ServerScoket}
	 */
	public synchronized Boolean aggiungiGiocatore() {
		if (at_giocatori_presenti.get() == 4 || at_permetti_avvio.get() || at_tempo_scaduto.get()) {
			return false;
		}
		at_giocatori_presenti.incrementAndGet();
		permettiAvvio();
		return true;
	}

	/**
	 * Ritorna <b>true</b> se si è raggiunto il limite massimo di giocatori per
	 * questa partita, <b>false</b> altrimenti
	 * 
	 * @return Ritorna un {@link Boolean} che indica se si è raggiunto il limite
	 *         massimo di giocatori per la partita
	 */
	public Boolean massimoGiocatori() {
		return (at_giocatori_presenti.get() == 4) ? true : false;
	}

	/**
	 * Ritorna <b>true</b> se si è raggiunto il limite minimo di giocatori per
	 * questa partita, <b>false</b> altrimenti
	 * 
	 * @return Ritorna un {@link Boolean} che indica se si è raggiunto il limite
	 *         minimo di giocatori per la partita
	 */
	public Boolean minimoGiocatori() {
		return (at_giocatori_presenti.get() >= 2) ? true : false;
	}

	/**
	 * Imposta la variabile at_permetti_avvio a <b>true</b> se rispettati i
	 * paremetri previsti:<br>
	 * - at_tempo_scaduto = <b>true</b><br>
	 * - at_giocatori_presenti = <b>4</b>
	 */
	private void permettiAvvio() {
		at_permetti_avvio.set((at_tempo_scaduto.get() || at_giocatori_presenti.get() == 4));
	}
}
