package controllerServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.EColore;
import model.Giocatore;
import model.PedinaPastore;
import model.mosse.IComunicazione;
import model.mosse.Messaggio;
import view.CLCHelper;
import view.Start;

/**
 * Classe per la gestione della partita con connesisone in modalità socket
 * 
 * @author federico
 * 
 */
public class StanzaSocket extends Stanza {

	/**
	 * rappresenta tutti client associati alla stanza, ad ogni client un solo
	 * giocatore
	 */
	private final Map<Integer, ClientSocket> atClient;

	/**
	 * Costruttore della classe {@link Stanza} Crea una stanza per accettare e
	 * gestire {@link ClientSocket}
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'id della partita
	 */
	public StanzaSocket(Integer idPartita) {
		super(idPartita);
		this.atClient = new LinkedHashMap<Integer, ClientSocket>();
		this.atIdPartita = idPartita;
	}

	/**
	 * Ritorna un valore che indica se sul client si è verificato un problema di
	 * connessione
	 * 
	 * @param idGiocatore
	 *            {@link Integer} id del giocatore/client
	 * @return {@link AtomicBoolean} che indica che se è presente un problema
	 *         con la connessione del client
	 */
	public AtomicBoolean getProblemaClient(Integer idGiocatore) {
		return this.atClient.get(idGiocatore).getProblemaClient();
	}

	/**
	 * Ritorna un valore che conferma lo stato di disconnessione del client
	 * specificato
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del gicoatore/client
	 * @return {@link AtomicBoolean} che indica che il client si è scollegato
	 */
	public AtomicBoolean getConfermaScollegato(Integer idGiocatore) {
		return this.atClient.get(idGiocatore).getConfermaScollegato();
	}

	/**
	 * Ritorna il set dei giocatori
	 * 
	 * @return {@link Set} di {@link Integer} che contiene gli id dei
	 *         client/giocatori della stanza
	 */
	public Set<Integer> getIdGiocatori() {
		return this.atClient.keySet();
	}

	/**
	 * Ritorna un oggetto che rappresenta il client indicato
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del giocatore
	 * @return {@link ClientSocket} che rappresenta il client indicato e che
	 *         contiene i metodi per gestirlo
	 */
	public ClientSocket getClient(Integer idGiocatore) {
		return this.atClient.get(idGiocatore);
	}

	/**
	 * Carica la mappa per la stanza
	 */
	public void caricaMappa() {
		CLCHelper.stampa("Ottengo giocatori");
		this.caricaMappa(this.ottieniGiocatori());
	}

	void calcolaProssimoGiocatoreMercato() {
		Integer[] wvIdGiocatori = this.atClient.keySet()
				.toArray(new Integer[0]);
		Integer wvIndice;
		Integer wvNumeroGiocatori = wvIdGiocatori.length;
		Integer wvContatore = 0;

		switch (this.atFaseMercato) {
		case 0:
			// imposto il primo giocatore per la proposta delle offerte
			this.atFaseMercato++;
			for (wvIndice = 0; wvIndice < wvNumeroGiocatori; wvIndice++) {
				if (!this.atClient.get(wvIdGiocatori[wvIndice])
						.getConfermaScollegato().get()) {
					this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
					break;
				}
			}
			break;
		case 1:
			// imposto i giocatori successivi per la proposta delle offerte
			wvContatore = 0;
			wvIndice = this.cercaIndiceGiocatore(wvIdGiocatori,
					this.atIdGiocatoreAttualeMercato);

			do {
				wvIndice++;
				if (wvIndice < wvNumeroGiocatori) {
					this.atIdGiocatoreAttualeMercato = (wvIdGiocatori[wvIndice]);
					wvContatore++;
				} else {
					this.atFaseMercato++;
					break;
				}
			} while (this.atClient.get(this.atIdGiocatoreAttualeMercato)
					.getConfermaScollegato().get()
					&& (wvContatore <= wvNumeroGiocatori));
			if (wvContatore.equals(4)) {
				this.atFaseMercato = 4;
			}
			break;
		// se la fase è < 2 mando _TURNOOFFERTA
		// se la fase mercato è maggiore di 2 e minore di 4 mando _TURNOACQUISTO
		// indico che è il momento di comprare
		case 2:
			wvContatore = 0;
			wvIndice = new Random().nextInt(wvNumeroGiocatori);
			this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
			while (this.atClient.get(this.atIdGiocatoreAttualeMercato)
					.getConfermaScollegato().get()
					&& (wvContatore <= wvNumeroGiocatori)) {
				this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
				this.atIdPrimoGiocatoreAcquisto = this.atIdGiocatoreAttualeMercato;
				wvIndice++;
				if (wvIndice >= wvNumeroGiocatori) {
					wvIndice = 0;
				}
			}
			this.atFaseMercato++;
			if (wvContatore.equals(4)) {
				this.atFaseMercato = 4;
			}
			break;
		case 3:
			wvContatore = 0;
			wvIndice = this.cercaIndiceGiocatore(wvIdGiocatori,
					this.atIdGiocatoreAttualeMercato);
			do {
				wvIndice++;
				if (wvIndice >= wvNumeroGiocatori) {
					wvIndice = 0;
				}
				this.atIdGiocatoreAttualeMercato = wvIdGiocatori[wvIndice];
			} while (this.atClient.get(this.atIdGiocatoreAttualeMercato)
					.getConfermaScollegato().get()
					&& (wvContatore <= wvNumeroGiocatori));
			if (this.atIdGiocatoreAttualeMercato
					.equals(this.atIdPrimoGiocatoreAcquisto)) {
				this.atFaseMercato++;
			}
			break;
		default:
			break;
		}
	}

	void calcolaProssimoGiocatore() {
		Integer[] wvIdGiocatori = this.atClient.keySet()
				.toArray(new Integer[0]);
		Integer wvIndice;
		Integer wvNumeroGiocatori = wvIdGiocatori.length;
		Integer wvContatore = -1;

		wvIndice = this.cercaIndiceGiocatore(wvIdGiocatori, this
				.getGiocatoreAttuale().get());
		this.getPartitaServer().calcolaFaseFinale();
		do {
			wvIndice++;
			if (wvIndice >= wvNumeroGiocatori) {
				wvIndice = 0;
				this.getPartitaServer().attivaFaseLupo();
				if (this.getPartitaServer().getFaseFinale()) {
					this.setTerminaPartita(true);
				}
			}
			this.getGiocatoreAttuale().set(wvIdGiocatori[wvIndice]);
			this.getPartitaServer().attivaFaseAgnelli();
			wvContatore++;
		} while (this.atClient.get((this.getGiocatoreAttuale().get()))
				.getConfermaScollegato().get()
				&& (wvContatore < wvNumeroGiocatori));

		if (wvContatore >= wvNumeroGiocatori) {
			this.setTerminaPartita(true);
		}
	}

	@Override
	ArrayList<Giocatore> ottieniGiocatori() {
		ArrayList<Giocatore> wvGiocatori = new ArrayList<Giocatore>();
		Integer wvId = 1;
		Integer wvDenaro;
		PedinaPastore[] wvPedine;
		for (Integer item : this.atClient.keySet()) {
			if (this.atClient.size() == 2) {
				wvDenaro = DENARODUEGIOCATORI;
				wvPedine = new PedinaPastore[2];
				wvPedine[0] = new PedinaPastore(this.calcolaIdPedina(wvId),
						EColore.values()[wvId / 3]);
				wvId++;
				wvPedine[1] = new PedinaPastore(this.calcolaIdPedina(wvId),
						EColore.values()[wvId / 3]);
			} else {
				wvDenaro = DENAROQUATTROGIOCATORI;
				wvPedine = new PedinaPastore[1];
				wvPedine[0] = new PedinaPastore(this.calcolaIdPedina(wvId),
						EColore.values()[wvId - 1]);
			}
			wvGiocatori.add(new Giocatore(item, this.atClient.get(item)
					.getNome(), item / 100, wvPedine, this
					.calcolaTesseraIniziale(), wvDenaro));
			this.assegnaGiocatore(wvGiocatori.get(wvGiocatori.size() - 1));
			wvId++;
		}
		return wvGiocatori;
	}

	/**
	 * assegno il giocatore alle sue pedine
	 */
	private void assegnaGiocatore(Giocatore giocatore) {
		for (PedinaPastore wvItem : giocatore.getPedinePastore()) {
			wvItem.setGiocatorePossessore(giocatore);
		}
	}

	/**
	 * Aggiunge un client nella stanza
	 * 
	 * @param client
	 *            {@link Socket} con cui comunicare con il client
	 * @param nome
	 *            {@link String} nome del client
	 */
	public void aggiungiClient(Socket client, String nome) {
		this.aggiungiGiocatore();
		Integer wvIdGiocatore = this.calcolaIDGocatore();
		CLCHelper.stampa("ClientSocket connesso. ID: " + wvIdGiocatore
				+ " - Nome: " + nome);

		this.atClient.put(wvIdGiocatore, new ClientSocket(nome));
		this.atClient.get(wvIdGiocatore).setScoketClient(client);

		Messaggio wvMessagio = new Messaggio();

		wvMessagio.idPartita(this.atIdPartita);
		this.atClient.get(wvIdGiocatore).invia(wvMessagio);
		this.atClient.get(wvIdGiocatore).ricevi();

		wvMessagio.idGiocatore(wvIdGiocatore);

		this.atClient.get(wvIdGiocatore).invia(wvMessagio);
		this.atClient.get(wvIdGiocatore).ricevi();

	}

	/**
	 * Riassegna il client specificato alla partita
	 * 
	 * @param client
	 *            {@link Socket} del client accetato
	 * @param idGiocatore
	 *            {@link Integer} id del giocatore che va ricollegato
	 */
	public void riassegnaClient(Socket client, Integer idGiocatore) {
		// sincronizzato per gestire problemi in lettura su altri thread
		synchronized (this) {
			this.atClient.get(idGiocatore).setScoketClient(client);
			this.atClient.get(idGiocatore).getProblemaClient().set(false);
			this.atClient.get(idGiocatore).getConfermaScollegato().set(false);
		}

	}

	/**
	 * Termina il collegamento con tutti i client
	 */
	public void termina() {
		for (Integer wvIdClient : this.getIdGiocatori()) {
			this.atClient.get(wvIdClient).chiudi();
		}

	}
}

/**
 * classe che mette a disposizione i metodi per inviare e ricevere dai client
 */
class ClientSocket {
	private static final Integer AT_MAX_TENTATIVI = 10;
	/**
	 * Socket associate al client, identificate dall'ID gicoatore
	 */
	private Socket atSocketClient;
	private final String atNome;

	// servono per la comunicazione seriale
	private InputStream atInputStream;
	private OutputStream atOutputStream;

	// servono per la disconnessione dei client
	/**
	 * se <code>true</code> significa che il client potrebbe essersi disconnesso
	 * se <code>false</code> significa che il client è connesso
	 */
	private final AtomicBoolean atProblemaClient;
	private final AtomicBoolean atConfermaScollegato;

	/**
	 * Costrruttore
	 * 
	 * @param nome
	 *            {@link String} nome del giocatore
	 */
	public ClientSocket(String nome) {
		this.atInputStream = null;
		this.atOutputStream = null;
		this.atProblemaClient = new AtomicBoolean(false);
		this.atConfermaScollegato = new AtomicBoolean(false);
		this.atNome = nome;
	}

	/**
	 * Chiuda la {@link Socket} e gli Stream associati al client
	 */
	public void chiudi() {
		try {
			this.atInputStream.close();
			this.atOutputStream.close();
			this.atSocketClient.close();
		} catch (Exception e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}

	}

	/**
	 * Ritorna il nome del gicoatore associato
	 * 
	 * @return {@link String} nome del gicoatore
	 */
	public String getNome() {
		return this.atNome;
	}

	/**
	 * Ritorna un valore che indica se il client ha problemi di connessione
	 * 
	 * @return {@link AtomicBoolean} che rappresenta se il client ha problemi di
	 *         connessione
	 */
	public AtomicBoolean getProblemaClient() {
		return this.atProblemaClient;
	}

	/**
	 * Ritorna un valore che indica se il client si è scollegato
	 * 
	 * @return {@link AtomicBoolean} ch rappresenta se il client è scollegato
	 */
	public AtomicBoolean getConfermaScollegato() {
		return this.atConfermaScollegato;
	}

	/**
	 * Imposta la {@link Socket} del client al valore passato e associandole gli
	 * {@link InputStream} e {@link OutputStream} automaticamente, se la
	 * {@link Socket} era già assegnata in precedenza ricrea gli Stream
	 * 
	 * @param socketClent
	 *            {@link Socket} che rappresenta la socket di comunicazione col
	 *            giocatore
	 */
	public void setScoketClient(Socket socketClent) {
		this.atSocketClient = socketClent;
		try {
			if (this.atOutputStream != null) {
				this.atOutputStream.close();
				this.atOutputStream = null;
			}
			this.atOutputStream = this.atSocketClient.getOutputStream();
			if (this.atInputStream != null) {
				this.atInputStream.close();
				this.atInputStream = null;
			}
			this.atInputStream = this.atSocketClient.getInputStream();
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			CLCHelper.stampa("Eccezione gestita\n----------");
			CLCHelper.stampaEccezione(e);
			CLCHelper.stampa("\n------\n Il Server continua a funzionare");
		}
	}

	/**
	 * Si mettte in attesa della ricezione di una comunicazione
	 * 
	 * @return Ritorna un oggetto di tipo {@link IComunicazione} che rappresenta
	 *         la comunicazione ricevuta, <code>null</code> altrimenti
	 */
	public IComunicazione ricevi() {
		if (this.atConfermaScollegato.get()) {
			return null;
		}
		ObjectInputStream wvInput;

		try {
			wvInput = new ObjectInputStream(this.atInputStream);
			return (IComunicazione) wvInput.readObject();
		} catch (ClassNotFoundException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			CLCHelper.stampa("Client scollegato: "
					+ this.atSocketClient.getPort());
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			CLCHelper.stampa("Client scollegato: "
					+ this.atSocketClient.getPort());
		}
		this.atProblemaClient.set(true);
		this.atConfermaScollegato.set(true);
		return null;
	}

	/**
	 * Invia una comunicazione al client
	 * 
	 * @param comunicazione
	 *            Oggetto che implementa l'interfaccia {@link IComunicazione} e
	 *            che rappresenta la comunicazione da inviare
	 */
	public void invia(IComunicazione comunicazione) {
		if (this.atConfermaScollegato.get()) {
			return;
		}
		ObjectOutputStream wvOut;
		try {
			wvOut = new ObjectOutputStream(this.atOutputStream);

		} catch (IOException e1) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.FINEST,
						this.getClass().toString(), e1);
			}
			return;
		}
		this.tentaInvio(wvOut, comunicazione);

	}

	/**
	 * Invia un ack al client
	 */
	public void inviaAck() {
		Messaggio wvMessaggio = new Messaggio();
		wvMessaggio.ack();
		this.invia(wvMessaggio);

	}

	/**
	 * Tenta di inviare il mesaggio al client, dopo 10 tentativi segna il client
	 * come scollegato
	 * 
	 * @param out
	 *            Rappresenta l'{@link ObjectOutputStream} su cui mandare i
	 *            messaggi
	 * @param comunicazione
	 *            L'ggetto che implementa l'interfaccia {@link IComunicazione} e
	 *            che rappresenta la comunicazione da inviare
	 */
	private void tentaInvio(ObjectOutputStream out, IComunicazione comunicazione) {
		int i = 0;
		do {
			try {

				if (Start.abilitaTest) {
					Thread.sleep(10);
				}

				out.writeObject(comunicazione);
				out.flush();
				this.atProblemaClient.set(false);
			} catch (IOException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
				this.atProblemaClient.set(true);
			} catch (InterruptedException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
				CLCHelper.stampaEccezione(e);
			}
			i++;
		} while ((i <= AT_MAX_TENTATIVI) && this.atProblemaClient.get());

		if (this.atProblemaClient.get()) {
			this.atConfermaScollegato.set(true);
		}
	}

}
