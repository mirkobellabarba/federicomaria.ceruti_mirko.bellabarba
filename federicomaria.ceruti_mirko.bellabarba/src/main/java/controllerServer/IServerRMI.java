package controllerServer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import model.Agnello;
import model.Animale;
import model.Lupo;
import model.Mappa;
import model.Montone;
import model.Pecora;
import model.Pecoranera;
import model.PedinaPastore;
import model.Regione;
import model.Strada;
import model.mosse.IComunicazione;
import model.mosse.Investimento;
import model.mosse.Offerta;

/**
 * Interfaccia che mette a disposizione i prototipi per i metodi da richiamare
 * per usare le funzionlità del {@link GestorePartitaRMI}
 */
public interface IServerRMI extends Remote {
	/**
	 * Da usare per gestire gli ID delle partite
	 */
	public static final Integer AT_SOMMATORE_ID = 100;
	public static final Integer AT_TEMPO_TIMEOUT = 30000;
	public static final Integer AT_MAX_GIOCATORI = 4;
	public static final Integer AT_PORTA_SERVER_RMI = 10000;
	public final static String AT_SERVER_NAME = "Sheepland_Server";

	/**
	 * Metodo per collegarsi ad un apartia la prima volta
	 * 
	 * @param nome
	 *            {@link String} che rappresent ail nome scelto dal gicoatore
	 * @return {@link ArrayList} di {@link Integer} che rappresenta gli ID della
	 *         partita e del gicoatori assegnati dal server
	 * @throws RemoteException
	 */
	public ArrayList<Integer> collegaPartita(String nome) throws RemoteException;

	/**
	 * Invia un ping al server
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapprsenta l'ID della partita di cui si
	 *            vuole controllare l'avvio
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che si
	 *            vuole colegare alla partitia
	 * @param istanteUltimoPing
	 *            {@link IstanteUltimoPing} che rappresenta il ping inviato al
	 *            server
	 * @throws RemoteException
	 */
	public void inviaPing(Integer idPartita, Integer idGiocatore, IstanteUltimoPing istanteUltimoPing) throws RemoteException;

	/**
	 * Controlla se è possibile avviare la partita
	 * 
	 * @param idPartitia
	 *            {@link Integer} che rapprsenta l'ID della partita di cui si
	 *            vuole controllare l'avvio
	 * @return Ritorna un {@link Boolean} che rappresenta se l'avvio è permesso
	 *         (<code>true</code>) oppure no <code>false</code>
	 * @throws RemoteException
	 */
	public Boolean avviaPartita(Integer idPartitia) throws RemoteException;

	/**
	 * Prepara lo stato interno della partita
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita da
	 *            preparare
	 * @throws RemoteException
	 */
	public void preparaPartita(Integer idPartita) throws RemoteException;

	/**
	 * Metodo da richiamare per ricollegarsi ad una partita appena ci si accorge
	 * di essersi scollegati
	 * 
	 * @param idPartitia
	 *            {@link Integer} che rapprsenta l'ID della partita a cui ci si
	 *            vuole ricollegare
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che si
	 *            vuole colegare alla partitia
	 * @return Ritorna un {@link Boolean} che indica se il client è riuscito a
	 *         ricollegarsi alla partita
	 * @throws RemoteException
	 */
	public Boolean ricollegaPartita(Integer idPartitia, Integer idGiocatore) throws RemoteException;

	/**
	 * Ritorna la {@link Mappa} della partita
	 * 
	 * @param idPartitia
	 *            {@link Integer} che rapprsenta l'ID della partita di cui
	 *            sapere la mappa
	 * @return Ritorna una {@link Mappa} che rappresenta la mappa della partita
	 * @throws RemoteException
	 */
	public Mappa ottieniMappa(Integer idPartitia) throws RemoteException;

	/**
	 * Ritorna un valore che indica se è il tuo turno di impostare la pedina
	 * oppure no
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapprsenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rapprsenta l'ID del giocatore che compie
	 *            l'azione
	 * @return <b>0</b> se è la mossa di un altro<br>
	 *         <b>1<b> se è il tuo turno di posizionare<br>
	 *         <b>2<b> se inizano le model.mosse della partita
	 */
	public Integer turnoImpostaPedina(Integer idPartita, Integer idGiocatore) throws RemoteException;

	/**
	 * Metodo per spere il primo giocatore dell partita
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita di cui
	 *            sapere il primo gicoatore
	 * @return {@link Integer} che rappresenta l'ID del primo giocatore della
	 *         partita
	 * @throws RemoteException
	 */
	public Integer getPrimoGiocatore(Integer idPartita) throws RemoteException;

	/**
	 * Metodo che riotrna se è il turno di chi lo invoca oppure no
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            osservare il turno
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che vuole
	 *            sapere se è il suo turno
	 * @return Ritorna un {@link Boolean} che indica se è il turno del giocatore
	 *         che ha invocato il metodo
	 * @throws RemoteException
	 */
	public Boolean mioTurno(Integer idPartita, Integer idGiocatore) throws RemoteException;

	/**
	 * MEtodo che calcola il prossimo gicatore che dovrà effettuare le sue
	 * model.mosse
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            osservare il turno
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che vuole
	 *            sapere se è il suo turno
	 * @throws RemoteException
	 */
	public void calcolaProssimoGiocatore(Integer idPartita, Integer idGiocatore) throws RemoteException;

	/**
	 * Metodo per impostare una pedina del giocatore
	 * 
	 * @param idPartitia
	 *            {@link Integer} che rapprsenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rapprsenta l'ID del giocatore che compie
	 *            l'azione
	 * @param idPedina
	 *            {@link Integer} che rappresenta l'ID della
	 *            {@link PedinaPastore} da impostare
	 * @param idStrada
	 *            {@link Integer} che rappresenta l'ID della strada in cui
	 *            mettere la {@link PedinaPastore}
	 * @throws RemoteException
	 */
	public void impostaPedina(Integer idPartitia, Integer idGiocatore, Integer idPedina, Integer idStrada) throws RemoteException;

	/**
	 * Ritorna la poszione del {@link Lupo} della partita specificata
	 * 
	 * @param idPartitia
	 *            {@link Integer} id della partita
	 * @return {@link Integer} che rappresenta l'id della regione in cui si
	 *         trova la pecora nera
	 * @throws RemoteException
	 */
	public Integer posizioneLupo(Integer idPartitia) throws RemoteException;

	/**
	 * Ritorna la poszione della {@link Pecoranera} della partita specificata
	 * 
	 * @param idPartitia
	 *            {@link Integer} id della partita
	 * @return {@link Integer} che rappresenta l'id della regione in cui si
	 *         trova la pecora nera
	 * @throws RemoteException
	 */
	public Integer posizionePecoraNera(Integer idPartitia) throws RemoteException;

	/**
	 * Metodo per notificare la mossa di un {@link Animale}
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapprsenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rapprsenta l'ID del giocatore che compie
	 *            l'azione
	 * @param idAnimale
	 *            {@link Integer} che rappresenta l'ID dell'{@link Animale} da
	 *            spostare
	 * @param idRegioneArrivo
	 *            {@link Integer} che rappresenta l'ID della {@link Regione} di
	 *            arrivo
	 * @throws RemoteException
	 */
	public void spostaAnimale(Integer idPartita, Integer idGiocatore, Integer idAnimale, Integer idRegioneArrivo) throws RemoteException;

	/**
	 * Metoodo per notificare la mossa di una {@link Pedina_Pastore}
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapprsenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rapprsenta l'ID del giocatore che compie
	 *            l'azione
	 * @param idPedina
	 *            {@link Integer} che rappresenta l'ID della
	 *            {@link Pedina_Pastore} da muovere
	 * @param idStradaArrivo
	 *            {@link Integer} che rappresenta l'ID della {@link Strada} di
	 *            arrivo
	 * @param costo
	 *            {@link Integer} che rappresenta il costo della mossa
	 * @throws RemoteException
	 */
	public void muoviPedina(Integer idPartita, Integer idGiocatore, Integer idPedina, Integer idStradaArrivo, Integer costo) throws RemoteException;

	/**
	 * Metodo per comprare una tessera terreno
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param investimento
	 *            {@link Investimento} che rappresnta l'investimento effettuato
	 * @throws RemoteException
	 */
	public void investi(Integer idPartita, Investimento investimento) throws RemoteException;

	/**
	 * Metodo per notificare l'accoppiamento
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che compie
	 *            l'azione
	 * @param idRegione
	 *            {@link Integer} che rappresenta l'ID della {@link Regione} su
	 *            cui effetuare l'azione
	 * @throws RemoteException
	 */
	public void accoppiamento(Integer idPartita, Integer idGiocatore, Integer idRegione) throws RemoteException;

	/**
	 * Metodo per notificare l'abbattimento
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che compie
	 *            l'azione
	 * @param idRegione
	 *            {@link Integer} che rappresenta l'ID della {@link Regione} su
	 *            cui effettuare l'azione
	 * @param idAnimale
	 *            {@link Integer} che rappresenta l'ID dell'{@link Animale} da
	 *            abbattere
	 * @param idGiocatoirVicini
	 *            {@link ArrayList} di {@link Integer} che rappresenta l'elenco
	 *            degli ID dei gicoatori vicini
	 * @throws RemoteException
	 */
	public void abbattimento(Integer idPartita, Integer idGiocatore, Integer idRegione, Integer idAnimale, ArrayList<Integer> idGiocatoirVicini)
			throws RemoteException;

	/**
	 * Metodo per vendere tessere al mercato
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param offerte
	 *            {@link ArrayList} di {@link Offerta} che rappresenta l'elenco
	 *            di offerte del giocatore
	 * @throws RemoteException
	 */
	public void inviaVenditaMercato(Integer idPartita, ArrayList<Offerta> offerte) throws RemoteException;

	/**
	 * Metodo per comprare tessere dal mercato
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che compie
	 *            l'azione
	 * @param idAcquisto
	 *            {@link Integer} che rappresenta l'ID dell'oggetto
	 *            {@link Offerta} da comprare se vale -1, significa che si ha
	 *            deciso di terminare la propria fase di aqcuisto
	 * @throws RemoteException
	 */
	public void acquistaDaMercato(Integer idPartita, Integer idGiocatore, Integer idAcquisto) throws RemoteException;

	/**
	 * Metodo che controlla gli oggetti di tipo {@link Agnello} e ne trasforma
	 * uno in {@link Montone} o {@link Pecora} inviando la mossa a tutti e
	 * ritornando se ci sono altri oggetti di tipo {@link Agnello} da svezzare
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapprsenta l'ID dela parita su cui
	 *            compiere l'azione
	 * @return Ritorna un {@link Boolean} che indica se ci sono altri agnelli da
	 *         sezzare oppure no
	 * @throws RemoteException
	 */
	public Boolean svezzaAgnello(Integer idPartita) throws RemoteException;

	/**
	 * Motodo che muove la {@link Pecoranera} e lo notifica a tutti i client
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @throws RemoteException
	 */
	public void muoviPecoraNera(Integer idPartita) throws RemoteException;

	/**
	 * Metodo che muove il {@link Lupo} e lo notifica a tutti i client
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            compiere l'azione
	 * @throws RemoteException
	 */
	public void muoviLupo(Integer idPartita) throws RemoteException;

	/**
	 * Metodo che segnala a tutti che è arrivato il momento di smettere di
	 * leggere le model.mosse preliminari
	 * 
	 * @param idPartita
	 *            la partita in cui si trova il giocatore
	 * @param primoGiocatore
	 *            il giocatore a cui tocca
	 * @throws RemoteException
	 */
	public void fineMossePreliminari(Integer idPartita, Integer primoGiocatore) throws RemoteException;

	/**
	 * Metodo per ricevere una comunicazione
	 * 
	 * @param idPartitia
	 *            {@link Integer} che rappresenta l'ID della partita su cui
	 *            attendere la mossa
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'ID del giocatore che attende
	 *            la mossa
	 * @return Ritorna un oggetto che implementa l'interfaccia
	 *         {@link IComunicazione} e che rappresenta una comunicazione da
	 *         parte di altri gioatori
	 * @throws RemoteException
	 */
	public IComunicazione riceviComunicazione(Integer idPartitia, Integer idGiocatore) throws RemoteException;

}
