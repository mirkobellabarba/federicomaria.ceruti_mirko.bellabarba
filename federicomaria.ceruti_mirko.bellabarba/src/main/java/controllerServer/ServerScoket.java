package controllerServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.mosse.EIntestazioneMessaggio;
import model.mosse.Messaggio;
import view.CLCHelper;
import view.Start;

/**
 * Classe che mette a dispozione i metodi per la gestione del server
 */
public class ServerScoket extends Thread {

	/**
	 * Da usare per gestire gli ID delle partite
	 */
	private static final Integer AT_SOMMATORE_ID = 100;
	private static final Integer AT_TEMPO_TIMEOUT = 15000;
	private static final Integer AT_MAX_GIOCATORI = 4;
	private static final Integer AT_PORTA_SERVER_SCOKET = 4000;

	private static ServerScoket atIstanza;
	private final ExecutorService atExecutor;
	private ServerSocket atServerSocket;
	private InputStream atInputStream;
	private Integer atPortaSocket;

	/**
	 * Indica l'id della partita attualmente in fase di avvio
	 */
	private final AtomicInteger atIdUltimaPartita;

	// HashMap per accedere rapidamente alle parttie in corso
	/**
	 * HashMap che contiene tutte le partite del server, la chiave è l'ID della
	 * partita
	 */
	private final Map<Integer, StanzaSocket> atStanze;

	/**
	 * Costruttore
	 */
	private ServerScoket() {
		this.atIdUltimaPartita = new AtomicInteger(0);
		// istanzio la prima partita sul server
		this.atStanze = new LinkedHashMap<Integer, StanzaSocket>();
		// Aggiungo la prima partita sul server
		this.aggiungiPartita();

		// oggetto per gestire tutte le partite del server
		this.atExecutor = Executors.newCachedThreadPool();

		this.impostaServerScoket();

	}

	/**
	 * Imposta i parametri sulla {@link ServerScoket} del server e la istanzia
	 */
	private void impostaServerScoket() {
		this.atPortaSocket = AT_PORTA_SERVER_SCOKET;

		try {
			this.atServerSocket = new ServerSocket(this.atPortaSocket);
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
			}
		}
	}

	/**
	 * Permette di generare un oggetto di tipo Server garantendo che sia univoco
	 * 
	 * @return ritorna un oggetto di tipo Server che ne rappresenta l'unica
	 *         istanza
	 */
	public static ServerScoket istanzia() {
		if (atIstanza == null) {
			atIstanza = new ServerScoket();
		}
		return atIstanza;
	}

	/**
	 * per il test
	 */
	@Override
	public void run() {
		this.avvia();
	}

	/**
	 * Avvia il server
	 */
	public void avvia() {

		CLCHelper.stampaAncheInTest("Server Ready");
		while (true) {
			if (Start.abilitaTest) {
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
					}
				}
				if (this.atStanze.size() > Start.atNUMEROPARTITETEST) {
					break;
				}
			}
			while (this.atStanze.get(this.atIdUltimaPartita.get()).getNumeroGiocatori() < AT_MAX_GIOCATORI) {
				// ciclo fino a che non raggiungo il limite massimo di giocatori
				// per la partita
				if (this.atStanze.get(this.atIdUltimaPartita.get()).minimoGiocatori()) {
					// se ho raggiunto il numero minimo di giocatori in una
					// partita
					// imposto il timeout
					if (Start.abilitaTest2G) {
						this.setTimoutScoket(500);
					} else {
						this.setTimoutScoket(AT_TEMPO_TIMEOUT);
					}

				}

				try {
					// welcome client
					Socket wvClientSocket = this.atServerSocket.accept();
					// guardo cosa mi dice il client
					String wvRispostaClient = this.getMessaggioPronto(wvClientSocket);
					// Analizzo la risposta
					if (this.controllaRispostaPronto(wvRispostaClient)) {
						String[] wvSplit = wvRispostaClient.split("\\,");
						Integer wvId = Integer.parseInt(wvSplit[wvSplit.length - 1]);
						if (!this.atStanze.get(this.getIdPartitaPrecedente(wvRispostaClient)).getPartitaTerminata()) {
							this.atStanze.get(this.getIdPartitaPrecedente(wvRispostaClient)).riassegnaClient(wvClientSocket, wvId);
						} else {
							try {
								OutputStream wvOutputStream = wvClientSocket.getOutputStream();
								ObjectOutputStream wvObjectOutputStream = new ObjectOutputStream(wvOutputStream);
								Messaggio wvMessaggio = new Messaggio();
								wvMessaggio.uscita();
								wvObjectOutputStream.writeObject(wvMessaggio);
							} catch (IOException e) {
								if (Start.abilitaLogger) {
									Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
								}
							}
						}
					} else {
						this.atStanze.get(this.atIdUltimaPartita.get()).aggiungiClient(wvClientSocket, wvRispostaClient);
					}
				} catch (SocketTimeoutException e) {
					// se passa troppo tempo reimposto il timout della socket
					// e notifico che è scaduto il tempo per accettere altri
					// client
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.INFO, this.getClass().toString(), e);
					}
					this.resetTimoutSocket();
					this.atStanze.get(this.atIdUltimaPartita.get()).setTempoScaduto(true);
				} catch (ClassNotFoundException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
					}
					CLCHelper.stampa("Eccezione gestita\n----------");
					CLCHelper.stampaEccezione(e);
					CLCHelper.stampa("\n------\n Il Server continua a funzionare");
				} catch (IOException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
					}
					CLCHelper.stampa("Eccezione gestita\n----------");
					CLCHelper.stampaEccezione(e);
					CLCHelper.stampa("\n------\n Il Server continua a funzionare");
				}

				if (this.atStanze.get(this.atIdUltimaPartita.get()).getTempoScaduto()) {
					// se il tempo scade, esco dal ciclo e avvio la partita
					break;
				}
			}

			// avvio il gestore_partita, che contiene la logica per gestire la
			// partita
			CLCHelper.stampa("Executor.submit()");

			this.resetTimoutSocket();
			this.atExecutor.submit(new GestorePartitaSocket(this.atStanze.get(this.atIdUltimaPartita.get())));
			this.aggiungiPartita();
		}
	}

	/**
	 * ritorno l'id della vecchia partita estrapolata dal messaggio di pronto
	 * 
	 * @param rispostaClient
	 *            il messaggio dal client
	 * @return
	 */
	private Integer getIdPartitaPrecedente(String rispostaClient) {
		return Integer.parseInt(rispostaClient.split("\\,")[1]);
	}

	/**
	 * Se la risposta splittata ha dimensione maggiore di uno vuol dire che i
	 * client era già connesso in precedenza e dovrò connetterlo alla rispettiva
	 * partita, quindi il prossimo passaggio sarà estrapolare il numero di
	 * partita
	 * 
	 * @param rispostaClient
	 *            la risposta dal client
	 * @return
	 */
	private Boolean controllaRispostaPronto(String rispostaClient) {
		return rispostaClient.split("\\,").length > 1 ? true : false;
	}

	/**
	 * Controlla la risposta del client, se è pronto, ritorno i dati del pronto,
	 * altrimenti null
	 * 
	 * @param clientSocket
	 *            la socket sul quale mettersi in ascolto
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private String getMessaggioPronto(Socket clientSocket) throws ClassNotFoundException, IOException {
		this.atInputStream = clientSocket.getInputStream();
		ObjectInputStream wvOis = new ObjectInputStream(this.atInputStream);
		Messaggio rispostaDaClient = (Messaggio) wvOis.readObject();
		if (rispostaDaClient.getMessaggio().split("\\:")[0].equals(EIntestazioneMessaggio._PRONTO.toString())) {
			return rispostaDaClient.getMessaggio().split("\\:")[1];
		}
		return null;
	}

	/**
	 * Aggiugne una partita all'elenco di partite in corso sul server Aggiugne
	 * il controllo ad essa associata
	 */
	synchronized void aggiungiPartita() {
		// incremento l'id delle partite avviate
		this.atIdUltimaPartita.addAndGet(AT_SOMMATORE_ID);
		// aggiungo la partita in elenco
		this.atStanze.put(this.atIdUltimaPartita.get(), new StanzaSocket(this.atIdUltimaPartita.get()));
		CLCHelper.stampa("Partita Socket aggiunta. ID: " + this.atIdUltimaPartita.get());
	}

	/**
	 * Imposta il timeout della {@link ServerSocket} al valore specificato
	 * 
	 * @param tempo
	 *            {@link Integer} rappresenta in millisecondi il tempo di
	 *            timeout
	 */
	void setTimoutScoket(Integer tempo) {
		try {
			this.atServerSocket.setSoTimeout(tempo);
		} catch (SocketException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
			}
			CLCHelper.stampa("Eccezione gestita\n----------");
			CLCHelper.stampaEccezione(e);
			CLCHelper.stampa("\n------\n Il Server continua a funzionare");
		}
	}

	/**
	 * Reimposta a <code>0</code> il valore del timeout della
	 * {@link ServerSocket}
	 */
	void resetTimoutSocket() {
		try {
			this.atServerSocket.setSoTimeout(0);
		} catch (SocketException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
			}
			CLCHelper.stampa("Eccezione gestita\n----------");
			CLCHelper.stampaEccezione(e);
			CLCHelper.stampa("\n------\n Il Server continua a funzionare");
		}
	}

	public static void termina() {

		try {
			atIstanza.atExecutor.shutdownNow();
			atIstanza.atInputStream.close();
			atIstanza.atServerSocket.close();
			return;
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, "ServerSocket.Termina", e);
			}
			CLCHelper.stampa("Eccezione gestita\n----------");
			CLCHelper.stampaEccezione(e);
		} catch (NullPointerException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, "ServerSocket.Termina", e);
			}
			CLCHelper.stampaEccezione(e);
		}
	}
}