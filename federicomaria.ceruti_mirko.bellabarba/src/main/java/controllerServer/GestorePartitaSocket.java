package controllerServer;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;

import model.Agnello;
import model.Animale;
import model.Mappa;
import model.Pecora;
import model.PedinaPastore;
import model.Strada;
import model.mosse.Abbattimento;
import model.mosse.Abbattuto;
import model.mosse.Accoppiamento;
import model.mosse.EIntestazioneMessaggio;
import model.mosse.ETipoOvino;
import model.mosse.IComunicazione;
import model.mosse.IMossa;
import model.mosse.Investimento;
import model.mosse.Messaggio;
import model.mosse.MossaVuota;
import model.mosse.Movimento;
import model.mosse.MovimentoLupo;
import model.mosse.Nascita;
import model.mosse.Posizionamento;
import model.mosse.Svezzamento;
import view.CLCHelper;

/**
 * Classe che implamenta i metodi per la gestione del client che si connette al
 * server
 * 
 */
public class GestorePartitaSocket implements Runnable {
	private final StanzaSocket atStanza;
	private final Set<Integer> atIdGiocatori;

	/**
	 * Costruttore del gestore del cllient
	 * 
	 * @param stanzaPartita
	 *            Stanza che contiene la partita
	 */
	public GestorePartitaSocket(StanzaSocket stanzaPartita) {
		this.atStanza = stanzaPartita;
		this.atIdGiocatori = this.atStanza.getIdGiocatori();
		// this.at_disconnessioni = new Gestore_Disconnesioni();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		this.preparaPartita();

		do {
			this.mossePreliminari();
			this.inviaTurno();
			this.giocaTurnoGiocatore();
			this.atStanza.calcolaProssimoGiocatore();
		} while (!this.atStanza.getTerminaPartita());
		Messaggio wvMessaggio = new Messaggio();
		wvMessaggio.uscita();
		this.inviaTutti(wvMessaggio);

		CLCHelper.stampaAncheInTest("Partita: " + this.atStanza.atIdPartita
				+ "terminata");

		this.riceviTuttiAck();
		this.atStanza.termina();
		/*
		 * wv_socket_out.close(); wv_socket_in.close();
		 * at_socket.get(0).close();
		 */
	}

	private void preparaPartita() {
		CLCHelper.stampa("GESTOREPARTITA: partita avviata. ID: "
				+ this.atStanza.getPartitaServer().getIDPartita());
		this.atStanza.atIdPrimoGiocatore = (Integer) this.atIdGiocatori
				.toArray()[0];
		this.atStanza.getGiocatoreAttuale().set(
				this.atStanza.atIdPrimoGiocatore);
		CLCHelper.stampa("Genero Mappa");
		this.atStanza.caricaMappa();
		CLCHelper.stampa("Invio Mappa");
		this.inviaTutti(this.atStanza.getPartitaServer().getMappa());
		this.riceviTuttiAck();

		this.richiediPedine();
		this.notificaAvviaPartita();
	}

	private void mossePreliminari() {
		if (this.atStanza.getPartitaServer().getFaseLupo()) {
			// avviaMercato();
			this.muoviLupo();
			this.atStanza.getPartitaServer().disattivaFaseLupo();
		}
		if (this.atStanza.getPartitaServer().getFaseAgnelli()) {
			this.svezzaAgnelli();
			this.atStanza.getPartitaServer().disattivaFaseAgnelli();
		}
		this.muoviPecoraNera();
	}

	private void svezzaAgnelli() {
		ArrayList<Agnello> wvAgnelli = this.atStanza.getPartitaServer()
				.getMappa().getAgnelliSvezzamento();
		ETipoOvino wvETipoOvino;
		for (Agnello wvAgnello : wvAgnelli) {
			Animale wvAnimale = this.atStanza.getPartitaServer().svezzaAgnello(
					wvAgnello);
			if (wvAnimale instanceof Pecora) {
				wvETipoOvino = ETipoOvino.Pecora;
			} else {
				wvETipoOvino = ETipoOvino.Montone;
			}
			this.inviaTutti(new Svezzamento(wvAnimale.getId(), wvETipoOvino));
		}

	}

	private void inviaTurno() {
		Messaggio wvMessaggio = new Messaggio();
		/*
		 * if(atStanza.getClient(atStanza.getGiocatoreAttuale().get()).
		 * getConfermaScollegato().get()){ atStanza.calcolaProssimoGiocatore();
		 * }
		 */
		wvMessaggio.turno(this.atStanza.getGiocatoreAttuale().get());
		this.inviaTutti(wvMessaggio);
	}

	private void giocaTurnoGiocatore() {
		Integer wvIdGiocatore = this.atStanza.getGiocatoreAttuale().get();
		ClientSocket wvClient = this.atStanza.getClient(wvIdGiocatore);
		for (int i = 0; (i < 2) && (!wvClient.getConfermaScollegato().get()); i++) {
			this.applicaMossa(wvClient.ricevi(), wvIdGiocatore);
			this.inviaTurno();
		}
		this.applicaMossa(wvClient.ricevi(), wvIdGiocatore);
	}

	private void richiediPedine() {
		Messaggio wvMessaggio;

		for (Integer wvId : this.atIdGiocatori) {

			wvMessaggio = (Messaggio) this
					.creaMesaggio(EIntestazioneMessaggio._POSIZIONAPEDINA);
			CLCHelper.stampa(wvMessaggio.getMessaggio());
			this.invia(wvId, wvMessaggio);
			CLCHelper
					.stampa("Richiedo posizionamento pedina a giocatore numero "
							+ wvId);
			for (PedinaPastore wvPedina : this.atStanza.getPartitaServer()
					.getPedineGiocatore(wvId)) {

				Posizionamento wvPosizionamento = this.riceviPedina(wvId);
				if (wvPosizionamento == null) {
					if (this.atStanza.getClient(wvId).getConfermaScollegato()
							.get()) {
						// Entro qui se mi si disconnette il giocatore durante
						// la scelta
						// delle pedine. gli assegno le pedine che non ha ancora
						// scelto
						for (PedinaPastore wvPedina2 : this.atStanza
								.getPartitaServer().getPedineGiocatore(wvId)) {
							/*
							 * if (wvPedina.getStradaOccupata() != null) {
							 * continue; }
							 */
							Strada wvStrada = this.atStanza.getPartitaServer()
									.getMappa().getStradeLibere().get(0);
							this.atStanza.getPartitaServer()
									.posizionaPedinaPastore(wvPedina2,
											wvStrada.getId());
							wvPosizionamento = new Posizionamento(wvId,
									wvPedina2.getId(), wvStrada.getId());
							this.inviaAgliAltri(wvId, wvPosizionamento);

						}
					}
					break;
				}
				CLCHelper.stampa("E' arrivata la mossa di " + wvId);
				Integer wvStrada = wvPosizionamento.getIdStrada();
				CLCHelper.stampa("Ha scelto " + wvStrada);
				this.atStanza.getPartitaServer().posizionaPedinaPastore(
						wvPedina, wvStrada);
				this.inviaAgliAltri(wvId, wvPosizionamento);
			}
		}
	}

	private void muoviLupo() {
		PartitaServer wvPartitaServer = this.atStanza.getPartitaServer();
		Integer wvIdExRegione = wvPartitaServer.getMappa().getLupo()
				.getRegioneOccuapta().getId();
		Integer wvIdRegione = wvPartitaServer.muoviLupo().getId();
		Integer wvIdAnimlaeMangiato = wvPartitaServer.mangiaAnimale();
		IMossa wvMossa = new MovimentoLupo(wvPartitaServer.getMappa().getLupo()
				.getId(), wvIdExRegione, wvIdRegione, wvIdAnimlaeMangiato);
		this.inviaTutti(wvMossa);

	}

	private void muoviPecoraNera() {
		PartitaServer wvPartitaServer = this.atStanza.getPartitaServer();
		Integer wvIdExRegione = wvPartitaServer.getMappa().getPecoranera()
				.getRegioneOccuapta().getId();
		Integer wvIdRegione = wvPartitaServer.muoviPecoraNera().getId();
		if (wvIdExRegione.equals(wvIdRegione)) {
			Messaggio wvMossa = new Messaggio();
			wvMossa.nack();
			this.inviaTutti(wvMossa);
		}
		IMossa wvMossa = new Movimento(wvPartitaServer.getMappa()
				.getPecoranera().getId(), wvIdExRegione, wvIdRegione, 0);
		this.inviaTutti(wvMossa);
	}

	private void applicaMossa(IComunicazione mossa, Integer idGiocatore) {
		PartitaServer wvPartitaServer = this.atStanza.getPartitaServer();
		Mappa wvMappa = wvPartitaServer.getMappa();
		if (mossa instanceof Abbattimento) {
			Abbattimento wvAbbattimento = (Abbattimento) mossa;

			Abbattuto wvAbbattuto = wvPartitaServer.abbattimento(
					wvAbbattimento.getIdAnimale(),
					wvAbbattimento.getIdPastore(),
					wvAbbattimento.getIdGiocatoriVicini());
			CLCHelper.stampa("ho abbattuto: "
					+ wvAbbattuto.getAnimaleAbbattuto());
			this.inviaTutti(wvAbbattuto);
		} else if (mossa instanceof Accoppiamento) {
			Accoppiamento wvAccoppiamento = (Accoppiamento) mossa;
			Integer wvIdRegione = wvAccoppiamento.getIdRegione();
			Integer wvIdAgnello = wvPartitaServer.accoppiamento(wvIdRegione);

			Nascita wvNascita = new Nascita(wvIdRegione, wvIdAgnello);
			this.inviaTutti(wvNascita);
		} else if (mossa instanceof Investimento) {
			Investimento wvInvestimento = (Investimento) mossa;
			wvMappa.getGiocatore(idGiocatore).aggiungiTesseraTerreno(
					wvInvestimento.getTipoTerreno());
			wvMappa.aggiornaDanari(idGiocatore, wvInvestimento.getCosto());
			this.inviaAgliAltri(idGiocatore, wvInvestimento);
		} else if (mossa instanceof Movimento) {
			Movimento wvMovimento = ((Movimento) mossa);
			if (wvMovimento.isAnimale()) {
				wvPartitaServer.spostaAnimale(wvMovimento.getAtIdSoggetto(),
						wvMovimento.getAtIdNuovaPosizione());
			} else {
				wvPartitaServer.spostaPastore(wvMovimento.getAtIdSoggetto(),
						wvMovimento.getAtIdNuovaPosizione(),
						wvMovimento.getAtCostoMossa());
			}
			this.inviaAgliAltri(idGiocatore, wvMovimento);
		} else {
			// CLCHelper.stampa("Implementare questo tipo di mossa: ");
			this.inviaAgliAltri(idGiocatore, new MossaVuota());
		}
	}

	private void notificaAvviaPartita() {
		Messaggio wvMessaggio = new Messaggio();
		wvMessaggio.inizioPartita();

		this.inviaTutti(wvMessaggio);
	}

	private IComunicazione creaMesaggio(
			EIntestazioneMessaggio intestazioneMessaggio) {
		Messaggio wvMessaggio = new Messaggio();
		switch (intestazioneMessaggio) {
		case _IDSTRADE:
			wvMessaggio.stadeLibere(this.atStanza.getPartitaServer().getMappa()
					.getIdStradeLibere());
			return wvMessaggio;
		case _POSIZIONAPEDINA:
			wvMessaggio.posizionaPedina();
			return wvMessaggio;
		default:
			break;
		}
		return null;
	}

	private void invia(Integer idCLient, IComunicazione comunicazione) {
		this.atStanza.getClient(idCLient).invia(comunicazione);
	}

	/**
	 * Invia a tutti tranne che al giocatore specificato
	 * 
	 * @param idGiocatore
	 * @param comunicazione
	 */
	private void inviaAgliAltri(Integer idGiocatore,
			IComunicazione comunicazione) {
		for (Integer wvItem : this.atIdGiocatori) {
			if (!wvItem.equals(idGiocatore)) {
				this.atStanza.getClient(wvItem).invia(comunicazione);
			}
		}
	}

	/**
	 * Invia a tutti i client lo stesso oggetto serializzabile
	 * 
	 * @return Ritorna l'{@link OutputStream} che ha generato l'errore nel caso
	 *         in cui l'errore non is verifichi, ritorna null
	 */
	private void inviaTutti(IComunicazione comunicazione) {
		CLCHelper.stampa("Invio comunicazione: "
				+ comunicazione.getClass().toString());
		if (comunicazione instanceof Messaggio) {
			Messaggio wvMessaggio = (Messaggio) comunicazione;
			CLCHelper.stampa(wvMessaggio.getMessaggio());
		}
		for (Integer wvItem : this.atIdGiocatori) {
			this.atStanza.getClient(wvItem).invia(comunicazione);
		}
	}

	private Posizionamento riceviPedina(Integer idClient) {
		return (Posizionamento) this.atStanza.getClient(idClient).ricevi();
	}

	private void riceviTuttiAck() {
		for (Integer wvItem : this.atIdGiocatori) {
			this.atStanza.getClient(wvItem).ricevi();
		}
	}

}