package controllerServer;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Agnello;
import model.Animale;
import model.ETipoID;
import model.Mappa;
import model.Pecora;
import model.PedinaPastore;
import model.Strada;
import model.mosse.Abbattuto;
import model.mosse.ETipoOvino;
import model.mosse.IComunicazione;
import model.mosse.IMossa;
import model.mosse.Investimento;
import model.mosse.Messaggio;
import model.mosse.Movimento;
import model.mosse.MovimentoLupo;
import model.mosse.Nascita;
import model.mosse.Offerta;
import model.mosse.Posizionamento;
import model.mosse.Svezzamento;
import model.mosse.finePreliminari;
import view.CLCHelper;
import view.Start;

/**
 * Clase che implementa i metodi per l'accesso al server RMI
 * 
 * @author federico
 * 
 */
public class GestorePartitaRMI extends Thread implements IServerRMI {
	private final Map<Integer, StanzaRMI> atStanze;
	private static GestorePartitaRMI atIstanza;
	/**
	 * Indica l'id della partita attualmente in fase di avvio
	 */
	private final AtomicInteger atIdUltimaPartita;
	private static Registry atRegistry;
	private static ControlloDisconnessioni atControlloDisconnessioni;

	private GestorePartitaRMI() {
		this.atStanze = new LinkedHashMap<Integer, StanzaRMI>();
		this.atIdUltimaPartita = new AtomicInteger(0);
		atControlloDisconnessioni = new ControlloDisconnessioni(this.atStanze, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		this.avvia();
	}

	/**
	 * Avvia il server RMI
	 */
	public void avvia() {
		try {

			// Crea un'istanza della classe che implementa l'interfaccia remota.
			IServerRMI wv_server_rmi = atIstanza;

			// Crea una versione remote di tale classe. Tale versione remota può
			// essere richiesta dai client e permette loro di effettuare
			// chiamate remote, cioè che sono eseguite sul server.
			IServerRMI wv_remote_server = (IServerRMI) UnicastRemoteObject.exportObject(wv_server_rmi, 0);

			// Crea un'instanza del registro RMI. Esso permette di associare
			// istanze di oggetti remoti a delle stringhe (well-known name) in
			// modo che i client, se a conoscenza di tale nome, possano ottenere
			// l'istanza.
			atRegistry = LocateRegistry.createRegistry(AT_PORTA_SERVER_RMI);

			// Registra l'associazione tra stringa e istanza
			atRegistry.rebind(AT_SERVER_NAME, wv_remote_server);
			this.aggiungiPartita();
			atControlloDisconnessioni.start();
			CLCHelper.stampaAncheInTest("Registro RMI pubblicato, ora in attesa di richieste.");
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
			}
		}
	}

	/**
	 * termina il server RMI
	 */
	public static void termina() {
		try {
			atControlloDisconnessioni.getContollaStanze().set(false);
			atRegistry.unbind(AT_SERVER_NAME);
			UnicastRemoteObject.unexportObject(atRegistry, true);
		} catch (AccessException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, "GestorePartitaRMI", e);
			}
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, "GestorePartitaRMI", e);
			}
		} catch (NotBoundException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, "GestorePartitaRMI", e);
			}
		}

		atIstanza = null;
	}

	/**
	 * Istanzia il server RMI secono la logica Singleton
	 * 
	 * @return {@link GestorePartitaRMI} che rappresenta l'unica istanza dle
	 *         server
	 */
	public static GestorePartitaRMI istanzia() {
		if (atIstanza == null) {
			atIstanza = new GestorePartitaRMI();
		}
		return atIstanza;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#collegaPartita(java.lang.String)
	 */
	public synchronized ArrayList<Integer> collegaPartita(String nome) throws RemoteException {
		ArrayList<Integer> wv_id = new ArrayList<Integer>();
		Integer wv_id_partita;
		if (this.atStanze.get(this.atIdUltimaPartita.get()).getPermettiAvvio()) {
			// Se la partita può essere avviata, ne creo una nuova e aggiungo li
			// il client
			wv_id_partita = this.aggiungiPartita();
			wv_id.add(wv_id_partita);
			wv_id.add(this.atStanze.get(wv_id_partita).aggiungiClient(nome));
		} else {
			wv_id_partita = this.atIdUltimaPartita.get();
			wv_id.add(wv_id_partita);
			wv_id.add(this.atStanze.get(wv_id_partita).aggiungiClient(nome));

		}
		return wv_id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#avviaPartita(java.lang.Integer)
	 */
	public Boolean avviaPartita(Integer idPartitia) throws RemoteException {
		synchronized (this.atStanze.get(idPartitia)) {
			this.atStanze.get(idPartitia).calcolaTempoScaduto();
			return this.atStanze.get(idPartitia).getPermettiAvvio();
		}
	}

	private Integer aggiungiPartita() {
		Integer wv_id_partita;
		wv_id_partita = this.atIdUltimaPartita.addAndGet(AT_SOMMATORE_ID);

		synchronized (this.atStanze) {
			CLCHelper.stampa("Partita RMI aggiunta. ID: " + wv_id_partita);
			this.atStanze.put(wv_id_partita, new StanzaRMI(wv_id_partita));
		}
		return wv_id_partita;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#ricollegaPartita(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public Boolean ricollegaPartita(Integer idPartitia, Integer idGiocatore) throws RemoteException {
		synchronized (this.atStanze.get(idPartitia)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartitia);

			if (wvStanza.getPartitaTerminata()) {
				return false;
			}

			wvStanza.riassegnaClient(idGiocatore);
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#ottieniMappa(java.lang.Integer)
	 */
	public synchronized Mappa ottieniMappa(Integer idPartitia) throws RemoteException {

		StanzaRMI wvStanza = this.atStanze.get(idPartitia);
		if (wvStanza.getPartitaServer().getMappa() == null) {
			wvStanza.caricaMappa();
		}
		if (Start.abilitaTest) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING, "GestorePartitaRMI", e);
				}
			}
		}
		CLCHelper.stampaAncheInTest("Mappa inviata");
		return wvStanza.getPartitaServer().getMappa();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#impostaPedina(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	public void impostaPedina(Integer idPartita, Integer idGiocatore, Integer idPedina, Integer idStrada) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			// Aggiorno tutti i parametri interessati
			wvStanza.getPartitaServer().posizionaPedinaPastore(idGiocatore, idPedina, idStrada);

			// Lascio il messaggio sul buffer per i client
			wvStanza.depositaComunicazione(new Posizionamento(idGiocatore, idPedina, idStrada), idGiocatore);
			CLCHelper.stampa("Posizionamento pedina " + idPedina + " in starada " + idStrada);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#investi(java.lang.Integer,
	 * model.mosse.Investimento)
	 */
	public void investi(Integer idPartita, Investimento investimento) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			PartitaServer wvPartitaServer = wvStanza.getPartitaServer();
			Mappa wvMappa = wvPartitaServer.getMappa();

			wvMappa.getGiocatore(investimento.getIdGiocatore()).aggiungiTesseraTerreno(investimento.getTipoTerreno());
			wvMappa.aggiornaDanari(investimento.getIdGiocatore(), investimento.getCosto());
			wvStanza.depositaComunicazione(investimento, investimento.getIdGiocatore());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#accoppiamento(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer)
	 */
	public void accoppiamento(Integer idPartita, Integer idGiocatore, Integer idRegione) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);

			Integer wvNascituro = wvStanza.getPartitaServer().accoppiamento(idRegione);
			wvStanza.depositaComunicazione(new Nascita(idRegione, wvNascituro));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#abbattimento(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.util.ArrayList)
	 */
	public void abbattimento(Integer idPartita, Integer idGiocatore, Integer idRegione, Integer idAnimale, ArrayList<Integer> idGiocatoirVicini) {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			PartitaServer wvPartitaServer = wvStanza.getPartitaServer();
			Abbattuto wvAbbattuto = wvPartitaServer.abbattimento(idAnimale, idGiocatore, idGiocatoirVicini);
			wvStanza.depositaComunicazione(wvAbbattuto);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#inviaVenditaMercato(java.lang.Integer,
	 * java.util.ArrayList)
	 */
	public void inviaVenditaMercato(Integer idPartita, ArrayList<Offerta> offerte) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#acquistaDaMercato(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer)
	 */
	public void acquistaDaMercato(Integer idPartita, Integer idGiocatore, Integer idAcquisto) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#riceviComunicazione(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public IComunicazione riceviComunicazione(Integer idPartitia, Integer idGiocatore) throws RemoteException {
		return this.atStanze.get(idPartitia).rilevaComunicazione(idGiocatore);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#mioTurno(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public Boolean mioTurno(Integer idPartita, Integer idGiocatore) throws RemoteException {
		return this.atStanze.get(idPartita).getGiocatoreAttuale().get() == idGiocatore;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * controllerServer.IServerRMI#calcolaProssimoGiocatore(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public void calcolaProssimoGiocatore(Integer idPartita, Integer idGiocatore) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			Messaggio wvMessaggio = new Messaggio();
			wvStanza.calcolaProssimoGiocatore();
			if (wvStanza.getTerminaPartita()) {
				wvMessaggio.uscita();
			} else {
				// wvStanza.setTerminaPartita(true);
				wvMessaggio.turno(wvStanza.getGiocatoreAttuale().get());
			}

			// wvStanza.depositaComunicazione(wvMessaggio, idGiocatore);
			// Versione
			// precedente
			wvStanza.depositaComunicazione(wvMessaggio);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#spostaAnimale(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	public void spostaAnimale(Integer idPartita, Integer idGiocatore, Integer idAnimale, Integer idRegioneArrivo) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			Integer wvVecchiaPosizione;
			try {
				wvVecchiaPosizione = wvStanza.getPartitaServer().getMappa().getAnimale(idAnimale).getRegioneOccuapta().getId();
			} catch (NullPointerException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING, "GestorePartitaRMI", e);
				}
				CLCHelper.stampa("Non sono riuscito a fare la mossa in spostaAnimale.. " + idAnimale + "|" + idRegioneArrivo);
				Messaggio messaggio = new Messaggio();
				messaggio.nack();
				wvStanza.depositaComunicazione(messaggio);
				return;
			}

			wvStanza.getPartitaServer().spostaAnimale(idAnimale, idRegioneArrivo);
			wvStanza.depositaComunicazione(new Movimento(idAnimale, wvVecchiaPosizione, idRegioneArrivo, 0), idGiocatore);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#muoviPedina(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.Integer)
	 */
	public void muoviPedina(Integer idPartita, Integer idGiocatore, Integer idPedina, Integer idStradaArrivo, Integer costo) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			Integer wvVecchiaPosizione = wvStanza.getPartitaServer().getMappa().getPedinaPastore(idPedina).getStradaOccupata().getId();
			wvStanza.getPartitaServer().spostaPastore(idPedina, idStradaArrivo, costo);
			wvStanza.depositaComunicazione(new Movimento(idPedina, wvVecchiaPosizione, idStradaArrivo, costo), idGiocatore);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#turnoImpostaPedina(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public Integer turnoImpostaPedina(Integer idPartita, Integer idGiocatore) {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanzaRMI;
			wvStanzaRMI = this.atStanze.get(idPartita);
			CLCHelper.stampa("Richiedo posizionamento pedina a giocatore numero " + idGiocatore + "della partita: " + idPartita);

			if (wvStanzaRMI.getGiocatorePedinaAttuale().get() == idGiocatore) {
				if (this.calcolaProssimaPedina(wvStanzaRMI, idGiocatore)) {
					// qui sono finite le model.mosse di posizionamento
					wvStanzaRMI.getFineMossePedine().set(true);
					;
					return 2;
				}
				if (wvStanzaRMI.getGiocatorePedinaAttuale().get() == idGiocatore) {
					// è il mio turno, ritorno 1
					wvStanzaRMI.eroGiocatoreAttuale.set(idGiocatore);
					return 1;
				}
			}
			if (wvStanzaRMI.getFineMossePedine().get()) {
				return 2;
			}
			// Return 0 se il turno è di un'altro e io devo aspettare le
			// model.mosse

			if (wvStanzaRMI.eroGiocatoreAttuale.get() != -1) {
				Messaggio messaggio = new Messaggio();
				messaggio.turno(wvStanzaRMI.eroGiocatoreAttuale.get());
				wvStanzaRMI.depositaComunicazione(messaggio, wvStanzaRMI.eroGiocatoreAttuale.get());
				wvStanzaRMI.eroGiocatoreAttuale.set(-1);
			}
			return 0;
		}
	}

	/**
	 * ritorno <code>true</code> se finiscono le pedine da impostare
	 * 
	 * @param stanzaRMI
	 * @param idGiocatore
	 * @return
	 */
	Boolean calcolaProssimaPedina(StanzaRMI stanzaRMI, Integer idGiocatore) {
		PedinaPastore[] wvPedine = stanzaRMI.getPartitaServer().getPedineGiocatore(idGiocatore);

		for (PedinaPastore wvItemPedinaPastore : wvPedine) {
			if ((wvItemPedinaPastore.getStradaOccupata() == null) && !stanzaRMI.getClient(idGiocatore).getTempoScaduto()) {
				return false;
			}
		}
		stanzaRMI.getGiocatorePedinaAttuale().addAndGet(AT_SOMMATORE_ID);

		Integer wvIdMassimo = (stanzaRMI.getNumeroGiocatori() * AT_SOMMATORE_ID) + ETipoID.GIOCATORE.ordinal();
		if (stanzaRMI.getGiocatorePedinaAttuale().get() > wvIdMassimo) {
			// stanzaRMI.getGiocatoreAttuale().set(stanzaRMI.getPrimoGiocatore());
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#svezzaAgnello(java.lang.Integer)
	 */
	public Boolean svezzaAgnello(Integer idPartita) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			PartitaServer wvPartitaServer = wvStanza.getPartitaServer();

			if (!wvPartitaServer.getFaseAgnelli()) {
				return false;
			}
			ArrayList<Agnello> wvAgnelli = wvPartitaServer.getMappa().getAgnelliSvezzamento();
			ETipoOvino wvETipoOvino;

			if (wvAgnelli.size() > 0) {
				Animale wvAnimale = wvStanza.getPartitaServer().svezzaAgnello(wvAgnelli.get(0));
				if (wvAnimale instanceof Pecora) {
					wvETipoOvino = ETipoOvino.Pecora;
				} else {
					wvETipoOvino = ETipoOvino.Montone;
				}
				wvStanza.depositaComunicazione(new Svezzamento(wvAnimale.getId(), wvETipoOvino));

				if (wvAgnelli.size() == 1) {
					wvPartitaServer.disattivaFaseAgnelli();
					return false;
				}
				return true;
			}

			wvPartitaServer.disattivaFaseAgnelli();
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#muoviPecoraNera(java.lang.Integer)
	 */
	public void muoviPecoraNera(Integer idPartita) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			PartitaServer wvPartitaServer = wvStanza.getPartitaServer();
			Integer wvIdExRegione = wvPartitaServer.getMappa().getPecoranera().getRegioneOccuapta().getId();
			Integer wvIdRegione = wvPartitaServer.muoviPecoraNera().getId();
			if (wvIdExRegione.equals(wvIdRegione)) {
				return;
			}
			IMossa wvMossa = new Movimento(wvPartitaServer.getMappa().getPecoranera().getId(), wvIdExRegione, wvIdRegione, 0);
			wvStanza.depositaComunicazione(wvMossa);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#muoviLupo(java.lang.Integer)
	 */
	public void muoviLupo(Integer idPartita) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			PartitaServer wvPartitaServer = wvStanza.getPartitaServer();

			if (wvPartitaServer.getFaseLupo()) {
				Integer wvIdExRegione = wvPartitaServer.getMappa().getLupo().getRegioneOccuapta().getId();
				Integer wvIdRegione = wvPartitaServer.muoviLupo().getId();
				Integer wvIdAnimlaeMangiato = wvPartitaServer.mangiaAnimale();
				IMossa wvMossa = new MovimentoLupo(wvPartitaServer.getMappa().getLupo().getId(), wvIdExRegione, wvIdRegione, wvIdAnimlaeMangiato);
				wvStanza.depositaComunicazione(wvMossa);
				wvPartitaServer.disattivaFaseLupo();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#fineMossePreliminari(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public void fineMossePreliminari(Integer idPartita, Integer idGiocatore) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			IMossa wvMossa = new finePreliminari(idGiocatore);
			wvStanza.depositaComunicazione(wvMossa);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#getPrimoGiocatore(java.lang.Integer)
	 */
	public Integer getPrimoGiocatore(Integer idPartita) throws RemoteException {
		return this.atStanze.get(idPartita).getPrimoGiocatore();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#preparaPartita(java.lang.Integer)
	 */
	public void preparaPartita(Integer idPartita) throws RemoteException {
		synchronized (this.atStanze.get(idPartita)) {
			StanzaRMI wvStanza = this.atStanze.get(idPartita);
			wvStanza.atIdPrimoGiocatore = (Integer) wvStanza.getIdGiocatori().toArray()[0];
			wvStanza.getGiocatoreAttuale().set(wvStanza.atIdPrimoGiocatore);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#inviaPing(java.lang.Integer,
	 * java.lang.Integer, controllerServer.IstanteUltimoPing)
	 */
	public void inviaPing(Integer idPartita, Integer idGiocatore, IstanteUltimoPing istanteUltimoPing) throws RemoteException {
		this.atStanze.get(idPartita).getClient(idGiocatore).setIstanteUltimoPing(istanteUltimoPing);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#posizioneLupo(java.lang.Integer)
	 */
	public Integer posizioneLupo(Integer idPartitia) throws RemoteException {
		synchronized (this.atStanze.get(idPartitia)) {
			return this.atStanze.get(idPartitia).getPartitaServer().getMappa().getLupo().getRegioneOccuapta().getId();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerServer.IServerRMI#posizionePecoraNera(java.lang.Integer)
	 */
	public Integer posizionePecoraNera(Integer idPartitia) throws RemoteException {
		synchronized (this.atStanze.get(idPartitia)) {
			return this.atStanze.get(idPartitia).getPartitaServer().getMappa().getPecoranera().getRegioneOccuapta().getId();
		}
	}
}

/**
 * Thread da avviare per il controllo delle disconnessioni in RMI
 * 
 * @author federico
 * 
 */
class ControlloDisconnessioni extends Thread {

	private final Map<Integer, StanzaRMI> atStanze;
	private final AtomicBoolean atControllaStanze;
	private final GestorePartitaRMI atServerRMI;

	/**
	 * Costrutore per istanziare il thread che si occuap del controlle delle
	 * disconnessioni
	 * 
	 * @param stanze
	 * @param serverRMI
	 */
	public ControlloDisconnessioni(Map<Integer, StanzaRMI> stanze, GestorePartitaRMI serverRMI) {
		this.atStanze = stanze;
		this.atControllaStanze = new AtomicBoolean(true);
		this.atServerRMI = serverRMI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		if (Start.abilitaTest) {
			return;
		}
		Integer[] wvListaIdStanze;
		while (this.atControllaStanze.get()) {
			synchronized (this.atStanze) {
				wvListaIdStanze = this.atStanze.keySet().toArray(new Integer[0]);
			}
			for (Integer wvInteger : wvListaIdStanze) {
				StanzaRMI wvStanza = this.atStanze.get(wvInteger);
				if (wvStanza.getPartitaTerminata()) {
					continue;
				}
				// indice che mi serve per avere accesso al giocatore specifico
				// per assegnare le pedine
				Integer wvIndice = 0;
				for (ClientRMI wvClientRMI : wvStanza.getClientRMI()) {
					if (wvClientRMI.getTempoScaduto()) {
						boolean wvExValoreScollegato = wvClientRMI.getScollegato().get();
						wvClientRMI.getScollegato().set(true);
						if (!wvExValoreScollegato) {
							if (!wvStanza.getFineMossePedine().get()) {
								// entro qui se si isconnette un client mentre
								// sta scegliendo le pedine, gli assegno che gli
								// mancano in modo automatico
								Integer wvId = wvStanza.getPartitaServer().getMappa().getGiocatori().get(wvIndice).getId();
								for (PedinaPastore wvPedina : wvStanza.getPartitaServer().getPedineGiocatore(wvId)) {
									if (wvPedina.getStradaOccupata() != null) {
										continue;
									}
									Strada wvStrada = wvStanza.getPartitaServer().getMappa().getStradeLibere().get(0);
									wvStanza.getPartitaServer().posizionaPedinaPastore(wvPedina, wvStrada.getId());
									Posizionamento wvPosizionamento = new Posizionamento(wvId, wvPedina.getId(), wvStrada.getId());
									wvStanza.depositaComunicazione(wvPosizionamento, wvId);

								}

								this.atServerRMI.turnoImpostaPedina(wvInteger, wvStanza.getGiocatorePedinaAttuale().get());
							} else {
								wvStanza.getPrimoTurno().set(false);
								wvStanza.calcolaProssimoGiocatore();
								Messaggio wvMessaggio = new Messaggio();
								wvMessaggio.turno(wvStanza.getGiocatoreAttuale().get());
								wvStanza.depositaComunicazione(wvMessaggio);
							}

							// CLCHelper.Stampa("Client scolegato, salto al prossimo giocatore");
						} else if (wvStanza.getPrimoTurno().get()) {
							wvStanza.getPrimoTurno().set(false);
							wvStanza.calcolaProssimoGiocatore();
							Messaggio wvMessaggio = new Messaggio();
							wvMessaggio.turno(wvStanza.getGiocatoreAttuale().get());
							wvStanza.depositaComunicazione(wvMessaggio);
						}
					}
					wvIndice++;
				}
			}

		}
	}

	/**
	 * Ritorna un valore che indica se si devono controlare ancora le stanze
	 * 
	 * @return {@link AtomicBoolean} che indica se si devono controllare ancora
	 *         le stanze
	 */
	public AtomicBoolean getContollaStanze() {
		return this.atControllaStanze;
	}

}
