package controllerServer;

import helper.MapHelper;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import model.ETipoID;
import model.ETipoTerreno;
import model.Giocatore;
import model.Mappa;
import view.CLCHelper;

public abstract class Stanza {
	/**
	 * Da usare per gestire gli ID dei giocatori
	 */
	static final Integer AT_MOLTIPLICATORE_ID = 100;

	static final int DENARODUEGIOCATORI = 30;
	static final int DENAROQUATTROGIOCATORI = 20;

	private final PermettiAccessoPartita atAccesso;
	private final PartitaServer atPartita;
	private ArrayList<Integer> terreniAssegnati;
	private final AtomicInteger atIdGiocatoreAttuale;
	private Boolean atTerminaPartita;
	private Boolean atPartitaTerminata;
	Integer atFaseMercato;
	Integer atIdGiocatoreAttualeMercato;
	Integer atIdPrimoGiocatoreAcquisto;
	Integer atIdPartita;
	Integer atIdPrimoGiocatore;

	/**
	 * Costruttore
	 * 
	 * @param iDPartita
	 *            {@link Integer} che rapresenta l'ID della partita associata
	 *            alla stanza
	 */
	Stanza(Integer iDPartita) {
		this.atAccesso = new PermettiAccessoPartita();
		this.atPartita = new PartitaServer(iDPartita);
		this.atIdGiocatoreAttuale = new AtomicInteger();
		this.atTerminaPartita = false;
		this.atPartitaTerminata = false;
		this.atIdGiocatoreAttualeMercato = 0;
		this.atFaseMercato = 0;
	}

	abstract ArrayList<Giocatore> ottieniGiocatori();

	/**
	 * Ritorna un valore che indica se la partita è da terminare
	 * 
	 * @return {@link Boolean} che indica se la partita è da terminare
	 */
	public Boolean getTerminaPartita() {
		return this.atTerminaPartita;
	}

	/**
	 * Imposta un valore che indica se la partita è da terminare al valore
	 * specificato
	 * 
	 * @param valore
	 *            {@link Boolean} che rappresenta il valore a cui impostare
	 *            l'attributo
	 */
	public void setTerminaPartita(Boolean valore) {
		this.atTerminaPartita = valore;
	}

	/**
	 * Imposta un valore che indica se la partita è terminata
	 * 
	 * @param valore
	 *            {@link Boolean} che rappresenta il valore a cui impostare
	 *            l'attributo
	 */
	public void setPartitaTerminata(Boolean valore) {
		this.atPartitaTerminata = valore;
	}

	/**
	 * Ritorna un valore che indica se la partita è terminata
	 * 
	 * @return {@link Boolean} che indica se la partita è terminata
	 */
	public Boolean getPartitaTerminata() {
		return this.atPartitaTerminata;
	}

	/**
	 * Ritorna il giocatore attuale
	 * 
	 * @return Ritorna un {@link AtomicInteger} che rappresenta l'ID del
	 *         giocatore attuale
	 */
	public AtomicInteger getGiocatoreAttuale() {
		return this.atIdGiocatoreAttuale;
	}

	/**
	 * Ritorna l'ID del primo giocatore
	 * 
	 * @return {@link Integer} che rapprsenta l'ID del primo giocatore
	 */
	public Integer getPrimoGiocatore() {
		return this.atIdPrimoGiocatore;
	}

	/**
	 * Ritorna la partita associata alla stanza
	 * 
	 * @return Ritorna un oggetto di tipo {@link PartitaServer} che rappresenta
	 *         la partita in corso
	 */
	public PartitaServer getPartitaServer() {
		return this.atPartita;
	}

	/**
	 * Ritorna il numero di client nella {@link Stanza}
	 * 
	 * @return Ritorna un {@link Integer} che rappresenta il numero di client
	 *         nella stanza
	 */
	public Integer getNumeroGiocatori() {
		return this.atAccesso.getNumeroGiocatori();
	}

	/**
	 * Ritorna un valore che indica se la partita può essere avviata
	 * 
	 * @return Ritorna un {@link Boolean} che indica se può essere avviata la
	 *         partita
	 */
	public Boolean getPermettiAvvio() {
		return this.atAccesso.getPermettiAvvio();
	}

	/**
	 * Ritorna un valore che indica se il tempo di attesa per altri client è
	 * scaduto oppure no
	 * 
	 * @return Ritorna un {@link Boolean} che indica se il tempo è scaduto
	 *         oppure no
	 */
	public Boolean getTempoScaduto() {
		return this.atAccesso.getTempoScaduto();
	}

	/**
	 * Ritorna un valore che indica se si è raggiunto il numero minimo di
	 * giocatori connessi alla partita
	 * 
	 * @return Ritorna un {@link Boolean} che indica se si è raggiunto il numero
	 *         minimo di giocatori
	 */
	public Boolean minimoGiocatori() {
		return this.atAccesso.minimoGiocatori();
	}

	/**
	 * Imposta un valore che indica se il tempo per l'accettazione di altri
	 * client nella {@link Stanza} è scaduto oppure no
	 * 
	 * @param valore
	 *            {@link Boolean} che rappresenta il valore da impostare
	 */
	public void setTempoScaduto(Boolean valore) {
		this.atAccesso.setTempoScaduto(valore);
	}

	Integer cercaIndiceGiocatore(Integer[] idGiocatori,
			Integer idGiocatoreCercare) {
		Integer wvIndice;
		for (wvIndice = 0; wvIndice < idGiocatori.length; wvIndice++) {
			if (idGiocatori[wvIndice].equals(idGiocatoreCercare)) {
				return wvIndice;
			}
		}
		return 0;
	}

	/**
	 * Aumenta il coneggio di giocatori nella stanza
	 */
	void aggiungiGiocatore() {
		this.atAccesso.aggiungiGiocatore();
	}

	void caricaMappa(ArrayList<Giocatore> giocatori) {
		CLCHelper.stampa("Sto caricando mappa 0");
		if (this.getPartitaServer().getMappa() != null) {
			return;
		}
		CLCHelper.stampa("Sto caricando mappa 1");
		MapHelper wvMapHelper = new MapHelper();

		wvMapHelper.caricaMappa();
		CLCHelper.stampa("Sto caricando mappa 2");
		Mappa mappa = wvMapHelper.getMappa();
		CLCHelper.stampa("Sto caricando mappa 3");
		mappa.setListaGiocatori(giocatori);
		this.getPartitaServer().setMappa(mappa);
	}

	Integer calcolaIDGocatore() {
		return (this.atAccesso.getNumeroGiocatori() * AT_MOLTIPLICATORE_ID)
				+ ETipoID.GIOCATORE.ordinal();
	}

	Integer calcolaIdPedina(Integer indice) {
		return ((indice * AT_MOLTIPLICATORE_ID) + ETipoID.PEDINA_GIOCATORE
				.ordinal());
	}

	ETipoTerreno calcolaTesseraIniziale() {
		if (this.terreniAssegnati == null) {
			this.terreniAssegnati = new ArrayList<Integer>();
		}
		int numeroEstratto = this.ottieniNumeroUnivoco();
		return ETipoTerreno.getTerreno(numeroEstratto);
	}

	private int ottieniNumeroUnivoco() {
		Integer numero = 0;
		Random rnd = new Random();
		do {
			numero = rnd.nextInt(6);
			if (!this.terreniAssegnati.contains(numero)) {
				break;
			}
			/*
			 * for (Integer item : terreniAssegnati) { if(item.equals(numero)){
			 * continua=true; break; } }
			 */
		} while (true);

		this.terreniAssegnati.add(numero);
		return numero;
	}
}
