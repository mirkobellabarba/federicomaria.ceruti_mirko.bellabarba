package controllerServer;

import java.lang.reflect.Array;
import java.rmi.MarshalException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Agnello;
import model.Animale;
import model.Giocatore;
import model.Lupo;
import model.Montone;
import model.Pecora;
import model.Pecoranera;
import model.PedinaPastore;
import model.Regione;
import model.Regione.LupoPersoException;
import model.Regione.PecoraNeraPersaException;
import model.Strada;
import model.mosse.Abbattuto;
import model.mosse.IComunicazione;
import model.mosse.IMossa;
import model.mosse.Mercato;
import model.mosse.Offerta;
import view.Start;
import controllerCommon.Partita;

public class PartitaServer extends Partita {
	private Mercato atMercato;
	private static final Integer AT_SOMMATORE_ID = 100;
	private static final Integer AT_NUMERO_DA_BATTERE = 5;
	private static final Integer AT_COSTO_ABBATTIMENTO = 2;
	private Boolean atFaseLupo;
	private Boolean atFaseAgnelli;

	/**
	 * Costruttore
	 * 
	 * @param idPartita
	 *            {@link Integer} che rapresenta l'ID della partita
	 */
	public PartitaServer(Integer idPartita) {
		super(null, idPartita);
		this.atFaseLupo = false;
		this.atFaseAgnelli = false;
	}

	/**
	 * Ritorna un valore che indica se siamo nella fase di movimento del
	 * {@link Lupo}
	 * 
	 * @return <code>true</code> se siamo nella fase dle movimento del
	 *         {@link Lupo} <code>false</code> altrimenti
	 */
	public Boolean getFaseLupo() {
		return this.atFaseLupo;
	}

	/**
	 * Imposta un valore che indica che siamo nella fase del movimento del
	 * {@link Lupo}
	 */
	public void attivaFaseLupo() {
		this.atFaseLupo = true;
	}

	/**
	 * Imposta un valore che indica che la fase del lupo è finita
	 */
	public void disattivaFaseLupo() {
		this.atFaseLupo = false;
	}

	/**
	 * Ritorna l'insieme di Pedine del gicoatore
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rapresenta l'ID del {@link Giocatore} di
	 *            cui avere le pedine
	 * @return Ritorna un {@link Array} di {@link PedinaPastore} che rappresenta
	 *         l'insieme di pedine del {@link Giocatore}
	 */
	public PedinaPastore[] getPedineGiocatore(Integer idGiocatore) {
		return this.getMappa().getGiocatore(idGiocatore).getPedinePastore();
	}

	/**
	 * Otiene la lista delle oferte di terreni in vendita per la fase di mercato
	 * 
	 * @return Ritorna un oggetto di tipo {@link Mercato} che rappresenta
	 *         l'elenco di offerte di vendita
	 */
	public Mercato getListaMercato() {
		return this.atMercato;
	}

	/**
	 * Rimuove un'offerta dall'elenco delle offerte dato un certo ID
	 * 
	 * @param idOfferta
	 *            {@link Integer} che rappresenta l'ID dell'offerta all'interno
	 *            dell'{@link ArrayList}
	 */
	public void rimuoviOfferta(Integer idOfferta) {
		this.atMercato.rimuoviOfferta(idOfferta);
	}

	/**
	 * Rimuove un'offerta dall'eelnco delle offerte dato un certo ID
	 * 
	 * @param offerta
	 *            {@link Offerta} che rappresenta l'offerta da riomuovere
	 */
	public void rimuoviOfferta(Offerta offerta) {
		this.atMercato.rimuoviOfferta(offerta);
	}

	/**
	 * Ricrea la lista di Offerte
	 */
	public void azzeraListaOfferte() {
		this.atMercato = new Mercato();
	}

	/**
	 * Aggiunge le offerte riciste all'elenco delle offerte attualmente raccolte
	 * per il mercato
	 * 
	 * @param offerte
	 *            {@link ArrayList} di {@link Offerta} che rappresenta le
	 *            offerte da aggiungere
	 */
	public void aggiungiOfferte(ArrayList<Offerta> offerte) {
		this.atMercato.aggiungiOfferte(offerte);
	}

	/**
	 * Aggiunge le offerte specificate al mercato
	 * 
	 * @param offerte
	 *            {@link Mercato} che contiene le offerte da aggiungere
	 */
	public void aggiungiOfferte(Mercato offerte) {
		this.atMercato.aggiungiOfferte(offerte.getOfferte());
	}

	/**
	 * Muove la {@link Pecoranera} in modo casuale e aggiorna la mappa
	 * 
	 * @return Ritorna una {@link Regione} che indica dove si è mossa la
	 *         {@link Pecoranera}
	 */
	public Regione muoviPecoraNera() {
		Pecoranera wvPecoraNera;
		Regione wvRegionePrimaMovimento;
		Integer wvNumeroMovimento;
		Strada wvStradaMovimento;

		wvNumeroMovimento = this.lanciaDado();

		wvPecoraNera = this.getMappa().getPecoranera();

		wvRegionePrimaMovimento = wvPecoraNera.getRegioneOccuapta();

		wvStradaMovimento = wvRegionePrimaMovimento
				.getStradaNumero(wvNumeroMovimento);
		if (wvStradaMovimento != null) {
			// se nella regine attuale della Pecora Nera esiste la strada con il
			// numero uguale a
			// quello del lancio del dado entro qui
			if (!wvStradaMovimento.Occupata()) {
				this.spostaAnimale(wvPecoraNera, wvStradaMovimento
						.getRegioneOpposta(wvRegionePrimaMovimento));
			}
		}
		return wvPecoraNera.getRegioneOccuapta();

	}

	/**
	 * Muove la {@link Lupo} in modo casuale e aggiorna la mappa
	 * 
	 * @return Ritorna una {@link Regione} che indica dove si è mossa la
	 *         {@link Lupo}
	 */
	public Regione muoviLupo() {
		Lupo wvLupo;
		Regione wvRegionePrimaMovimento;
		Integer wvNumeroMovimento;
		Integer wvNumeroStradeLimitrofe;
		Integer wvNumeroStradeOccupateRecinto;
		Strada wvStradaMovimento;

		wvNumeroMovimento = this.lanciaDado();

		wvLupo = this.getMappa().getLupo();

		wvRegionePrimaMovimento = wvLupo.getRegioneOccuapta();

		wvStradaMovimento = wvRegionePrimaMovimento
				.getStradaNumero(wvNumeroMovimento);
		if (wvStradaMovimento != null) {
			// se nella regione attuale del Lupo esiste la strada con il numero
			// uguale a
			// quello del lancio del dado entro qui
			if (wvStradaMovimento.occupataDaRecinto()) {
				// se la strada è occupata da recinti entro qui
				// salvo il numero di strade occupate da recinti
				wvNumeroStradeOccupateRecinto = wvRegionePrimaMovimento
						.getStradeLimitrofeOccupateRecinto().size();
				// salvo il numero di strade limitrofe
				wvNumeroStradeLimitrofe = wvRegionePrimaMovimento
						.getStradeLimitrofe().size();

				if (wvNumeroStradeLimitrofe
						.compareTo(wvNumeroStradeOccupateRecinto) > 0) {
					// se le strade limitrofe sono di più del numero di strade
					// occuapte entro qui
					// e ritorno la posizione attuale
					return wvLupo.getRegioneOccuapta();
				}
			}

			// muovo l'animale, potrebbe rimanere nella stessa regione
			this.spostaAnimale(wvLupo, wvStradaMovimento
					.getRegioneOpposta(wvRegionePrimaMovimento));
		}

		return wvLupo.getRegioneOccuapta();
	}

	/**
	 * Trasforma un {@link Agnello} in una {@link Pecora} o in un
	 * {@link Montone} deciddendolo casualmente, e aggiornando automaticamente
	 * la mappa
	 * 
	 * @param agnelloSvezzamento
	 *            {@link Agnello} da far diventare {@link Pecoranera} o
	 *            {@link Montone}
	 * @return Ritorna un oggetto di tipo {@link Pecora} o {@link Montone} che
	 *         rappresenta l'evoluzione dell'agnello
	 */
	public Animale svezzaAgnello(Agnello agnelloSvezzamento) {
		Integer wvDado = this.lanciaDado();
		Integer idAgnello = agnelloSvezzamento.getId();
		Regione wvRegioneOccupata = agnelloSvezzamento.getRegioneOccuapta();
		Animale wvAnimaleFinale;
		if ((wvDado % 2) == 0) {
			// se ottengono un numero divisibile per 2 entro qui
			wvAnimaleFinale = new Pecora(agnelloSvezzamento.getId(),
					wvRegioneOccupata);
		} else {
			wvAnimaleFinale = new Montone(agnelloSvezzamento.getId(),
					wvRegioneOccupata);
		}
		this.getMappa().scambiaAnimale(idAgnello, wvAnimaleFinale);
		return wvAnimaleFinale;

	}

	/**
	 * Effettua l'accoppiamento, ritornando l'id dell'{@link Agnello} generato
	 * 
	 * @param idRegione
	 *            {@link Integer} che rappresenta l'id della {@link Regione}
	 *            dove effettuare l'accoppiamento
	 * @return Ritorna un {@link Integer} che rppresenta l'id dell'
	 *         {@link Agnello} generato
	 */
	public Integer accoppiamento(Integer idRegione) {

		Integer wvId = this.getMappa().getAnimali().size();

		wvId = this.ottieniIdAgnello();

		this.getMappa().aggiungiAgnello(wvId, idRegione);

		return wvId;
	}

	private Integer ottieniIdAgnello() {
		Integer wvId = 0;
		for (Animale wvAnimale : this.getMappa().getAnimali()) {
			if (wvId.compareTo(wvAnimale.getId()) < 0) {
				wvId = wvAnimale.getId();
			}
		}
		return wvId += AT_SOMMATORE_ID;
	}

	/**
	 * Effettua l'abbatimento di un animale, ritonando una
	 * {@link IComunicazione} di tipo {@link Abbattuto}
	 * 
	 * @param idAnimale
	 *            {@link Integer} che rapresenta l'ID dell'animale da abbattere
	 * @param idPastore
	 *            {@link Integer} che rapresenta l'ID del pastore che abbatte
	 * @param idGiocatoriVicini
	 *            {@link ArrayList} di {@link Integer} che rapresenta gli ID dei
	 *            giocatori vicino a chi abbatte
	 * @return Ritorna una {@link IMossa} di tipo {@link Abbattuto}
	 */
	public Abbattuto abbattimento(Integer idAnimale, Integer idPastore,
			ArrayList<Integer> idGiocatoriVicini) {
		Abbattuto wvAbbattuto = new Abbattuto();
		wvAbbattuto.setAnimaleAbbattuo(idAnimale);
		wvAbbattuto.setDebitore(idPastore);
		wvAbbattuto.setIdRegioneAppartenenza(this.getMappa()
				.getAnimale(idAnimale).getRegioneOccuapta().getId());
		try {
			this.getMappa().rimuoviAnimale(idAnimale);
		} catch (PecoraNeraPersaException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString() + " id: " + idAnimale, e);
			}
		} catch (LupoPersoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString() + " id: " + idAnimale, e);
			}
		}

		for (Integer wvItem : idGiocatoriVicini) {
			if (this.lanciaDado() >= AT_NUMERO_DA_BATTERE) {
				wvAbbattuto.aggiungiCreditore(wvItem);
				this.getMappa().getGiocatore(idPastore)
						.spendiDanaro(AT_COSTO_ABBATTIMENTO);
				this.getMappa().getGiocatore(wvItem)
						.spendiDanaro(-AT_COSTO_ABBATTIMENTO);
			}

		}
		return wvAbbattuto;
	}

	/**
	 * Il {@link Lupo} mangia un {@link Animale} nella {@link Regione} in cui è
	 * 
	 * @return Ritorna un {@link Integer} che rappresenta l'ID dell'
	 *         {@link Animale} mangiato
	 */
	public Integer mangiaAnimale() {
		Regione wvRegioneOccupata = this.getMappa().getLupo()
				.getRegioneOccuapta();
		Integer wvIdAnimaleMangiato = -1;
		Integer wvNumeroOvini = wvRegioneOccupata.getAnimali().size();
		if (!wvNumeroOvini.equals(0)) {
			Integer wvRisultato;
			Animale wvAnimale;
			while (wvRegioneOccupata.contieneOviniMangiabili()) {
				wvRisultato = this.lanciaDado(wvNumeroOvini);
				wvAnimale = wvRegioneOccupata.getAnimali().get(wvRisultato);
				if ((wvAnimale instanceof Lupo)
						|| (wvAnimale instanceof Pecoranera)) {
					continue;
				}
				wvIdAnimaleMangiato = wvAnimale.getId();
				this.getMappa().rimuoviAnimale(wvAnimale, wvRegioneOccupata);
				break;
			}

		}
		return wvIdAnimaleMangiato;
	}

	/**
	 * Ritorna se è la fase dello svezzamento agnelli
	 * 
	 * @return <code>true</code> se è ora della fase, <code>false</code>
	 *         altrimenti
	 */
	public boolean getFaseAgnelli() {

		return this.atFaseAgnelli;
	}

	/**
	 * Imposta la fase agnello
	 */
	public void attivaFaseAgnelli() {
		this.atFaseAgnelli = true;
	}

	/**
	 * Imposta un valore che indica che la fase agnelli è disattivata
	 */
	public void disattivaFaseAgnelli() {
		this.atFaseAgnelli = false;
	}

	/**
	 * Rimuove le offerte specificate dalla lista di offerte del ercato
	 * 
	 * @param wvMercato
	 *            Ogetto di tipo {@link MarshalException} contenente le offerte
	 *            da rimuovere
	 */
	public void rimuoviOfferte(Mercato wvMercato) {
		for (Offerta wvOfferta : wvMercato.getOfferte()) {
			this.atMercato.rimuoviOfferta(wvOfferta);
		}

	}
}
