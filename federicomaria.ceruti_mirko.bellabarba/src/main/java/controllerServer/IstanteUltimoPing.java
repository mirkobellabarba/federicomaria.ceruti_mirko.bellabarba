package controllerServer;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Classe ping per la connessione RMI
 */
public class IstanteUltimoPing implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7289665388801194176L;
	/**
	 * indica il tempo massimo di attesa prima di segnare un client come
	 * scollegato
	 */
	private static final int TEMPO_CADUTA_CLIENT = 8;
	private Date atIstantePing;

	/**
	 * Costruttore
	 */
	public IstanteUltimoPing() {
		atIstantePing = new Date();
	}

	/**
	 * Imposta la data a quelle attuale
	 */
	public void impostaIstanteAttuale() {
		atIstantePing = new Date();
	}

	/**
	 * Ritorna un valore che indica se è scaduto il tempo di ping di un client
	 * 
	 * @return {@link Boolean}:<br>
	 *         <code>true</code> indica che è scaduto il tempo di ping del
	 *         client<br>
	 *         <code>false</code> indica che non è scaduto il tempo di ping del
	 *         client
	 */
	public Boolean tempoScaduto() {
		return getDateDiff(atIstantePing, new Date(), TimeUnit.SECONDS) > TEMPO_CADUTA_CLIENT;
	}

	/**
	 * Ottiene la differenza tra 2 date
	 * 
	 * @param date1
	 *            la data più vecchia
	 * @param date2
	 *            la data più nuova
	 * @param timeUnit
	 *            l'unita di misura in cui vuoi la differenza
	 * @return la differenza tr 2 date nell'unita di misura specificata
	 */
	public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
}
