package controllerCommon;

/**
 * Indica la modalità di gestione della connessione<br>
 * <br>
 * <b>RMI</b>: gestisce la connessione ad una partita online con l'ausilio del
 * paradigma RMI <br>
 * <b>SOCKET</b>: gestisce la connesisone ad una partita online con l'ausilio
 * delle Socket
 */
public enum ETipoConnessione {
	RMI, SOCKET;
}
