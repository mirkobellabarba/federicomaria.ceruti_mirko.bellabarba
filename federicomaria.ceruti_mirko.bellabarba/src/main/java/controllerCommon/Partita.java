package controllerCommon;

import java.util.Random;

import model.Animale;
import model.Mappa;
import model.PedinaPastore;
import model.PedinaRecinto;
import model.PedinaRecintoFinale;
import model.Regione;
import model.Strada;

/**
 * Rappresenta la partita, fornisce i metodi per gestirla
 */
public abstract class Partita {
	private Integer atIdPartita;
	private Mappa atMappa;
	private Boolean atFaseFinale;

	/**
	 * Costruttore della classe partita
	 */
	public Partita() {
		atIdPartita = null;
		atMappa = null;
		atFaseFinale = false;
	}

	/**
	 * Costruttore della classe partita
	 * 
	 * @param mappa
	 *            {@link Mappa} rappresenta la mappa su cui effettuare la
	 *            partita
	 * @param idPatita
	 *            {@link Integer} rappresenta l'id, sul server, della partita
	 */
	public Partita(Mappa mappa, Integer idPatita) {
		atMappa = mappa;
		atIdPartita = idPatita;
		atFaseFinale = false;
	}

	/**
	 * Ritorna un valore che rappresenta la mappa di gioco
	 * 
	 * @return {@link Mappa} ritorna la mappa della partitia
	 */
	public Mappa getMappa() {
		return atMappa;
	}

	/**
	 * Imposta la {@link Mappa} dell'oggetto {@link Partita}
	 * 
	 * @param mappa
	 *            Oggetto di tipo {@link Mappa} che idica la mappa da impostare
	 */
	public void setMappa(Mappa mappa) {
		this.atMappa = mappa;
	}

	/**
	 * Ritorna l'ID della partita
	 * 
	 * @return Ritorna un'{@link Integer} che rappresenta l'ID della partita
	 */
	public Integer getIDPartita() {
		return atIdPartita;
	}

	/**
	 * Imposta l'id della partita
	 * 
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'id
	 */
	public void setIDPartita(Integer idPartita) {
		this.atIdPartita = idPartita;
	}

	/**
	 * Posiziona la posizione iniziale della pedina pastore specificata
	 * 
	 * @param pedina
	 *            {@link PedinaPastore} che rappresenta la pedina da impostare
	 * @param idStrada
	 *            {@link Integer} che rappresenta l'id della strada dove
	 *            posizionare la pedina
	 */
	public void posizionaPedinaPastore(PedinaPastore pedina, Integer idStrada) {
		pedina.setStradaOccupata(atMappa.getStrada(idStrada));
		atMappa.getStrada(idStrada).setOccupante(pedina);
	}

	/**
	 * Posiziona la pedina pastore in un'altra strada
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 * @param idPedina
	 *            l'id della pedina
	 * @param idStrada
	 *            l'id della strada che si vuole raggiungere
	 */
	public void posizionaPedinaPastore(Integer idGiocatore, Integer idPedina, Integer idStrada) {
		// Ricavo al strada
		Strada wvStrada = atMappa.getStrada(idStrada);
		// Aggiorno i parametri della pedina
		PedinaPastore wvPedina = atMappa.getGiocatore(idGiocatore).aggiornaPosizionePedina(idPedina, wvStrada);
		// Aggiorno la mappa
		wvStrada.setOccupante(wvPedina);
	}

	/**
	 * Mentre un pastore lascia la propria strada attuale per spostarsi nella
	 * strada scelta, posso richimare questo metodo che posiziona un recinto
	 * nella strada appena lasciata
	 * 
	 * @param exStradaPastore
	 */
	public void posizionaRecinto(Strada exStradaPastore) {
		System.out.println("Partita.Posizione_Recinto: Non ancora implementato");
	}

	/**
	 * Sposta un animale da una regione ad un'altra
	 * 
	 * @param animaleSpostamento
	 *            {@link Animale} rappresenta l'animale da spostare
	 * @param regioneArrivo
	 *            {@link Regione} rappresenta la regione dove spostare
	 *            l'aanimale
	 * 
	 */
	public void spostaAnimale(Animale animaleSpostamento, Regione regioneArrivo) {
		Regione wvRegionePrimaSpostamento = animaleSpostamento.getRegioneOccuapta();
		if (wvRegionePrimaSpostamento.equals(regioneArrivo))
			return;

		wvRegionePrimaSpostamento.rimuoviAnimale(animaleSpostamento);
		regioneArrivo.aggiungiAnimale(animaleSpostamento);
		animaleSpostamento.cambiaRegioneOccupata(regioneArrivo);
	}

	/**
	 * Sposta un animlae da un aregiona ad un'altra
	 * 
	 * @param idAnimale
	 *            {@link Integer} reppresenta l'id dell'animale da spostare
	 * @param idRegioneArrivo
	 *            {@link Integer} rappresenta l'id della regione di arrivo
	 */
	public void spostaAnimale(Integer idAnimale, Integer idRegioneArrivo) {
		Animale wvAnimale = atMappa.getAnimale(idAnimale);
		Regione wvRegioneArrivo = atMappa.getRegione(idRegioneArrivo);

		if (wvAnimale.getRegioneOccuapta().equals(wvRegioneArrivo))
			return;

		wvAnimale.getRegioneOccuapta().rimuoviAnimale(wvAnimale);
		wvRegioneArrivo.aggiungiAnimale(wvAnimale);
		wvAnimale.cambiaRegioneOccupata(wvRegioneArrivo);
	}

	/**
	 * Sposta un pastore da una strada ad un'altra
	 * 
	 * @param pedinaSpostamento
	 *            {@link PedinaPastore} rappresenta la pedina da spostare
	 * @param stradaArrivo
	 *            {@link Strada} rappresenta la strada in cui spostare la pedina
	 * @param costo
	 *            {@link Integer} rappresenta il costo della movimento
	 */
	public void spostaPastore(PedinaPastore pedinaSpostamento, Strada stradaArrivo, Integer costo) {
		Strada wvStradaPrimaSpostamento = pedinaSpostamento.getStradaOccupata();
		if (wvStradaPrimaSpostamento.equals(stradaArrivo)) {
			return;
		}
		if (atMappa.getRecintiAttuali() > 0) {
			wvStradaPrimaSpostamento.pastoreLascia();
		} else {
			wvStradaPrimaSpostamento.pastoreLasciaFinale();
		}
		atMappa.riduciRecinti();
		stradaArrivo.setOccupante(pedinaSpostamento);
		pedinaSpostamento.setStradaOccupata(stradaArrivo);
		pedinaSpostamento.getGiocatorePossessore().spendiDanaro(costo);
	}

	/**
	 * Sposta la {@link PedinaPastore} specificata nella {@link Strada}
	 * specificata, lasciando una {@link PedinaRecinto} o una
	 * {@link PedinaRecintoFinale}
	 * 
	 * @param idPedina
	 *            {@link Integer} che rappresenta l'ID della
	 *            {@link PedinaPastore} da muovere
	 * @param idStrada
	 *            {@link Integer} che rappresenta l'ID della strada in cui
	 *            spostarsi
	 * @param costo
	 *            {@link Integer} che rappresenta il costo dello spstameto
	 */
	public void spostaPastore(Integer idPedina, Integer idStrada, Integer costo) {
		PedinaPastore wvPedina = atMappa.getPedinaPastore(idPedina);
		Strada wvStradaArrivo = atMappa.getStrada(idStrada);

		if (wvPedina.getStradaOccupata().equals(wvStradaArrivo)) {
			return;
		}
		if (atMappa.getRecintiAttuali().compareTo(0) > 0) {
			wvPedina.getStradaOccupata().pastoreLascia();
		} else {

			wvPedina.getStradaOccupata().pastoreLasciaFinale();
		}
		atMappa.riduciRecinti();
		wvStradaArrivo.setOccupante(wvPedina);
		wvPedina.setStradaOccupata(wvStradaArrivo);
		wvPedina.getGiocatorePossessore().spendiDanaro(costo);
	}

	/**
	 * Imposta un valore che indica se è iniziata la fase finale del gioco,
	 * ovvero se sono stati messi dei {@link PedinaRecintoFinale}
	 */
	public void calcolaFaseFinale() {
		if (getMappa().getRecintiAttuali().equals(0)) {
			atFaseFinale = true;
		}
	}

	/**
	 * Rtorna un valore che indica se è iniziata la fase finale
	 * 
	 * @return {@link Boolean} che indica se è iniziata la fase finale
	 */
	public Boolean getFaseFinale() {
		return atFaseFinale;
	}

	/**
	 * Ottine un numero random che simula il lancio del dado
	 * 
	 * @return un Intger da 1 a 6
	 */
	public Integer lanciaDado() {
		Random rnd = new Random();
		return (rnd.nextInt(6) + 1);
	}

	/**
	 * Ottine un numero random che simula il lancio del dado
	 * 
	 * @return un Intger da 0 a limiteSuperiore-1
	 */
	public Integer lanciaDado(Integer limiteSuperiore) {
		Random rnd = new Random();
		return (rnd.nextInt(limiteSuperiore));
	}

}
