package controllerClient;

import helper.FileIOHelper;
import helper.SwingHelper;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Animale;
import model.ETipoTerreno;
import model.Giocatore;
import model.Lupo;
import model.Pecoranera;
import model.PedinaPastore;
import model.PedinaRecinto;
import model.Regione;
import model.Regione.LupoPersoException;
import model.Regione.PecoraNeraPersaException;
import model.Strada;
import model.mosse.Abbattimento;
import model.mosse.Abbattuto;
import model.mosse.Accoppiamento;
import model.mosse.IMossa;
import model.mosse.Investimento;
import model.mosse.Mercato;
import model.mosse.Movimento;
import model.mosse.Nascita;
import model.mosse.Posizionamento;
import model.mosse.Svezzamento;
import model.mosse.tentativoFallito;
import view.CLCHelper;
import view.Start;
import controllerClient.Connessione.FaseMercatoException;
import controllerClient.Connessione.FinePartitaException;
import controllerClient.Connessione.FineTurnoException;
import controllerClient.Connessione.InizioPartitaException;
import controllerClient.Connessione.InizioTurnoException;
import controllerClient.Connessione.TurnoPersonaleException;
import controllerClient.ConnessioneRMI.MappaDaAggiornareException;
import controllerCommon.ETipoConnessione;
import controllerCommon.Partita;

/**
 * classe dove risiedono tutte le info della partita del client
 * 
 * @author Mirko
 * 
 */
public class PartitaClient extends Partita {
	private Integer atIdGiocatore;
	private final Connessione atConnessione;
	private ArrayList<IMossa> atMossePossibili;
	private final boolean atFinePartita;
	private Mercato atListaMercato;
	private final SwingHelper swingHelper;

	/**
	 * Costruttore
	 * 
	 * @param tipoConnessione
	 *            il tipo di connessione scelto per la partita
	 */
	public PartitaClient(ETipoConnessione tipoConnessione) {
		super();
		this.atConnessione = Connessione.instanzia(tipoConnessione);
		this.atFinePartita = false;
		this.swingHelper = new SwingHelper();
	}

	/**
	 * Questo metodo avvia la comunicazione con il server <i>attenzione perchè
	 * l'effettiva comunicazione avverà nei metodi più sotto</i> in seguito
	 * richiamare il metodo Accetta_Partita
	 * 
	 * @throws DisconnessioneException
	 * 
	 */
	public void connettiAlServer(String nomeGiocatore)
			throws DisconnessioneException {
		boolean esito = this.atConnessione.connetti(nomeGiocatore);
		if (!Start.abilitaTest) {
			System.out.println(esito ? "CLIENT CONNESSO"
					: "CLIENT NON CONNESSO");
		}

	}

	/**
	 * La partita si mette in ascolto del server, che invierà quando pronto
	 * tutti i dati sulla partita e la mappa.
	 * 
	 * Suggerimento per client/server: Ora devo far posizionare i pastori. Dopo
	 * averli raccolti posso richiamare PosizionaPastori()
	 * 
	 * @throws DisconnessioneException
	 * 
	 */
	public void accettaPartita() throws DisconnessioneException {
		// Ricevo l'id della partita
		this.setIDPartita(this.atConnessione.getIdPartita());
		// Aspetto l'id giocatore assegnato
		this.atIdGiocatore = this.atConnessione.getIdGiocatore();
		// Invio conferma

		// Salvo i dati della partita
		this.salvaDatiPartita();

		// Aspetto la mappa
		this.setMappa(this.atConnessione.getMappa());

		if (Start.abilitaTest) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
		}

		CLCHelper.stampa("Invio ACK, ricevuta mappa");
	}

	/**
	 * Salva i dati della partita mentre aspetta la mappa dal server. Nel caso
	 * il client si disconnetta e si riconnette durante la partita, questi dati
	 * verranno usati per rientrare nella stanza corretta
	 */
	private void salvaDatiPartita() {
		if (Start.abilitaTest) {
			return;
		}
		FileIOHelper fileDaScrivere = new FileIOHelper("datiPartita.txt");
		ArrayList<String> righeDaScrivere = new ArrayList<String>();
		righeDaScrivere.add(this.getIDPartita().toString());
		righeDaScrivere.add(this.atIdGiocatore.toString());
		fileDaScrivere.writeFile(righeDaScrivere);
	}

	/**
	 * Posiziona i pastori nelle strade che vengono passate come parametro
	 * 
	 * @param idStrade
	 *            un arraylist di id di strade, una per ogni pedina
	 * @throws DisconnessioneException
	 */
	public void posizionaPastori(ArrayList<Integer> idStrade)
			throws DisconnessioneException {
		if (idStrade == null) {
			return;
		}
		// Posizioni i pastori 1 per 1
		for (int i = 0; i < idStrade.size(); i++) {
			this.posizionaPastore(
					this.getMappa().getGiocatore(this.atIdGiocatore)
							.getPedinePastore()[i], idStrade.get(i));
		}
	}

	/**
	 * Spedisce lo spostamento al server e aggiorna la propria posizione sulla
	 * mappa locale
	 * 
	 * @param pastore
	 *            la pedina che si muove
	 * @param idStrada
	 *            l'id della strada in cui mi sposto
	 * @throws DisconnessioneException
	 */
	public void posizionaPastore(PedinaPastore pastore, Integer idStrada)
			throws DisconnessioneException {
		// spedisco l'id della strada
		this.atConnessione.inviaPosizioneIniziale(pastore.getId(), idStrada);
		// Aggiorno la mia posizione sulla mappa
		pastore.setStradaOccupata(this.getMappa().getStrada(idStrada));
	}

	/**
	 * Ottiene il numero di giocatori attualmente in gioco
	 * 
	 * @return un intero che rappresenta il numero di giocatori
	 */
	public Integer getNumeroGiocatori() {
		if (this.getMappa() == null) {
			return null;
		}
		return this.getMappa().getGiocatori().size();
	}

	/**
	 * Ottiene i giocatori attualmente in gioco
	 * 
	 * @return un array di giocatori
	 */
	public ArrayList<Giocatore> getGiocatori() {
		if (this.getMappa() == null) {
			return null;
		}
		return this.getMappa().getGiocatori();
	}

	/**
	 * Ottiene il giocatore personale
	 * 
	 * @return un giocatore
	 */
	public Giocatore getGiocatore() {
		if (this.getMappa() == null) {
			return null;
		}
		return this.getMappa().getGiocatore(this.atIdGiocatore);
	}

	/**
	 * Ottiene il giocatore specificato
	 * 
	 * @return un giocatore
	 */
	public Giocatore getGiocatore(Integer idGiocatore) {
		if (this.getMappa() == null) {
			return null;
		}
		return this.getMappa().getGiocatore(idGiocatore);
	}

	// Ritorna l'id del giocatore personale
	public Integer getIdGiocatore() {
		return this.atIdGiocatore;
	}

	/**
	 * Ottine un numero random che simula il lancio del dado
	 * 
	 * @return un Intger da 1 a 6
	 */
	@Override
	public Integer lanciaDado() {
		Random rnd = new Random();
		return (rnd.nextInt(6) + 1);
	}

	/**
	 * Permette di spostare un animale da una regione ad un'altra poi notifica
	 * la mossa al server
	 * 
	 */
	public void spostaAnimale(Animale animale, Regione regioneArrivo,
			Integer costoMossa) {
		Integer idExRegione = animale.getRegioneOccuapta().getId();
		animale.cambiaRegioneOccupata(regioneArrivo);

		this.atConnessione.inviaSpostamento(animale.getId(), idExRegione,
				regioneArrivo.getId(), costoMossa);
	}

	/**
	 * Permette di spostare un pastore da una strada ad un'altra
	 * 
	 * @param idPedina
	 *            è l'id della pedina che la grafica dovrà recuperare e passare.
	 *            E' l'id all'intenro dell'array che contine la pedina
	 */
	public void spostaPastore(Strada stradaSuccessiva, Integer idPedina,
			Integer costoMossa) {
		/*
		 * ottengo la strada occupata dalla pedina (con indice passato dalla
		 * strada) possedute dal giocatore con id corrispondente all'id che mi
		 * ha assegnato la partita atIdGiocatore è diviso 100 perchè l'id
		 * mandato dal server è diviso per 100 e -1 perchè l'indice
		 * dell'arraylist parte da 0
		 */
		PedinaPastore wvPastore = this.getMappa().getGiocatori()
				.get((this.atIdGiocatore / 100) - 1).getPedinePastore()[idPedina];
		Strada wvExStrada = wvPastore.getStradaOccupata();
		// Sposto il pastore
		wvPastore.setStradaOccupata(stradaSuccessiva);
		// Posiziono un recinto da dove mi muovo
		this.posizionaRecinto(wvExStrada);

		// Invio la mossa che verrà serializzata all'interno del metodo
		this.atConnessione.inviaSpostamento(wvPastore.getId(),
				wvExStrada.getId(), stradaSuccessiva.getId(), costoMossa);
	}

	/**
	 * Ritorna gli oggetti di tipo {@link Regione} in cui si può spostare l'
	 * {@link Animale} selezionato
	 * 
	 * @param animale
	 *            {@link Animale} di cui si vogliono sapere le regioni in cui si
	 *            può muovere
	 * @return Ritorna {@link ArrayList} di {@link Regione} che rappresenta le
	 *         regioni in cui l'animale si può muovere se l'animale non si può
	 *         muovere, ritorna <code>null</code>
	 */
	public ArrayList<Regione> controlloRegioniMovimentoAnimale(Animale animale) {
		ArrayList<Regione> wvRegioniMovimento;
		ArrayList<Strada> wvStradeLimitrofe;
		Regione wvRegioneAnimale;

		wvRegioneAnimale = animale.getRegioneOccuapta();
		wvStradeLimitrofe = wvRegioneAnimale.getStradeLimitrofe();

		wvRegioniMovimento = new ArrayList<Regione>();
		// Cerco in tutte le strade collegate alla regione in cui si situa
		// l'animale le regioni in cui si può muovere
		for (Strada itemStrada : wvStradeLimitrofe) {
			if (itemStrada.Occupata() && !itemStrada.occupadaDaPastore()) {
				// se la strada è occupata, ma non è occupata da un pastore
				// entro qui
				continue;
			}
			for (Regione itemRegione : itemStrada.getRegioniLimitrofe()) {
				if (!itemRegione.equals(wvRegioneAnimale)) {
					// se la regione in esame è diversa da quella attualmente
					// occupata dall'animale, l'aggiungo alla lista
					// di regioni in cui l'animale si può spostare
					wvRegioniMovimento.add(itemRegione);
				}
			}

		}

		if (wvRegioniMovimento.size() == 0) {
			// se l'elenco delle regioni in cui l'animale si può spostare è di
			// lunghezza zero entro qui
			return null;
		}

		return wvRegioniMovimento;
	}

	/**
	 * Aspetta un ack dal server
	 * 
	 * @return true o false se è finito il turno
	 * @throws DisconnessioneException
	 */
	public Boolean aspettaACK() throws DisconnessioneException {
		try {
			boolean wvEsito = this.atConnessione.getACK();
			return wvEsito;
		} catch (FineTurnoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return false;
		}

	}

	/**
	 * Ascolta il server aspettando una tessera terreno iniziale
	 * 
	 * @return
	 * @throws DisconnessioneException
	 */
	public String getTesseraTerrenoIniziale() throws DisconnessioneException {
		return this.atConnessione.getTesseraTerreno();
	}

	/**
	 * Invia un ACK al server
	 * 
	 * @throws DisconnessioneException
	 */
	public void inviaACK() throws DisconnessioneException {
		this.atConnessione.inviaACK();
	}

	/**
	 * Determina se l'id dindicato dal server corrisponde al mio
	 * 
	 * @param idGiocatore
	 *            l'id che arriva dal server
	 * @return true o false
	 */
	public boolean isMioTurno(Integer idGiocatore) {

		if (idGiocatore.equals(this.getGiocatore().getId())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Ottiene una mossa generica dal server
	 * 
	 * @return la mossa efettuata
	 * @throws DisconnessioneException
	 */
	public IMossa ottieniMossaAndAggiorna() throws DisconnessioneException {
		try {
			IMossa mossa = this.atConnessione.getMosseAltriGiocatori();
			try {
				CLCHelper.stampa("---------\nMossa dell'avversario"
						+ mossa.toString() + "\n---------");
				this.interpretaMossa(mossa);
			} catch (NullPointerException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
			return mossa;
		} catch (FineTurnoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return null;
		}
	}

	/**
	 * Riceve una mossa, la interpreta e aggiorna i relativi valori sulla mappa
	 * 
	 * @param mossa
	 *            la mossa che arriva dal server
	 */
	@SuppressWarnings("unused")
	public void interpretaMossa(IMossa mossa) {
		if (mossa == null) {
			return;
		}
		if (mossa instanceof Abbattuto) {
			Abbattuto abbattuto = (Abbattuto) mossa;
			try {
				this.getMappa().rimuoviAnimale(
						abbattuto.getAnimaleAbbattuto(),
						this.getMappa()
								.getAnimale(abbattuto.getAnimaleAbbattuto())
								.getRegioneOccuapta().getId());
			} catch (PecoraNeraPersaException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(
							Level.WARNING,
							this.getClass().toString() + " mossa: "
									+ mossa.toString(), e);
				}
			} catch (LupoPersoException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(
							Level.WARNING,
							this.getClass().toString() + " mossa"
									+ mossa.toString(), e);
				}
			}
			if (!Start.abilitaTest) {
				CLCHelper.stampa("abbattuto animale: "
						+ abbattuto.getAnimaleAbbattuto());
			}

			// Rimuovo il danaro
			for (Integer idGiocatore : abbattuto.getCreditori()) {
				this.getMappa().getGiocatore(idGiocatore).spendiDanaro(-2);
				this.getMappa().getGiocatore(abbattuto.getIdDebitore())
						.spendiDanaro(2);
			}
		} else if (mossa instanceof Nascita) {
			Nascita nascita = (Nascita) mossa;
			this.getMappa().aggiungiAgnello(nascita.getIdAgnello(),
					nascita.getIdRegione());
		} else if (mossa instanceof Investimento) {
			Investimento investimento = (Investimento) mossa;
			Giocatore giocatore = this.getMappa().getGiocatore(
					investimento.getIdGiocatore());
			this.getMappa().terrenoVenduto(investimento.getTipoTerreno());
			this.getMappa().aggiornaDanari(investimento.getIdGiocatore(),
					investimento.getCosto());
			giocatore.aggiungiTesseraTerreno(investimento.getTipoTerreno());
		} else if (mossa instanceof Movimento) {
			try {
				this.getMappa().movimento((Movimento) mossa);
			} catch (PecoraNeraPersaException e) {
				Regione regionePecoraNera = this.getMappa().cercaPecoraNera();
				if (regionePecoraNera.equals(Integer.MIN_VALUE)) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString() + " socket", e);
				}
				if (regionePecoraNera == null) {
					this.rigeneraPecoraNera();
				} else {
					Integer idRegione = this.atConnessione
							.getRegioneOccupataPecoraNera();
					try {
						this.getMappa().movimento(
								new Movimento(1003, regionePecoraNera.getId(),
										((Movimento) mossa)
												.getAtIdNuovaPosizione(), 0));
					} catch (PecoraNeraPersaException e1) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString() + " jkfagh", e1);
						}
					} catch (LupoPersoException e1) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString() + " jkfagh", e1);
						}
					}
				}
			} catch (LupoPersoException e) {
				Regione regioneLupo = this.getMappa().cercaLupo();
				if (regioneLupo.equals(Integer.MIN_VALUE)) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString() + " socket", e);
				}
				if (regioneLupo == null) {
					this.rigeneraLupo();
				} else {
					Integer idRegione = this.atConnessione
							.getRegioneOccupataLupo();
					try {
						this.getMappa().movimento(
								new Movimento(1103, regioneLupo.getId(),
										((Movimento) mossa)
												.getAtIdNuovaPosizione(), 0));
					} catch (PecoraNeraPersaException e1) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString() + " jkfagh", e1);
						}
					} catch (LupoPersoException e1) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString() + " jkfagh", e1);
						}
					}
				}
			}
			if (!((Movimento) mossa).isAnimale()) {
				this.getMappa()
						.getPedinaPastore(((Movimento) mossa).getAtIdSoggetto())
						.getGiocatorePossessore()
						.spendiDanaro(((Movimento) mossa).getAtCostoMossa());
			}
		} else if (mossa instanceof Posizionamento) {
			Posizionamento posizionamento = (Posizionamento) mossa;
			this.getMappa().posizionaPedina(posizionamento.getIdPedina(),
					posizionamento.getIdStradaScelta());
		} else if (mossa instanceof Abbattimento) {
			if (!Start.abilitaTest) {
				CLCHelper.stampa("Aspetto esito dal server");
			}

		} else if (mossa instanceof tentativoFallito) {
			if (!Start.abilitaTest) {
				CLCHelper
						.stampa("Sfortunato! non sei riuscito ad abbattere animale");
			}
		} else if (mossa instanceof Svezzamento) {
			CLCHelper.stampa("Sto per svezzare l'agnello "
					+ ((Svezzamento) mossa).getIdAgnello());
			this.getMappa().scambiaAnimale(
					((Svezzamento) mossa).getIdAgnello(),
					((Svezzamento) mossa).getTipoOvino());
			CLCHelper.stampa("Ora è un "
					+ ((Svezzamento) mossa).getTipoOvino().toString());
		} else if (mossa instanceof Mercato) {
			CLCHelper.stampa("Lista mercato ottenuta.");
			this.atListaMercato = (Mercato) mossa;
		} else {
			if (!Start.abilitaTest) {
				System.out
						.println("NON ANCORA IMPLEMENTATO: Partita_CLient.InterpretaMossa:"
								+ mossa.getClass().toString());
			}

		}
	}

	private void rigeneraLupo() {
		Integer idRegione = this.atConnessione.getRegioneOccupataLupo();
		Animale animale = new Lupo(1103, this.getMappa().getRegione(idRegione));
		this.getMappa().AggiungiLupo(animale);
		this.getMappa().getRegione(idRegione).aggiungiAnimale(animale);
	}

	private void rigeneraPecoraNera() {
		Integer idRegione = this.atConnessione.getRegioneOccupataPecoraNera();
		Animale animale = new Pecoranera(1003, this.getMappa().getRegione(
				idRegione));
		this.getMappa().AggiungiPecoraNera(animale);
		this.getMappa().getRegione(idRegione).aggiungiAnimale(animale);

	}

	/**
	 * ottiene la lista del mercato
	 * 
	 * @return
	 */
	public Mercato getListaMercato() {
		return this.atListaMercato;
	}

	/**
	 * Si mette in ascolto di model.mosse preliminari, quando capisce che è ora
	 * di iniziare il turno, gestisce l'eccezione
	 * 
	 * @return
	 * @throws FinePartitaException
	 * @throws FaseMercatoException
	 * @throws InizioTurnoException
	 * @throws DisconnessioneException
	 * @throws NumberFormatException
	 */
	public IMossa ascoltaMossePreliminari() throws FinePartitaException,
			FaseMercatoException, InizioTurnoException, NumberFormatException,
			DisconnessioneException {
		IMossa mossa = this.atConnessione.getMossePreliminari();
		this.interpretaMossa(mossa);
		return mossa;
	}

	/**
	 * Prepara l'array contenete tutte le model.mosse che un pastore può fare
	 * 
	 * @param stradaOccupata
	 * @param class1
	 * @return
	 */
	public ArrayList<IMossa> precalcolaMosse(Strada stradaOccupata,
			IMossa ultimaMossa) {
		this.atMossePossibili = new ArrayList<IMossa>();
		boolean isPastoreMosso = false;

		if (ultimaMossa == null) {
			// Questa è la prima mossa, stampo tutto
			this.precaricaMosseSpostamentoOvini(stradaOccupata);
			this.precaricaMosseAccoppiamento(stradaOccupata
					.getRegioniLimitrofe());
			this.precaricaMosseSpostamentoPastore(stradaOccupata);
			this.precaricaMosseAbbattimento(stradaOccupata);
			this.precaricaMosseInvestimento(stradaOccupata);
		} else {
			// Ho già fatto qualche mossa, verifico che l'ultima riguardi un
			// pastore che si è spoststato
			if (ultimaMossa instanceof Movimento) {
				Movimento wvMovimento = (Movimento) ultimaMossa;
				isPastoreMosso = wvMovimento.isAnimale() ? false : true;
			}

			if (isPastoreMosso) {
				// se si, posso stampare ciò che voglio
				this.precaricaMosseSpostamentoOvini(stradaOccupata);
				this.precaricaMosseAccoppiamento(stradaOccupata
						.getRegioniLimitrofe());
				this.precaricaMosseSpostamentoPastore(stradaOccupata);
				this.precaricaMosseAbbattimento(stradaOccupata);
				this.precaricaMosseInvestimento(stradaOccupata);
			} else {
				// se no, occorre stampare solo model.mosse diverse da quella
				// precedente
				if (ultimaMossa instanceof Movimento) {
					this.precaricaMosseAccoppiamento(stradaOccupata
							.getRegioniLimitrofe());
					this.precaricaMosseSpostamentoPastore(stradaOccupata);
					this.precaricaMosseAbbattimento(stradaOccupata);
					this.precaricaMosseInvestimento(stradaOccupata);
				} else if (ultimaMossa instanceof Accoppiamento) {
					this.precaricaMosseSpostamentoOvini(stradaOccupata);
					this.precaricaMosseSpostamentoPastore(stradaOccupata);
					this.precaricaMosseAbbattimento(stradaOccupata);
					this.precaricaMosseInvestimento(stradaOccupata);
				} else if ((ultimaMossa instanceof Abbattimento)
						|| (ultimaMossa instanceof tentativoFallito)) {
					this.precaricaMosseAccoppiamento(stradaOccupata
							.getRegioniLimitrofe());
					this.precaricaMosseSpostamentoOvini(stradaOccupata);
					this.precaricaMosseSpostamentoPastore(stradaOccupata);
					this.precaricaMosseInvestimento(stradaOccupata);
				} else if (ultimaMossa instanceof Investimento) {
					this.precaricaMosseAccoppiamento(stradaOccupata
							.getRegioniLimitrofe());
					this.precaricaMosseSpostamentoOvini(stradaOccupata);
					this.precaricaMosseSpostamentoPastore(stradaOccupata);
					this.precaricaMosseAbbattimento(stradaOccupata);
				}
			}

		}

		return this.atMossePossibili;
	}

	/**
	 * Metodo da richiamare nel caso in cui il giocatore non si sia ancora
	 * mosso, e per via del regolamento in quel turno è obbligato.
	 * 
	 * @param stradaOccupata
	 *            la strada in cui si torva il pasotre
	 * @return un array list di sole model.mosse spostamento pastore
	 *         (obbligatorie per il refolamento)
	 */
	public ArrayList<IMossa> precalcolaMosseSoloSpostamento(
			Strada stradaOccupata) {
		this.atMossePossibili = new ArrayList<IMossa>();
		this.precaricaMosseSpostamentoPastore(stradaOccupata);
		return this.atMossePossibili;
	}

	/**
	 * Precarica le model.mosse che permettono al pastore di spostarsi
	 * 
	 * @param stradaOccupata
	 *            la strada in cui è il pastore
	 */
	private void precaricaMosseSpostamentoPastore(Strada stradaOccupata) {
		for (Strada stradaVicina : stradaOccupata.getStradeCollegate()) {
			if (!stradaVicina.Occupata()) {
				if (stradaOccupata.getOccupante() instanceof PedinaRecinto) {
					return;
				}

				IMossa wvMossa = new Movimento(stradaOccupata.getOccupante()
						.getId(), stradaOccupata.getId(), stradaVicina.getId(),
						new Integer(0));
				if (wvMossa != null) {
					this.atMossePossibili.add(wvMossa);
				}

			}
		}

		for (Strada stradaLontana : this.getMappa().getStradeLontaneLibere(
				stradaOccupata)) {
			if (stradaOccupata.getOccupante() instanceof PedinaRecinto) {
				return;
			}
			IMossa wvMossa = new Movimento(stradaOccupata.getOccupante()
					.getId(), stradaOccupata.getId(), stradaLontana.getId(),
					new Integer(1));
			if (wvMossa != null) {
				this.atMossePossibili.add(wvMossa);
			}
		}
	}

	/**
	 * Precarica un array di model.mosse possibili riguardo agli ovini che posso
	 * spostare dalla strada in cui mi trovo
	 * 
	 * @param stradaOccupata
	 *            la strada in cui mi trovo
	 */
	private void precaricaMosseSpostamentoOvini(Strada stradaOccupata) {
		for (Regione regione : stradaOccupata.getRegioniLimitrofe()) {
			if (stradaOccupata.getRegioneOpposta(regione).getTipoTerreno()
					.equals(ETipoTerreno.SHEEPSBURG)) {
				continue;
			}
			// Entro nel ciclo solo se la regione ha una regione opposta a se
			if (!stradaOccupata.getRegioneOpposta(regione).equals(regione)) {
				ArrayList<Animale> wvAnimaliSpostabili = regione
						.getAnimaliSpostabili();
				if (wvAnimaliSpostabili != null) {
					for (Animale animale : wvAnimaliSpostabili) {
						IMossa wvMossa = new Movimento(animale.getId(), animale
								.getRegioneOccuapta().getId(), stradaOccupata
								.getRegioneOpposta(regione).getId(),
								new Integer(0));
						if (wvMossa != null) {
							this.atMossePossibili.add(wvMossa);
						}
					}

				}
			}
		}
	}

	/**
	 * Controlla se un pastore può abbattere degli animali in base al
	 * regolamento e nel caso precarica le possibili model.mosse
	 * 
	 * @param stradaOccupata
	 *            la strada dove si trova il pastore
	 */
	private void precaricaMosseAbbattimento(Strada stradaOccupata) {
		int denaroNecessario = 0;
		ArrayList<Integer> wvIdPastori = new ArrayList<Integer>();
		for (Strada stradaVicina : stradaOccupata.getStradeCollegate()) {
			if (stradaVicina.getOccupante() instanceof PedinaPastore) {
				boolean isMia = false;
				for (PedinaPastore pedinaPastore : this.getGiocatore()
						.getPedinePastore()) {
					if (pedinaPastore.getId().equals(
							stradaVicina.getOccupante().getId())) {
						isMia = true;
					}
				}
				if (isMia) {
					continue;
				}
				denaroNecessario += 2;
				wvIdPastori.add(stradaVicina.getOccupante().getId());
			}
		}
		if ((this.getMappa().getGiocatore(this.atIdGiocatore)
				.getDanaroResiduo() - denaroNecessario) >= 0) {
			// se sono entrato qui singnifica che ho denaro a sufficienza
			for (Regione regione : stradaOccupata.getRegioniLimitrofe()) {
				for (Animale animale : regione.getAnimaliAbbattibili()) {
					IMossa wvMossa = new Abbattimento(stradaOccupata
							.getOccupante().getId(), regione.getId(), animale,
							wvIdPastori, stradaOccupata.getNumeroStrada());
					if (wvMossa != null) {
						this.atMossePossibili.add(wvMossa);
					}
				}
			}
		}

	}

	/**
	 * Prepara le model.mosse investimento nel caso fosse possibile farle
	 * 
	 * @param stradaOccupata
	 *            la strada occupata dal pastore
	 */
	private void precaricaMosseInvestimento(Strada stradaOccupata) {
		for (Regione regione : stradaOccupata.getRegioniLimitrofe()) {
			if (regione.getTipoTerreno().equals(ETipoTerreno.SHEEPSBURG)) {
				continue;
			}
			if ((this.getMappa().getTerreniVenduti(regione.getTipoTerreno()) < 6)
					&& (this.getGiocatore().getDanaroResiduo() >= this
							.getMappa().getTerreniVenduti(
									regione.getTipoTerreno()))) {
				IMossa wvMossa = new Investimento(this.getGiocatore().getId(),
						regione.getTipoTerreno(), this.getMappa()
								.getTerreniVenduti(regione.getTipoTerreno()));
				if (wvMossa != null) {
					this.atMossePossibili.add(wvMossa);
				}
			}
		}
	}

	/**
	 * Precarica in un array le possibili model.mosse di accoppiamento rispetto
	 * a dove si trova il pastore
	 * 
	 * @param RegioniLimitrofe
	 *            le regioni limitrofe alle strade dove si trova il pastore
	 */
	private void precaricaMosseAccoppiamento(Regione[] RegioniLimitrofe) {
		for (Regione regione : RegioniLimitrofe) {
			if (regione.occupataDaPecoraNonNera()
					&& regione.occupataDaMontoni()) {
				IMossa wvMossa = new Accoppiamento(regione.getId());
				if (wvMossa != null) {
					this.atMossePossibili.add(wvMossa);
				}
			}
		}
	}

	/**
	 * Permette di capire se bisogna iniziare la partita o aspettare la fase di
	 * posizionamento
	 * 
	 * @return true o false
	 * @throws DisconnessioneException
	 */
	public boolean isInizioPartita() throws DisconnessioneException {
		return this.atConnessione.getInizioPartita();
	}

	/**
	 * Aspetta una mossa di posizionamento dal server. se arriva un messaggio
	 * che mi fa capire che è il mio turno, scatena un'eccezione
	 * 
	 * @return la mossa di tipo posizionamento
	 * @throws TurnoPersonaleException
	 *             un eccezione che fa capire che il turno è il nostro
	 * @throws InizioPartitaException
	 * @throws DisconnessioneException
	 */
	public Posizionamento getMossaPosizionamento()
			throws TurnoPersonaleException, InizioPartitaException,
			DisconnessioneException {
		return this.atConnessione.getMossaPedina();
	}

	/**
	 * Invia una mossa di tipo {@link Posizionamento}
	 * 
	 * @param idGiocatore
	 * @param idPedina
	 * @param idStrada
	 * @throws DisconnessioneException
	 */
	public void inviaMossaPosizionamento(Integer idGiocatore, Integer idPedina,
			Integer idStrada) throws DisconnessioneException {
		this.atConnessione.inviaPosizionamento(idGiocatore, idPedina, idStrada);
		this.posizionaPedinaPastore(idGiocatore, idPedina, idStrada);
	}

	/**
	 * Aggiorna la posizione della pedina sia per le pedine che per le strade
	 * 
	 * @param mossaPosizionamento
	 *            la mossa di tipo {@link Posizionamento}
	 */
	public void posizionaPedinaPastore(Posizionamento mossaPosizionamento) {
		Giocatore giocatore = this.getMappa().getGiocatore(
				mossaPosizionamento.getIdGiocatore());
		Strada wvStradaOccupataStrada = this.getMappa().getStrada(
				mossaPosizionamento.getIdStrada());
		PedinaPastore wvPedina = giocatore.aggiornaPosizionePedina(
				mossaPosizionamento.getIdPedina(), wvStradaOccupataStrada);
		this.getMappa().posizionaPedina(wvPedina, wvStradaOccupataStrada);

	}

	/**
	 * Invia una mossa attraverso connessione passandola per parametro
	 * 
	 * @param MossaLetta
	 * @throws DisconnessioneException
	 */
	public void inviaMossa(IMossa MossaLetta) throws DisconnessioneException {
		this.atConnessione.inviaMossa(MossaLetta);
	}

	/**
	 * ritorna se il turno è finito o no
	 * 
	 * @return
	 * @throws DisconnessioneException
	 */
	public boolean isFineTurno() throws DisconnessioneException {
		return this.atConnessione.isFineTurno();
	}

	/**
	 * Ritorna una mossa di tipo abbattuto
	 * 
	 * @return
	 * @throws DisconnessioneException
	 */
	public IMossa getAbbattuto() throws DisconnessioneException {
		return this.atConnessione.getAbbattuto();
	}

	/**
	 * Ritorna se è possibile effettuare un abbattimento trovandosi nella sata
	 * strada
	 * 
	 * @param numeroStradaOccupata
	 * @return
	 */
	public boolean permettiAbbattimento(Integer numeroStradaOccupata) {
		Integer numero = this.lanciaDado();
		if (numeroStradaOccupata.equals(numero)) {
			CLCHelper.stampa("Numero dado: " + numero + " Strada: "
					+ numeroStradaOccupata + " true");
			return true;
		} else {
			CLCHelper.stampa("Numero dado: " + numero + " Strada: "
					+ numeroStradaOccupata + " false");
			return false;
		}

	}

	/**
	 * aggiorna la mappa di gioco se è necessario
	 */
	public void aggiornaMappaSeNecessario() {
		try {
			this.atConnessione.aggiornaMappa();
		} catch (MappaDaAggiornareException e) {
			this.setMappa(e.getMappa());
		}
	}

}
