package controllerClient;

/**
 * Eccezzione lanciata quando il client si sconnette dal server
 */
public class DisconnessioneException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7782427357595838898L;

	/**
	 * 
	 */
	public DisconnessioneException() {
		super("Ti sei DISCONNESSO, riavvia per riconnetterti");

	}

}
