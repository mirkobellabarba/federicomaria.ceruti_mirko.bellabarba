package controllerClient;

import model.Mappa;
import model.mosse.IComunicazione;
import model.mosse.IMossa;
import model.mosse.Messaggio;
import model.mosse.Posizionamento;
import view.CLCHelper;
import controllerClient.ConnessioneRMI.MappaDaAggiornareException;
import controllerCommon.ETipoConnessione;

/**
 * classe astratta per la connessione rmi/socket
 * 
 * @author Mirko
 * 
 */
public abstract class Connessione {
	/**
	 * Determina il tipo di connessione da richiamare
	 * 
	 * @param tipoConnessione
	 *            il tipo di connessione desidertao
	 * @return connessione RMI o connessione Socket
	 */
	public static Connessione instanzia(ETipoConnessione tipoConnessione) {
		switch (tipoConnessione) {
		case RMI:
			CLCHelper.stampa("Mi connetto in RMI");
			return new ConnessioneRMI();
		case SOCKET:
			CLCHelper.stampa("Mi connetto in Socket");
			return new ConnessioneSocket();
		default:
			return null;
		}
	}

	/**
	 * Prende i dati dal file socketInfo.txt, e crea la socket con questi. Poi
	 * manda subito il segnale di pronto al server con i relativi dati secondo
	 * la logica
	 * 
	 * Per comuicare poi basta usare i metodi get presenti nella classe.
	 * 
	 * @return un boolean che è true se tutto è andato bene, false se c'è stato
	 *         qualche errore
	 */
	public abstract Boolean connetti(String nomeGiocatore)
			throws DisconnessioneException;

	/**
	 * Controlla il flusso di informazioni in entrata finchè non riceve l'ide
	 * della partita
	 * 
	 * @return un intero rappresentante l'id della partita
	 */
	public abstract Integer getIdPartita() throws DisconnessioneException;

	/**
	 * Si mette in ascolto sul canale di ingresso finchè non arriva l'id del
	 * giocatore
	 * 
	 * @return un intero rappresentatnte l'id del giocatore
	 */
	public abstract Integer getIdGiocatore() throws DisconnessioneException;

	/**
	 * Si mette in ascolto sul server finchè non arriva un oggetto serializzato
	 * di tipo Mappa
	 * 
	 * @return la Mappa di gioco
	 */
	public abstract Mappa getMappa() throws DisconnessioneException;

	/**
	 * Invia una mossa di tipo spostamento
	 * 
	 * @param idSoggetto
	 *            {@link Iterable} che rappresenta l'id di chi si deve spostare
	 * @param idVecchiaPosizione
	 *            {@link Integer} che rappresenta l'id della posizione di
	 *            partenza
	 * @param idNuovaPosizione
	 *            {@link Integer} che rappresenta l'id dell aposizione in cui
	 *            spostare il soggetto
	 * @param costoMossa
	 *            {@link Integer} che rappresenta il costo dela mossa
	 */
	public abstract void inviaSpostamento(Integer idSoggetto,
			Integer idVecchiaPosizione, Integer idNuovaPosizione,
			Integer costoMossa);

	/**
	 * Il client invia al server la posizione iniziale che ha deciso
	 * 
	 * @param idPastore
	 *            l'id del pastore
	 * @param posizioneScelta
	 *            la posizione scelta che rappresenta l'id della strada
	 */
	public abstract void inviaPosizioneIniziale(Integer idPastore,
			Integer posizioneScelta) throws DisconnessioneException;

	/**
	 * Riceve una tessera terreno dalla rete
	 * 
	 * @return una stringa per poter ricavare il tipo enumerativo corrispondente
	 *         tramite valueOf()
	 */
	public abstract String getTesseraTerreno() throws DisconnessioneException;

	/**
	 * Rimane in ascolto delle model.mosse degli altri giocatori comunicandole
	 * ai livelli superiori. Ritorna un'eccezione {@link FineTurnoException} nel
	 * caso in cui sia finito il turno di uno dei giocatori
	 */
	public abstract IMossa getMosseAltriGiocatori() throws FineTurnoException,
			DisconnessioneException;

	/**
	 * Rimane in ascolto del server, comunicando le model.mosse che arrivano
	 * oppure scatenando l'eccezione {@link InizioTurnoException} nel caso in
	 * cui arrivi una comunicazione di inizio turno
	 * 
	 * @throws FinePartitaException
	 *             , InizioPartitaException, NumberFormatException
	 * @throws FaseMercatoException
	 */
	public abstract IMossa getMossePreliminari() throws NumberFormatException,
			InizioTurnoException, FinePartitaException, FaseMercatoException,
			DisconnessioneException;

	/**
	 * Permette di inviare una mossa generica
	 * 
	 * @param mossaLetta
	 *            la mossa letta dal client
	 * @throws FineTurnoException
	 */
	public abstract void inviaMossa(IMossa mossaLetta)
			throws DisconnessioneException;

	/**
	 * Permette di inviare una mossa di posizionamento
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 * @param idPedina
	 *            l'id della pedina
	 * @param idStrada
	 *            l'id della strada
	 * @return la mossa Posizionamento costruita
	 */
	public abstract Posizionamento inviaPosizionamento(Integer idGiocatore,
			Integer idPedina, Integer idStrada) throws DisconnessioneException;

	/**
	 * Rimane in ascolto del server, se il server richiede il posizionamento di
	 * una pedina restituisce un eccezione {@link TurnoPersonaleException} per
	 * far capire al client che è il suo turno, altimenti comunica il
	 * posizionamento fatto da un avversario
	 * 
	 * @return un posizonamento di una pedina avversaria
	 * 
	 * @throws TurnoPersonaleException
	 *             se è il proprio turno
	 * @throws InizioPartitaException
	 */
	public abstract Posizionamento getMossaPedina()
			throws TurnoPersonaleException, InizioPartitaException,
			DisconnessioneException;

	/**
	 * Determina se il messaggio che arriva è un messaggio di avvio partita
	 * 
	 * @return un booleano true o false
	 */
	public abstract boolean getInizioPartita() throws DisconnessioneException;

	/**
	 * Aspetta un ack dal server
	 * 
	 * @return
	 */
	public abstract boolean getACK() throws FineTurnoException,
			DisconnessioneException;

	/**
	 * Invia un ack al server
	 */
	public abstract void inviaACK() throws DisconnessioneException;

	/**
	 * Ritorna un valore che indica se è la fine del turno
	 * 
	 * @return
	 * @throws DisconnessioneException
	 */
	public abstract boolean isFineTurno() throws DisconnessioneException;

	public class TurnoPersonaleException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -8504696165859783020L;

		/**
		 * Costruttore
		 */
		public TurnoPersonaleException() {
			super();
		}
	}

	/**
	 * Questa eccezione viene scatenata quando arriva un {@link IComunicazione}
	 * di tipo {@link Messaggio} che ci comunica un _INIZIOPARTITA
	 * 
	 * @author Mirko
	 * 
	 */
	public class InizioPartitaException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -8342315075731305527L;

		/**
		 * Costruttore
		 */
		public InizioPartitaException() {
			super();
		}
	}

	/**
	 * Questa eccezione viene scatenata mentre il server è in ascolto di
	 * model.mosse. Nel caso arrivi un messaggio che abbia dentro l'id del
	 * giocatore a cui tocca, scateno questa eccezione passando l'id del
	 * giocatore
	 * 
	 * @author Mirko
	 * 
	 */
	public class InizioTurnoException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 9139993423551212003L;
		private final Integer atIdGiocatore;

		/**
		 * Costruttore
		 * 
		 * @param idGiocatore
		 *            l'id del giocatore a cui tocca
		 */
		public InizioTurnoException(Integer idGiocatore) {
			super();
			this.atIdGiocatore = idGiocatore;
		}

		/**
		 * @return Restituisce l'id del giocatore
		 */
		public Integer getIdGiocatore() {
			return this.atIdGiocatore;
		}
	}

	/**
	 * Questa eccezione viene scatenata quando, ricevendo {@link IMossa}, mi
	 * arriva un {@link Messaggio} contenente un fine turno
	 * 
	 * @author Mirko
	 * 
	 */
	public class FineTurnoException extends Exception {

		private static final long serialVersionUID = 7442523314819031673L;

		/**
		 * Costruttore
		 */
		public FineTurnoException() {
			super();
		}
	}

	/**
	 * Questa eccezione viene scatenata se arriva un messaggio di fine partita
	 * 
	 * @author Mirko
	 * 
	 */
	public class FinePartitaException extends Exception {

		private static final long serialVersionUID = 780981702290661220L;

		/**
		 * Costruttore
		 */
		public FinePartitaException() {
			super();
		}
	}

	public class FaseMercatoException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2385487065712573012L;
		private final Integer atIdGicatore;
		private final Integer atFase;

		/**
		 * Costruttore
		 * 
		 * @param idGioctore
		 *            {@link Integer} che raprpesenta l'id del gioatore di cui è
		 *            il turno nel mercato
		 * @param fase
		 *            {@link Integer} che rappresenta la fase del mercato
		 */
		public FaseMercatoException(Integer idGioctore, Integer fase) {
			super();
			this.atIdGicatore = idGioctore;
			this.atFase = fase;
		}

		/**
		 * Ritorna l'id del giocatore
		 * 
		 * @return {@link Integer} che rappresenta l'id del giocatore
		 */
		public Integer getIdGiocatore() {
			return this.atIdGicatore;
		}

		/**
		 * Ritorna la fase del mercato
		 * 
		 * @return Ritorna un {@link Integer} che rappresenta la fase del
		 *         mercato <b>1</b> per la fase di offerta <b>2</b> per la fase
		 *         di aquisto
		 */
		public Integer getFase() {
			return this.atFase;
		}

	}

	/**
	 * Ritorna una mossa dal server di tipo abbattuto
	 * 
	 * @return
	 * @throws DisconnessioneException
	 */
	public abstract IMossa getAbbattuto() throws DisconnessioneException;

	/**
	 * Richiede al server la posizione della pecora nera
	 * 
	 * @return
	 */
	public abstract Integer getRegioneOccupataPecoraNera();

	/**
	 * Aggiorna la mappa
	 * 
	 * @throws MappaDaAggiornareException
	 */
	public abstract void aggiornaMappa() throws MappaDaAggiornareException;

	/**
	 * richiede al server la posizione del lupo
	 * 
	 * @return
	 */
	public abstract Integer getRegioneOccupataLupo();
}
