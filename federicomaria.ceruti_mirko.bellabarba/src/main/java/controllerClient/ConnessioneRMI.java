package controllerClient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Mappa;
import model.mosse.Abbattimento;
import model.mosse.Accoppiamento;
import model.mosse.EIntestazioneMessaggio;
import model.mosse.IComunicazione;
import model.mosse.IMossa;
import model.mosse.Investimento;
import model.mosse.Messaggio;
import model.mosse.Movimento;
import model.mosse.MovimentoLupo;
import model.mosse.Posizionamento;
import model.mosse.finePreliminari;
import model.mosse.tentativoFallito;
import view.CLCHelper;
import view.Start;
import controllerServer.IServerRMI;
import controllerServer.IstanteUltimoPing;

public class ConnessioneRMI extends Connessione {

	private IServerRMI atServerRMI;
	private Integer atIdPartita;
	private Integer atIdGiocatore;
	private Pinger atPinger;

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#connetti(java.lang.String)
	 */
	public Boolean connetti(String nomeGiocatore) {

		try {
			// Ottengo il riferimento al registry
			Registry registry = LocateRegistry.getRegistry("localhost",
					IServerRMI.AT_PORTA_SERVER_RMI);

			// Ricerco nel registry il server a cui devo puntare e ottengo lo
			// stub
			this.atServerRMI = (IServerRMI) registry
					.lookup(IServerRMI.AT_SERVER_NAME);

			ArrayList<Integer> wvId = this.atServerRMI
					.collegaPartita(nomeGiocatore);
			this.atIdPartita = wvId.get(0);
			this.atIdGiocatore = wvId.get(1);
			this.atPinger = new Pinger(this.atServerRMI, this.atIdPartita,
					this.atIdGiocatore);
			this.atPinger.start();
			while (!this.atServerRMI.avviaPartita(this.atIdPartita)) {
				this.aspetta();
			}
			this.atServerRMI.preparaPartita(this.atIdPartita);
		} catch (RemoteException ex) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), ex);
			}
			return false;
		} catch (NotBoundException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return false;
		}
		return true;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getIdPartita()
	 */
	public Integer getIdPartita() {
		return this.atIdPartita;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getIdGiocatore()
	 */
	public Integer getIdGiocatore() {
		return this.atIdGiocatore;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getMappa()
	 */
	public Mappa getMappa() {
		try {
			CLCHelper.stampa("Sto aspettando la mappa");
			Mappa wvMappa = null;
			do {
				wvMappa = this.atServerRMI.ottieniMappa(this.atIdPartita);
				this.aspetta();
			} while (wvMappa == null);
			CLCHelper.stampa("Ricevuta la mappa");
			return wvMappa;
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}

			return null;
		}
	}

	private void aspetta() {
		try {
			Thread.sleep(2);
		} catch (InterruptedException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#inviaSpostamento(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	public void inviaSpostamento(Integer idSoggetto,
			Integer idVecchiaPosizione, Integer idNuovaPosizione,
			Integer costoMossa) {

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * controllerClient.Connessione#inviaPosizioneIniziale(java.lang.Integer,
	 * java.lang.Integer)
	 */
	public void inviaPosizioneIniziale(Integer idPastore,
			Integer posizioneScelta) {

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getTesseraTerreno()
	 */
	public String getTesseraTerreno() {
		return null;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getMosseAltriGiocatori()
	 */
	public IMossa getMosseAltriGiocatori() throws FineTurnoException {
		IComunicazione wvComunicazione;
		try {
			do {

				wvComunicazione = this.atServerRMI.riceviComunicazione(
						this.atIdPartita, this.atIdGiocatore);
				try {
					Thread.sleep(4);
				} catch (InterruptedException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
				}
			} while (wvComunicazione == null);

			if (wvComunicazione instanceof IMossa) {
				return (IMossa) wvComunicazione;
			} else if (wvComunicazione instanceof Messaggio) {
				Messaggio messaggio = (Messaggio) wvComunicazione;
				if (messaggio.getIntestazione().equals(
						EIntestazioneMessaggio._FINETURNO)) {
					throw new FineTurnoException();
				}
			}

		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return null;
		}
		return null;
	}

	private Integer atPrimoGiocatore;
	private ArrayList<IComunicazione> comunicazioni = new ArrayList<IComunicazione>();
	private boolean atGiaCalcolate = false;
	private Integer atIndiceAttuale = 0, atIndiceMax;

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getMossePreliminari()
	 */
	public IMossa getMossePreliminari() throws NumberFormatException,
			InizioTurnoException, FinePartitaException {
		try {
			// ricevo il numero di chi tocca nel turno e lo salvo per dopo
			this.atPrimoGiocatore = this.atServerRMI.mioTurno(this.atIdPartita,
					this.atIdGiocatore) ? this.atIdGiocatore : -1;

			if (this.atPrimoGiocatore.equals(this.atIdGiocatore)) {
				if (!this.atGiaCalcolate) {
					// Se non ho calcolato le model.mosse e tocca a me calcolo
					// le model.mosse preliminari
					this.popolaMossePreliminari();
				} else {
					this.atServerRMI.fineMossePreliminari(this.atIdPartita,
							this.atIdGiocatore);
					IComunicazione comunicazione = this.atServerRMI
							.riceviComunicazione(this.atIdPartita,
									this.atIdGiocatore);
					if (comunicazione instanceof Messaggio) {
						Messaggio messaggio = (Messaggio) comunicazione;
						if (messaggio.getIntestazione().equals(
								EIntestazioneMessaggio._TURNOGIOCATORE)) {
							if (!messaggio.getParametriMessaggio()[0]
									.equals(this.atIdGiocatore.toString())) {
								throw new InizioTurnoException(
										Integer.parseInt(messaggio
												.getParametriMessaggio()[0]));
							}
						} else if (messaggio.getIntestazione().equals(
								EIntestazioneMessaggio._USCITA)) {
							throw new FinePartitaException();
						}
					}
				}

				while (this.atIndiceAttuale < this.atIndiceMax) {
					if (this.comunicazioni.get(this.atIndiceAttuale) instanceof IMossa) {
						IMossa mossa = (IMossa) this.comunicazioni
								.get(this.atIndiceAttuale);
						if (mossa instanceof Messaggio) {
							if (((Messaggio) mossa).getIntestazione().equals(
									EIntestazioneMessaggio._USCITA)) {
								throw new FinePartitaException();
							}
						}
						this.atIndiceAttuale++;
						return mossa;
					}
				}

				throw new InizioTurnoException(this.atPrimoGiocatore);
			} else {
				// controllare quando ci sono model.mosse preliminari
				// disponibili, poi ritorno l'id
				try {
					IComunicazione wvComunicazione;
					this.azzeraParametri();

					do {
						wvComunicazione = this.atServerRMI.riceviComunicazione(
								this.atIdPartita, this.atIdGiocatore);
						Thread.sleep(10);
					} while (wvComunicazione == null);
					CLCHelper.stampa(wvComunicazione.toString());
					// Ho ricevuto le prime cose
					if (wvComunicazione instanceof finePreliminari) {
						CLCHelper.stampa("Fine preliminari");
						throw new InizioTurnoException(
								((finePreliminari) wvComunicazione)
										.getIdGiocatore());
					}
					if (wvComunicazione instanceof IMossa) {
						CLCHelper.stampa("Ricevuto: "
								+ wvComunicazione.getClass().toString());
						return (IMossa) wvComunicazione;
					} else if (wvComunicazione instanceof Messaggio) {
						Messaggio messaggio = (Messaggio) wvComunicazione;
						if (messaggio.getIntestazione().equals(
								EIntestazioneMessaggio._TURNOGIOCATORE)) {
							CLCHelper.stampa("Basta model.mosse preliminari");
							throw new InizioTurnoException(
									Integer.parseInt(messaggio
											.getParametriMessaggio()[0]));
						} else if (messaggio.getIntestazione().equals(
								EIntestazioneMessaggio._USCITA)) {
							throw new FinePartitaException();
						}
					}

				} catch (RemoteException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
					return null;
				} catch (InterruptedException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
				}
			}
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
		return null;
	}

	/**
	 * Azzera i parametri che servono per le model.mosse preliminari
	 */
	private void azzeraParametri() {
		this.atGiaCalcolate = false;
		this.atIndiceAttuale = 0;
		this.atIndiceMax = 0;
		this.comunicazioni = new ArrayList<IComunicazione>();
	}

	private void popolaMossePreliminari() {

		// Uno dei due client va in errore perchè va a leggere le model.mosse
		// preliminari quando ancora non è il momento giusto
		// io farei un boolean da settare dal client sul server che indica se si
		// può o no leggere

		try {
			// atServerRMI.mercato

			// movimento lupo
			this.atServerRMI.muoviLupo(this.atIdPartita);
			// Il movimento del lupo, se si muove
			IComunicazione mossaLupo = this.atServerRMI.riceviComunicazione(
					this.atIdPartita, this.atIdGiocatore);
			if (mossaLupo instanceof MovimentoLupo) {
				this.comunicazioni.add(mossaLupo);
			}

			// svezzamento
			while (this.atServerRMI.svezzaAgnello(this.atIdPartita)) {
				// l'agnello svezzato de arriva
				this.comunicazioni.add(this.atServerRMI.riceviComunicazione(
						this.atIdPartita, this.atIdGiocatore));
			}

			// muovo la pecora nera
			this.atServerRMI.muoviPecoraNera(this.atIdPartita);
			IComunicazione mossaPecora = this.atServerRMI.riceviComunicazione(
					this.atIdPartita, this.atIdGiocatore);
			if (mossaPecora instanceof Movimento) {
				if (((Movimento) mossaPecora).getAtIdSoggetto().equals(
						new Integer(1003))) {
					// il movimento della pecora nera
					CLCHelper
							.stampa("E' errivata effettivamente la mossa di una pecora nera");
					this.comunicazioni.add(mossaPecora);
				}
			}

			this.atServerRMI.fineMossePreliminari(this.atIdPartita,
					this.atPrimoGiocatore);
			this.comunicazioni.add(this.atServerRMI.riceviComunicazione(
					this.atIdPartita, this.atIdGiocatore));

			// atServerRMI.calcolaProssimoGiocatore(at_id_partita,
			// at_id_giocatore);
			// atServerRMI.riceviComunicazione(at_id_partita, at_id_giocatore);

			this.atIndiceMax = this.comunicazioni.size();
			this.atGiaCalcolate = true;

		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#inviaMossa(model.mosse.IMossa)
	 */
	public void inviaMossa(IMossa mossaLetta) {
		if (mossaLetta != null) {
			try {
				if (mossaLetta instanceof Investimento) {
					this.atServerRMI.investi(this.atIdPartita,
							(Investimento) mossaLetta);
				} else if (mossaLetta instanceof Movimento) {
					Movimento movimento = (Movimento) mossaLetta;
					if (movimento.isAnimale()) {
						this.atServerRMI.spostaAnimale(this.atIdPartita,
								this.atIdGiocatore,
								movimento.getAtIdSoggetto(),
								movimento.getAtIdNuovaPosizione());
					} else {
						this.atServerRMI.muoviPedina(this.atIdPartita,
								this.atIdGiocatore,
								movimento.getAtIdSoggetto(),
								movimento.getAtIdNuovaPosizione(),
								movimento.getAtCostoMossa());
					}
				} else if (mossaLetta instanceof Abbattimento) {
					Abbattimento abbattimento = (Abbattimento) mossaLetta;
					this.atServerRMI.abbattimento(this.atIdPartita,
							this.atIdGiocatore, abbattimento.getIdRegione(),
							abbattimento.getIdAnimale(),
							abbattimento.getIdGiocatoriVicini());
					CLCHelper.stampa("Aspetto esito dal server");
				} else if (mossaLetta instanceof tentativoFallito) {
					CLCHelper
							.stampa("Sfortunato! non sei riuscito ad abbattere animale");
				} else if (mossaLetta instanceof Accoppiamento) {
					Accoppiamento accoppiamento = (Accoppiamento) mossaLetta;
					this.atServerRMI.accoppiamento(this.atIdPartita,
							this.atIdGiocatore, accoppiamento.getIdRegione());
				} else {
					// Fare vendita
					CLCHelper
							.stampa("NON ANCORA IMPLEMENTATO: Partita_CLient.InterpretaMossa:"
									+ mossaLetta.getClass().toString());
				}

				this.atServerRMI.calcolaProssimoGiocatore(this.atIdPartita,
						this.atIdGiocatore);
			} catch (RemoteException ex) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), ex);
				}
			}
		}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#inviaPosizionamento(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer)
	 */
	public Posizionamento inviaPosizionamento(Integer idGiocatore,
			Integer idPedina, Integer idStrada) {
		Posizionamento posizionamento = new Posizionamento(idGiocatore,
				idPedina, idStrada);
		try {
			this.atServerRMI.impostaPedina(this.atIdPartita, idGiocatore,
					idPedina, idStrada);
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return null;

		}
		return posizionamento;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getMossaPedina()
	 */
	public Posizionamento getMossaPedina() throws TurnoPersonaleException,
			InizioPartitaException {
		try {

			Integer turnoPedina = this.atServerRMI.turnoImpostaPedina(
					this.atIdPartita, this.atIdGiocatore);
			if (turnoPedina.equals(new Integer(1))) {
				throw new TurnoPersonaleException();
			} else if (turnoPedina.equals(new Integer(0))) {
				IComunicazione mossa;
				do {
					this.aspetta();
					mossa = this.atServerRMI.riceviComunicazione(
							this.atIdPartita, this.atIdGiocatore);
				} while (mossa == null);
				if (mossa instanceof Posizionamento) {
					return (Posizionamento) mossa;
				} else if (mossa instanceof Messaggio) {
					Messaggio messaggio = (Messaggio) mossa;
					int idRicevuto = Integer.parseInt(messaggio
							.getParametriMessaggio()[0]);
					if (idRicevuto == this.atIdGiocatore) {
						throw new TurnoPersonaleException();
					}
				}
			} else if (turnoPedina.equals(new Integer(2))) {
				throw new InizioPartitaException();
			}
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return null;
		}
		return null;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getInizioPartita()
	 */
	public boolean getInizioPartita() {
		return false;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getACK()
	 */
	public boolean getACK() throws FineTurnoException {
		return false;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#inviaACK()
	 */
	public void inviaACK() {

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#isFineTurno()
	 */
	public boolean isFineTurno() {
		return false;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see controllerClient.Connessione#getAbbattuto()
	 */
	public IMossa getAbbattuto() {
		return null;
	}

	/**
	 * Ottiene dal server rmi la posizione della pecora nera
	 */
	@Override
	public Integer getRegioneOccupataPecoraNera() {
		try {
			return this.atServerRMI.posizionePecoraNera(this.atIdPartita);
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
		return null;
	}

	@Override
	public void aggiornaMappa() throws MappaDaAggiornareException {
		try {
			Mappa mappa = this.atServerRMI.ottieniMappa(this.atIdPartita);
			throw new MappaDaAggiornareException(mappa);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Lancia un eccezione e passa la mappa aggiornata ai livelli superiori
	 * 
	 * @author Mirko
	 * 
	 */
	public class MappaDaAggiornareException extends Exception {
		private final Mappa atMappa;

		/**
		 * Costruttore
		 * 
		 * @param mappaAggiornata
		 */
		public MappaDaAggiornareException(Mappa mappaAggiornata) {
			this.atMappa = mappaAggiornata;
		}

		/**
		 * Ritorna la mappa aggiornata
		 */
		public Mappa getMappa() {
			return this.atMappa;
		}
	}

	@Override
	public Integer getRegioneOccupataLupo() {
		try {
			return this.atServerRMI.posizioneLupo(this.atIdPartita);
		} catch (RemoteException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
		return null;
	}

}

class Pinger extends Thread {

	private final IstanteUltimoPing atIstanteUltimoPing;
	private final IServerRMI atIServerRMI;
	private final AtomicBoolean atInviaPing;
	private final Integer atIdPartita;
	private final Integer atIdGiocatore;

	/**
	 * Costruttore
	 * 
	 * @param serverRMI
	 *            Oggetto che implemente l'interfaccia {@link IServerRMI} e che
	 *            rappresente il collegamento al server RMI
	 * @param idPartita
	 *            {@link Integer} che rappresenta l'id della partita
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del giocatore
	 */
	public Pinger(IServerRMI serverRMI, Integer idPartita, Integer idGiocatore) {
		this.atIstanteUltimoPing = new IstanteUltimoPing();
		this.atIServerRMI = serverRMI;
		this.atIdPartita = idPartita;
		this.atIdGiocatore = idGiocatore;
		this.atInviaPing = new AtomicBoolean(true);
	}

	/**
	 * Ritorna un valore che indica se bisogna mandare il pin oppure no
	 * 
	 * @return {@link AtomicBoolean} che indica se bisogna inviare il ping
	 */
	public AtomicBoolean getInviaPing() {
		return this.atInviaPing;
	}

	/**
	 * Metodo run() richiamato da start()
	 */
	@Override
	public void run() {
		if (Start.abilitaTest) {
			return;
		}
		while (this.atInviaPing.get()) {
			this.atIstanteUltimoPing.impostaIstanteAttuale();
			try {

				this.atIServerRMI.inviaPing(this.atIdPartita,
						this.atIdGiocatore, this.atIstanteUltimoPing);
				Thread.sleep(5000);
			} catch (RemoteException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
				CLCHelper.stampa("Ti sei sollegato");
			} catch (InterruptedException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
		}

	}
}
