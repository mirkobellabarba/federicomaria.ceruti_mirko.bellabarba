package controllerClient;

import helper.FileIOHelper;
import helper.FileIOHelper.FileNonTrovatoException;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Mappa;
import model.mosse.Abbattuto;
import model.mosse.EIntestazioneMessaggio;
import model.mosse.IComunicazione;
import model.mosse.IMossa;
import model.mosse.Messaggio;
import model.mosse.MossaVuota;
import model.mosse.Movimento;
import model.mosse.Posizionamento;
import view.CLCHelper;
import view.Start;
import controllerClient.ConnessioneRMI.MappaDaAggiornareException;

/**
 * Gestisce la connessione tra client e server in socket
 * 
 * @author Mirko
 * 
 */
public class ConnessioneSocket extends Connessione {
	private Socket atSocket;
	private String atIndirizzo;
	private Integer atPorta;
	private InputStream inServer;
	private OutputStream outServer;

	/**
	 * Costruttore della classe ConnessioneClient.<br>
	 * In seguiro richiamare il metodo Connetti()
	 * 
	 * @param Tipo_Connessione
	 */
	public ConnessioneSocket() {

	}

	/**
	 * Prende i dati dal file socketInfo.txt, e crea la socket con questi. Poi
	 * manda subito il segnale di pronto al server con i relativi dati secondo
	 * la logica
	 * 
	 * Per comuicare poi basta usare i metodi get presenti nella classe.
	 * 
	 * @return un boolean che è true se tutto è andato bene, false se c'è stato
	 *         qualche errore
	 */
	@Override
	public Boolean connetti(String nomeGiocatore) {
		try {
			this.recuperaDatiSocket();
		} catch (FileNonTrovatoException e) {
			if (Start.abilitaLogger) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
			return false;
		}
		// provo a connettermi alla socket
		try {
			this.atSocket = new Socket(this.atIndirizzo, this.atPorta);
			if (!Start.abilitaTest) {
				System.out.println("CLIENT: Server Connesso");
			}
			this.inServer = this.atSocket.getInputStream();
			this.outServer = this.atSocket.getOutputStream();
			if (!Start.abilitaTest) {
				System.out.println("CLIENT: Assegnati in/out");
			}
			// Invio che sono pronto e i miei dati
			this.InviaSegnaleReady(nomeGiocatore);
		} catch (UnknownHostException e) {
			if (Start.abilitaLogger) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
			return false;
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
			return false;
		}
		return true;

	}

	/**
	 * Chiude la socket e gli stream in entrara ed uscita
	 */
	public void chiudiSocket() {
		try {
			// Invio messaggio di uscita al server
			Messaggio uscitaComunicazione = new Messaggio();
			uscitaComunicazione.uscita();
			this.inviaComunicazioneSocket(uscitaComunicazione);

			// Ricevo risposta
			// Messaggio rispostaMessaggio = (Messaggio) riceviComunicazione();

			this.inServer.close();
			this.outServer.close();
			this.atSocket.close();

			// La partita è finita, posso cancellare il file dei dati partita
			FileIOHelper fileDaCancellare = new FileIOHelper("datiPartita.txt");
			fileDaCancellare.deleteFile();
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}

	}

	/**
	 * Controlla il flusso di informazioni in entrata finchè non riceve l'ide
	 * della partita
	 * 
	 * @return un intero rappresentante l'id della partita
	 */
	@Override
	public Integer getIdPartita() {
		IComunicazione comunicazione;
		Messaggio messaggio;
		CLCHelper.stampa("GETIDPARTITA: aspetto comunicazione...");

		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().split("\\:")[0]
					.equals(EIntestazioneMessaggio._IDPARTITA.toString())) {
				this.inviaACK();
				return Integer
						.parseInt(messaggio.getMessaggio().split("\\:")[1]);
			}
		}

		return null;
	}

	/**
	 * Si mette in ascolto sul canale di ingresso finchè non arriva l'id del
	 * giocatore
	 * 
	 * @return un intero rappresentatnte l'id del giocatore
	 */
	@Override
	public Integer getIdGiocatore() {
		IComunicazione comunicazione;
		Messaggio messaggio;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().split("\\:")[0]
					.equals(EIntestazioneMessaggio._IDGIOCATORE.toString())) {
				this.inviaACK();
				return Integer
						.parseInt(messaggio.getMessaggio().split("\\:")[1]);
			}
		}
		return null;

	}

	/**
	 * Si mette in ascolto sul server finchè non arriva un oggetto serializzato
	 * di tipo Mappa
	 * 
	 * @return la Mappa di gioco
	 */
	@Override
	public Mappa getMappa() {
		IComunicazione comunicazione;
		Mappa mappa;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Mappa) {
			mappa = (Mappa) comunicazione;
			this.inviaACK();
			return mappa;
		}
		return null;

	}

	@Override
	public void inviaSpostamento(Integer idSoggetto,
			Integer idVecchiaPosizione, Integer idNuovaPosizione,
			Integer costoMossa) {
		Movimento wvMovimento = new Movimento(idSoggetto, idVecchiaPosizione,
				idNuovaPosizione, costoMossa);
		this.inviaComunicazioneSocket(wvMovimento);
	}

	// TODO forse è da rimuovere in quanto ripetuto
	private void InviaSegnaleReady(String nomeGiocatore) {
		FileIOHelper file = new FileIOHelper("ee.txt");// datiPartita.txt
		try {
			ArrayList<String> righe = file.getFile();
			// Se sono arrivato fino a qua, vuol dire che è presente il file e
			// quindi devo inviare i miei dati
			this.inviaDatiPartitaPrecedente(nomeGiocatore, righe);
		} catch (FileNonTrovatoException e) {
			// Qui dentro non ho trovato il file, vuol dire che è la prima volta
			// che mi connetto
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
			this.inviaDatiPartita(nomeGiocatore);
		}
	}

	/**
	 * Apre il file e assegna agli attributi privati l'indirizzo ip e la porta
	 * da usare nella socket
	 * 
	 * @throws FileNonTrovatoException
	 *             se il file non viene trovato
	 */
	private void recuperaDatiSocket() throws FileNonTrovatoException {
		FileIOHelper file = new FileIOHelper("socketInfo.txt");
		ArrayList<String> righe = file.getFile();
		this.atIndirizzo = righe.get(0);
		this.atPorta = Integer.parseInt(righe.get(1));
	}

	/**
	 * Invia un oggetto di tipo I_Comunicazione sullo stream di uscita dopo
	 * averlo serializzato
	 * 
	 * @param pacchetto
	 *            l'oggetto I_Comunicazione da inviare
	 */
	private void inviaComunicazioneSocket(IComunicazione pacchetto) {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(this.outServer);
			oos.writeObject(pacchetto);
			oos.flush();
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
	}

	/**
	 * Si mette in ascolto sul canale in ingresso e tenta di deserializzare ciò
	 * che arriva per poi restituire una comunicazione
	 * 
	 * @return una I_Comunicazione che arriva dal server
	 */
	private IComunicazione riceviComunicazioneSocket() {
		IComunicazione comunicazione = null;
		ObjectInputStream ois;
		try {
			CLCHelper.stampa("RICEVICOMUNICAZIONE: Aspetto oggetto");
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}

			this.inServer = this.atSocket.getInputStream();
			ois = new ObjectInputStream(this.inServer);
			CLCHelper.stampa("RICEVICOMUNICAZIONE: Ho ricevuto qualcosa...");

			comunicazione = (IComunicazione) ois.readObject();
			// CLCHelper.Stampa("RICEVICOMUNICAZIONE: qualcosa tipo: "
			// + (comunicazione.toString()));

		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		} catch (ClassNotFoundException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
		return comunicazione;
	}

	/**
	 * Il client invia al server la posizione iniziale che ha deciso
	 * 
	 * @param idPastore
	 *            l'id del pastore
	 * @param posizioneScelta
	 *            la posizione scelta che rappresenta l'id della strada
	 */
	@Override
	public void inviaPosizioneIniziale(Integer idPastore,
			Integer posizioneScelta) {
		Messaggio messaggio = new Messaggio();
		messaggio.posizioneIniziale(idPastore, posizioneScelta);
		this.inviaComunicazioneSocket(messaggio);
	}

	/**
	 * Aspetta un ack dalla rete
	 * 
	 * @return true se è arrivato ACK, false se è arrivato NACK
	 */
	@Override
	public boolean getACK() throws FineTurnoException {
		IComunicazione comunicazione;
		Messaggio messaggio;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._ACK.toString())) {
				return true;
			} else if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._NACK.toString())) {
				return false;
			} else if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._FINETURNO.toString())) {
				throw new FineTurnoException();
			}
		}
		return true;
	}

	/**
	 * Crea l'ack e lo invia sulla rete
	 */
	@Override
	public void inviaACK() {
		Messaggio messaggio = new Messaggio();
		messaggio.ack();
		this.inviaComunicazioneSocket(messaggio);
	}

	/**
	 * Riceve una tessera terreno dalla rete
	 * 
	 * @return una stringa per poter ricavare il tipo enumerativo corrispondente
	 *         tramite valueOf()
	 */
	@Override
	public String getTesseraTerreno() {
		IComunicazione comunicazione;
		Messaggio messaggio;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().split("\\:")[0]
					.equals(EIntestazioneMessaggio._TESSERATERRENO.toString())) {
				// Recupero il tipo di terreno
				String tipo = messaggio.getMessaggio().split("\\:")[1];
				// ritorno il tipo
				return tipo;
			}
		}
		return null;
	}

	/**
	 * Invia il nome al server dentro al messaggio di pronto
	 */
	private void inviaDatiPartita(String nome) {
		Messaggio messaggio = new Messaggio();
		messaggio.pronto(nome);
		this.inviaComunicazioneSocket(messaggio);
	}

	/**
	 * Invia i dati della partita precedete al server in caso di disconnesione
	 */
	private void inviaDatiPartitaPrecedente(String nome, ArrayList<String> righe) {
		Messaggio messaggio = new Messaggio();
		messaggio.pronto(nome, righe);
		this.inviaComunicazioneSocket(messaggio);
	}

	/**
	 * Rimane in ascolto delle model.mosse degli altri giocatori comunicandole
	 * ai livelli superiori. Ritorna un'eccezione {@link FineTurnoException} nel
	 * caso in cui sia finito il turno di uno dei giocatori
	 */
	@Override
	public IMossa getMosseAltriGiocatori() throws FineTurnoException {
		IComunicazione comunicazione;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			Messaggio messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._FINETURNO.toString())) {
				throw new FineTurnoException();
			} else if (messaggio.getIntestazione().equals(
					EIntestazioneMessaggio._ACK)) {
				return new MossaVuota();
			}
		} else if (comunicazione instanceof IMossa) {
			return (IMossa) comunicazione;
		}
		return null;
	}

	/**
	 * Rimane in ascolto del server, comunicando le model.mosse che arrivano
	 * oppure scatenando l'eccezione {@link InizioTurnoException} nel caso in
	 * cui arrivi una comunicazione di inizio turno
	 * 
	 * @throws FaseMercatoException
	 */
	@Override
	public IMossa getMossePreliminari() throws NumberFormatException,
			InizioTurnoException, FinePartitaException, FaseMercatoException {
		IComunicazione comunicazione;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			Messaggio messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().split("\\:")[0]
					.equals(EIntestazioneMessaggio._TURNOGIOCATORE.toString())) {
				throw new InizioTurnoException(Integer.parseInt(messaggio
						.getMessaggio().split("\\:")[1]));
			} else if (messaggio.getIntestazione().equals(
					EIntestazioneMessaggio._USCITA)) {
				this.inviaACK();
				throw new FinePartitaException();
			} else if (messaggio.getIntestazione().equals(
					EIntestazioneMessaggio._TURNOOFFERTA)) {
				Integer wvId = Integer.parseInt(messaggio
						.getParametriMessaggio()[0]);
				throw new FaseMercatoException(wvId, 1);
			} else if (messaggio.getIntestazione().equals(
					EIntestazioneMessaggio._TURNOACQUISTO)) {
				Messaggio wvMessaggio = (Messaggio) comunicazione;
				Integer wvId = Integer.parseInt(wvMessaggio
						.getParametriMessaggio()[0]);
				throw new FaseMercatoException(wvId, 2);
			}
		} else if (comunicazione instanceof IMossa) {
			return (IMossa) comunicazione;
		}
		return null;
	}

	/**
	 * Determina se il messaggio che arriva è un messaggio di avvio partita
	 * 
	 * @return un booleano true o false
	 */
	@Override
	public boolean getInizioPartita() {
		IComunicazione comunicazione;
		Messaggio messaggio;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._AVVIAPARTITA.toString())) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Rimane in ascolto del server, se il server richiede il posizionamento di
	 * una pedina restituisce un eccezione {@link TurnoPersonaleException} per
	 * far capire al client che è il suo turno, altimenti comunica il
	 * posizionamento fatto da un avversario
	 * 
	 * @return un posizonamento di una pedina avversaria
	 * 
	 * @throws TurnoPersonaleException
	 *             se è il proprio turno
	 * @throws InizioPartitaException
	 */
	@Override
	public Posizionamento getMossaPedina() throws TurnoPersonaleException,
			InizioPartitaException {
		IComunicazione comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Posizionamento) {
			return (Posizionamento) comunicazione;
		} else if (comunicazione instanceof Messaggio) {
			Messaggio messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._POSIZIONAPEDINA.toString())) {
				throw new TurnoPersonaleException();
			} else if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._AVVIAPARTITA.toString())) {
				throw new InizioPartitaException();
			}
		}

		return null;
	}

	/**
	 * Invia al server il posizionamento iniziale della pedina pastore
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 * @param idPedina
	 *            l'id della pedina
	 * @param idStrada
	 *            l'id della strada
	 * @return la mossa creata
	 */
	@Override
	public Posizionamento inviaPosizionamento(Integer idGiocatore,
			Integer idPedina, Integer idStrada) {
		Posizionamento mossaPosizionamento = new Posizionamento(idGiocatore,
				idPedina, idStrada);
		this.inviaComunicazioneSocket(mossaPosizionamento);
		return mossaPosizionamento;
	}

	/**
	 * Invia una mossa generica al server
	 * 
	 * @param mossaLetta
	 *            la mossa che l'utente esegue
	 * @throws FineTurnoException
	 */
	@Override
	public void inviaMossa(IMossa mossaLetta) {
		this.inviaComunicazioneSocket(mossaLetta);
		// getACK();
	}

	/**
	 * Questa eccezione viene scatenata quando dal server invece che arrivare
	 * una {@link IMossa} arriva un Messaggio che ci indica l'inizio del turno
	 * personale
	 * 
	 * @author Mirko
	 * 
	 */

	@Override
	public boolean isFineTurno() {
		IComunicazione comunicazione;
		Messaggio messaggio;
		comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Messaggio) {
			messaggio = (Messaggio) comunicazione;
			if (messaggio.getMessaggio().equals(
					EIntestazioneMessaggio._FINETURNO.toString())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public IMossa getAbbattuto() {
		IComunicazione comunicazione = this.riceviComunicazioneSocket();
		if (comunicazione instanceof Abbattuto) {
			return (Abbattuto) comunicazione;
		}
		return null;
	}

	@Override
	public Integer getRegioneOccupataPecoraNera() {
		return Integer.MIN_VALUE;
	}

	@Override
	public void aggiornaMappa() throws MappaDaAggiornareException {
		return;
	}

	@Override
	public Integer getRegioneOccupataLupo() {
		return Integer.MIN_VALUE;
	}

}
