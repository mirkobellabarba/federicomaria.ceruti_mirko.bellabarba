package helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import view.Start;

public class SwingHelper {

	private Integer atIdCliccato;

	public SwingHelper() {
		this.atIdCliccato = null;
	}

	/**
	 * Ottine l'id dell'elemento cliccato
	 * 
	 * @return l'id
	 */
	public Integer getIdCliccato() {
		synchronized (this) {
			while (this.atIdCliccato == null) {
				try {
					this.wait();
				} catch (Exception e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
					}
				}
			}
			Integer idCliccato = this.atIdCliccato;
			this.atIdCliccato = null;
			return idCliccato;
		}
	}

	/**
	 * Setta l'id cliccato
	 * 
	 * @param idCliccato
	 *            l'id cliccato
	 */
	public void setIdCliccato(Integer idCliccato) {
		if (this.atIdCliccato == null) {
			this.atIdCliccato = idCliccato;
			synchronized (this) {
				this.notify();
			}
		}
	}

	/**
	 * Resetta i parametri
	 */
	public void resetId() {
		this.atIdCliccato = null;
	}

}
