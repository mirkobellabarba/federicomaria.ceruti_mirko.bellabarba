package helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import view.Start;

/**
 * @author Mirko
 * 
 *         Questa classe, aiuta a leggere (ed eventualmente scrivere) su file
 * 
 */
public class FileIOHelper {
	private final String indirizzo;
	private ArrayList<String> righe;

	/**
	 * Instanzia l'oggetto FileIOHelper, richiamare il metodo getFile per
	 * ottenerne il contenuto
	 * 
	 * @param Indirizzo
	 *            è una stringa rappresentante l'indirizzo del file
	 */
	public FileIOHelper(String Indirizzo) {
		this.indirizzo = Indirizzo;
	}

	/**
	 * Ritorna le righe del file divise in una lista
	 * 
	 * @return un ArrayList di stringhe
	 * @throws FileNonTrovatoException
	 */
	public ArrayList<String> getFile() throws FileNonTrovatoException {
		this.leggiFile();
		if (this.righe == null) {
			throw new FileNonTrovatoException();
		}
		return this.righe;
	}

	/**
	 * Scrive su file un arraylist di righe. Ad ogni riga il metodo aggiunge un
	 * "a capo", fatta eccezione per l'ultima
	 * 
	 * @param righe
	 *            le righe da scrivere
	 */
	public void writeFile(ArrayList<String> righe) {
		BufferedWriter writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(this.indirizzo), "utf-8"));
			for (int i = 0; i < righe.size(); i++) {
				writer.write(righe.get(i));
				if ((i + 1) < righe.size()) {
					writer.newLine();
				}
				writer.flush();
			}
		} catch (IOException ex) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), ex);
			}
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), ex);
				}
			}
		}
	}

	/**
	 * Cancella il file dal disco
	 * 
	 * @return un booleano che dice se il file viene cancellato o no
	 */
	public boolean deleteFile() {
		try {

			File file = new File(this.indirizzo);

			if (file.delete()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			return false;
		}
	}

	/**
	 * Ritorna l'indirizzo del file che si sta leggendo
	 * 
	 * @return
	 */
	public String getIndirizzo() {
		return this.indirizzo;
	}

	/**
	 * Legge il file il cui indirizzo è stato indicato nel costruttore, se la
	 * lettura non va a buon fine ritorna null
	 */
	private void leggiFile() {
		try {
			this.righe = this.openFile();
		} catch (IOException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
			this.righe = null;
		}
	}

	// Variabile booleana che specifica la modalità di lettura del file
	private final boolean getFromResources = false;

	/**
	 * Apre il file, legge le righe e si ferma quando trova #end
	 * 
	 * @return Un ArrayList di stringhe contenente le righe del documento
	 * @throws IOException
	 *             se il file non viene trovato
	 */
	private ArrayList<String> openFile() throws IOException {
		// String urlString = getClass().getResource("/"+indirizzo ).toString();
		BufferedReader textReader;

		if (!Start.abilitaTest) {
			InputStream iStream = this.getClass().getResourceAsStream(
					"/" + this.indirizzo);
			if (iStream == null) {
				FileReader fReader = new FileReader(this.indirizzo);
				if (fReader == null) {
					throw new IOException();
				}
				textReader = new BufferedReader(fReader);

			} else {
				textReader = new BufferedReader(new InputStreamReader(iStream));
			}
		} else {
			FileReader fReader = new FileReader(this.indirizzo);
			textReader = new BufferedReader(fReader);
		}

		ArrayList<String> righe = new ArrayList<String>();
		while (true) {
			// Leggo la riga
			String riga = textReader.readLine();
			// Controllo se non sono arrivato alla fine del file
			if (riga != null) {
				// Aggiungo la riga letta alla lista
				righe.add(riga);
			} else {
				// se la riga è null interrompo il ciclo
				break;
			}
		}
		// chiudo lo stream di lettura
		textReader.close();
		// ritorno le righe lette
		return righe;
	}

	/**
	 * Questa eccezione viene lanciata quando il file non viene trovato su disco
	 * 
	 * @author Mirko
	 * 
	 */
	public class FileNonTrovatoException extends Exception {

		private static final long serialVersionUID = -3466839475710876519L;

		public FileNonTrovatoException() {
			super("Il file che stai cercando di leggere non esiste!");
		}
	}
}