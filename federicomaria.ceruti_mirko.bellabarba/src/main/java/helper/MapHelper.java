package helper;

import helper.FileIOHelper.FileNonTrovatoException;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Agnello;
import model.Animale;
import model.ETipoID;
import model.ETipoTerreno;
import model.FileRegione;
import model.FileStrade;
import model.Giocatore;
import model.Lupo;
import model.Mappa;
import model.Montone;
import model.Pecora;
import model.Pecoranera;
import model.Regione;
import model.Regione.StradeInseriteException;
import model.Strada;
import view.Start;

/**
 * Questa classe ritorna un oggetto mappa dopo averlo letto da file
 * 
 * @author Mirko
 * 
 */
public class MapHelper {
	private Mappa mappa;
	private ArrayList<Giocatore> giocatori;
	private final ArrayList<Regione> regioni;
	private final ArrayList<Strada> strade;
	private final ArrayList<Animale> animali;
	private final ArrayList<FileRegione> tempRegioni;
	private final ArrayList<FileStrade> tempStrade;
	private ArrayList<String> righeRegioni;
	private ArrayList<String> righeStrade;
	private final FileIOHelper fileRegione;
	private final FileIOHelper fileStrade;

	private final String percorsoRegioni = "./regioni.txt";
	private final String percorsoStrade = "./strade.txt";
	private Integer numeroPecore = 0;
	private Integer numeroMontoni = 0;
	private Integer idProgressivoAnimale = 1;

	/**
	 * Costruttore che inizializza i contenitori della mappa. per settare i
	 * giocatori occorrerà richiamare setGiocatori()
	 */
	public MapHelper() {
		this.fileRegione = new FileIOHelper(this.percorsoRegioni);
		this.fileStrade = new FileIOHelper(this.percorsoStrade);
		this.giocatori = new ArrayList<Giocatore>();
		this.tempRegioni = new ArrayList<FileRegione>();
		this.tempStrade = new ArrayList<FileStrade>();
		this.regioni = new ArrayList<Regione>();
		this.strade = new ArrayList<Strada>();
		this.animali = new ArrayList<Animale>();
	}

	/**
	 * Costruttore della classe MapHelper. Permette di ottenere una mappa
	 * caricandola da file
	 * 
	 * @param giocatori
	 */
	public MapHelper(ArrayList<Giocatore> giocatori) {
		this.fileRegione = new FileIOHelper(this.percorsoRegioni);
		this.fileStrade = new FileIOHelper(this.percorsoStrade);
		this.giocatori = giocatori;
		this.tempRegioni = new ArrayList<FileRegione>();
		this.tempStrade = new ArrayList<FileStrade>();
		this.regioni = new ArrayList<Regione>();
		this.strade = new ArrayList<Strada>();
		this.animali = new ArrayList<Animale>();
	}

	/**
	 * Ottiene le righe dei file letti sia per le regioni che per le strade
	 * 
	 */
	public void caricaMappa() {
		try {
			this.righeRegioni = this.fileRegione.getFile();
		} catch (FileNonTrovatoException e1) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e1);
			}
		}
		try {
			this.righeStrade = this.fileStrade.getFile();
		} catch (FileNonTrovatoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
			}
		}
	}

	/**
	 * Carica le classi temporanee, inizia a creare gli oggetti con le
	 * informazioni base, e infine, dopo aver letto tutte le regioni e le
	 * strade, finalizza la mappa riempiendola con tutte le info mancanti
	 * creando dipendenza fra le informazioni di regioni e strade
	 * 
	 * @return la mappa di gioco di tipo Mappa
	 */
	public Mappa getMappa() {
		this.popolaListeTemporaneee();
		this.creaMappaBase();
		this.mappa = new Mappa(this.giocatori, this.regioni, this.strade, this.animali);
		return this.mappa;
	}

	/**
	 * Con i dati "grezzi" appena raccolti, CreaMappaBase crea l'oggetto mappa
	 * Collegando le regioni con le strade
	 */
	private void creaMappaBase() {
		this.creoOggettiDisaccoppiati();
		this.associoGliOggetti();
	}

	/**
	 * lancia 2 metodi per completare le liste di regioni e strade legandole fra
	 * loro
	 */
	private void associoGliOggetti() {
		this.completoRegioni();
		this.completaStrade();
		this.controllaPresenzaPecoraMontone();
	}

	/**
	 * Se il numero di pecore e montoni è superiore a 0, il metodo si ferma
	 * altrimenti aggiunge in una regione a caso (sostituendo il vecchio
	 * animale) il tipo di animale che manca (pecora/montone) al fine di averne
	 * almeno uno per i due tipi
	 * 
	 */
	private void controllaPresenzaPecoraMontone() {
		if ((this.numeroPecore > 0) && (this.numeroMontoni > 0)) {
			return;
		} else {
			// ottengo un id random controllando che non corrisponda a
			// sheepsburg
			int idRegione = this.getRandom(19, true);
			// ottengo la regione dove farò il cambio
			Regione regione = this.regioni.get(idRegione);
			// ottengo l'oggetto animale da rimuovere
			Animale animaleDaRimuovere = regione.getAnimali().get(0);
			// instanzio l'animale da creare
			Animale nuovoAnimale;
			// se mi mancano pecore ne aggiungo una nella regione scelta a caso
			if (this.numeroPecore == 0) {
				nuovoAnimale = new Pecora(animaleDaRimuovere.getId(), regione);
				// rimuovo il vecchio animale
				regione.rimuoviAnimale(animaleDaRimuovere);
				// aggiungo la pecora
				regione.aggiungiAnimale(nuovoAnimale);
				// aggiorno l'arraylist con il nuovo animale
				this.animali.set(this.animali.indexOf(animaleDaRimuovere), nuovoAnimale);
				this.numeroPecore++;
			}
			if (this.numeroMontoni == 0) {
				nuovoAnimale = new Montone(animaleDaRimuovere.getId(), regione);
				// rimuovo il vecchio animale
				regione.rimuoviAnimale(animaleDaRimuovere);
				// aggiungo il montone
				regione.aggiungiAnimale(nuovoAnimale);
				// aggiorno l'arraylist
				this.animali.set(this.animali.indexOf(animaleDaRimuovere), nuovoAnimale);
				this.numeroMontoni++;
			}

		}

	}

	/**
	 * Aggiunge i parametri mancanti alle strade
	 */
	private void completaStrade() {
		for (Integer i = 0; i < this.strade.size(); i++) {
			Strada strada = this.strade.get(i);
			FileStrade tempStrada = this.tempStrade.get(i);

			strada.setRegioniLimitrofe(this.getListaRegioni(tempStrada.getRegioniLimitrofe()));

			strada.setStradeCollegate(this.getStradeCollegate(tempStrada.getStradeLimitrofe()));
		}

	}

	/**
	 * Ottiene le strade collegate passandogli gli id di queste strade
	 * 
	 * @param stradeLimitrofe
	 *            gli id delle strade limitrofe
	 * @return le strade limitrofe
	 */
	private ArrayList<Strada> getStradeCollegate(Integer[] stradeLimitrofe) {
		ArrayList<Strada> strade = new ArrayList<Strada>();
		for (Strada strada : this.strade) {
			for (Integer id : stradeLimitrofe) {

				if (id.equals(strada.getId())) {
					strade.add(strada);
					break;
				}
			}
		}
		return strade;
	}

	/**
	 * Ritorna la lista di regioni limitrofe secondo gli id che mi sono arrivati
	 * nell'array
	 * 
	 * @param regioniLimitrofe
	 *            Un array di interi
	 * @return un array di regioni (sempre 2)
	 */
	private Regione[] getListaRegioni(Integer[] regioniLimitrofe) {
		ArrayList<Regione> regioni = new ArrayList<Regione>();
		for (Regione regione : this.regioni) {
			for (Integer id : regioniLimitrofe) {

				if (id.equals(regione.getId())) {
					regioni.add(regione);
					break;
				}
			}
		}
		return regioni.toArray(new Regione[regioni.size()]);
	}

	/**
	 * Inserisce le strade limitrofe nelle regioni, associandole quindi con la
	 * lista di strade
	 * 
	 */
	private void completoRegioni() {
		// Guardo tutte le regioni una per una
		for (Integer i = 0; i < this.regioni.size(); i++) {
			try {
				/*
				 * nella regione in esame, setto le strade limitrofe, passando
				 * gli id delle strade limitrofe
				 */
				this.regioni.get(i).setStradeLimitrofe(this.getListaStrade(this.tempRegioni.get(i).getStradeLimitrofe()));
			} catch (StradeInseriteException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
				}
			}
			// Aggiungo un animale
			this.aggiungiAnimale(this.regioni.get(i));

		}

	}

	/**
	 * Aggiunge gli animali controllando se la regione è sheepsburg o no
	 * 
	 * @param regione
	 */
	private void aggiungiAnimale(Regione regione) {
		if (regione.getTipoTerreno() != ETipoTerreno.SHEEPSBURG) {
			regione.aggiungiAnimale(this.aggiungiAnimaleRandom(regione));
		} else {
			this.aggiungiAnimaliSheepsburg(regione);
		}
	}

	/**
	 * Aggiunge il lupo e la pecora nera a sheepsburg
	 * 
	 * @param regione
	 *            la regione di sheepsburg
	 */
	private void aggiungiAnimaliSheepsburg(Regione regione) {
		// TODO Auto-generated method stub
		Animale pecoraNera = new Pecoranera((this.idProgressivoAnimale * 100) + ETipoID.ANIMALE.ordinal(), regione);
		this.idProgressivoAnimale++;
		Animale Lupo = new Lupo((this.idProgressivoAnimale * 100) + ETipoID.ANIMALE.ordinal(), regione);
		this.idProgressivoAnimale++;
		this.animali.add(pecoraNera);
		this.animali.add(Lupo);
		regione.aggiungiAnimale(pecoraNera);
		regione.aggiungiAnimale(Lupo);
	}

	/**
	 * Aggiunge un amimale alla regione passata
	 * 
	 * @param regione
	 *            la regione da associare all'animale
	 * @return l'animale da aggiungere alla regione
	 */
	private Animale aggiungiAnimaleRandom(Regione regione) {
		// TODO meccanisco random per buttare fuori animali di tipo diverso
		Animale animale;
		animale = this.getAnimaleCasuale(regione);

		this.idProgressivoAnimale++;
		this.animali.add(animale);
		return animale;
	}

	/**
	 * estrae un numero e in base a quello aggiunge una pecora, un montone o un
	 * agnello
	 * 
	 * @param regione
	 *            la regione da legare all'animale
	 * @return l'animale appena creato
	 */
	private Animale getAnimaleCasuale(Regione regione) {
		// #TODO se l'utente non vuole pecore o montoni questo metodo va saltato
		switch (this.getRandom(3, false)) {
		case 0:
			return new Agnello(this.getIDProgressivoAnimale(), regione);
		case 1:
			this.numeroPecore++;
			return new Pecora(this.getIDProgressivoAnimale(), regione);
		case 2:
			this.numeroMontoni++;
			return new Montone(this.getIDProgressivoAnimale(), regione);
		default:
			return null;
		}

	}

	/**
	 * Calcola l'id progressivo da mettere all'animale
	 * 
	 * @return un intero rappresentante l'id univoco dell'animale
	 */
	private Integer getIDProgressivoAnimale() {
		return (this.idProgressivoAnimale * 100) + ETipoID.ANIMALE.ordinal();
	}

	/**
	 * Ottiene un random secondo i parametri passati
	 * 
	 * @param max
	 *            il numero massimo (escluso) dell'estrazione
	 * @param CheckSheepsburg
	 *            se deve ritornare un numero diverso dall'id di sheepsburg
	 * @return un intero casuale tra 0 e max (escluso)
	 */
	private int getRandom(Integer max, Boolean CheckSheepsburg) {
		Random rndRandom = new Random();
		if (CheckSheepsburg) {
			int numero = rndRandom.nextInt(max);
			while (numero == 9) {
				numero = rndRandom.nextInt(max);
			}
			return numero;
		} else {
			return rndRandom.nextInt(max);
		}
	}

	/**
	 * Restituisce le strade che hanno un id uguale ad uno di quelli passati
	 * nella lista
	 * 
	 * @param stradeLimitrofe
	 * @return un ArrayList di tipo Strada
	 */
	private ArrayList<Strada> getListaStrade(Integer[] stradeLimitrofe) {
		ArrayList<Strada> strade = new ArrayList<Strada>();
		// Scorro le strade
		for (Strada strada : this.strade) {
			// Per ogni strada guardo se il suo id comapre nelle stradeLimitrofe
			for (Integer id : stradeLimitrofe) {
				// Se la trovo aggiungo quella strada nella lista
				if (id.equals(strada.getId())) {
					strade.add(strada);
				}
			}
		}
		// ritorno le strade individuate
		return strade;
	}

	/**
	 * Crea regioni e strade popolandole con quei dati che non creano una
	 * dipendenza tra le due
	 */
	private void creoOggettiDisaccoppiati() {
		for (FileRegione item : this.tempRegioni) {
			Regione regione = new Regione(item.getId(), item.getTipoTerreno());
			this.regioni.add(regione);
		}
		for (FileStrade item : this.tempStrade) {
			Strada strada = new Strada(item.getId(), item.getNumeroStrada());
			this.strade.add(strada);
		}
	}

	/**
	 * Popola le liste temporanee di appoggio per regioni e strade
	 */
	private void popolaListeTemporaneee() {
		this.popolaTempRegioni();
		this.popolaTempStrade();
	}

	/**
	 * Carica nella lista di appoggio tutte le info "grezze" sulle regioni
	 */
	private void popolaTempRegioni() {
		for (String riga : this.righeRegioni) {
			// id|tipoTerreno|stradeLimitrofe
			String[] regione = riga.split("\\|");
			// E_Tipo_Terreno terreno =
			// E_Tipo_Terreno.valueOf(regione[1].toUpperCase());
			FileRegione tempRegione = new FileRegione(Integer.parseInt(regione[0]), ETipoTerreno.valueOf(regione[1].toUpperCase()),
					this.getIDFromString(regione[2]));
			this.tempRegioni.add(tempRegione);
		}
	}

	/**
	 * Carica nella lista di appoggio tutte le info "grezze" sulle strade
	 */
	private void popolaTempStrade() {
		for (String riga : this.righeStrade) {
			// id|regioniLimitrofe|stradeLimitrofe|numeroStrada
			String[] strada = riga.split("\\|");
			FileStrade tempStrada = new FileStrade(Integer.parseInt(strada[0]), this.getIDFromString(strada[1]), this.getIDFromString(strada[2]),
					Integer.parseInt(strada[3]));
			this.tempStrade.add(tempStrada);
		}
	}

	/**
	 * passata una stringa contenenete serie di id separati da virgola, ritorna
	 * un array di interi con le gli id convertiti ad int
	 * 
	 * @param ids
	 *            la stringa di id separati da virgola
	 * @return un array di interi
	 */
	private Integer[] getIDFromString(String ids) {
		String[] strArray = ids.split("\\,");
		Integer[] intArray = new Integer[strArray.length];
		for (int i = 0; i < strArray.length; i++) {
			intArray[i] = Integer.parseInt(strArray[i]);
		}
		return intArray;
	}

	/**
	 * 
	 * @return il numero di pecore
	 */
	public int getNumeroPecore() {
		return this.numeroPecore;
	}

	/**
	 * 
	 * @return il numero dei montoni
	 */
	public int getNumeroMontoni() {
		return this.numeroMontoni;
	}

	/**
	 * Setta la lista dei giocatori se è vuota
	 * 
	 * @param giocatori
	 */
	public void setListaGiocatori(ArrayList<Giocatore> giocatori) {
		if (this.giocatori.size() > 0) {
			return;
		}
		this.giocatori = giocatori;
	}
}
