package helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import view.Start;

/**
 * Classe di appoggio per lockare l'esecuzione del programma senza bloccare la
 * grafica
 * 
 * @author Mirko
 * 
 */
public class ThreadHelper {

	private Boolean atLocked;

	/**
	 * Costruttore
	 */
	public ThreadHelper() {
		this.atLocked = false;
	}

	/**
	 * Ottine l'id dell'elemento cliccato
	 * 
	 * @return l'id
	 */
	public Boolean getLocked() {
		synchronized (this) {
			while (this.atLocked == null) {
				try {
					this.wait();
				} catch (Exception e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
					}
				}
			}
			Boolean idCliccato = this.atLocked;
			return idCliccato;
		}
	}

	/**
	 * Setta l'id cliccato
	 * 
	 * @param idCliccato
	 *            l'id cliccato
	 */
	public void setLocked(Boolean locked) {
		if (this.atLocked == null) {
			this.atLocked = locked;
			synchronized (this) {
				this.notify();
			}
		}
	}

	/**
	 * Resetta i parametri
	 */
	public void resetId() {
		this.atLocked = null;
	}
}
