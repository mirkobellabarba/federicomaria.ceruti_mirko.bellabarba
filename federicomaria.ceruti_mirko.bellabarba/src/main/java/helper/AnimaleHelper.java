package helper;

import java.util.logging.Level;
import java.util.logging.Logger;

import model.Animale;
import view.Start;

/**
 * Classe per bloccare l'esecuzione del programma aspettando un input da utente
 * 
 * @author Mirko
 * 
 */
public class AnimaleHelper {
	private Animale atAnimale;

	/**
	 * Costruttore
	 */
	public AnimaleHelper() {
		this.atAnimale = null;
	}

	/**
	 * Ottine l'id dell'elemento cliccato
	 * 
	 * @return l'id
	 */
	public Animale getTipoAnimale() {
		synchronized (this) {
			while (this.atAnimale == null) {
				try {
					this.wait();
				} catch (Exception e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING, this.getClass().toString(), e);
					}
				}
			}
			Animale animale = this.atAnimale;
			this.atAnimale = null;
			return animale;
		}
	}

	/**
	 * Setta l'id cliccato
	 * 
	 * @param idCliccato
	 *            l'id cliccato
	 */
	public void setTipoAnimale(Animale animale) {
		if (this.atAnimale == null) {
			this.atAnimale = animale;
			synchronized (this) {
				this.notify();
			}
		}
	}

	/**
	 * Resetta i parametri
	 */
	public void resetAnimale() {
		this.atAnimale = null;
	}
}
