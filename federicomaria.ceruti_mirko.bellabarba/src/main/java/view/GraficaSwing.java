package view;

import helper.AnimaleHelper;
import helper.FileIOHelper;
import helper.FileIOHelper.FileNonTrovatoException;
import helper.SwingHelper;
import helper.ThreadHelper;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import model.Agnello;
import model.Animale;
import model.Classifica;
import model.ETipoID;
import model.ETipoTerreno;
import model.Giocatore;
import model.Montone;
import model.Pecora;
import model.Pecoranera;
import model.PedinaPastore;
import model.Regione;
import model.Strada;
import model.mosse.Abbattimento;
import model.mosse.Abbattuto;
import model.mosse.Accoppiamento;
import model.mosse.IMossa;
import model.mosse.Investimento;
import model.mosse.Movimento;
import model.mosse.MovimentoLupo;
import model.mosse.Nascita;
import model.mosse.Posizionamento;
import model.mosse.tentativoFallito;
import model.swing.AnimazioneSpostamento;
import model.swing.AnimazioneZoom;
import model.swing.IconaSpostabile;
import model.swing.ImagePanel;
import controllerClient.Connessione.FaseMercatoException;
import controllerClient.Connessione.FinePartitaException;
import controllerClient.Connessione.InizioPartitaException;
import controllerClient.Connessione.InizioTurnoException;
import controllerClient.Connessione.TurnoPersonaleException;
import controllerClient.DisconnessioneException;
import controllerClient.PartitaClient;

/**
 * Classe che mette a disposizione i metodi per gestire l'interfaccia utente con
 * Swing
 * 
 */
public class GraficaSwing {

	private final SwingHelper swingHelper;
	private final ThreadHelper socketThreadHelper;
	private final AnimaleHelper animaleHelper;
	private final PartitaClient partitaSwing;
	private static GraficaSwing at_istanza;
	private JFrame wvFramePrincipale;

	private JLabel lblLavagna;
	private JLabel lblIdentitaGiocatore;
	private IconaSpostabile lblRecinti;
	private IconaSpostabile lblRecintiFinali;

	private ArrayList<String> coordinateRegioni;
	private ArrayList<String> coordinateStrade;
	/**
	 * Le coordinate delle pecore ordinate per regione
	 */
	private ArrayList<String> coordinatePecore;
	/**
	 * Le coordinate della pecora nera ordinate per regione
	 */
	private ArrayList<String> coordinatePecoraNera;
	/**
	 * Le coordinate del lupo ordinate per regione
	 */
	private ArrayList<String> coordinateLupo;
	/**
	 * le icone che rappresentano le icone delle pecore bianche ordinate per
	 * regione
	 */
	private final ArrayList<IconaSpostabile> iconePastori;
	private IconaSpostabile atPedinaPecoraNera, atPedinaLupo;

	JButton btnSpostamentoPastore;
	JButton btnSpostamentoOvino;
	JButton btnInvestimento;
	JButton btnAbbattimento;
	JButton btnAccoppiamento;

	JButton btnPianura;
	JButton btnCollina;
	JButton btnMontagna;
	JButton btnDeserto;
	JButton btnLago;
	JButton btnBosco;

	JButton btnScegliPecora;
	JButton btnScegliPecoraNera;
	JButton btnScegliPecoraMontone;
	JButton btnScegliPecoraAgnello;
	Animale atTipoAnimaleCliccato = null;

	// variabili booleane indicanti la fase di gioco, in modo da influenzare il
	// comportamento dei bottoni
	private Boolean atFasePosizionamentoPastori = false;
	private Boolean atFaseDecisioneMossa = false;
	private Boolean atFaseSpostamentoPastore = false;
	private Boolean atFaseSpostamentoOvini = false;
	private Boolean atFaseInvestimento = false;
	private Boolean atFaseVenditaTerreni = false;
	private Boolean atFaseAcquistoTerreno = false;
	private Boolean atFaseAbbattimento = false;
	private Boolean atFaseAccoppiamento = false;
	private boolean atFaseFinePartita;
	private boolean atFaseSceltaPedina = false;

	private Integer atFaseMercato;

	private ArrayList<ArrayList<IMossa>> atContenitoreMossePossibi;

	private int atIndicePedina;
	private PedinaPastore pedinaSelezionata;
	private ArrayList<IMossa> atMosseEffettuateNelTurno;
	private boolean atPastoreMosso;

	/**
	 * Costruttore
	 */
	private GraficaSwing(PartitaClient partita) {
		this.swingHelper = new SwingHelper();
		this.socketThreadHelper = new ThreadHelper();
		this.animaleHelper = new AnimaleHelper();
		this.iconePastori = new ArrayList<IconaSpostabile>();
		this.partitaSwing = partita;
		this.atFaseMercato = 0;
		this.atIndicePedina = -1;

	}

	/**
	 * Carica le coordinate delle strade, regioni e centro regioni
	 */
	public void caricaCoordinate() {
		FileIOHelper wvFileStrade = new FileIOHelper("stradeCoord.txt");
		FileIOHelper wvFileRegioni = new FileIOHelper("regioniGrafica.txt");
		FileIOHelper wvFileCentroRegioni = new FileIOHelper(
				"coordinateCentroRegioni.txt");
		FileIOHelper wvFilePecoraNera = new FileIOHelper(
				"coordinatePecoraNera.txt");
		FileIOHelper wvFileLupo = new FileIOHelper("coordinateLupo.txt");

		try {
			this.coordinateStrade = wvFileStrade.getFile();
			this.coordinateRegioni = wvFileRegioni.getFile();
			this.coordinatePecore = wvFileCentroRegioni.getFile();
			this.coordinatePecoraNera = wvFilePecoraNera.getFile();
			this.coordinateLupo = wvFileLupo.getFile();
		} catch (FileNonTrovatoException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}

	}

	/**
	 * Costruisce la finestra principale
	 * 
	 * @param nomeGiocatore
	 */
	public void mostraUI(String nomeGiocatore) {
		// Creo il frame
		this.wvFramePrincipale = new JFrame("SheepLand : " + nomeGiocatore);
		// Definisco il comportamento della chiusura
		this.wvFramePrincipale.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Blocco il resize
		this.wvFramePrincipale.setResizable(false);
		// Definisco LeafElement dimensioni
		this.wvFramePrincipale.setSize(905, 725);

		this.wvFramePrincipale.setContentPane(this.caricaBackground());
		this.aggiungiBottoni();
		this.aggiungiTerreni();
		this.attivaViewCoordinate(this.wvFramePrincipale);
		this.attivaLavagna();

		// Carico i bottoni e le pecore
		this.creoZoneAttive();
		this.popolaConAnimali();

		this.preparaPedinePastore();

		this.wvFramePrincipale.setLayout(null);
		this.wvFramePrincipale.setVisible(true);
	}

	/**
	 * Aggiunge i bottoni terreni al lato
	 */
	private void aggiungiTerreni() {

		BufferedImage image;
		try {
			image = ImageIO.read(this.getClass().getResource("/PIANURA.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}

		this.btnPianura = new JButton(new ImageIcon(image));
		this.btnPianura.setSize(125, 80);
		this.btnPianura.setLocation(775, 90);
		this.btnPianura.setText("0");
		this.btnPianura.setOpaque(false);
		this.btnPianura.setContentAreaFilled(false);
		this.btnPianura.setBorderPainted(false);
		this.wvFramePrincipale.getContentPane().add(this.btnPianura);

		try {
			image = ImageIO.read(this.getClass().getResource("/COLLINA.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnCollina = new JButton(new ImageIcon(image));
		this.btnCollina.setSize(125, 80);
		this.btnCollina.setLocation(775, 180);
		this.btnCollina.setText("0");
		this.btnCollina.setOpaque(false);
		this.btnCollina.setContentAreaFilled(false);
		this.btnCollina.setBorderPainted(false);
		this.wvFramePrincipale.getContentPane().add(this.btnCollina);

		try {
			image = ImageIO.read(this.getClass().getResource("/LAGO.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnLago = new JButton(new ImageIcon(image));
		this.btnLago.setSize(125, 80);
		this.btnLago.setLocation(775, 270);
		this.btnLago.setText("0");
		this.btnLago.setOpaque(false);
		this.btnLago.setContentAreaFilled(false);
		this.btnLago.setBorderPainted(false);
		this.wvFramePrincipale.getContentPane().add(this.btnLago);

		try {
			image = ImageIO.read(this.getClass().getResource("/BOSCO.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnBosco = new JButton(new ImageIcon(image));
		this.btnBosco.setSize(125, 80);
		this.btnBosco.setLocation(775, 360);
		this.btnBosco.setText("0");
		this.btnBosco.setOpaque(false);
		this.btnBosco.setContentAreaFilled(false);
		this.btnBosco.setBorderPainted(false);
		this.wvFramePrincipale.getContentPane().add(this.btnBosco);

		try {
			image = ImageIO.read(this.getClass().getResource("/DESERTO.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnDeserto = new JButton(new ImageIcon(image));
		this.btnDeserto.setSize(125, 80);
		this.btnDeserto.setLocation(775, 450);
		this.btnDeserto.setText("0");
		this.btnDeserto.setOpaque(false);
		this.btnDeserto.setContentAreaFilled(false);
		this.btnDeserto.setBorderPainted(false);
		this.wvFramePrincipale.getContentPane().add(this.btnDeserto);

		try {
			image = ImageIO.read(this.getClass().getResource("/MONTAGNA.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnMontagna = new JButton(new ImageIcon(image));
		this.btnMontagna.setSize(125, 80);
		this.btnMontagna.setLocation(775, 540);
		this.btnMontagna.setText("0");
		this.btnMontagna.setOpaque(false);
		this.btnMontagna.setContentAreaFilled(false);
		this.btnMontagna.setBorderPainted(false);
		this.wvFramePrincipale.getContentPane().add(this.btnMontagna);

	}

	/**
	 * Aggiunge i bottoni per effettuare le mosse, e li rende invisibili
	 */
	private void aggiungiBottoni() {
		this.btnSpostamentoPastore = new JButton();
		this.btnSpostamentoPastore.setLocation(500, 150);
		this.btnSpostamentoPastore.setSize(150, 50);
		this.btnSpostamentoPastore.setText("Sposta pastore");
		this.btnSpostamentoPastore.setEnabled(false);
		this.btnSpostamentoPastore.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.atFasePosizionamentoPastori = false;
				GraficaSwing.this.atFaseDecisioneMossa = false;
				GraficaSwing.this.atFaseSpostamentoPastore = true;
				GraficaSwing.this.atFaseSpostamentoOvini = false;
				GraficaSwing.this.atFaseInvestimento = false;
				GraficaSwing.this.atFaseVenditaTerreni = false;
				GraficaSwing.this.atFaseAcquistoTerreno = false;
				GraficaSwing.this.atFaseAbbattimento = false;
				GraficaSwing.this.atFaseAccoppiamento = false;
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnSpostamentoPastore);

		this.btnSpostamentoOvino = new JButton();
		this.btnSpostamentoOvino.setLocation(500, 220);
		this.btnSpostamentoOvino.setSize(150, 50);
		this.btnSpostamentoOvino.setText("Sposta ovino");
		this.btnSpostamentoOvino.setEnabled(false);
		this.btnSpostamentoOvino.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.atFasePosizionamentoPastori = false;
				GraficaSwing.this.atFaseDecisioneMossa = false;
				GraficaSwing.this.atFaseSpostamentoPastore = false;
				GraficaSwing.this.atFaseSpostamentoOvini = true;
				GraficaSwing.this.atFaseInvestimento = false;
				GraficaSwing.this.atFaseVenditaTerreni = false;
				GraficaSwing.this.atFaseAcquistoTerreno = false;
				GraficaSwing.this.atFaseAbbattimento = false;
				GraficaSwing.this.atFaseAccoppiamento = false;
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnSpostamentoOvino);

		this.btnInvestimento = new JButton();
		this.btnInvestimento.setLocation(500, 290);
		this.btnInvestimento.setSize(150, 50);
		this.btnInvestimento.setText("Compra terreno");
		this.btnInvestimento.setEnabled(false);
		this.btnInvestimento.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.atFasePosizionamentoPastori = false;
				GraficaSwing.this.atFaseDecisioneMossa = false;
				GraficaSwing.this.atFaseSpostamentoPastore = false;
				GraficaSwing.this.atFaseSpostamentoOvini = false;
				GraficaSwing.this.atFaseInvestimento = true;
				GraficaSwing.this.atFaseVenditaTerreni = false;
				GraficaSwing.this.atFaseAcquistoTerreno = false;
				GraficaSwing.this.atFaseAbbattimento = false;
				GraficaSwing.this.atFaseAccoppiamento = false;
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnInvestimento);

		this.btnAccoppiamento = new JButton();
		this.btnAccoppiamento.setLocation(500, 360);
		this.btnAccoppiamento.setSize(150, 50);
		this.btnAccoppiamento.setText("Accoppiamento");
		this.btnAccoppiamento.setEnabled(false);
		this.btnAccoppiamento.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.atFasePosizionamentoPastori = false;
				GraficaSwing.this.atFaseDecisioneMossa = false;
				GraficaSwing.this.atFaseSpostamentoPastore = false;
				GraficaSwing.this.atFaseSpostamentoOvini = false;
				GraficaSwing.this.atFaseInvestimento = false;
				GraficaSwing.this.atFaseVenditaTerreni = false;
				GraficaSwing.this.atFaseAcquistoTerreno = false;
				GraficaSwing.this.atFaseAbbattimento = false;
				GraficaSwing.this.atFaseAccoppiamento = true;
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnAccoppiamento);

		this.btnAbbattimento = new JButton();
		this.btnAbbattimento.setLocation(500, 430);
		this.btnAbbattimento.setSize(150, 50);
		this.btnAbbattimento.setText("Abbattimento");
		this.btnAbbattimento.setEnabled(false);
		this.btnAbbattimento.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.atFasePosizionamentoPastori = false;
				GraficaSwing.this.atFaseDecisioneMossa = false;
				GraficaSwing.this.atFaseSpostamentoPastore = false;
				GraficaSwing.this.atFaseSpostamentoOvini = false;
				GraficaSwing.this.atFaseInvestimento = false;
				GraficaSwing.this.atFaseVenditaTerreni = false;
				GraficaSwing.this.atFaseAcquistoTerreno = false;
				GraficaSwing.this.atFaseAbbattimento = true;
				GraficaSwing.this.atFaseAccoppiamento = false;
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnAbbattimento);
		BufferedImage image;
		this.btnScegliPecora = new JButton();
		this.btnScegliPecora.setLocation(500, 610);
		this.btnScegliPecora.setSize(80, 80);
		try {
			image = ImageIO
					.read(this.getClass().getResource("/tipoPecora.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnScegliPecora.setIcon(new ImageIcon(image));
		this.btnScegliPecora.setOpaque(false);
		this.btnScegliPecora.setContentAreaFilled(false);
		this.btnScegliPecora.setBorderPainted(false);
		this.btnScegliPecora.setEnabled(false);
		this.btnScegliPecora.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.animaleHelper.setTipoAnimale(new Pecora(0,
						null));
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnScegliPecora);

		this.btnScegliPecoraNera = new JButton();
		this.btnScegliPecoraNera.setLocation(590, 610);
		this.btnScegliPecoraNera.setSize(80, 80);
		try {
			image = ImageIO.read(this.getClass().getResource("/tipoNera.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnScegliPecoraNera.setIcon(new ImageIcon(image));
		this.btnScegliPecoraNera.setOpaque(false);
		this.btnScegliPecoraNera.setContentAreaFilled(false);
		this.btnScegliPecoraNera.setBorderPainted(false);
		this.btnScegliPecoraNera.setEnabled(false);
		this.btnScegliPecoraNera.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.animaleHelper.setTipoAnimale(new Pecoranera(
						0, null));
			}
		});
		this.wvFramePrincipale.getContentPane().add(this.btnScegliPecoraNera);

		this.btnScegliPecoraMontone = new JButton();
		this.btnScegliPecoraMontone.setLocation(680, 610);
		this.btnScegliPecoraMontone.setSize(80, 80);
		try {
			image = ImageIO.read(this.getClass()
					.getResource("/tipoMontone.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnScegliPecoraMontone.setIcon(new ImageIcon(image));
		this.btnScegliPecoraMontone.setOpaque(false);
		this.btnScegliPecoraMontone.setContentAreaFilled(false);
		this.btnScegliPecoraMontone.setBorderPainted(false);
		this.btnScegliPecoraMontone.setEnabled(false);
		this.btnScegliPecoraMontone.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.animaleHelper.setTipoAnimale(new Montone(0,
						null));
			}
		});
		this.wvFramePrincipale.getContentPane()
				.add(this.btnScegliPecoraMontone);

		this.btnScegliPecoraAgnello = new JButton();
		this.btnScegliPecoraAgnello.setLocation(770, 610);
		this.btnScegliPecoraAgnello.setSize(80, 80);
		try {
			image = ImageIO.read(this.getClass()
					.getResource("/tipoAgnello.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.btnScegliPecoraAgnello.setIcon(new ImageIcon(image));
		this.btnScegliPecoraAgnello.setOpaque(false);
		this.btnScegliPecoraAgnello.setContentAreaFilled(false);
		this.btnScegliPecoraAgnello.setBorderPainted(false);
		this.btnScegliPecoraAgnello.setEnabled(false);
		this.btnScegliPecoraAgnello.setEnabled(false);
		this.btnScegliPecoraAgnello.addActionListener(new ActionListener() {

			/**
			 * Evento di clic
			 */
			public void actionPerformed(ActionEvent e) {
				GraficaSwing.this.animaleHelper.setTipoAnimale(new Agnello(0,
						null));
			}
		});
		this.wvFramePrincipale.getContentPane()
				.add(this.btnScegliPecoraAgnello);
	}

	/**
	 * Prepara le pedine pastore e le posiziona in basso a sinistar sulla
	 * finestra di gioco
	 */
	private void preparaPedinePastore() {
		for (Giocatore giocatore : this.partitaSwing.getMappa().getGiocatori()) {
			for (PedinaPastore pedina : giocatore.getPedinePastore()) {
				String path = "/" + pedina.getColore().toString() + ".png";
				BufferedImage image;
				try {
					image = ImageIO.read(this.getClass().getResource(path));
				} catch (IOException e) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
					return;
				}
				IconaSpostabile icona = new IconaSpostabile(
						new ImageIcon(image), pedina.getId(),
						pedina.getEColore());
				this.iconePastori.add(icona);
				icona.setLocation(10, 650);
				this.wvFramePrincipale.getContentPane().add(icona);
				this.wvFramePrincipale.repaint();
				this.wvFramePrincipale.revalidate();
			}
		}
	}

	/**
	 * Attiva e posiziona la lavagna di gioco dove vengono mostrate le fasi di
	 * gioco
	 */
	private void attivaLavagna() {
		this.lblLavagna = new JLabel();
		this.lblLavagna.setLocation(500, 0);
		this.lblLavagna.setSize(350, 50);
		this.wvFramePrincipale.getContentPane().add(this.lblLavagna);

		BufferedImage image;
		try {
			image = ImageIO.read(this.getClass().getResource(
					"/"
							+ this.partitaSwing.getGiocatore()
									.getPedinePastore()[0].getColore()
									.toString() + ".png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.lblIdentitaGiocatore = new IconaSpostabile(
				("Denaro: " + this.partitaSwing.getGiocatore()
						.getDanaroResiduo()), new ImageIcon(image),
				JLabel.HORIZONTAL);
		this.lblIdentitaGiocatore.setLocation(500, 45);
		this.wvFramePrincipale.getContentPane().add(this.lblIdentitaGiocatore);

		try {
			image = ImageIO.read(this.getClass().getResource("/recinto.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.lblRecinti = new IconaSpostabile(("Recinti: " + this.partitaSwing
				.getMappa().getRecintiAttuali()), new ImageIcon(image),
				JLabel.HORIZONTAL);
		this.lblRecinti.setLocation(600, 45);
		this.wvFramePrincipale.getContentPane().add(this.lblRecinti);

		try {
			image = ImageIO.read(this.getClass().getResource("/recinto.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		this.lblRecintiFinali = new IconaSpostabile(
				("Recinti Finali: " + this.partitaSwing.getMappa()
						.getRecintiFinaliAttuali()), new ImageIcon(image),
				JLabel.HORIZONTAL, 150);
		this.lblRecintiFinali.setLocation(700, 45);
		this.wvFramePrincipale.getContentPane().add(this.lblRecintiFinali);
	}

	/**
	 * Cicla aspettando le mosse dell'avversario o capendo che è il momento di
	 * posizionare una pedina, quando inizia la partita termina
	 */
	public void richiediPosizionamentoPedina() {

		while (true) {
			this.atFasePosizionamentoPastori = false;
			try {
				this.lblLavagna
						.setText("In attesa della mossa dell'avversario");
				Posizionamento mossaAvversario = this.partitaSwing
						.getMossaPosizionamento();
				if (mossaAvversario == null) {
					continue;
				}
				this.lblLavagna
						.setText("Un avversario ha posizionato il proprio pastore");
				this.partitaSwing.posizionaPedinaPastore(mossaAvversario);
				this.posizionaPedinaSuMappa(mossaAvversario);
			} catch (TurnoPersonaleException e) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
				// Questo è il mio turno
				this.lblLavagna
						.setText("Posiziona la tua pedina pastore su una delle strade libere...");
				this.atFasePosizionamentoPastori = true;
				for (PedinaPastore pedina : this.partitaSwing.getGiocatore()
						.getPedinePastore()) {
					this.atFasePosizionamentoPastori = true;
					Integer scelta = this.swingHelper.getIdCliccato();
					try {
						this.partitaSwing.inviaMossaPosizionamento(
								this.partitaSwing.getGiocatore().getId(),
								pedina.getId(), scelta);
					} catch (DisconnessioneException e1) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
						JOptionPane
								.showMessageDialog(null,
										"Ti sei disconnesso, chiudi e riapri il client");
					}
					this.posizionaPedinaSuMappa(new Posizionamento(
							this.partitaSwing.getGiocatore().getId(), pedina
									.getId(), scelta));
				}
				this.atFasePosizionamentoPastori = false;
			} catch (InizioPartitaException e) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
				this.lblLavagna.setText("La partita sta per iniziare!");
				break;
			} catch (DisconnessioneException e) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
				JOptionPane.showMessageDialog(null,
						"Ti sei disconnesso, chiudi e riapri il client");
			}
		}

	}

	/**
	 * Posizione una pedina pastore in base alla mossa di posizionamento passata
	 * 
	 * @param mossaAvversario
	 *            la mossa posizionamento passata
	 */
	private void posizionaPedinaSuMappa(Posizionamento mossaAvversario) {
		for (IconaSpostabile iconaPastore : this.iconePastori) {
			if (iconaPastore.getIdPastore().equals(
					mossaAvversario.getIdPedina())) {
				Integer wvIdStradaScelta = (this.partitaSwing.getMappa()
						.getStrada(mossaAvversario.getIdStradaScelta()).getId() / 100) - 1;
				this.avviaTransizioneMovimento(
						iconaPastore,
						iconaPastore.getX(),
						iconaPastore.getY(),
						Integer.parseInt(this.coordinateStrade.get(
								wvIdStradaScelta).split("\\,")[0]),
						Integer.parseInt(this.coordinateStrade.get(
								wvIdStradaScelta).split("\\,")[1]), false);
				break;
			}
		}
	}

	/**
	 * Genera una transizione di movimento
	 * 
	 * @param oggettoDaSpostare
	 *            l'oggetto da spostare (oggetti supportati:
	 *            {@link IconaSpostabile} etc...)
	 * @param xIniziale
	 *            la posizione x iniziale dell'oggetto
	 * @param yIniziale
	 *            la posizione y iniziale dell'oggetto
	 * @param xFinale
	 *            la posizione x finale dell'oggetto
	 * @param yFinale
	 *            la posizione y finale dell'oggetto
	 */
	private void avviaTransizioneMovimento(Object oggettoDaSpostare,
			int xIniziale, int yIniziale, int xFinale, int yFinale,
			boolean rimuovereAllaFine) {
		AnimazioneSpostamento spostamento = new AnimazioneSpostamento(
				this.wvFramePrincipale, oggettoDaSpostare, xIniziale,
				yIniziale, xFinale, yFinale, rimuovereAllaFine);
		spostamento.start();
	}

	/**
	 * Genera una trasizione con effetto zoom/slide
	 * 
	 * @param x
	 * @param y
	 * @param url
	 */
	private void avviaTransizioneZoom(int x, int y, String url) {
		BufferedImage image;
		try {
			image = ImageIO.read(this.getClass().getResource("/" + url));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return;
		}
		IconaSpostabile iconaSpostabile = new IconaSpostabile(new ImageIcon(
				image));
		iconaSpostabile.setLocation(x, y);
		AnimazioneZoom animazioneZoom = new AnimazioneZoom(
				this.wvFramePrincipale, x, y, iconaSpostabile);
		animazioneZoom.start();

	}

	JLabel lblMousePosition;

	/**
	 * Attiva una label chje mostra le coordinate del mouse sulla mappa
	 * 
	 * @param wvFramePrincipale
	 */
	private void attivaViewCoordinate(JFrame wvFramePrincipale) {
		this.lblMousePosition = new JLabel();
		this.lblMousePosition.setLocation(25, 25);
		this.lblMousePosition.setSize(200, 50);
		wvFramePrincipale.getContentPane().add(this.lblMousePosition);

		wvFramePrincipale.addMouseMotionListener(new MouseMotionListener() {

			/**
			 * Evento di mosso
			 */
			public void mouseMoved(MouseEvent e) {
				GraficaSwing.this.lblMousePosition.setText("mouse coordinate: "
						+ e.getX() + "," + e.getY());
			}

			/**
			 * Evento trascinamento
			 */
			public void mouseDragged(MouseEvent e) {

			}
		});
	}

	private ArrayList<JLabel> lblAnimaliInRegione;

	/**
	 * Popola la regione con pedine e label
	 */
	private void popolaConAnimali() {
		this.lblAnimaliInRegione = new ArrayList<JLabel>();
		for (Regione regione : this.partitaSwing.getMappa().getRegioni()) {
			IconaSpostabile icon = null;
			if (regione.getTipoTerreno().equals(ETipoTerreno.SHEEPSBURG)) {
				this.lblAnimaliInRegione.add(null);
				for (Animale animale : regione.getAnimali()) {
					if (animale instanceof Pecoranera) {
						String position = this.coordinatePecoraNera
								.get(this.partitaSwing.getMappa().getRegioni()
										.indexOf(regione));
						int x = Integer.parseInt(position.split("\\,")[0]);
						int y = Integer.parseInt(position.split("\\,")[1]);
						BufferedImage image;
						try {
							image = ImageIO.read(this.getClass().getResource(
									"/pecoraNera.png"));
						} catch (IOException e) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
							return;
						}
						icon = new IconaSpostabile(new ImageIcon(image),
								animale.getId());
						icon.setLocation(x, y);
						icon.setSize(30, 30);
						this.atPedinaPecoraNera = icon;

					} else {
						String position = this.coordinateLupo
								.get(this.partitaSwing.getMappa().getRegioni()
										.indexOf(regione));
						int x = Integer.parseInt(position.split("\\,")[0]);
						int y = Integer.parseInt(position.split("\\,")[1]);
						BufferedImage image;
						try {
							image = ImageIO.read(this.getClass().getResource(
									"/lupo.png"));
						} catch (IOException e) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
							return;
						}
						icon = new IconaSpostabile(new ImageIcon(image),
								animale.getId());
						icon.setLocation(x, y);
						icon.setSize(30, 30);
						this.atPedinaLupo = icon;

					}
					if (icon != null) {
						this.wvFramePrincipale.getContentPane().add(icon);
					}
				}

			}
			if (regione.getAnimali().size() > 0) {
				if (regione.occupataDaOviniNonNeri()) {
					String position = this.coordinatePecore
							.get(this.partitaSwing.getMappa().getRegioni()
									.indexOf(regione));
					int x = Integer.parseInt(position.split("\\,")[0]);
					int y = Integer.parseInt(position.split("\\,")[1]);
					String situazionreRegione = "P:"
							+ regione.getNumeroPecore() + "M:"
							+ regione.getNumeroMontoni() + "A:"
							+ regione.getNumeroAgnelli();
					BufferedImage image;
					try {
						image = ImageIO.read(this.getClass().getResource(
								"/pecora.png"));
					} catch (IOException e) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
						return;
					}
					icon = new IconaSpostabile(situazionreRegione,
							new ImageIcon(image), JLabel.CENTER);
					icon.setLocation(x, y);

					this.lblAnimaliInRegione.add(icon);

				}

				if (icon != null) {
					this.wvFramePrincipale.getContentPane().add(icon);
				}
			}

		}
	}

	/**
	 * Carica le coordinate da file e piazza dei bottoni sopra le aree attive
	 * della mappa
	 * 
	 * @param wvFramePrincipale
	 *            il frame su cui agire con già l'immagine caricata
	 */
	private void creoZoneAttive() {
		this.posizionaStrade();
		this.posizionaRegioni();

	}

	ActionEvent event;

	/**
	 * Posiziona dei bottoni rappresentatnti le regioni sopra la mappa
	 * 
	 * @param wvFramePrincipale
	 * @param partita
	 */
	private void posizionaRegioni() {
		for (int i = 0; i < this.coordinateRegioni.size(); i++) {
			String wvRiga = this.coordinateRegioni.get(i);

			JButton wvRegioneButton = new JButton("");
			wvRegioneButton.setBounds(Integer.parseInt(wvRiga.split("\\,")[0]),
					Integer.parseInt(wvRiga.split("\\,")[1]),
					Integer.parseInt(wvRiga.split("\\,")[2]),
					Integer.parseInt(wvRiga.split("\\,")[3]));
			wvRegioneButton.setName("" + i);
			wvRegioneButton.setOpaque(false);
			wvRegioneButton.setContentAreaFilled(false);
			wvRegioneButton.setBorderPainted(false);
			wvRegioneButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					GraficaSwing.this.event = evt;
					if (GraficaSwing.this.atFaseFinePartita) {
						return;
					}
					Thread thread = new Thread(new Runnable() {

						/**
						 * run
						 */
						public void run() {
							Integer indiceCliccato = Integer
									.parseInt(((JButton) GraficaSwing.this.event
											.getSource()).getName());
							Animale ovinoCliccato = null;
							Regione regioneCliccata = GraficaSwing.this.partitaSwing
									.getMappa()
									.getRegione(
											(((indiceCliccato + 1) * 100) + ETipoID.REGIONE
													.ordinal()));
							if (GraficaSwing.this.atFaseSpostamentoOvini) {
								Regione regioneOpposta = GraficaSwing.this.pedinaSelezionata
										.getStradaOccupata().getRegioneOpposta(
												regioneCliccata);
								IMossa mossaSpostamento = null;
								if (regioneCliccata
										.occupataDaAnimaliSpostabili()) {
									GraficaSwing.this.socketThreadHelper
											.setLocked(null);
									GraficaSwing.this.lblLavagna
											.setText("Che tipo di animale vuoi spostare?");
									GraficaSwing.this.attivaBottoniTipoAnimale(
											regioneCliccata, false);
									try {
										Thread.sleep(20);
									} catch (InterruptedException e) {
										Logger.getGlobal().log(Level.WARNING,
												this.getClass().toString(), e);
									}
									GraficaSwing.this.atTipoAnimaleCliccato = GraficaSwing.this.animaleHelper
											.getTipoAnimale();
									for (Animale animale : regioneCliccata
											.getAnimaliSpostabili()) {
										if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Pecoranera) {
											if (animale instanceof Pecoranera) {
												ovinoCliccato = animale;
												mossaSpostamento = GraficaSwing.this
														.recuperaMossaSpostamentoOvino(
																ovinoCliccato
																		.getId(),
																ovinoCliccato
																		.getRegioneOccuapta()
																		.getId(),
																regioneOpposta
																		.getId());
												if (mossaSpostamento == null) {
													return;
												}
												break;
											}
											String posizioneFinale = GraficaSwing.this.coordinatePecoraNera.get((regioneOpposta
													.getId() / 100) - 1);
											GraficaSwing.this
													.avviaTransizioneMovimento(
															GraficaSwing.this.atPedinaPecoraNera,
															GraficaSwing.this.atPedinaPecoraNera
																	.getX(),
															GraficaSwing.this.atPedinaPecoraNera
																	.getY(),
															Integer.parseInt(posizioneFinale
																	.split("\\,")[0]),
															Integer.parseInt(posizioneFinale
																	.split("\\,")[1]),
															false);
										} else if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Pecora) {
											if (((animale instanceof Pecora) && !(animale instanceof Pecoranera))) {
												ovinoCliccato = animale;

												mossaSpostamento = GraficaSwing.this
														.recuperaMossaSpostamentoOvino(
																ovinoCliccato
																		.getId(),
																ovinoCliccato
																		.getRegioneOccuapta()
																		.getId(),
																regioneOpposta
																		.getId());
												if (mossaSpostamento == null) {
													return;
												}
												BufferedImage image;
												try {
													image = ImageIO
															.read(this
																	.getClass()
																	.getResource(
																			"/pecora.png"));
												} catch (IOException e) {
													Logger.getGlobal()
															.log(Level.WARNING,
																	this.getClass()
																			.toString(),
																	e);
													return;
												}
												IconaSpostabile iconaSpostabile = new IconaSpostabile(
														new ImageIcon(image));
												iconaSpostabile.setLocation(
														Integer.parseInt(GraficaSwing.this.coordinatePecore
																.get(indiceCliccato)
																.split("\\,")[0]),
														Integer.parseInt(GraficaSwing.this.coordinatePecore
																.get(indiceCliccato)
																.split("\\,")[1]));
												iconaSpostabile.setSize(30, 30);
												GraficaSwing.this.wvFramePrincipale
														.getContentPane()
														.add(iconaSpostabile);

												String posizioneFinale = GraficaSwing.this.coordinatePecore.get((regioneOpposta
														.getId() / 100) - 1);
												GraficaSwing.this
														.avviaTransizioneMovimento(
																iconaSpostabile,
																Integer.parseInt(GraficaSwing.this.coordinatePecore
																		.get(indiceCliccato)
																		.split("\\,")[0]),
																Integer.parseInt(GraficaSwing.this.coordinatePecore
																		.get(indiceCliccato)
																		.split("\\,")[1]),
																Integer.parseInt(posizioneFinale
																		.split("\\,")[0]),
																Integer.parseInt(posizioneFinale
																		.split("\\,")[1]),
																true);

											}
										} else if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Montone) {
											if (animale instanceof Montone) {
												ovinoCliccato = animale;

												mossaSpostamento = GraficaSwing.this
														.recuperaMossaSpostamentoOvino(
																ovinoCliccato
																		.getId(),
																ovinoCliccato
																		.getRegioneOccuapta()
																		.getId(),
																regioneOpposta
																		.getId());
												if (mossaSpostamento == null) {
													return;
												}
												BufferedImage image;
												try {
													image = ImageIO
															.read(this
																	.getClass()
																	.getResource(
																			"/pecora.png"));
												} catch (IOException e) {
													Logger.getGlobal()
															.log(Level.WARNING,
																	this.getClass()
																			.toString(),
																	e);
													return;
												}
												IconaSpostabile iconaSpostabile = new IconaSpostabile(
														new ImageIcon(image));
												iconaSpostabile.setLocation(
														Integer.parseInt(GraficaSwing.this.coordinatePecore
																.get(indiceCliccato)
																.split("\\,")[0]),
														Integer.parseInt(GraficaSwing.this.coordinatePecore
																.get(indiceCliccato)
																.split("\\,")[1]));
												iconaSpostabile.setSize(30, 30);
												GraficaSwing.this.wvFramePrincipale
														.getContentPane()
														.add(iconaSpostabile);

												String posizioneFinale = GraficaSwing.this.coordinatePecore.get((regioneOpposta
														.getId() / 100) - 1);
												GraficaSwing.this
														.avviaTransizioneMovimento(
																iconaSpostabile,
																Integer.parseInt(GraficaSwing.this.coordinatePecore
																		.get(indiceCliccato)
																		.split("\\,")[0]),
																Integer.parseInt(GraficaSwing.this.coordinatePecore
																		.get(indiceCliccato)
																		.split("\\,")[1]),
																Integer.parseInt(posizioneFinale
																		.split("\\,")[0]),
																Integer.parseInt(posizioneFinale
																		.split("\\,")[1]),
																true);

											}
										} else if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Agnello) {
											if (animale instanceof Agnello) {
												ovinoCliccato = animale;

												mossaSpostamento = GraficaSwing.this
														.recuperaMossaSpostamentoOvino(
																ovinoCliccato
																		.getId(),
																ovinoCliccato
																		.getRegioneOccuapta()
																		.getId(),
																regioneOpposta
																		.getId());
												if (mossaSpostamento == null) {
													return;
												}
												BufferedImage image;
												try {
													image = ImageIO
															.read(this
																	.getClass()
																	.getResource(
																			"/pecora.png"));
												} catch (IOException e) {
													Logger.getGlobal()
															.log(Level.WARNING,
																	this.getClass()
																			.toString(),
																	e);
													return;
												}
												IconaSpostabile iconaSpostabile = new IconaSpostabile(
														new ImageIcon(image));
												iconaSpostabile.setLocation(
														Integer.parseInt(GraficaSwing.this.coordinatePecore
																.get(indiceCliccato)
																.split("\\,")[0]),
														Integer.parseInt(GraficaSwing.this.coordinatePecore
																.get(indiceCliccato)
																.split("\\,")[1]));
												iconaSpostabile.setSize(30, 30);
												GraficaSwing.this.wvFramePrincipale
														.getContentPane()
														.add(iconaSpostabile);

												String posizioneFinale = GraficaSwing.this.coordinatePecore.get((regioneOpposta
														.getId() / 100) - 1);
												GraficaSwing.this
														.avviaTransizioneMovimento(
																iconaSpostabile,
																Integer.parseInt(GraficaSwing.this.coordinatePecore
																		.get(indiceCliccato)
																		.split("\\,")[0]),
																Integer.parseInt(GraficaSwing.this.coordinatePecore
																		.get(indiceCliccato)
																		.split("\\,")[1]),
																Integer.parseInt(posizioneFinale
																		.split("\\,")[0]),
																Integer.parseInt(posizioneFinale
																		.split("\\,")[1]),
																true);

											}
										}
									}

									GraficaSwing.this.swingHelper
											.setIdCliccato(GraficaSwing.this.atContenitoreMossePossibi
													.get(GraficaSwing.this.atIndicePedina)
													.indexOf(mossaSpostamento));

									GraficaSwing.this.atFaseSpostamentoOvini = false;
								}

								GraficaSwing.this.socketThreadHelper
										.setLocked(false);
							} else if (GraficaSwing.this.atFaseAccoppiamento) {
								IMossa mossaAccoppiamento = null;
								if (regioneCliccata.occupataDaMontoni()
										&& regioneCliccata
												.occupataDaPecoraNonNera()) {
									// accoppiamento possibile
									mossaAccoppiamento = GraficaSwing.this
											.recuperaMossaAccoppiamento(regioneCliccata
													.getId());
									GraficaSwing.this.avviaTransizioneZoom(
											Integer.parseInt(GraficaSwing.this.coordinatePecore
													.get((regioneCliccata
															.getId() / 100) - 1)
													.split("\\,")[0]),
											Integer.parseInt(GraficaSwing.this.coordinatePecore
													.get((regioneCliccata
															.getId() / 100) - 1)
													.split("\\,")[1]),
											"accoppiamento.png");
								}
								if (mossaAccoppiamento == null) {
									return;
								}
								GraficaSwing.this.swingHelper
										.setIdCliccato(GraficaSwing.this.atContenitoreMossePossibi
												.get(GraficaSwing.this.atIndicePedina)
												.indexOf(mossaAccoppiamento));
								GraficaSwing.this.atFaseAccoppiamento = false;
							} else if (GraficaSwing.this.atFaseAbbattimento) {
								IMossa mossaAbbattimento = null;
								PedinaPastore pedinaPastore = GraficaSwing.this.partitaSwing
										.getGiocatore().getPedinePastore()[GraficaSwing.this.atIndicePedina];
								Strada stradaOccupata = pedinaPastore
										.getStradaOccupata();
								ArrayList<Integer> pastoriVicini = stradaOccupata
										.getPastoriVicini(GraficaSwing.this.partitaSwing
												.getGiocatore());
								Animale animaleSelezionato = null;
								GraficaSwing.this.socketThreadHelper
										.setLocked(null);
								GraficaSwing.this.lblLavagna
										.setText("Che tipo di animale vuoi spostare?");
								GraficaSwing.this.attivaBottoniTipoAnimale(
										regioneCliccata, true);
								GraficaSwing.this.atTipoAnimaleCliccato = GraficaSwing.this.animaleHelper
										.getTipoAnimale();

								if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Pecora) {
									animaleSelezionato = regioneCliccata
											.getPecora();
								} else if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Montone) {
									animaleSelezionato = regioneCliccata
											.getMontone();
								} else if (GraficaSwing.this.atTipoAnimaleCliccato instanceof Agnello) {
									animaleSelezionato = regioneCliccata
											.getAgnello();
								}

								if (animaleSelezionato == null) {
									return;
								}

								mossaAbbattimento = GraficaSwing.this
										.recuperaMossaAbbattimento(
												regioneCliccata.getId(),
												pedinaPastore.getId(),
												pastoriVicini,
												animaleSelezionato.getId());
								GraficaSwing.this.swingHelper
										.setIdCliccato(GraficaSwing.this.atContenitoreMossePossibi
												.get(GraficaSwing.this.atIndicePedina)
												.indexOf(mossaAbbattimento));
								GraficaSwing.this.socketThreadHelper
										.setLocked(false);
								GraficaSwing.this.atFaseAbbattimento = false;
							} else if (GraficaSwing.this.atFaseInvestimento) {

								IMossa mossaInvestimento = GraficaSwing.this
										.recuperaMossaInvestimento(
												GraficaSwing.this.partitaSwing
														.getGiocatore().getId(),
												regioneCliccata
														.getTipoTerreno());

								if (mossaInvestimento == null) {
									JOptionPane
											.showMessageDialog(null,
													"Non puoi effettuare questa mossa!");
									return;
								}
								GraficaSwing.this.swingHelper
										.setIdCliccato(GraficaSwing.this.atContenitoreMossePossibi
												.get(GraficaSwing.this.atIndicePedina)
												.indexOf(mossaInvestimento));
								GraficaSwing.this.atFaseInvestimento = false;
							} else {
								String animaliString = GraficaSwing.this.partitaSwing
										.getMappa()
										.getRegione(
												((Integer
														.parseInt(((JButton) GraficaSwing.this.event
																.getSource())
																.getName()) + 1) * 100)
														+ ETipoID.REGIONE
																.ordinal())
										.getAnimali().toString();
								JOptionPane
										.showMessageDialog(
												null,
												"Regione indice: "
														+ ((JButton) GraficaSwing.this.event
																.getSource())
																.getName()
														+ "\n\n"
														+ animaliString);
							}
						}

					});

					thread.start();
				}
			});
			this.wvFramePrincipale.getContentPane().add(wvRegioneButton);

		}
	}

	/**
	 * Posiziona dei bottoni rappresentanti le strade sopra la mappa
	 * 
	 * @param wvFramePrincipale
	 * @param partita
	 */
	private void posizionaStrade() {

		if (this.atFaseFinePartita) {
			return;
		}
		for (int i = 0; i < this.coordinateStrade.size(); i++) {
			String wvRiga = this.coordinateStrade.get(i);

			JButton wvStradaButton = new JButton("");
			wvStradaButton.setBounds(Integer.parseInt(wvRiga.split("\\,")[0]),
					Integer.parseInt(wvRiga.split("\\,")[1]), 25, 25);
			wvStradaButton.setName("" + i);
			wvStradaButton.setOpaque(false);
			wvStradaButton.setContentAreaFilled(false);
			wvStradaButton.setBorderPainted(false);
			wvStradaButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					Integer wvIdInArray = Integer.parseInt(((JButton) evt
							.getSource()).getName());
					Integer wvIdStradaCliccata = ((wvIdInArray + 1) * 100)
							+ ETipoID.STRADA.ordinal();

					if (GraficaSwing.this.atFasePosizionamentoPastori) {

						if (GraficaSwing.this.partitaSwing
								.getMappa()
								.getStradeLibere()
								.contains(
										GraficaSwing.this.partitaSwing
												.getMappa().getStrada(
														wvIdStradaCliccata))) {
							GraficaSwing.this.swingHelper
									.setIdCliccato(wvIdStradaCliccata);
						} else {
							JOptionPane.showMessageDialog(null,
									"Strada già occupata!");
						}

						GraficaSwing.this.atFasePosizionamentoPastori = false;
					} else if (GraficaSwing.this.atFaseSceltaPedina) {
						Strada stradaSelezionata = GraficaSwing.this.partitaSwing
								.getMappa().getStrada(wvIdStradaCliccata);
						if (stradaSelezionata.occupadaDaPastore()) {
							if (((PedinaPastore) stradaSelezionata
									.getOccupante()).getGiocatorePossessore()
									.equals(GraficaSwing.this.partitaSwing
											.getGiocatore())) {
								// la pedina è mia, posso selezionarla!
								GraficaSwing.this.pedinaSelezionata = ((PedinaPastore) stradaSelezionata
										.getOccupante());
							}
							for (int i = 0; i < GraficaSwing.this.partitaSwing
									.getGiocatore().getPedinePastore().length; i++) {
								if (GraficaSwing.this.partitaSwing
										.getGiocatore().getPedinePastore()[i]
										.equals(GraficaSwing.this.pedinaSelezionata)) {
									GraficaSwing.this.swingHelper
											.setIdCliccato(i);
									return;
								}
							}
							GraficaSwing.this.swingHelper.setIdCliccato(-1);
						} else {
							GraficaSwing.this.swingHelper.setIdCliccato(-1);
						}
					} else if (GraficaSwing.this.atFaseSpostamentoPastore) {
						PedinaPastore pedinaDaSpostare = GraficaSwing.this.pedinaSelezionata;
						Strada stradaSelezionata = GraficaSwing.this.partitaSwing
								.getMappa().getStrada(wvIdStradaCliccata);

						IMossa mossaSpostamento = GraficaSwing.this
								.recuperaMossaSpostamentoPastore(
										pedinaDaSpostare.getId(),
										pedinaDaSpostare.getStradaOccupata()
												.getId(), stradaSelezionata
												.getId());
						if (mossaSpostamento == null) {
							JOptionPane.showMessageDialog(null, "Non puoi!");
							return;
						}
						IconaSpostabile iconaSpostabile = null;
						for (IconaSpostabile icona : GraficaSwing.this.iconePastori) {
							if (icona.getIdPastore().equals(
									pedinaDaSpostare.getId())) {
								iconaSpostabile = icona;
								break;
							}
						}
						if (iconaSpostabile == null) {
							return;
						}

						if (!stradaSelezionata.Occupata()) {
							String posizioneFinale = GraficaSwing.this.coordinateStrade
									.get(wvIdInArray);
							int xAttuale = iconaSpostabile.getX(), yAttuale = iconaSpostabile
									.getY();
							GraficaSwing.this.avviaTransizioneMovimento(
									iconaSpostabile, xAttuale, yAttuale,
									Integer.parseInt(posizioneFinale
											.split("\\,")[0]), Integer
											.parseInt(posizioneFinale
													.split("\\,")[1]), false);

							IconaSpostabile icona;
							BufferedImage image;

							if (GraficaSwing.this.partitaSwing.getMappa()
									.getRecintiAttuali() > 0) {
								try {
									image = ImageIO.read(this.getClass()
											.getResource("/recinto.png"));
								} catch (IOException e) {
									Logger.getGlobal().log(Level.WARNING,
											this.getClass().toString(), e);
									return;
								}
								icona = new IconaSpostabile(
										new ImageIcon(image));
							} else {
								try {
									image = ImageIO.read(this.getClass()
											.getResource("/recintoFinale.png"));
								} catch (IOException e) {
									Logger.getGlobal().log(Level.WARNING,
											this.getClass().toString(), e);
									return;
								}
								icona = new IconaSpostabile(
										new ImageIcon(image));
							}

							icona.setLocation(xAttuale, yAttuale);
							icona.setSize(30, 30);
							GraficaSwing.this.wvFramePrincipale
									.getContentPane().add(icona);
						}

						GraficaSwing.this.swingHelper
								.setIdCliccato(GraficaSwing.this.atContenitoreMossePossibi
										.get(GraficaSwing.this.atIndicePedina)
										.indexOf(mossaSpostamento));

						GraficaSwing.this.atFaseSpostamentoPastore = false;
					} else {
						JOptionPane.showMessageDialog(null, "Strada indice: "
								+ ((JButton) evt.getSource()).getName());
					}

				}

			});
			this.wvFramePrincipale.getContentPane().add(wvStradaButton);

			/*
			 * JLabel label = new JLabel(""+i);
			 * label.setLocation(Integer.parseInt(wvRiga.split("\\,")[0]),
			 * Integer.parseInt(wvRiga.split("\\,")[1])); label.setSize(30, 30);
			 * 
			 * wvFramePrincipale.getContentPane().add(label);
			 */
		}
	}

	private Regione copiaRegioneCliccata;

	/**
	 * Attiva i bottoni per scegliere il tipo di animale
	 * 
	 * @param regioneCliccata
	 * @param isFaseAbbattimento
	 */
	private void attivaBottoniTipoAnimale(Regione regioneCliccata,
			boolean isFaseAbbattimento) {
		this.copiaRegioneCliccata = regioneCliccata;
		this.lblLavagna.setText("Scegli il tipo di animale!!!");
		this.btnScegliPecora.setEnabled(false);
		this.btnScegliPecoraNera.setEnabled(false);
		this.btnScegliPecoraMontone.setEnabled(false);
		this.btnScegliPecoraAgnello.setEnabled(false);

		for (Animale animale : this.copiaRegioneCliccata.getAnimali()) {
			if ((animale instanceof Pecoranera) && !isFaseAbbattimento) {
				this.btnScegliPecoraNera.setEnabled(true);
			} else if ((animale instanceof Pecora)
					&& !(animale instanceof Pecoranera)) {
				this.btnScegliPecora.setEnabled(true);
			} else if (animale instanceof Montone) {
				this.btnScegliPecoraMontone.setEnabled(true);
			} else if (animale instanceof Agnello) {
				this.btnScegliPecoraAgnello.setEnabled(true);
			}
		}
		this.wvFramePrincipale.repaint();
		this.wvFramePrincipale.revalidate();
	}

	/**
	 * Ritorna la mossa accoppiamento corrispondente ai parametri raccolti dalla
	 * grafica
	 * 
	 * @param idRegione
	 * @return
	 */
	private IMossa recuperaMossaAccoppiamento(Integer idRegione) {
		for (IMossa mossa : this.atContenitoreMossePossibi
				.get(this.atIndicePedina)) {
			if (mossa instanceof Accoppiamento) {
				Accoppiamento accoppiamento = (Accoppiamento) mossa;
				if (accoppiamento.getIdRegione().equals(idRegione)) {
					return accoppiamento;
				}
			}
		}
		return null;
	}

	/**
	 * Recupera le mosse di abbattimento dati i paramentri provenienti dalla
	 * grafica
	 * 
	 * @param idRegione
	 * @param idPastore
	 * @param idPastoriVicini
	 * @param idAnimale
	 * @return
	 */
	private IMossa recuperaMossaAbbattimento(Integer idRegione,
			Integer idPastore, ArrayList<Integer> idPastoriVicini,
			Integer idAnimale) {
		for (IMossa mossa : this.atContenitoreMossePossibi
				.get(this.atIndicePedina)) {
			if (mossa instanceof Abbattimento) {
				Abbattimento abbattimento = (Abbattimento) mossa;
				if (abbattimento.getIdRegione().equals(idRegione)
						&& abbattimento.getIdPastore().equals(idPastore)
						&& abbattimento.getIdGiocatoriVicini().equals(
								idPastoriVicini)
						&& abbattimento.getIdAnimale().equals(idAnimale)) {
					return abbattimento;
				}
			}
		}
		return null;
	}

	/**
	 * Recupera le mosse di investimento dati i paramentri provenienti dalla
	 * grafica
	 * 
	 * @param idGiocatore
	 * @param tipoTerreno
	 * @return
	 */
	private Investimento recuperaMossaInvestimento(Integer idGiocatore,
			ETipoTerreno tipoTerreno) {
		for (IMossa mossa : this.atContenitoreMossePossibi
				.get(this.atIndicePedina)) {
			if (mossa instanceof Investimento) {
				Investimento investimento = (Investimento) mossa;
				if (investimento.getIdGiocatore().equals(idGiocatore)
						&& investimento.getTipoTerreno().equals(tipoTerreno)) {
					return investimento;
				}
			}
		}
		return null;
	}

	/**
	 * Recupera le mosse di spostamento pastore dati i paramentri provenienti
	 * dalla grafica
	 * 
	 * @param idPedina
	 * @param idExStrada
	 * @param idNuovaStrada
	 * @return
	 */
	private IMossa recuperaMossaSpostamentoPastore(Integer idPedina,
			Integer idExStrada, Integer idNuovaStrada) {
		for (IMossa mossa : this.atContenitoreMossePossibi
				.get(this.atIndicePedina)) {
			if (mossa instanceof Movimento) {
				Movimento movimento = (Movimento) mossa;
				if (!movimento.isAnimale()) {
					if (movimento.getAtIdSoggetto().equals(idPedina)
							&& movimento.getAtIdVecchiaPosizione().equals(
									idExStrada)
							&& movimento.getAtIdNuovaPosizione().equals(
									idNuovaStrada)) {
						return movimento;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Recupera le mosse di spostamento ovino dati i paramentri provenienti
	 * dalla grafica
	 * 
	 * @param idAnimale
	 * @param idExRegione
	 * @param idNuovaRegione
	 * @return
	 */
	private IMossa recuperaMossaSpostamentoOvino(Integer idAnimale,
			Integer idExRegione, Integer idNuovaRegione) {
		for (IMossa mossa : this.atContenitoreMossePossibi
				.get(this.atIndicePedina)) {
			if (mossa instanceof Movimento) {
				Movimento movimento = (Movimento) mossa;
				if (movimento.isAnimale()) {
					if (movimento.getAtIdSoggetto().equals(idAnimale)
							&& movimento.getAtIdVecchiaPosizione().equals(
									idExRegione)
							&& movimento.getAtIdNuovaPosizione().equals(
									idNuovaRegione)) {
						return movimento;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Carica un png come background della finestra
	 */
	private ImagePanel caricaBackground() {
		BufferedImage wvImage;
		try {
			wvImage = ImageIO.read(this.getClass().getResource(
					"/background.png"));
		} catch (IOException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			return null;
		}
		return new ImagePanel(wvImage);
	}

	/**
	 * Permette di generare un oggetto GraficaSwing garantendo che sia univoco
	 * 
	 * @return Ritorna un oggetto di tipo GraficaSwing che ne rappresenta
	 *         l'unica istanza
	 */
	static GraficaSwing istanzia(PartitaClient partita) {
		if (at_istanza == null) {
			at_istanza = new GraficaSwing(partita);
		}
		return at_istanza;
	}

	/**
	 * Permette di eliminare un Oggetto GraficaSwing, garantendo che tutti gli
	 * Stream aperti vengano chiusi
	 * 
	 * @return Ritorna un puntatore a <b>null</b> da associare and un oggetto di
	 *         tipo GraficaSwing
	 */
	GraficaSwing termina() {
		if (at_istanza != null) {
			at_istanza = null;
		}
		return at_istanza;
	}

	/**
	 * Gestisce i vari turni della partita
	 */
	public void avviaPartitaSwing() {
		// Ciclo a true per l'intera partita
		while (true) {
			boolean mioTurno = false;
			// ciclo in ascolto delle model.mosse preliminari (movimenti lupo,
			// pecora nera e agnelli che crescono)
			while (true) {
				// qui mi arriva null o il numero del giocatore nel caso sia
				// arrivato l'id del giocatore del turno dal server
				Integer idGiocatore = null;
				try {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
					this.socketThreadHelper.getLocked();
					IMossa wvMossa = this.partitaSwing
							.ascoltaMossePreliminari();
					this.atFaseMercato = 0;
					idGiocatore = null;
					this.aggiornaUI(wvMossa);
				} catch (FinePartitaException e) {
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
					// E' finita la partita
					JOptionPane.showMessageDialog(null, this.generaClassifica()
							.toString());
					this.atFaseFinePartita = true;
					CLCHelper.stampaAncheInTest(this.generaClassifica()
							.toString());
					return;
				} catch (FaseMercatoException e) {
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
					idGiocatore = e.getIdGiocatore();
					this.atFaseMercato = e.getFase();
				} catch (InizioTurnoException e) {
					this.disattivaBottoni();
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
					idGiocatore = e.getIdGiocatore();
				} catch (NumberFormatException e) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				} catch (DisconnessioneException e) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
					JOptionPane.showMessageDialog(null,
							"Ti sei disconnesso, chiudi e riapri il client");
				}

				if (idGiocatore != null) {
					// ricevo l'id del giocatore a cui tocca e ottengo true(se
					// sono io) o false
					mioTurno = this.partitaSwing.isMioTurno(idGiocatore);
					break;
				} else {
					CLCHelper.stampa("Ascolto model.mosse preliminari...");
				}
			}

			this.aggiornaLabelInformazioni();

			if (mioTurno) {
				if (this.atFaseMercato.equals(0)) {
					this.lblLavagna
							.setText("E' il tuo turno\nDenaro disponibile: "
									+ this.partitaSwing.getGiocatore()
											.getDanaroResiduo()
									+ "\nRecinti rimanenti: "
									+ this.partitaSwing.getMappa()
											.getRecintiAttuali()
									+ "\nRecinti finali: "
									+ this.partitaSwing.getMappa()
											.getRecintiFinaliAttuali()
									+ "\n-----------------");
					// azzero le model.mosse effettuate

					this.atContenitoreMossePossibi = new ArrayList<ArrayList<IMossa>>();

					// restare in ascolto delle model.mosse del proprio pipotto
					PedinaPastore[] wvPedine = this.partitaSwing.getGiocatore()
							.getPedinePastore();
					// Se la pedina è una sola calcolo mosse altrimenti richiedo
					// di selezionare la pedina

					if (wvPedine.length == 1) {
						this.atIndicePedina = 0;
						this.calcolaMosseAndAttivaBottoni(wvPedine[0], true);
						this.pedinaSelezionata = wvPedine[this.atIndicePedina];
					} else {
						boolean esciDalCiclo = false;
						do {
							for (int i = 0; i < wvPedine.length; i++) {
								if (this.atIndicePedina != -1) {
									esciDalCiclo = true;
									if (i == this.atIndicePedina) {
										this.calcolaMosseAndAttivaBottoni(
												wvPedine[i], true);
										this.pedinaSelezionata = wvPedine[this.atIndicePedina];
										this.atFaseSceltaPedina = false;
										try {
											Thread.sleep(50);
										} catch (InterruptedException e) {
											Logger.getGlobal().log(
													Level.WARNING,
													this.getClass().toString(),
													e);
										}
										break;
									} else {
										this.calcolaMosseAndAttivaBottoni(
												wvPedine[i], false);
										this.atFaseSceltaPedina = false;
									}
								} else {
									this.lblLavagna
											.setText("Seleziona un pastore...");
									this.atFaseSceltaPedina = true;
									this.atIndicePedina = this.swingHelper
											.getIdCliccato();
									break;
								}
							}
						} while (!esciDalCiclo);

					}
					Thread leggiMossa = new Thread(new Runnable() {
						public void run() {

							GraficaSwing.this.leggiMossaAndEsegui();
						}
					});
					leggiMossa.start();

					this.lblLavagna.setText("Seleziona una mossa");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}

				} else if (this.atFaseMercato.equals(1)) {
					CLCHelper.stampa("Scagli cosa vendere:\n");
					this.scegliOfferteEdEsegui();
				} else if (this.atFaseMercato.equals(2)) {

				}
			} else {
				// Azzero variabili che mi servono solo durante il mio turno
				this.disattivaBottoni();
				this.atIndicePedina = -1;
				this.atMosseEffettuateNelTurno = new ArrayList<IMossa>();
				this.atPastoreMosso = false;

				CLCHelper.stampa("Turno dell'avversario...");
				// Qui cicla finche non torna false, ovvero finche non arriva
				// _FINETURNO dal server
				IMossa mossaEffettuata;
				try {
					mossaEffettuata = this.partitaSwing
							.ottieniMossaAndAggiorna();
					this.aggiornaUI(mossaEffettuata);
					CLCHelper.stampa("Un giocatore ha effettuato una mossa...");
				} catch (DisconnessioneException e) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
					JOptionPane.showMessageDialog(null,
							"Ti sei disconnesso, chiudi e riapri il client");
				}

			}
		}

	}

	/**
	 * Aggiorna tutte le label sotto le regioni indicando che tipi di ovini
	 * ospita, e altre label che vanno aggiornate ad ogni turno (denaro, recinti
	 * mancanti,...)
	 */
	private void aggiornaLabelInformazioni() {
		this.lblIdentitaGiocatore.setText(this.partitaSwing.getGiocatore()
				.getDanaroResiduo().toString());
		this.lblRecinti.setText(this.partitaSwing.getMappa()
				.getRecintiAttuali().toString());
		this.lblRecintiFinali.setText(this.partitaSwing.getMappa()
				.getRecintiFinaliAttuali().toString());

		int bosco = 0, collina = 0, deserto = 0, lago = 0, montagna = 0, pianura = 0;
		for (ETipoTerreno terrenoPosseduto : this.partitaSwing.getGiocatore()
				.getTipiTerreniPosseduti()) {
			if (terrenoPosseduto.equals(ETipoTerreno.BOSCO)) {
				bosco++;
			} else if (terrenoPosseduto.equals(ETipoTerreno.COLLINA)) {
				collina++;
			} else if (terrenoPosseduto.equals(ETipoTerreno.DESERTO)) {
				deserto++;
			} else if (terrenoPosseduto.equals(ETipoTerreno.LAGO)) {
				lago++;
			} else if (terrenoPosseduto.equals(ETipoTerreno.MONTAGNA)) {
				montagna++;
			} else {
				pianura++;
			}
		}

		this.btnBosco.setText("" + bosco);
		this.btnCollina.setText("" + collina);
		this.btnDeserto.setText("" + deserto);
		this.btnLago.setText("" + lago);
		this.btnMontagna.setText("" + montagna);
		this.btnPianura.setText("" + pianura);

		for (JLabel label : this.lblAnimaliInRegione) {
			if (label != null) {
				Regione regione = this.partitaSwing.getMappa().getRegioni()
						.get(this.lblAnimaliInRegione.indexOf(label));
				String situazione = "P:" + regione.getNumeroPecore() + "M:"
						+ regione.getNumeroMontoni() + "A:"
						+ regione.getNumeroAgnelli();
				label.setText(situazione);
				if ((regione.getNumeroPecore() == 0)
						&& (regione.getNumeroMontoni() == 0)
						&& (regione.getNumeroAgnelli() == 0)) {
					label.setVisible(false);
				} else {
					label.setVisible(true);
				}
			}
		}
	}

	/**
	 * Disattiva i bottoni in preparazione al nuovo ordine di attivazione
	 */
	private void disattivaBottoni() {
		this.btnAbbattimento.setEnabled(false);
		this.btnAccoppiamento.setEnabled(false);
		this.btnInvestimento.setEnabled(false);
		this.btnSpostamentoOvino.setEnabled(false);
		this.btnSpostamentoPastore.setEnabled(false);
		this.btnScegliPecora.setEnabled(false);
		this.btnScegliPecoraNera.setEnabled(false);
		this.btnScegliPecoraMontone.setEnabled(false);
		this.btnScegliPecoraAgnello.setEnabled(false);
	}

	/**
	 * aggiorna l'iterfaccia grafica in base alla mossa che arriva
	 * 
	 * @param mossa
	 */
	private void aggiornaUI(IMossa mossa) {
		this.aggiornaLabelInformazioni();
		if (mossa instanceof Movimento) {
			if (mossa instanceof MovimentoLupo) {
				String posizioneFinale = this.coordinateLupo
						.get((((MovimentoLupo) mossa).getAtIdNuovaPosizione() / 100) - 1);
				this.avviaTransizioneMovimento(this.atPedinaLupo,
						this.atPedinaLupo.getX(), this.atPedinaLupo.getY(),
						Integer.parseInt(posizioneFinale.split("\\,")[0]),
						Integer.parseInt(posizioneFinale.split("\\,")[1]),
						false);
				if (((MovimentoLupo) mossa).haMangiatoAnimale()) {
					this.avviaTransizioneZoom(
							Integer.parseInt(posizioneFinale.split("\\,")[0]),
							Integer.parseInt(posizioneFinale.split("\\,")[1]),
							"mangiato.png");
				}
			} else {
				if (((Movimento) mossa).isAnimale()) {
					if (this.partitaSwing.getMappa().getAnimale(
							((Movimento) mossa).getAtIdSoggetto()) instanceof Pecoranera) {
						String posizioneFinale = this.coordinatePecoraNera
								.get((((Movimento) mossa)
										.getAtIdNuovaPosizione() / 100) - 1);
						this.avviaTransizioneMovimento(
								this.atPedinaPecoraNera,
								this.atPedinaPecoraNera.getX(),
								this.atPedinaPecoraNera.getY(),
								Integer.parseInt(posizioneFinale.split("\\,")[0]),
								Integer.parseInt(posizioneFinale.split("\\,")[1]),
								false);
					} else {
						Integer idRegionePartenza = (((Movimento) mossa)
								.getAtIdVecchiaPosizione() / 100) - 1;
						Integer idRegioneArrivo = (((Movimento) mossa)
								.getAtIdNuovaPosizione() / 100) - 1;

						BufferedImage image;
						try {
							image = ImageIO.read(this.getClass().getResource(
									"/pecora.png"));
						} catch (IOException e) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
							return;
						}

						IconaSpostabile iconaSpostabile = new IconaSpostabile(
								new ImageIcon(image));
						iconaSpostabile.setLocation(
								Integer.parseInt(this.coordinatePecore.get(
										idRegionePartenza).split("\\,")[0]),
								Integer.parseInt(this.coordinatePecore.get(
										idRegionePartenza).split("\\,")[1]));
						iconaSpostabile.setSize(30, 30);
						this.wvFramePrincipale.getContentPane().add(
								iconaSpostabile);

						String posizioneFinale = this.coordinatePecore
								.get(idRegioneArrivo);
						this.avviaTransizioneMovimento(
								iconaSpostabile,
								Integer.parseInt(this.coordinatePecore.get(
										idRegionePartenza).split("\\,")[0]),
								Integer.parseInt(this.coordinatePecore.get(
										idRegionePartenza).split("\\,")[1]),
								Integer.parseInt(posizioneFinale.split("\\,")[0]),
								Integer.parseInt(posizioneFinale.split("\\,")[1]),
								true);
					}
				} else {
					// Mossa di un pastore
					IconaSpostabile pastore = this.iconePastori
							.get((((Movimento) mossa).getAtIdSoggetto() / 100) - 1);
					String posizioneFinale = this.coordinateStrade
							.get((((Movimento) mossa).getAtIdNuovaPosizione() / 100) - 1);
					IconaSpostabile iconaRecinto;
					BufferedImage image;

					if (this.partitaSwing.getMappa().getRecintiAttuali() > 0) {
						try {
							image = ImageIO.read(this.getClass().getResource(
									"/recinto.png"));
						} catch (IOException e) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
							return;
						}
					} else {
						try {
							image = ImageIO.read(this.getClass().getResource(
									"/recintoFinale.png"));
						} catch (IOException e) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
							return;
						}
					}
					iconaRecinto = new IconaSpostabile(new ImageIcon(image));
					iconaRecinto.setSize(30, 30);
					iconaRecinto.setLocation(pastore.getX(), pastore.getY());
					this.wvFramePrincipale.getContentPane().add(iconaRecinto);
					this.avviaTransizioneMovimento(pastore, pastore.getX(),
							pastore.getY(),
							Integer.parseInt(posizioneFinale.split("\\,")[0]),
							Integer.parseInt(posizioneFinale.split("\\,")[1]),
							false);
				}
			}
		} else if (mossa instanceof Nascita) {
			Regione regioneIndicata = this.partitaSwing.getMappa().getRegione(
					((Nascita) mossa).getIdRegione());
			if (regioneIndicata.occupataDaMontoni()
					&& regioneIndicata.occupataDaPecoraNonNera()) {
				// accoppiamento possibile
				this.avviaTransizioneZoom(
						Integer.parseInt(this.coordinatePecore.get(
								(regioneIndicata.getId() / 100) - 1).split(
								"\\,")[0]),
						Integer.parseInt(this.coordinatePecore.get(
								(regioneIndicata.getId() / 100) - 1).split(
								"\\,")[1]), "accoppiamento.png");
			}

		} else if (mossa instanceof Abbattuto) {
			Regione regioneAbbattimento = this.partitaSwing.getMappa()
					.getRegione(((Abbattuto) mossa).getIdRegioneAppartenenza());
			this.lblLavagna.setText("Abbattuto");
			this.avviaTransizioneZoom(
					Integer.parseInt(this.coordinatePecore.get(
							(regioneAbbattimento.getId() / 100) - 1).split(
							"\\,")[0]),
					Integer.parseInt(this.coordinatePecore.get(
							(regioneAbbattimento.getId() / 100) - 1).split(
							"\\,")[1]), "abbattimento.png");
		} else if (mossa instanceof Investimento) {
			Strada strada = this.partitaSwing.getMappa().getStrada(
					((Investimento) mossa).getIdGiocatore());
		}

	}

	/**
	 * Calcola le mosse e attiva i bottoni per effettuarle
	 * 
	 * @param pedina
	 *            la pedina del quale bisogna calcolare le mosse
	 * @param attivareBottoni
	 *            la variabile che indica se attivare o no i bottoni
	 */
	private void calcolaMosseAndAttivaBottoni(PedinaPastore pedina,
			boolean attivareBottoni) {
		IMossa mossa;
		if (this.atMosseEffettuateNelTurno == null) {
			this.atMosseEffettuateNelTurno = new ArrayList<IMossa>();
		}
		if (this.atMosseEffettuateNelTurno.size() == 0) {
			mossa = null;
		} else {
			int index = this.atMosseEffettuateNelTurno.size() - 1;
			mossa = this.atMosseEffettuateNelTurno.get(index);
		}
		ArrayList<IMossa> mossePossibili = null;
		if (!this.atPastoreMosso
				&& (this.atMosseEffettuateNelTurno.size() == 2)) {

			mossePossibili = this.partitaSwing
					.precalcolaMosseSoloSpostamento(pedina.getStradaOccupata());
		} else {
			mossePossibili = this.partitaSwing.precalcolaMosse(
					pedina.getStradaOccupata(), mossa);
		}

		this.atContenitoreMossePossibi.add(mossePossibili);
		if (attivareBottoni) {
			this.attivaBottoni();
		}
	}

	/**
	 * Attiva i bottoni in base a ciò che è stato calcolato nelle mosse
	 */
	private void attivaBottoni() {
		for (IMossa mossa : this.atContenitoreMossePossibi
				.get(this.atIndicePedina)) {
			if (mossa instanceof Movimento) {
				if (((Movimento) mossa).isAnimale()) {
					this.btnSpostamentoOvino.setEnabled(true);
				} else {
					this.btnSpostamentoPastore.setEnabled(true);
				}
			} else if (mossa instanceof Investimento) {
				this.btnInvestimento.setEnabled(true);
			} else if (mossa instanceof Abbattimento) {
				this.btnAbbattimento.setEnabled(true);
			} else if (mossa instanceof Accoppiamento) {
				this.btnAccoppiamento.setEnabled(true);
			}
		}
	}

	/**
	 * permette di scegliere le offerte
	 */
	private void scegliOfferteEdEsegui() {

	}

	/**
	 * genera la classifica
	 * 
	 * @return
	 */
	private Object generaClassifica() {
		this.partitaSwing.aggiornaMappaSeNecessario();
		this.lblLavagna.setText("Partita finita!");
		Classifica classifica = new Classifica(this.partitaSwing.getMappa());
		classifica.calcolaPunteggi();
		return classifica;
	}

	/**
	 * legge la mossa e la invia. dopo genera le animazioni
	 */
	private void leggiMossaAndEsegui() {
		this.lblLavagna.setText("Aspetto la mossa...");
		this.socketThreadHelper.resetId();
		// socketThreadHelper.setLocked(null);
		int indiceMossa = this.swingHelper.getIdCliccato();
		this.swingHelper.resetId();
		IMossa wvMossaLetta;
		try {
			wvMossaLetta = this.atContenitoreMossePossibi.get(
					this.atIndicePedina).get(indiceMossa);
		} catch (Exception e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			wvMossaLetta = null;
		}

		if (wvMossaLetta instanceof Movimento) {
			if (!((Movimento) wvMossaLetta).isAnimale()) {
				this.atPastoreMosso = true;
			}
		}
		if (wvMossaLetta instanceof Abbattimento) {
			if (this.partitaSwing
					.permettiAbbattimento(((Abbattimento) wvMossaLetta)
							.getNumeroStradaOccupata())) {
				Abbattimento wvAbbattimentoPerServer = new Abbattimento(
						((Abbattimento) wvMossaLetta).getIdPastore(),
						((Abbattimento) wvMossaLetta).getIdRegione(),
						((Abbattimento) wvMossaLetta).getAnimale().getId(),
						((Abbattimento) wvMossaLetta).getIdGiocatoriVicini());
				wvMossaLetta = wvAbbattimentoPerServer;
			} else {
				wvMossaLetta = new tentativoFallito(
						"Non mi è stato concessa la mossa di abbattimento");
			}
		}
		this.atMosseEffettuateNelTurno.add(wvMossaLetta);
		// Aggiorno sulla maoppa
		this.partitaSwing.interpretaMossa(wvMossaLetta);
		// propago al server
		try {
			this.partitaSwing.inviaMossa(wvMossaLetta);
			this.aggiornaLabelInformazioni();
			this.lblLavagna.setText("Mossa inviata...");
		} catch (DisconnessioneException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			JOptionPane.showMessageDialog(null,
					"Ti sei disconnesso, chiudi e riapri il client");
		}

		this.socketThreadHelper.setLocked(false);
	}
}
