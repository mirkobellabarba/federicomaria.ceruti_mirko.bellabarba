package view;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * gestisce la grafica su console
 * 
 * @author Mirko
 * 
 */
public class CLCHelper {
	private static CLCHelper atIstanza;
	private Scanner atScannerStdIn;
	private boolean atTest;

	/**
	 * Costruttore
	 */
	private CLCHelper(boolean test) {
		atScannerStdIn = new Scanner(System.in);
		atTest = test;
	}

	/**
	 * Permette di generare un oggetto Grafica_CLC garantendo che sia univoco
	 * 
	 * @return Ritorna un oggetto di tipo {@link CLCHelper} che ne rappresenta
	 *         l'unica istanza
	 */
	static CLCHelper istanzia() {
		if (atIstanza == null) {
			atIstanza = new CLCHelper(false);
		}
		return atIstanza;
	}

	/**
	 * per istanziare la {@link CLCHelper} in modalità test
	 * 
	 * @return
	 */
	static CLCHelper istanziaTest() {
		return new CLCHelper(true);
	}

	/**
	 * Permette di eliminare un Oggetto Grafica_CLC, garantendo che tutti gli
	 * Stream aperti vengano chiusi
	 * 
	 * @return Ritorna un puntatore a <b>null</b> da associare and un oggetto di
	 *         tipo Grafica_CLC
	 */
	CLCHelper termina() {
		if (atIstanza != null) {
			atIstanza = null;
			atScannerStdIn.close();
		}
		return atIstanza;
	}

	/**
	 * Stampa un messaggio nella console tranne che in fase di test
	 * 
	 * @param messaggio
	 *            Stringa da stampare
	 */
	public static void stampa(Object messaggio) {
		if (!Start.abilitaTest) {
			System.out.println(messaggio.toString());
		}
	}

	/**
	 * Stampa un messaggio nella console
	 * 
	 * @param messaggio
	 *            Stringa da stampare
	 */
	public static void stampaAncheInTest(Object messaggio) {
		System.out.println(messaggio.toString());
	}

	/**
	 * Stampa la lista di chiamate di un'eccezione in {@link System#err}
	 * 
	 * @param Eccezione
	 *            {@link Exception} di cui stampare la lista di chiamate
	 */
	public static void stampaEccezione(Exception Eccezione) {
		if (Start.abilitaTest) {
			System.err.println(Eccezione.getMessage());
			for (StackTraceElement wvItem : Eccezione.getStackTrace()) {
				System.err.println(wvItem.toString());
			}
		}
	}

	/**
	 * Legge una stringa da console
	 * 
	 * @return Ritorna una string ache rappresenta il testo digitato da console
	 */
	String leggi() {
		String wvIstruzione;
		wvIstruzione = atScannerStdIn.next();
		return wvIstruzione;

	}

	/**
	 * Richiede il numero di giocatori che vogliono parteciapare alla partita <br>
	 * il numero dei giocatori è compreso tra 2 e 4 inclusi
	 * 
	 * @return Ritorna un'intero che rappresenta il numero di giocatori che
	 *         vogliono partecipare alla partita
	 */
	Integer richiediNumeroGiocatori() {
		Integer wvNumeroGiocatori = 0;
		String wvDato;
		Boolean wvDatoNumerico;

		do {
			stampa("Inserire numero compreso tra 2 e 4, inclusi, per indicare il numero di partecipanti alla partita:");
			wvDato = leggi();

			try {
				wvNumeroGiocatori = Integer.parseInt(wvDato);
				wvDatoNumerico = true;
			} catch (NumberFormatException e) {
				Logger.getGlobal().log (Level.WARNING, this.getClass().toString(), e);
				wvDatoNumerico = false;
			}
		} while (wvNumeroGiocatori < 2 || wvNumeroGiocatori > 4 || !wvDatoNumerico);

		return wvNumeroGiocatori;
	}

	/**
	 * Richiede il nome dei giocatori che parteciapno alla partita
	 * 
	 * @param Numero_Giocatori
	 *            Numerod dei giocatori partecipanti alla partita
	 * 
	 * @return Ritorna un ArrayList<String> che rappresenta l'elenco dei nomi
	 *         dei giocatori che parteciperanno alla partita
	 */
	ArrayList<String> richiediNomiGiocatori(Integer Numero_Giocatori) {
		ArrayList<String> wvNomiGiocatori;

		wvNomiGiocatori = new ArrayList<String>();
		for (int i = 0; i < Numero_Giocatori; i++) {
			stampa("Giocatore numero " + (i + 1) + " inserisci il tuo nome:");
			wvNomiGiocatori.add(leggi());
			// Stampa("\n");
		}
		return wvNomiGiocatori;
	}

	public static void stampaSuLinea(String Messaggio) {
		System.out.print(Messaggio.toString());
	}

	public static void vaiACapo() {
		System.out.println("");
	}

}