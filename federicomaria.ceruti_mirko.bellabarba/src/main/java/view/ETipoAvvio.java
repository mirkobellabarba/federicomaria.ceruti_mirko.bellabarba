package view;

/**
 * Indica la modalità di avvio<br>
 * <br>
 * <b>CLIENT</b>: avvviare in modalità client<br>
 * <b>SERVER</b>: avvviare in modalità server
 */
public enum ETipoAvvio {
	CLIENT, SERVER;
}
