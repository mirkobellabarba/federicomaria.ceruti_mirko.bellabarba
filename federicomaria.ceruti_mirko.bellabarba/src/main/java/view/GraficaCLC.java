package view;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Classifica;
import model.ETipoTerreno;
import model.Giocatore.RimozioneTesseraException;
import model.PedinaPastore;
import model.mosse.Abbattimento;
import model.mosse.IMossa;
import model.mosse.Mercato;
import model.mosse.MossaVuota;
import model.mosse.Movimento;
import model.mosse.Offerta;
import model.mosse.Posizionamento;
import model.mosse.tentativoFallito;
import controllerClient.Connessione.FaseMercatoException;
import controllerClient.Connessione.FinePartitaException;
import controllerClient.Connessione.InizioPartitaException;
import controllerClient.Connessione.InizioTurnoException;
import controllerClient.Connessione.TurnoPersonaleException;
import controllerClient.DisconnessioneException;
import controllerClient.PartitaClient;
import controllerCommon.ETipoConnessione;

public class GraficaCLC extends Thread {
	private final ETipoGrafica atTipoGrafica;
	private final PartitaClient atPartita;
	private static GraficaCLC atIstanza;
	private final String atNomeGiocatore;
	private ArrayList<IMossa> atMosseEffettuateNelTurno = new ArrayList<IMossa>();

	/**
	 * Costruttore di grafica
	 * 
	 * @param interfacciaGrafica
	 *            Indica la modalità di interfaccia utente
	 * @param nomeGiocatore
	 * @param ETipoConnessione
	 *            Indica la modalità di connessione al server
	 */
	private GraficaCLC(ETipoGrafica interfacciaGrafica,
			ETipoConnessione tipoConnessione, String nomeGiocatore) {
		this.atTipoGrafica = interfacciaGrafica;
		this.atPartita = new PartitaClient(tipoConnessione);
		this.atNomeGiocatore = nomeGiocatore;
		this.atFaseMercato = 0;
		this.atMossaMercatoPossibile = new ArrayList<Offerta>();
		this.atMossaMercatoScelta = new ArrayList<Offerta>();
	}

	/**
	 * Permette di generare un oggetto Grafica garantendo che sia univoco
	 * 
	 * @param interfacciaGrafica
	 *            Indica la modalità di interfaccia utente
	 * @param tipoConnessione
	 *            Indica la modalità di connessione al server
	 * @param nomeGiocatore
	 * @return Ritorno un oggetto di tipo GraficaCLC
	 */
	public static GraficaCLC istanzia(ETipoGrafica interfacciaGrafica,
			ETipoConnessione tipoConnessione, String nomeGiocatore) {
		if (atIstanza == null) {
			atIstanza = new GraficaCLC(interfacciaGrafica, tipoConnessione,
					nomeGiocatore);
		}
		return atIstanza;
	}

	/**
	 * Costruttore per i test
	 * 
	 * @param interfacciaGrafica
	 * @param tipoConnessione
	 * @param nomeGiocatore
	 * @return
	 */
	public static GraficaCLC istanziaTest(ETipoGrafica interfacciaGrafica,
			ETipoConnessione tipoConnessione, String nomeGiocatore) {
		return new GraficaCLC(interfacciaGrafica, tipoConnessione,
				nomeGiocatore);
	}

	/*
	 * public void Mostra() {
	 * System.out.println("Grafica.Mostra: Non ancora implementato"); }
	 */
	/**
	 * Metodo run da richiamare per avviare la grafica
	 */
	@Override
	public void run() {

		if (this.atTipoGrafica == ETipoGrafica.CLC) {
			this.graficaCLC();
		} else if (this.atTipoGrafica == ETipoGrafica.SWING) {
			this.graficaSwing();
		}
		return;
	}

	/**
	 * Metodo da richiamare per avviare la grafica
	 */
	public void avvia() {
		this.run();
	}

	/**
	 * Da richiamare per gestire l'interfccia utente in CLC
	 */
	private void graficaCLC() {
		if (Start.abilitaTest) {
			CLCHelper.istanziaTest();
		} else {
			CLCHelper.istanzia();
		}
		this.partitaCLC();

		CLCHelper.stampaAncheInTest("Sono clc");
		return;
	}

	private void partitaCLC() {
		// Viene effettuata la connessione alla socket
		try {
			this.atPartita.connettiAlServer(this.atNomeGiocatore);
			// Recupero le info della partita (id, idGiocatore, mappa)
			this.atPartita.accettaPartita();
		} catch (DisconnessioneException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}

		// if (!Start.abilitaTest) {
		CLCHelper.stampa("Richiedo i pastori...");
		// }

		// Serve per posizionare i pastori
		this.faiScegliereStrade();
		// if (!Start.abilitaTest) {
		CLCHelper.stampa("Avvio la partita");
		// }

		this.avviaPartitaCLC();
	}

	/**
	 * Richiede all'utente la strada dove vuole posizionare la pedina dopo aver
	 * mostrato le strade disponibili che il server fornisce. Spedisce il
	 * risultato al server e aspetta l'_ACK
	 * 
	 * Tutto ciò viene ripetuto per ogni pedina associata al giocatore
	 */
	private void faiScegliereStrade() {
		while (true) {
			try {
				Posizionamento mossaAvversario = this.atPartita
						.getMossaPosizionamento();
				if (mossaAvversario == null) {
					continue;
				}
				this.atPartita.posizionaPedinaPastore(mossaAvversario);
			} catch (TurnoPersonaleException e) {
				// Questo è il mio turno
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
				}
				for (PedinaPastore pedina : this.atPartita.getGiocatore()
						.getPedinePastore()) {
					Integer scelta = this.stampaStradeDisponibili();
					try {
						this.atPartita
								.inviaMossaPosizionamento(this.atPartita
										.getGiocatore().getId(),
										pedina.getId(), scelta);
					} catch (DisconnessioneException e1) {
						Logger.getGlobal().log(Level.INFO,
								this.getClass().toString(), e1);
						CLCHelper.stampaEccezione(e);
					}
				}
			} catch (InizioPartitaException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
				}
				CLCHelper.stampa("La partita sta per iniziare");
				break;
			} catch (DisconnessioneException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
		}
	}

	private ArrayList<ArrayList<IMossa>> atContenitoreMossePossibi;
	private Boolean atPastoreMosso = false;
	private Integer atFaseMercato;

	private void avviaPartitaCLC() {
		// Ciclo a true per l'intera partita
		while (true) {
			if (Start.abilitaTest) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
				}
			}
			boolean mioTurno = false;
			// ciclo in ascolto delle model.mosse preliminari (movimenti lupo,
			// pecora nera e agnelli che crescono)
			while (true) {
				// qui mi arriva null o il numero del giocatore nel caso sia
				// arrivato l'id del giocatore del turno dal server
				Integer idGiocatore = null;

				try {
					this.atPartita.ascoltaMossePreliminari();
					this.atFaseMercato = 0;
					idGiocatore = null;
				} catch (FinePartitaException e) {
					// E' finita la partita
					CLCHelper
							.stampa("La partità è finita, ecco la classifica:\n");
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.INFO,
								this.getClass().toString(), e);
					}
					CLCHelper.stampaAncheInTest(this.generaClassifica()
							.toString());
					return;
				} catch (FaseMercatoException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.INFO,
								this.getClass().toString(), e);
					}
					idGiocatore = e.getIdGiocatore();
					this.atFaseMercato = e.getFase();
				} catch (InizioTurnoException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.INFO,
								this.getClass().toString(), e);
					}
					idGiocatore = e.getIdGiocatore();
				} catch (NumberFormatException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
				} catch (DisconnessioneException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
				}

				if (idGiocatore != null) {
					// ricevo l'id del giocatore a cui tocca e ottengo true(se
					// sono io) o false
					mioTurno = this.atPartita.isMioTurno(idGiocatore);
					break;
				} else {
					CLCHelper.stampa("Ascolto model.mosse preliminari...");
				}
			}
			if (mioTurno) {

				if (this.atFaseMercato.equals(0)) {
					CLCHelper.stampa("E' il tuo turno\nDenaro disponibile: "
							+ this.atPartita.getGiocatore().getDanaroResiduo()
							+ "\nRecinti rimanenti: "
							+ this.atPartita.getMappa().getRecintiAttuali()
							+ "\nRecinti finali: "
							+ this.atPartita.getMappa()
									.getRecintiFinaliAttuali()
							+ "\n-----------------");
					// azzero le model.mosse effettuate

					this.atContenitoreMossePossibi = new ArrayList<ArrayList<IMossa>>();

					// restare in ascolto delle model.mosse del proprio pipotto
					PedinaPastore[] wvPedine = this.atPartita.getGiocatore()
							.getPedinePastore();
					for (int i = 0; i < wvPedine.length; i++) {
						if (this.atIndicePedina != -1) {
							if (i == this.atIndicePedina) {
								this.stampaMossePossibili(wvPedine[i], true);
							} else {
								this.stampaMossePossibili(wvPedine[i], false);
							}
						} else {
							this.stampaMossePossibili(wvPedine[i], true);
						}
					}
					this.leggiMossaAndEsegui();
				} else if (this.atFaseMercato.equals(1)) {
					CLCHelper.stampa("Scegli cosa vendere:\n");
					this.scegliOfferteEdEsegui();
				} else if (this.atFaseMercato.equals(2)) {
					CLCHelper.stampa("Scegli cosa comprare:\n");
					this.scegliAcquistoEdEsegui();
				}
			} else {
				if (this.atFaseMercato.equals(0)) {
					// Azzero variabili che mi servono solo durante il mio turno
					this.atIndicePedina = -1;
					this.atMosseEffettuateNelTurno = new ArrayList<IMossa>();
					this.atPastoreMosso = false;

					CLCHelper.stampa("Turno dell'avversario...");
					// Qui cicla finche non torna false, ovvero finche non
					// arriva
					// _FINETURNO dal server
					try {
						this.atPartita.ottieniMossaAndAggiorna();
					} catch (DisconnessioneException e) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
						}
					}
					CLCHelper.stampa("Un giocatore ha effettuato una mossa...");
				} else {
					CLCHelper.stampa("Aspetto la scelta degli altri giocatori");
				}
			}
		}

	}

	private ArrayList<Offerta> atMossaMercatoPossibile;
	private final ArrayList<Offerta> atMossaMercatoScelta;

	private void scegliOfferteEdEsegui() {
		int wvNumeroMossa = -1;
		this.atMossaMercatoPossibile = new ArrayList<Offerta>();
		do {
			this.stampaOffertePossibili();
			String wvNumeroString = Leggi();
			try {
				wvNumeroMossa = Integer.parseInt(wvNumeroString);
			} catch (NumberFormatException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
				wvNumeroMossa = -1;
			}

			if ((wvNumeroMossa != -1)
					&& (wvNumeroMossa < this.atMossaMercatoPossibile.size())) {
				this.atMossaMercatoScelta.add(this.atMossaMercatoPossibile
						.get(wvNumeroMossa));
				this.atMossaMercatoPossibile
						.remove(this.atMossaMercatoPossibile.get(wvNumeroMossa));
				wvNumeroMossa--;
				int prezzo = 1;
				do {
					CLCHelper.stampa("Scelgi prezzo di vendita tra 1 e 4");
					try {
						wvNumeroString = Leggi();
						prezzo = Integer.parseInt(wvNumeroString);
					} catch (NumberFormatException e) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
						}
						prezzo = -1;
					}
				} while ((prezzo < 1) || (prezzo > 4));
				this.atMossaMercatoScelta.get(
						this.atMossaMercatoScelta.size() - 1).setPrezzo(prezzo);
			}

		} while (wvNumeroMossa < this.atMossaMercatoPossibile.size());

		Mercato wvMercato = new Mercato();
		wvMercato.aggiungiOfferte(this.atMossaMercatoScelta);
		CLCHelper.stampa("Numero offerte: " + wvMercato.getOfferte().size());
		try {
			this.atPartita.inviaMossa(wvMercato);
		} catch (DisconnessioneException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
	}

	private void scegliAcquistoEdEsegui() {
		int wvNumeroMossa = -1;
		String wvNumeroString;
		try {
			this.atPartita.ottieniMossaAndAggiorna();
		} catch (DisconnessioneException e1) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e1);
			}
		}
		for (Offerta wvOfferta : this.atMossaMercatoPossibile) {
			if (!wvOfferta.getVenditore().equals(
					this.atPartita.getGiocatore().getId())) {
				this.atMossaMercatoPossibile = this.atPartita.getListaMercato()
						.getOfferte();
			}

		}

		do {
			this.stampaAcquistiPossibili();
			wvNumeroString = Leggi();
			try {
				wvNumeroMossa = Integer.parseInt(wvNumeroString);
			} catch (NumberFormatException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
				wvNumeroMossa = -1;
			}

			if ((wvNumeroMossa != -1)
					&& (wvNumeroMossa < this.atMossaMercatoPossibile.size())) {
				Offerta wvOfferta = this.atMossaMercatoPossibile
						.get(wvNumeroMossa);
				this.atMossaMercatoScelta.add(wvOfferta);
				this.atMossaMercatoPossibile.remove(wvOfferta);

				this.atPartita.getGiocatore().spendiDanaro(
						wvOfferta.getPrezzo());
				this.atPartita.getGiocatore(wvOfferta.getVenditore())
						.spendiDanaro(-wvOfferta.getPrezzo());

				this.atPartita.getGiocatore().aggiungiTesseraTerreno(
						wvOfferta.getTerreno());
				try {
					this.atPartita.getGiocatore(wvOfferta.getVenditore())
							.rimuoviTesseraTerreno(wvOfferta.getTerreno());
				} catch (RimozioneTesseraException e) {
					if (Start.abilitaLogger) {
						Logger.getGlobal().log(Level.WARNING,
								this.getClass().toString(), e);
					}
				}
			}

		} while (wvNumeroMossa < this.atMossaMercatoPossibile.size());
		Mercato wvMercato = new Mercato();
		wvMercato.aggiungiOfferte(this.atMossaMercatoScelta);
		try {
			this.atPartita.inviaMossa(wvMercato);
		} catch (DisconnessioneException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
	}

	private void stampaAcquistiPossibili() {
		CLCHelper.stampa("Puoi acquistare:");
		int i = 0;
		for (Offerta wvOfferta : this.atMossaMercatoPossibile) {
			if (wvOfferta.getVenditore().equals(
					this.atPartita.getGiocatore().getId())) {
				continue;
			}
			CLCHelper.stampa(i + ") " + wvOfferta.propostaMossa());
			i++;
		}
		if (i == 0) {
			CLCHelper.stampa("Nussun acquisto possibile");
		}
		CLCHelper.stampa(i + ") Termina scelta acquisto");
	}

	private void stampaOffertePossibili() {
		this.precaricaMosseMercatoPossibili();
		CLCHelper.stampa("Puoi vendere:");
		int i = 0;
		for (Offerta wvOfferta : this.atMossaMercatoPossibile) {
			CLCHelper.stampa(i + ") " + wvOfferta.propostaMossa());
			i++;
		}
		CLCHelper.stampa(i + ") Termina scelta offerte");
	}

	private void precaricaMosseMercatoPossibili() {
		if (this.atMossaMercatoPossibile.size() == 0) {
			ArrayList<ETipoTerreno> wvTerreni = this.atPartita.getGiocatore()
					.getTerreniComprati();
			for (ETipoTerreno eTipoTerreno : wvTerreni) {
				Offerta wvOfferta = new Offerta(eTipoTerreno, this.atPartita
						.getGiocatore().getId());
				if (!this.atMossaMercatoScelta.contains(wvOfferta)) {
					this.atMossaMercatoPossibile.add(wvOfferta);
				}
			}
		}
	}

	/**
	 * Genera la classifica e la ritorna
	 * 
	 * @return
	 */
	private Classifica generaClassifica() {
		this.atPartita.aggiornaMappaSeNecessario();
		Classifica classifica = new Classifica(this.atPartita.getMappa());
		classifica.calcolaPunteggi();
		return classifica;
	}

	private int atIndicePedina = -1;

	private void leggiMossaAndEsegui() {
		if (this.atIndicePedina == -1) {
			if (this.atPartita.getGiocatore().getPedinePastore().length > 1) {

				do {
					try {
						CLCHelper
								.stampa("Scegli il numero della Pedina_Pastore (0 o 1)");
						if (Start.abilitaTest) {
							try {
								Thread.sleep(10);
							} catch (InterruptedException e) {
								if (Start.abilitaLogger) {
									Logger.getGlobal().log(Level.WARNING,
											this.getClass().toString(), e);
								}
							}
							this.atIndicePedina = new Random().nextInt(2);
						} else {
							this.atIndicePedina = Integer.parseInt(Leggi());
						}
					} catch (NumberFormatException e) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.WARNING,
									this.getClass().toString(), e);
						}
						CLCHelper.stampa("Input errato...");
						this.atIndicePedina = -1;
					}

				} while ((this.atIndicePedina != 0)
						&& (this.atIndicePedina != 1));
			} else {
				this.atIndicePedina = 0;
			}
		}

		int wvNumeroMossa = -2;
		do {
			try {
				if (Start.abilitaTest) {
					try {
						Thread.sleep(10);
						wvNumeroMossa = new Random()
								.nextInt(this.atContenitoreMossePossibi.get(
										this.atIndicePedina).size());
					} catch (InterruptedException e) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.INFO,
									this.getClass().toString(), e);
						}
						CLCHelper.stampaAncheInTest(e.getMessage());
					} catch (IllegalArgumentException e) {
						if (Start.abilitaLogger) {
							Logger.getGlobal().log(Level.INFO,
									this.getClass().toString(), e);
						}
						CLCHelper.stampa("Invio Mossa vuota");
					}
				} else {
					CLCHelper
							.stampa("Indica il numero della mossa che vuoi eseguire:");
					wvNumeroMossa = Integer.parseInt(Leggi());
				}
			} catch (NumberFormatException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
				}
				CLCHelper.stampa("Input errato...");
				wvNumeroMossa = -1;
			}
		} while ((wvNumeroMossa == -1)
				|| (wvNumeroMossa >= this.atContenitoreMossePossibi.get(
						this.atIndicePedina).size()));

		if (wvNumeroMossa == -2) {
			try {
				this.atPartita.inviaMossa(new MossaVuota());
			} catch (DisconnessioneException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.WARNING,
							this.getClass().toString(), e);
				}
			}
			return;
		}

		IMossa wvMossaLetta = this.atContenitoreMossePossibi.get(
				this.atIndicePedina).get(wvNumeroMossa);
		if (wvMossaLetta instanceof Movimento) {
			if (!((Movimento) wvMossaLetta).isAnimale()) {
				this.atPastoreMosso = true;
			}
		}
		if (wvMossaLetta instanceof Abbattimento) {
			if (this.atPartita
					.permettiAbbattimento(((Abbattimento) wvMossaLetta)
							.getNumeroStradaOccupata())) {
				Abbattimento wvAbbattimentoPerServer = new Abbattimento(
						((Abbattimento) wvMossaLetta).getIdPastore(),
						((Abbattimento) wvMossaLetta).getIdRegione(),
						((Abbattimento) wvMossaLetta).getAnimale().getId(),
						((Abbattimento) wvMossaLetta).getIdGiocatoriVicini());
				wvMossaLetta = wvAbbattimentoPerServer;
			} else {
				wvMossaLetta = new tentativoFallito(
						"Non mi è stato concessa la mossa di abbattimento");
			}
		}
		this.atMosseEffettuateNelTurno.add(wvMossaLetta);
		// Aggiorno sulla mappa
		this.atPartita.interpretaMossa(wvMossaLetta);

		try {
			this.atPartita.inviaMossa(wvMossaLetta);
		} catch (DisconnessioneException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.WARNING,
						this.getClass().toString(), e);
			}
		}
	}

	private void stampaMossePossibili(PedinaPastore pedina, boolean stampare) {
		// Stampo info utili all'utente per identificare la pedina
		CLCHelper.stampa("Mosse possibili con la pedina " + pedina.getColore());
		IMossa mossa;
		if (this.atMosseEffettuateNelTurno.size() == 0) {
			mossa = null;
		} else {
			int index = this.atMosseEffettuateNelTurno.size() - 1;
			mossa = this.atMosseEffettuateNelTurno.get(index);
		}
		ArrayList<IMossa> mossePossibili = null;
		if (!this.atPastoreMosso
				&& (this.atMosseEffettuateNelTurno.size() == 2)) {
			CLCHelper
					.stampa("Devi per forza muovere un pastore in questo turno!");
			mossePossibili = this.atPartita
					.precalcolaMosseSoloSpostamento(pedina.getStradaOccupata());
		} else {
			mossePossibili = this.atPartita.precalcolaMosse(
					pedina.getStradaOccupata(), mossa);
		}
		if (stampare && !Start.abilitaTest) {
			for (int i = 0; i < mossePossibili.size(); i++) {
				CLCHelper.stampa(i + ") "
						+ mossePossibili.get(i).propostaMossa());
			}
		}
		this.atContenitoreMossePossibi.add(mossePossibili);
	}

	private Integer stampaStradeDisponibili() {
		ArrayList<Integer> wvIdStradeLibere = this.atPartita.getMappa()
				.getIdStradeLibere();
		if (Start.abilitaTest) {
			return (((new Random().nextInt(42) + 1) * 100) + 2);
		}
		CLCHelper.stampa("Ecco le strade dove puoi posizionare il pastore");
		for (Integer strada : wvIdStradeLibere) {
			CLCHelper.stampaSuLinea(strada + "|");
		}
		CLCHelper.vaiACapo();
		Integer scelta;
		do {
			try {
				CLCHelper.stampa("Immetti qui la tua scelta");
				scelta = Integer.parseInt(Leggi());
			} catch (NumberFormatException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.INFO,
							this.getClass().toString(), e);
				}
				CLCHelper.stampa("Hai scritto un numero che non va bene...");
				scelta = -1;
			}

		} while (!wvIdStradeLibere.contains(scelta));
		return scelta;
	}

	@SuppressWarnings("resource")
	private static String Leggi() {
		String wvIstruzione;
		Scanner wv_in = new Scanner(System.in);
		wvIstruzione = wv_in.next();
		// wv_in.close();
		wv_in = null;
		return wvIstruzione;
	}

	/**
	 * Da richiamare per gestire l'interfaccia utente in Swing
	 * 
	 */
	private void graficaSwing() {
		// Grafica_CLC.Stampa("Sono swing");
		try {
			this.atPartita.connettiAlServer(this.atNomeGiocatore);
			this.atPartita.accettaPartita();
		} catch (DisconnessioneException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		}

		GraficaSwing ui = GraficaSwing.istanzia(this.atPartita);
		ui.caricaCoordinate();
		ui.mostraUI(this.atNomeGiocatore);

		// richiedo il posizionamento delle pedina
		ui.richiediPosizionamentoPedina();

		// La partita sta per essere avviata
		ui.avviaPartitaSwing();
	}

}
