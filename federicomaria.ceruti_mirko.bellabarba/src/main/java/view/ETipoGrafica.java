package view;

/**
 * Indica la modalità di gestione della grafica<br>
 * <br>
 * <b>CLC</b>: avvviare in modalità console<br>
 * <b>SWING</b>: avvviare in modalità grafica avanzata
 */
public enum ETipoGrafica {
	CLC, SWING;
}
