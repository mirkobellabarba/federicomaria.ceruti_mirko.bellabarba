package view;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import controllerCommon.ETipoConnessione;
import controllerServer.GestorePartitaRMI;
import controllerServer.ServerScoket;

/**
 * Classe di avvio del programma
 * 
 * @author federico
 * 
 */
public class Start {
	/**
	 * Impostare a <code>true</code> quando in fase di test
	 */
	public static boolean abilitaTest;
	/**
	 * Impostare a <code>true</code> quando in fase di test per 2 giocatori
	 */
	public static Boolean abilitaTest2G;
	/**
	 * Impostare a <code>true</code> per abilitare il logger delle eccezioni
	 */
	public static Boolean abilitaLogger;
	/**
	 * Indica il numero di partite da eseguire durante un test
	 */
	public static final Integer atNUMEROPARTITETEST = 3;

	/**
	 * Metodo main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		GraficaCLC wvGrafica;
		ETipoAvvio wvTipoAvvio;
		ETipoGrafica wvTipoGrafica;
		ETipoConnessione wvTipoConnessione;
		String wvNome;
		abilitaTest = false;
		abilitaTest2G = false;
		abilitaLogger = false;
		// System.out.println("Avvio partita....");

		CLCHelper.stampaAncheInTest("Digita ESCI per uscire, qualunque altra cosa per continuare");
		if (leggi().toUpperCase().equals("ESCI")) {
			return;
		}
		CLCHelper.stampaAncheInTest("Digita LOG per abilitare il loegger degli errori, qualunque altra cosa per continuare");
		if (leggi().toUpperCase().equals("LOG")) {
			abilitaLogger = true;
		}
		wvTipoAvvio = selezionaModalitaAvvio();
		// completare la scelta del tipo di connessione

		wvGrafica = null;
		if (wvTipoAvvio == ETipoAvvio.CLIENT) {
			wvTipoConnessione = selezioneTipoConnessione();
			wvTipoGrafica = selezionaTipoGrafica();
			wvNome = immettiNome();
			wvGrafica = GraficaCLC.istanzia(wvTipoGrafica, wvTipoConnessione, wvNome);
			wvGrafica.avvia();

		} else {
			ServerScoket wvServer = ServerScoket.istanzia();
			GestorePartitaRMI wvServerRmi = GestorePartitaRMI.istanzia();
			wvServerRmi.avvia();
			wvServer.avvia();
		}

		stampa("Terminato");
	}

	/**
	 * Immette il nome
	 * 
	 * @return
	 */
	private static String immettiNome() {
		String wvSelezione;
		stampa("Immetti il tuo nome...");
		wvSelezione = leggi();
		return wvSelezione;
	}

	/**
	 * permette di selezionare la mosalità di avvio
	 * 
	 * @return
	 */
	private static ETipoAvvio selezionaModalitaAvvio() {
		String wvSelezione;
		wvSelezione = "";

		do {

			stampa("Slezionare modalità di avvio:\n- " + ETipoAvvio.CLIENT + "\n- " + ETipoAvvio.SERVER);
			wvSelezione = leggi();
			try {
				return ETipoAvvio.valueOf(wvSelezione.toUpperCase());
			} catch (IllegalArgumentException e) {
				Logger.getGlobal().log(Level.WARNING, "Start", e);
			}

		} while (true);

	}

	/**
	 * selezione il tipo di grafica
	 * 
	 * @return
	 */
	private static ETipoGrafica selezionaTipoGrafica() {
		String wvSelezione;
		wvSelezione = "";
		do {
			stampa("Selezionare tipo di grafica:\n- " + ETipoGrafica.CLC + "\n- " + ETipoGrafica.SWING);

			wvSelezione = leggi();

			try {
				return ETipoGrafica.valueOf(wvSelezione.toUpperCase());
			} catch (IllegalArgumentException e) {
				Logger.getGlobal().log(Level.WARNING, "Start", e);
			}

		} while (true);

	}

	/**
	 * seleziona il tipo di connessione
	 * 
	 * @return
	 */
	private static ETipoConnessione selezioneTipoConnessione() {
		String wvSelezione;
		wvSelezione = "";
		do {
			stampa("Selezionare tipo di connessione:\n- " + ETipoConnessione.RMI + "\n- " + ETipoConnessione.SOCKET);

			wvSelezione = leggi();

			try {
				return ETipoConnessione.valueOf(wvSelezione.toUpperCase());
			} catch (IllegalArgumentException e) {
				Logger.getGlobal().log(Level.WARNING, "Start", e);
			}

		} while (true);
	}

	/**
	 * stampa un messaggio
	 * 
	 * @param Messaggio
	 */
	private static void stampa(Object Messaggio) {
		System.out.println(Messaggio);
	}

	/**
	 * legge da console
	 * 
	 * @return
	 */
	private static String leggi() {
		String wvIstruzione;
		@SuppressWarnings("resource")
		Scanner wvIn = new Scanner(System.in);
		wvIstruzione = wvIn.next();
		wvIn = null;
		return wvIstruzione;
	}

}
