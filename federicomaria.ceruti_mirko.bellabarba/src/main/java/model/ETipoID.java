package model;

/**
 * Enumarativo per indicare il tipo di ID<br>
 * <br>
 * <b>PARTIA</b>:0<br>
 * <b>REGIONE</b>:1<br>
 * <b>STRADA</b>:2<br>
 * <b>ANIMALE</b>:3<br>
 * <b>PEDINA_GIOCATORE</b>:4<br>
 * <b>PEDINA_RECITNO</b>:5<br>
 */
public enum ETipoID {
	PARTITA, REGIONE, STRADA, ANIMALE, PEDINA_GIOCATORE, PEDINA_RECINTO, GIOCATORE, MAPPA
}
