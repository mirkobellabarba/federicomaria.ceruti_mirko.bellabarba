package model;

/**
 * Rappresenta un'estensione della Classe astratta Animale
 * 
 * @author Federico
 * 
 */
public class Agnello extends Animale {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4945350375008183614L;
	private Integer atVita;
	private final Integer atTurniVita = 2;

	/**
	 * Fornisce il costruttore per l'oggetto Agnello
	 * 
	 * @param id
	 *            Id per l'identifacazione dell'oggetto
	 * @param regioneOccupata
	 *            Regione inizialmente occupata dall'Animale
	 */
	public Agnello(Integer id, Regione regioneOccupata) {
		super(id, regioneOccupata);
		atVita = atTurniVita;
	}

	/**
	 * Decrementa il contatore che regola il passaggio all'età adulta
	 * dell'Agnello
	 */
	public void decrementaVita() {
		atVita--;
	}

	/**
	 * Ritorna se l'{@link Agnello} da svezzare
	 * 
	 * @return Ritorna un {@link Boolean} che ritorna
	 */
	public Boolean daSvezzare() {
		return atVita <= 0;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "agnello";
	}
}
