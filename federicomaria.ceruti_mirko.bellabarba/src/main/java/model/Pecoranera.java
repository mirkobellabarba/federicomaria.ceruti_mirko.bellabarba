package model;

/**
 * 
 * Rappresenta un'estensione della Classe astratta Animale
 * 
 * @author Federico
 * 
 */
public class Pecoranera extends Pecora {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3090162664875398260L;

	/**
	 * Fornisce il costruttore per l'oggetto Pecora Nera
	 * 
	 * @param id
	 *            Id per l'identifacazione dell'oggetto
	 * @param regioneOccupata
	 *            Regione inizialmente occupata dalla Pecora Nera
	 */
	public Pecoranera(Integer id, Regione regioneOccupata) {
		super(id, regioneOccupata);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see model.Pecora#toString()
	 */
	public String toString() {
		return "pecora nera";
	}
}
