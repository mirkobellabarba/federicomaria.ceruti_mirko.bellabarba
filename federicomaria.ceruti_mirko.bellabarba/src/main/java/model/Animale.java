package model;

import java.io.Serializable;

/**
 * Classe astratta per la definizione di un Animale che può occupare una Regione
 * 
 * @author Mirko
 * 
 */
public abstract class Animale implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7370348813058065713L;
	private final Integer atID;
	private Regione atRegioneOccupata;

	/**
	 * Fornisce il costruttore per l'oggetto Animale
	 * 
	 * @param id
	 *            Id per l'identifacazione dell'oggetto
	 * @param regioneOccupata
	 *            Regione inizialmente occupata dall'Animale
	 */
	public Animale(Integer id, Regione regioneOccupata) {
		atID = id;
		atRegioneOccupata = regioneOccupata;
	}

	/**
	 * Ritorna l'ID dell'Animale
	 * 
	 * @return ritorna un intero rappresentante l'ID dell'Animale
	 */
	public Integer getId() {
		return atID;
	}

	/**
	 * Ritorna la Regione occupata dall'Animale
	 * 
	 * @return ritorna una Regione rappresentante la Regione occupata
	 *         dall'Animale
	 */
	public Regione getRegioneOccuapta() {
		return atRegioneOccupata;
	}

	/**
	 * Cambia l'attuale Regione che occupa l'Animale con un altra Regione
	 * 
	 * @param regioneArrivo
	 *            Oggetto di tipo Regione che sostituisce l'attuale Regione
	 *            occuapata dall'Animale
	 */
	public void cambiaRegioneOccupata(Regione regioneArrivo) {
		// prima era sposta animale
		// at_regione_occupata.Rimuovi_Animale(this);
		// Regione_Arrivo.Aggiungi_Animale(this);
		atRegioneOccupata = regioneArrivo;
	}
}
