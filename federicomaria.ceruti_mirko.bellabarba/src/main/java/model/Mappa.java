package model;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Regione.LupoPersoException;
import model.Regione.PecoraNeraPersaException;
import model.mosse.ETipoOvino;
import model.mosse.IComunicazione;
import model.mosse.Movimento;
import model.mosse.MovimentoLupo;
import view.CLCHelper;

/**
 * Rappresenta la mappa di gioco
 * 
 * @author Mirko
 * 
 */
public class Mappa implements IComunicazione {

	private final Integer atRECINTIINIZIALI = 20;
	private final Integer atRECINTIFINALIINIZIALI = 12;
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -2505473455618130724L;
	private final Integer atTERRRENIMASSIMI = 5;
	private final Integer atNumeroGiocatori;
	private Integer atRecintiAttuali;
	private Integer atRecintiFinaliAttuali;
	private ArrayList<Giocatore> atGiocatori;
	private final ArrayList<Strada> atStrade;
	private final ArrayList<Animale> atAnimali;
	private final ArrayList<Regione> atRegioni;
	private final Integer[] atTerreniVenduti;

	/**
	 * Costruttore della classe Mappa
	 * 
	 * @param giocatori
	 *            di tipo ArrayList<Giocatore> contiene gli oggetti giocatore
	 * @param regioni
	 *            di tipo ArrayList<Regione> contiene la lista delle regioni
	 * @param strade
	 *            di tipo ArrayList<Strade> contiene la lista delle strade
	 * @param animali
	 *            di tipo ArrayList<Animale> contiene la lista degli animali
	 */
	public Mappa(ArrayList<Giocatore> giocatori, ArrayList<Regione> regioni,
			ArrayList<Strada> strade, ArrayList<Animale> animali) {
		// Qui non viene controllato se Giocatori esiste e se il .Size va in
		// errore
		this.atNumeroGiocatori = giocatori.size();
		this.atGiocatori = giocatori;
		this.atRegioni = regioni;
		this.atStrade = strade;
		this.atAnimali = animali;
		this.atRecintiAttuali = this.atRECINTIINIZIALI;
		this.atRecintiFinaliAttuali = this.atRECINTIFINALIINIZIALI;
		this.atTerreniVenduti = new Integer[ETipoTerreno.values().length - 1];
		for (int i = 0; i < this.atTerreniVenduti.length; i++) {
			this.atTerreniVenduti[i] = 0;
		}
	}

	/**
	 * Ritorna la quantita di terreni venduti per un dato {@link ETipoTerreno}
	 * 
	 * @param tipoTerreno
	 *            Oggetto {@link ETipoTerreno} che indica il tipo di terreno di
	 *            cui controllare il numero delle vendite
	 * @return Ritorna un {@link Integer} che rappresenta il numero di vendite
	 *         del tipo di terreno specificato
	 */
	public Integer getTerreniVenduti(ETipoTerreno tipoTerreno) {
		return this.atTerreniVenduti[tipoTerreno.ordinal()];
	}

	/**
	 * Aumenta di 1 il contatore dei terreni venduti per il tipo specificato
	 * 
	 * @param tipoTerreno
	 *            Rappresenta il tipo di terren di cui aumentare il contatore di
	 *            tessere vendute
	 */
	public void terrenoVenduto(ETipoTerreno tipoTerreno) {
		this.atTerreniVenduti[tipoTerreno.ordinal()]++;
		if (this.atTerreniVenduti[tipoTerreno.ordinal()] > this.atTERRRENIMASSIMI) {
			this.atTerreniVenduti[tipoTerreno.ordinal()]--;
		}
	}

	/**
	 * 
	 * @return un intero che rappresenta il numero di giocatori nella partita
	 */
	public Integer getNumeroGiocatori() {
		return this.atNumeroGiocatori;
	}

	/**
	 * 
	 * @return un ArrayList di giocatori rappresentante tutti gli oggetti
	 *         <bold>Giocatore</bold> attualmente in gioco
	 */
	public ArrayList<Giocatore> getGiocatori() {
		return this.atGiocatori;
	}

	/**
	 * Ritorna il giocatore selezionato passando l'id del giocatore
	 * 
	 * @param IdGiocatore
	 *            l'id del giocatore che si vuole
	 * @return il Giocatore
	 */
	public Giocatore getGiocatore(Integer IdGiocatore) {
		for (Giocatore giocatore : this.atGiocatori) {
			if (giocatore.getId().equals(IdGiocatore)) {
				return giocatore;
			}
		}
		return null;
	}

	/**
	 * Popola la lista dei giocatori con un arraylist di giocatori inviato dal
	 * server. se la lista è già settata nulla verrà cambiato
	 * 
	 * @param giocatori
	 *            l'arraylist di giocatori
	 */
	public void setListaGiocatori(ArrayList<Giocatore> giocatori) {
		if (this.atGiocatori.size() > 0) {
			return;
		}
		this.atGiocatori = giocatori;
	}

	/**
	 * 
	 * @return un ArrayList di regioni rappresentante tutti gli oggetti
	 *         <bold>Regione</bold> che compongono la Mappa
	 */
	public ArrayList<Regione> getRegioni() {
		return this.atRegioni;
	}

	/**
	 * 
	 * @return ArrayList di strade rappresentante tutti gli oggetti
	 *         <bold>Strada</bold> che compongono la Mappa
	 */
	public ArrayList<Strada> getStrade() {
		return this.atStrade;
	}

	/**
	 * Ottiene la strada sapendo il suo id
	 * 
	 * @param idStrada
	 *            l'id della strada
	 * @return l'oggetto strada
	 */
	public Strada getStrada(Integer idStrada) {
		for (Strada strada : this.atStrade) {
			if (strada.getId().equals(idStrada)) {
				return strada;
			}
		}
		return null;
	}

	/**
	 * @return un ArrayList di animali rappresentante tutti gli oggetti
	 *         <bold>Animale</bold> che popolano la Mappa di gioco
	 */
	public ArrayList<Animale> getAnimali() {
		return this.atAnimali;
	}

	/**
	 * Ottieni l'oggetto {@link Pecoranera} della collezzioni di {@link Animale}
	 * della Mappa
	 * 
	 * @return Ritorna un oggetto {@link Pecoranera} che rappresenta la Pecora
	 *         Nera nella Mappa
	 */
	public Pecoranera getPecoranera() {
		for (Animale item : this.atAnimali) {
			if (item instanceof Pecoranera) {
				return (Pecoranera) item;
			}
		}

		return null;
	}

	/**
	 * Ritorna il numero di recinti finali
	 * 
	 * @return {@link Integer} che rappresenta il numero dei recinti finali
	 *         rimasti
	 */
	public Integer getRecintiFinaliAttuali() {
		return this.atRecintiFinaliAttuali;
	}

	/**
	 * @return un intero che rappresenta il numero di <bold>recinti</bold>
	 *         disponibili
	 */
	public Integer getRecintiAttuali() {
		return this.atRecintiAttuali;
	}

	/**
	 * Ottiene un animale passando l'id
	 * 
	 * @param idAnimale
	 *            l'id dell'animale
	 * @return l'animale trovato, null altrimenti
	 */
	public Animale getAnimale(Integer idAnimale) {
		for (Animale animale : this.atAnimali) {
			if (animale.getId().equals(idAnimale)) {
				return animale;
			}

		}
		return null;
	}

	/**
	 * Ottieni il {@link Lupo} presente nella mappa
	 * 
	 * @return
	 */
	public Lupo getLupo() {
		for (Animale item : this.atAnimali) {
			if (item instanceof Lupo) {
				return (Lupo) item;
			}
		}

		return null;
	}

	/**
	 * Sostituisce un animale indicato, con un animale indicato per tipo avente
	 * come id quello del precedente
	 * 
	 * @param idAnimale
	 *            l'id dell'animale che si vuole rimuovere
	 * @param tipoOvino
	 *            il tipo di ovino, Pecora o Montone
	 */
	public void scambiaAnimale(Integer idAnimale, ETipoOvino tipoOvino) {
		Animale animaleDaRimuovere = this.getAnimale(idAnimale);
		Regione regioneOccupata;
		try {
			regioneOccupata = this.getAnimale(idAnimale).getRegioneOccuapta();

		} catch (NullPointerException e) {
			Logger.getGlobal()
					.log(Level.WARNING, this.getClass().toString(), e);
			CLCHelper.stampa("Null in scambia animale " + idAnimale);
			return;
		}

		Integer indexInArray = this.atAnimali.indexOf(animaleDaRimuovere);
		Animale animaleDaAggiungere;
		if (tipoOvino.equals(ETipoOvino.Pecora)) {
			animaleDaAggiungere = new Pecora(idAnimale, regioneOccupata);
		} else {
			animaleDaAggiungere = new Montone(idAnimale, regioneOccupata);
		}

		regioneOccupata.scambiaAnimale(animaleDaRimuovere, animaleDaAggiungere);
		this.atAnimali.set(indexInArray, animaleDaAggiungere);
	}

	/**
	 * Sostituisce un {@link Animale} con un altro
	 * 
	 * @param idAnimale
	 *            {@link Integer} che rappresenta l'ID dell'animale da
	 *            sostituire
	 * @param animaleSostituto
	 *            {@link Animale} che rappresenta ciò che si vuole inserire
	 */
	public void scambiaAnimale(Integer idAnimale, Animale animaleSostituto) {
		for (int i = 0; i < this.atAnimali.size(); i++) {
			if (this.atAnimali.get(i).getId().equals(idAnimale)) {
				Regione wvRegioneOccuapta = this.atAnimali.get(i)
						.getRegioneOccuapta();
				wvRegioneOccuapta.scambiaAnimale(this.atAnimali.get(i),
						animaleSostituto);
				this.atAnimali.set(i, animaleSostituto);
				return;
			}
		}
	}

	/**
	 * Sostituisce un {@link Animale} con un altro
	 * 
	 * @param animaleDaSostituire
	 *            {@link Animale} che deve essere sostituito
	 * @param animaleSostituto
	 *            {@link Animale} con cui sostituire
	 */
	public void scambiaAnimale(Animale animaleDaSostituire,
			Animale animaleSostituto) {
		Integer wvIndice = this.atAnimali.indexOf(animaleDaSostituire);
		Regione wvRegioneOccuapta = animaleDaSostituire.getRegioneOccuapta();
		wvRegioneOccuapta.scambiaAnimale(animaleDaSostituire, animaleSostituto);
		this.atAnimali.set(wvIndice, animaleSostituto);

	}

	/**
	 * Ritorna tutte le strade non occupate
	 * 
	 * @return un array list di strade
	 */
	public ArrayList<Strada> getStradeLibere() {
		ArrayList<Strada> stradeLontane = new ArrayList<Strada>();
		for (Strada strada : this.atStrade) {
			if (!strada.Occupata()) {
				stradeLontane.add(strada);
			}
		}
		return stradeLontane;
	}

	/**
	 * Ritorna tutte le strade non occupate
	 * 
	 * @return un array list di id di strade
	 */
	public ArrayList<Integer> getIdStradeLibere() {
		ArrayList<Integer> wvStradeLibere = new ArrayList<Integer>();
		for (Strada strada : this.atStrade) {
			if (!strada.Occupata()) {
				wvStradeLibere.add(strada.getId());
			}
		}
		return wvStradeLibere;
	}

	/**
	 * Ritorna tutte le strade lontane alla strada passata come parametro
	 * 
	 * @param stradaOccupata
	 *            la strada dove ci si trova attualmente
	 * @return un arrayList di strade lontane
	 */
	public ArrayList<Strada> getStradeLontaneLibere(Strada stradaOccupata) {
		ArrayList<Strada> stradeLontane = new ArrayList<Strada>();
		for (Strada strada : this.atStrade) {
			if (!stradaOccupata.getStradeCollegate().contains(strada)
					&& !strada.Occupata()) {
				stradeLontane.add(strada);
			}
		}
		return stradeLontane;
	}

	/**
	 * Permette di usare il metodo PosizionePedina passando dagli id invece che
	 * dagli oggetti
	 * 
	 * @param idPedina
	 *            l'id della pedina
	 * @param atIdStradaScelta
	 *            l'id della strada scelta
	 */
	public void posizionaPedina(Integer idPedina, Integer atIdStradaScelta) {
		this.posizionaPedina(this.getPedinaPastore(idPedina),
				this.getStrada(atIdStradaScelta));
	}

	/**
	 * Posiziona una pedina nella strada indicata
	 * 
	 * @param wvPedina
	 *            l'oggetto pedina
	 * @param wvStradaOccupataStrada
	 *            la strada da occupare
	 */
	public void posizionaPedina(PedinaPastore wvPedina,
			Strada wvStradaOccupataStrada) {
		Strada strada = this.atStrade.get(this.atStrade
				.indexOf(wvStradaOccupataStrada));
		strada.setOccupante(wvPedina);
	}

	/**
	 * Rimuove un animale da un aregione specificata
	 * 
	 * @param animale
	 *            {@link Animale} che rappresenta l'animale da rimuovere
	 * @param regione
	 *            {@link Regione} che rappresenta la regione in cui rimuovere
	 *            l'animale
	 */
	public void rimuoviAnimale(Animale animale, Regione regione) {
		this.atAnimali.remove(animale);
		regione.rimuoviAnimale(animale);
	}

	/**
	 * Rimuove un animale dalla regione indicata
	 * 
	 * @param atIdAnimale
	 *            l'id dell'animale
	 * @param idRegione
	 *            l'id della regione
	 * @throws PecoraNeraPersaException
	 * @throws LupoPersoException
	 */
	public void rimuoviAnimale(Integer atIdAnimale, Integer idRegione)
			throws PecoraNeraPersaException, LupoPersoException {
		Regione regioneColpita = this.getRegione(idRegione);
		regioneColpita.rimuoviAnimale(atIdAnimale);
		this.eliminaAnimale(atIdAnimale);
	}

	/**
	 * Rimuove un animale dalla regione in cui è
	 * 
	 * @param idAnimale
	 *            {@link Integer} che rappresenta lid dell'animale da rimuovere
	 * @throws PecoraNeraPersaException
	 * @throws LupoPersoException
	 */
	public void rimuoviAnimale(Integer idAnimale)
			throws PecoraNeraPersaException, LupoPersoException {
		Animale wvAnimale = this.getAnimale(idAnimale);
		this.atAnimali.remove(wvAnimale);
		wvAnimale.getRegioneOccuapta().rimuoviAnimale(idAnimale);
	}

	/**
	 * rimuove un animale dalla lista degli Animali
	 * 
	 * @param idAnimale
	 *            l'id dell'animale da eliminare
	 */
	private void eliminaAnimale(Integer idAnimale) {
		Animale wvAnimale = this.getAnimale(idAnimale);
		this.atAnimali.remove(wvAnimale);
	}

	/**
	 * Ottiene la regione passando il suo id
	 * 
	 * @param idRegione
	 *            l'id della regione
	 * @return una regione
	 */
	public Regione getRegione(Integer idRegione) {
		for (Regione regione : this.atRegioni) {
			if (regione.getId().equals(idRegione)) {
				return regione;
			}
		}
		return null;
	}

	/**
	 * Aggiunge un agnello alla regione indicata
	 * 
	 * @param idAgnello
	 *            di tipo nascita
	 * @param idRegione
	 * 
	 */
	public void aggiungiAgnello(Integer idAgnello, Integer idRegione) {
		Regione regione = this.getRegione(idRegione);
		Agnello agnello = new Agnello(idAgnello, regione);
		regione.aggiungiAnimale(agnello);
		this.atAnimali.add(agnello);
	}

	/**
	 * Interpreta e aggiorna i valori coinvolti da una mossa Movimento
	 * 
	 * @param mossa
	 *            la mossa ricevuta dal server
	 * @throws PecoraNeraPersaException
	 * @throws LupoPersoException
	 */
	public void movimento(Movimento mossa) throws PecoraNeraPersaException,
			LupoPersoException {
		if (mossa.isAnimale()) {
			if (mossa instanceof MovimentoLupo) {
				Integer wvIdAnimaleMangiato = ((MovimentoLupo) mossa)
						.getIdAnimaleMangiato();
				if (!wvIdAnimaleMangiato.equals(new Integer(-1))) {
					CLCHelper.stampa("Lupo mangia animale: "
							+ wvIdAnimaleMangiato);
					// rimuovo l'animale da regione e mappa
					this.rimuoviAnimale(wvIdAnimaleMangiato);
				}

			}
			Animale animale = this.getAnimale(mossa.getAtIdSoggetto());
			// CLCHelper.Stampa("Sto per muovere l'animale "+animale.getId()+" di tipo "+animale.getClass().toString());
			this.getRegione(mossa.getAtIdVecchiaPosizione()).rimuoviAnimale(
					mossa.getAtIdSoggetto());
			animale.cambiaRegioneOccupata(this.getRegione(mossa
					.getAtIdNuovaPosizione()));
			this.getRegione(mossa.getAtIdNuovaPosizione()).aggiungiAnimale(
					animale);
			// CLCHelper.Stampa("Mosso");

		} else {
			PedinaPastore pedina = this.getPedinaPastore(mossa
					.getAtIdSoggetto());
			Strada wvNuovaStrada = this
					.getStrada(mossa.getAtIdNuovaPosizione());
			Strada wvVecchiaStrada = this.getStrada(mossa
					.getAtIdVecchiaPosizione());
			// aggiorno al nuova strada in pedina
			pedina.setStradaOccupata(wvNuovaStrada);
			// metto un recinto nella strada vecchia
			this.riduciRecinti();
			if (this.atRecintiAttuali.compareTo(0) > 0) {
				wvVecchiaStrada.pastoreLascia();
			} else {
				wvVecchiaStrada.pastoreLasciaFinale();
			}
			// segnallo alla strada che ora "ospita" un pastore
			wvNuovaStrada.setOccupante(pedina);
		}

	}

	/**
	 * Cerca la pecora nera nella mappa
	 * 
	 * @return
	 */
	public Regione cercaPecoraNera() {
		for (Regione regione : this.atRegioni) {
			if (regione.occupataDaPecoraNera()) {
				return regione;
			}
		}
		return null;
	}

	/**
	 * Riduce di uno il conteggio dei recinti
	 */
	public void riduciRecinti() {
		if (this.atRecintiAttuali.compareTo(0) > 0) {
			this.atRecintiAttuali--;
		} else {
			this.atRecintiFinaliAttuali--;
		}
	}

	/**
	 * Ritorna la pedina pastore conoscendo l'id di quest'ultima
	 * 
	 * @param idPedina
	 *            l'id della pedina
	 * @return una pedina pastore
	 */
	public PedinaPastore getPedinaPastore(Integer idPedina) {
		for (Giocatore giocatore : this.getGiocatori()) {
			for (PedinaPastore pedina : giocatore.getPedinePastore()) {
				if (pedina.getId().equals(idPedina)) {
					return pedina;
				}
			}
		}
		return null;
	}

	/**
	 * Aggiorna il bilancio economico di un giocatore
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 * @param costo
	 *            il costo della mossa
	 */
	public void aggiornaDanari(Integer idGiocatore, Integer costo) {
		this.getGiocatore(idGiocatore).spendiDanaro(costo);
	}

	/**
	 * Ritorna gli oggetti di tipo {@link Agnello} da svezzare
	 * 
	 * @return Ritorna un {@link ArrayList} di {@link Agnello} che contiene gli
	 *         agnelli da svezzare
	 */
	public ArrayList<Agnello> getAgnelliSvezzamento() {
		ArrayList<Agnello> wvAgnelliSvezzamento = new ArrayList<Agnello>();
		for (Animale wvAnimale : this.atAnimali) {
			if (wvAnimale instanceof Agnello) {
				Agnello wvAgnello = (Agnello) wvAnimale;
				wvAgnello.decrementaVita();
				if (wvAgnello.daSvezzare()) {
					wvAgnelliSvezzamento.add(wvAgnello);
				}

			}
		}
		return wvAgnelliSvezzamento;
	}

	/**
	 * Aggiunge la pecora nera alla mappa
	 * 
	 * @param animale
	 */
	public void AggiungiPecoraNera(Animale animale) {
		if (animale instanceof Pecoranera) {
			this.atAnimali.add(animale);
		}
	}

	/**
	 * cerca il lupo nella mappa
	 * 
	 * @return
	 */
	public Regione cercaLupo() {
		for (Regione regione : this.atRegioni) {
			if (regione.occupataDaLupo()) {
				return regione;
			}
		}
		return null;
	}

	/**
	 * Aggiunge il lupo alla mappa
	 * 
	 * @param animale
	 */
	public void AggiungiLupo(Animale animale) {
		if (animale instanceof Lupo) {
			this.atAnimali.add(animale);
		}
	}
}
