package model;

/**
 * il recinto finale
 * 
 * @author Mirko
 * 
 */
public class PedinaRecintoFinale extends PedinaRecinto {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8337784283683138114L;

	public PedinaRecintoFinale(Integer id) {
		super(id);
	}

	/**
	 * costruttore
	 */
	public PedinaRecintoFinale() {
		super();
	}
}
