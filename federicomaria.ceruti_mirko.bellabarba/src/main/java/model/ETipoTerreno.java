package model;

/**
 * gli enum del terreno
 * 
 * @author Mirko
 * 
 */
public enum ETipoTerreno {
	BOSCO, COLLINA, DESERTO, LAGO, MONTAGNA, PIANURA, SHEEPSBURG;

	/**
	 * Ottiene un {@link ETipoTerreno} dato il valore Ordinal ad esso associato
	 * 
	 * @param ordinal
	 *            Valore .Ordinal() associato al tipo terreno
	 * @return Ritorna un oggetto di tipo {@link ETipoTerreno}
	 */
	public static ETipoTerreno getTerreno(Integer ordinal) {
		switch (ordinal) {
		case 0:
			return BOSCO;
		case 1:
			return COLLINA;
		case 2:
			return DESERTO;
		case 3:
			return LAGO;
		case 4:
			return MONTAGNA;
		case 5:
			return PIANURA;
		default:
			return SHEEPSBURG;
		}
	}

}
