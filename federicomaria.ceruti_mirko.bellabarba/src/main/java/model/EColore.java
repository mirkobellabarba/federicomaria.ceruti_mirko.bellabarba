package model;

/**
 * gli enum del colore
 * 
 * @author Mirko
 * 
 */
public enum EColore {
	ROSSO, GIALLO, BLU, VERDE;
}
