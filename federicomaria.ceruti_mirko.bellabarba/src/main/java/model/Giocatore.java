package model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.print.DocFlavor.STRING;

/**
 * Calsse ch rappresenta il giocatore
 */
public class Giocatore implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4583768475506958970L;
	private final Integer atID;
	private String atNome;
	private Integer atNumeroGiocatore;
	private Integer atDanaroResiduo;
	private Integer atPunteggio;
	private ETipoTerreno atTesseraTerrenoIniziale;
	/*
	 * Dato che ogni giocatore pu� possedere pi� di una tessera per tipo
	 * terreno, questa possibilit� comparir� come un elemento in pi� nella lista
	 */
	private ArrayList<ETipoTerreno> atTessereTerrenoPossedute;

	private PedinaPastore[] atPedine;

	/**
	 * Eccezzione che occorre quando si tenta di rimuovere una tessera non
	 * esistente
	 */
	public class RimozioneTesseraException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * Costruttore
		 * 
		 * @param Messaggio
		 *            {@link String} messaggio da contenere nell'eccezione
		 */
		public RimozioneTesseraException(String Messaggio) {
			super(Messaggio);
		}

	}

	/**
	 * Costruttore
	 * 
	 * @param iD
	 *            {@link Integer} che rappresenta l'id del giocatore
	 * @param nome
	 *            {@link String} che rappresenta il nome del gcoatore
	 * @param numeroGiocatore
	 *            {@link Integer} numero del gicoatore all'interno del turno
	 * @param tesseraTerrenoIniziale
	 *            {@link ETipoTerreno} rappresenta il terreno iniziale
	 */
	public Giocatore(Integer iD, String nome, Integer numeroGiocatore, ETipoTerreno tesseraTerrenoIniziale) {
		this.atID = iD;
		this.atNome = nome;
		this.atNumeroGiocatore = numeroGiocatore;
		this.atPunteggio = 0;
		this.atTessereTerrenoPossedute = null;
		this.atPedine = null;
		this.atTesseraTerrenoIniziale = tesseraTerrenoIniziale;
		aggiungiTesseraTerreno(this.atTesseraTerrenoIniziale);
	}

	/**
	 * Costruttore
	 * 
	 * @param iD
	 *            {@link Integer} che rappresenta l'id del giocatore
	 * @param nome
	 *            {@link String} che rappresenta il nome del gcoatore
	 * @param numeroGiocatore
	 *            {@link Integer} numero del gicoatore all'interno del turno
	 * @param pedinePastore
	 *            Array di {@link PedinaPastore} che rappresennta le pedine del
	 *            gicoatore
	 * @param tesseraTerrenoIniziale
	 *            {@link ETipoTerreno} rappresenta il terreno iniziale
	 */
	public Giocatore(Integer iD, String nome, Integer numeroGiocatore, PedinaPastore[] pedinePastore, ETipoTerreno tesseraTerrenoIniziale) {
		this.atID = iD;
		this.atNome = nome;
		this.atNumeroGiocatore = numeroGiocatore;
		this.atPunteggio = 0;
		this.atTessereTerrenoPossedute = null;
		this.atPedine = pedinePastore;
		this.atTesseraTerrenoIniziale = tesseraTerrenoIniziale;
		aggiungiTesseraTerreno(atTesseraTerrenoIniziale);
	}

	/**
	 * 
	 * Costruttore
	 * 
	 * @param iD
	 *            {@link Integer} che rappresenta l'id del giocatore
	 * @param nome
	 *            {@link String} che rappresenta il nome del gcoatore
	 * @param numeroGiocatore
	 *            {@link Integer} numero del gicoatore all'interno del turno
	 * @param pedinePastore
	 *            Array di {@link PedinaPastore} che rappresennta le pedine del
	 *            gicoatore
	 * @param tesseraTerrenoIniziale
	 *            {@link ETipoTerreno} rappresenta il terreno iniziale
	 * @param denaroIniziale
	 *            {@link Integer} che rappresenta la quantità di danaro iniziale
	 */
	public Giocatore(Integer iD, String nome, Integer numeroGiocatore, PedinaPastore[] pedinePastore, ETipoTerreno tesseraTerrenoIniziale,
			Integer denaroIniziale) {
		this.atID = iD;
		this.atNome = nome;
		this.atNumeroGiocatore = numeroGiocatore;
		this.atPunteggio = 0;
		this.atTessereTerrenoPossedute = null;
		this.atPedine = pedinePastore;
		this.atTesseraTerrenoIniziale = tesseraTerrenoIniziale;
		aggiungiTesseraTerreno(this.atTesseraTerrenoIniziale);
		this.atDanaroResiduo = denaroIniziale;
	}

	/**
	 * Ritorna l'id del giocatore
	 * 
	 * @return {@link Integer} che raprpesenta l'id
	 */
	public Integer getId() {
		return this.atID;
	}

	/**
	 * Ritorna il nome del gicoatore
	 * 
	 * @return {@link STRING} che rappresenta il nome del giocatore
	 */
	public String getNome() {
		return this.atNome;
	}

	/**
	 * Ritorna il numero del giocatore nel turno
	 * 
	 * @return {@link Integer} che rappresenta il numero del giocatore nel turno
	 */
	public Integer getNumeroGiocatore() {
		return this.atNumeroGiocatore;
	}

	/**
	 * Ritorna il danaro residuo del gicoatore
	 * 
	 * @return {@link Integer} che rappresenta il danaro residuo
	 */
	public Integer getDanaroResiduo() {
		return this.atDanaroResiduo;
	}

	/**
	 * Ritorna il punteggio del gicatore
	 * 
	 * @return {@link Integer} che rappresenta il punteggio del gicoatore
	 */
	public Integer getPunteggio() {
		return this.atPunteggio;
	}

	/**
	 * Ritorna i terreni comprati
	 * 
	 * @return {@link ArrayList} di {@link ETipoTerreno} che rappresentano i
	 *         terreni comprati
	 */
	public ArrayList<ETipoTerreno> getTerreniComprati() {
		return atTessereTerrenoPossedute;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Ritorna i tipi di terreno posseduti
	 * @return {@link ArrayList} di {@link ETipoTerreno} che rappresentano i terreni posseduti
	 */
	public ArrayList<ETipoTerreno> getTipiTerreniPosseduti() {
		ArrayList<ETipoTerreno> wvListaTessere = new ArrayList<ETipoTerreno>();
		if (this.atTessereTerrenoPossedute != null) {
			wvListaTessere.addAll(atTessereTerrenoPossedute);
		}

		return (ArrayList<ETipoTerreno>) wvListaTessere.clone();
	}

	/**
	 * Ritorna le pedine del pastore
	 * 
	 * @return Array di {@link PedinaPastore} che rappresenta le pedine del
	 *         pastore
	 */
	public PedinaPastore[] getPedinePastore() {
		return this.atPedine;
	}

	/**
	 * Imposta le pedine del pastore
	 * 
	 * @param pedine
	 *            Array di {@link PedinaPastore} che rappresenta le pedine del
	 *            pastore
	 */
	public void setPedinePastore(PedinaPastore[] pedine) {
		atPedine = pedine;
	}

	/**
	 * Imposta il punteggio del giocatore
	 * 
	 * @param punteggio
	 *            {@link Integer} che rappresenta il punteggio del gicoatore
	 */
	public void setPunteggo(Integer punteggio) {
		this.atPunteggio = punteggio;
	}

	/**
	 * Setta il denaro ad un valore se e solo se il valore at_danaro_residuo
	 * risulta null. Da richiamare quando devo dare denaro ai giocatori dopo
	 * aver saputo quanto giocatori giocano
	 * 
	 * @param danaro
	 *            è un Integer (20 o 30)
	 */
	public void setDenaroIniziale(Integer danaro) {
		if (atDanaroResiduo == null)
			atDanaroResiduo = danaro;
	}

	/**
	 * Riduce il danaro residuo della quantità specificata
	 * 
	 * @param spesa
	 *            {@link Integer} che rappresenta di quanto ridurre il danaro
	 */
	public void spendiDanaro(Integer spesa) {
		this.atDanaroResiduo -= spesa;
	}

	/**
	 * Aggiunge una tessera terreno al gicoatore
	 * 
	 * @param tipoTerreno
	 *            {@link ETipoTerreno} che rappresenta il terreno da aggiungere
	 */
	public void aggiungiTesseraTerreno(ETipoTerreno tipoTerreno) {
		if (this.atTessereTerrenoPossedute == null) {
			this.atTessereTerrenoPossedute = new ArrayList<ETipoTerreno>();
		}

		this.atTessereTerrenoPossedute.add(tipoTerreno);
	}

	/**
	 * Rimuove la tessera tereno specificata
	 * 
	 * @param tipoTerreno
	 *            {@link ETipoTerreno} che rappresenta il terreno da rimuovere
	 * @throws RimozioneTesseraException
	 */
	public void rimuoviTesseraTerreno(ETipoTerreno tipoTerreno) throws RimozioneTesseraException {
		if (this.atTessereTerrenoPossedute == null) {
			throw new RimozioneTesseraException("Non è presente alcuna tessera");
		}

		if (!this.atTessereTerrenoPossedute.contains(tipoTerreno)) {
			throw new RimozioneTesseraException("Tessera/Tipo Terreno non posseduta/o dal giocatore: " + this.toString());
		}

		this.atTessereTerrenoPossedute.remove(tipoTerreno);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Nome: " + this.atNome + " Numero: " + this.atNumeroGiocatore;
	}

	/**
	 * Permette di aggiornare la posizione di una pedina
	 * 
	 * @param idPedina
	 *            l'id della pedina
	 * @param wvStradaOccupataStrada
	 *            la strada che verrà occupata dopo il movimento
	 * @return la pedina con i paramentri aggiornati
	 */
	public PedinaPastore aggiornaPosizionePedina(Integer idPedina, Strada wvStradaOccupataStrada) {
		PedinaPastore pedina = getPedina(idPedina);
		pedina.setStradaOccupata(wvStradaOccupataStrada);
		return pedina;
	}

	/**
	 * Ottiene la pedina specificando l'id
	 * 
	 * @param idPedina
	 *            l'id della pedina
	 * @return la pedina corrispondente
	 */
	public PedinaPastore getPedina(Integer idPedina) {
		for (PedinaPastore pedina : atPedine) {
			if (pedina.getId().equals(idPedina)) {
				return pedina;
			}
		}
		return null;
	}
}
