package model;

/**
 * 
 * Rappresenta un'estensione della Classe astratta Occupante
 * 
 * @author Federico
 * 
 */
public class PedinaPastore extends Occupante {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8159428387030068984L;
	/**
	 * giocatore che possiede la pedina
	 */
	private Giocatore atGiocatore;
	private EColore atColore;

	/**
	 * costruttore
	 * 
	 * @param id
	 * @param colore
	 */
	public PedinaPastore(Integer id, EColore colore) {
		super(id);
		atColore = colore;
		atGiocatore = null;
	}

	/**
	 * ritorna il colore
	 * 
	 * @return
	 */
	public String getColore() {
		return atColore.toString();
	}

	/**
	 * ritorna il giocatore che possiede la pedina
	 * 
	 * @return
	 */
	public Giocatore getGiocatorePossessore() {
		// TODO return null
		return atGiocatore;
	}

	/**
	 * setta il giocatore possessore
	 * 
	 * @param giocatore
	 */
	public void setGiocatorePossessore(Giocatore giocatore) {
		if (atGiocatore != null)
			return;
		atGiocatore = giocatore;
	}

	/**
	 * ritorna il colore
	 * 
	 * @return
	 */
	public EColore getEColore() {
		return atColore;
	}
}
