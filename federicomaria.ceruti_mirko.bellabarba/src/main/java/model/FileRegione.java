package model;

/**
 * classe di appoggio che prende i dati delle regioni da un file e genera i
 * corrispondenti oggetti regione
 * 
 * @author Mirko
 * 
 */
public class FileRegione {
	private Integer id;
	private ETipoTerreno tipoTerreno;
	private Integer[] stradeLimitrofe;

	public FileRegione(Integer id, ETipoTerreno tipoTerreno, Integer[] stradeLimitrofe) {
		this.setId(id);
		this.setTipoTerreno(tipoTerreno);
		this.setStradeLimitrofe(stradeLimitrofe);
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the tipoTerreno
	 */
	public ETipoTerreno getTipoTerreno() {
		return tipoTerreno;
	}

	/**
	 * @param tipoTerreno
	 *            the tipoTerreno to set
	 */
	public void setTipoTerreno(ETipoTerreno tipoTerreno) {
		this.tipoTerreno = tipoTerreno;
	}

	/**
	 * @return the stradeLimitrofe
	 */
	public Integer[] getStradeLimitrofe() {
		return stradeLimitrofe;
	}

	/**
	 * @param stradeLimitrofe
	 *            the stradeLimitrofe to set
	 */
	public void setStradeLimitrofe(Integer[] stradeLimitrofe) {
		this.stradeLimitrofe = stradeLimitrofe;
	}
}
