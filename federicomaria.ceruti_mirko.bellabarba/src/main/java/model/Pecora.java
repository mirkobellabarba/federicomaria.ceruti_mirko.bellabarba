package model;

/**
 * 
 * Rappresenta un'estensione della Classe astratta Animale
 * 
 * @author Federico
 * 
 */
public class Pecora extends Animale {
	/**
	 * 
	 */
	private static final long serialVersionUID = -474810716156711365L;

	/**
	 * Fornisce il costruttore per l'oggetto Pecora
	 * 
	 * @param id
	 *            Id per l'identifacazione dell'oggetto
	 * @param regioneOccupata
	 *            Regione inizialmente occupata dalla Pecora
	 */
	public Pecora(Integer id, Regione regioneOccupata) {
		super(id, regioneOccupata);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "pecora";
	}
}
