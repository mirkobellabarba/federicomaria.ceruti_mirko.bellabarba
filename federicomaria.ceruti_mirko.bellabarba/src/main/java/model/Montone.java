package model;

/**
 * 
 * Rappresenta un'estensione della Classe astratta Animale
 * 
 * @author Federico
 * 
 */
public class Montone extends Animale {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2347881613734641391L;

	/**
	 * Fornisce il costruttore per l'oggetto Montone
	 * 
	 * @param id
	 *            Id per l'identifacazione dell'oggetto
	 * @param regioneOccupata
	 *            Regione inizialmente occupata dal Montone
	 */
	public Montone(Integer id, Regione regioneOccupata) {
		super(id, regioneOccupata);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "montone";
	}

}
