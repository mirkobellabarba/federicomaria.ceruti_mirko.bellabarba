package model.mosse;

/**
 * Mossa vuota, utile da inviare per far salatre i normali corsi del gioco
 * 
 * @author federico
 * 
 */
public class MossaVuota implements IMossa {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2556771059367819936L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "Mossa vuota";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "";
	}
}
