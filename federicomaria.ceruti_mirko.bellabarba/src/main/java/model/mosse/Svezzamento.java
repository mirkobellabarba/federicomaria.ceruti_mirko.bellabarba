package model.mosse;

import model.Agnello;

/**
 * Mossa di tipo Svezzamento, serve per indicare l'evoluzione di un
 * {@link Agnello}
 */
public class Svezzamento implements IMossa {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2314761180915929523L;
	private Integer at_id_agnello;
	private ETipoOvino at_tipo_ovino;

	/**
	 * costruttore
	 * 
	 * @param idAgnello
	 *            {@link Integer} che rappresenta l'ID dell'agnello svezzato
	 * @param tipoOvino
	 *            {@link ETipoOvino} che rappresenta il tipo di ovino che è
	 *            diventato l'{@link Agnello} dopo lo svezzameto
	 */
	public Svezzamento(Integer idAgnello, ETipoOvino tipoOvino) {
		at_id_agnello = idAgnello;
		at_tipo_ovino = tipoOvino;
	}

	/**
	 * Ritorna l'ID dell'agnello
	 * 
	 * @return {@link Integer} che rappresenta l'id dell'agnello
	 */
	public Integer getIdAgnello() {
		return at_id_agnello;
	}

	/**
	 * ritorna il tipo di ovino in cui si è svezzato l'agnello
	 * 
	 * @return {@link ETipoOvino}
	 */
	public ETipoOvino getTipoOvino() {
		return at_tipo_ovino;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return null;
	}

}
