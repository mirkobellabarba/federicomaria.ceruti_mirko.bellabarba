package model.mosse;

import model.ETipoTerreno;

/**
 * Offerta del mercato
 * 
 */
public class Offerta implements IMossa {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3100646555366355012L;
	private transient ETipoTerreno at_tipo_terreno;
	private transient Integer at_prezzo;
	private transient Integer at_venditore;

	/**
	 * Costruttore dell'offerta
	 * 
	 * @param tipo
	 *            {@link ETipoTerreno} che rappresenta il terreno messo in
	 *            vendita nell'offerta
	 * @param idVenditore
	 *            {@link Integer} che rappresenta l'id del giocatore he vende il
	 *            terreno
	 */
	public Offerta(ETipoTerreno tipo, Integer idVenditore) {
		this.at_tipo_terreno = tipo;
		this.at_prezzo = 0;
		this.at_venditore = idVenditore;
	}

	/**
	 * Costruttore
	 * 
	 * @param tipo
	 *            {@link ETipoTerreno} che rappresenta il terreno messo in
	 *            vendita nell'offerta
	 * @param idVenditore
	 *            {@link Integer} che rappresenta l'id del giocatore he vende il
	 *            terreno
	 * @param prezzo
	 *            {@link Integer} che rappresenta il prezzo di vendita
	 */
	public Offerta(ETipoTerreno tipo, Integer idVenditore, Integer prezzo) {
		this.at_tipo_terreno = tipo;
		this.at_prezzo = prezzo;
		this.at_venditore = idVenditore;
	}

	/**
	 * Imosta il prezzo dell'{@link Offerta}
	 * 
	 * @param prezzo
	 *            {@link Integer} che rappresenta il prezzo dell'offerta
	 */
	public void setPrezzo(Integer prezzo) {
		at_prezzo = prezzo;
	}

	/**
	 * Ritorna il tipo terreno che viene venduto
	 * 
	 * @return un enum E_Tipo_Terreno rappresentatnte il tipo di terreno messo
	 *         in vendita
	 */
	public ETipoTerreno getTerreno() {
		return at_tipo_terreno;
	}

	/**
	 * Dato un oggetto vendesi, ottengo il prezzo con il quale esso viene
	 * venduto sul mercato
	 * 
	 * @return un Integer che rappresenta il prezzo del terreno venduto
	 */
	public Integer getPrezzo() {
		return at_prezzo;
	}

	/**
	 * Dato un terreno, posso ottenere l'ID del giocatore che ha messo in
	 * vendita quel terreno
	 * 
	 * @return un Integer che rappresenta l'id del giocatore
	 */
	public Integer getVenditore() {
		return at_venditore;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "tipo: " + getTerreno() + " costo: " + getPrezzo() + " venduto da: " + getVenditore();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "tipo: " + getTerreno();
	}

}
