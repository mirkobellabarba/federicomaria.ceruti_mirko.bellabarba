package model.mosse;

import model.Animale;
import model.Lupo;
import model.Regione;

/**
 * Classe da inviare per muovere il lupo
 * 
 * @author federico
 * 
 */
public class MovimentoLupo extends Movimento {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1827283441383243427L;
	private final Integer atAnimaleMangiato;

	/**
	 * Costruttore della mossa per muovere il {@link Lupo}
	 * 
	 * @param idLupo
	 *            {@link Integer} che rappresenta l'ID del {@link Lupo}
	 * @param idVecchiaPosizione
	 *            {@link Integer} che rappresenta l'ID della {@link Regione} di
	 *            partenza del {@link Lupo}
	 * @param idNuovaPosizione
	 *            {@link Integer} che rappresenta l'ID della {@link Regione} in
	 *            cui si sposta il {@link Lupo}
	 * @param idAnimaleMangiato
	 *            {@link Integer} che rappresenta l'ID dell'{@link Animale}
	 *            mangiato
	 */
	public MovimentoLupo(Integer idLupo, Integer idVecchiaPosizione, Integer idNuovaPosizione, Integer idAnimaleMangiato) {
		super(idLupo, idVecchiaPosizione, idNuovaPosizione, 0);
		this.atAnimaleMangiato = idAnimaleMangiato;
		this.setAtIsAnimale(true);
	}

	/**
	 * Ritorna un valore che indica se è stato mangiato un animale
	 * 
	 * @return Ritorna <code>true</code> se è stato mangiato un {@link Animale},
	 *         <code>false</code> altrimenti
	 */
	public Boolean haMangiatoAnimale() {
		return !this.atAnimaleMangiato.equals(-1);
	}

	/**
	 * Ritorna id dell'{@link Animale} mangiato
	 * 
	 * @return Id dell'{@link Animale} mangiato
	 */
	public Integer getIdAnimaleMangiato() {
		return this.atAnimaleMangiato;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.Movimento#toString()
	 */
	public String toString() {
		String wvStringa = new String("");
		wvStringa += "è stato mosso il Lupo: " + this.getAtIdSoggetto() + " nella regione: " + this.getAtIdNuovaPosizione();
		if (this.haMangiatoAnimale()) {
			wvStringa += " mangiando l'animale " + this.getIdAnimaleMangiato();
		} else {
			wvStringa += " senza mangaire animali";
		}
		return wvStringa;
	}
}
