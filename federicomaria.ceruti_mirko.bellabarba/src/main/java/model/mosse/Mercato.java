/**
 * 
 */
package model.mosse;

import java.util.ArrayList;

/**
 * Mossa per effetuare il mercato
 * 
 * @author federico
 * 
 */
public class Mercato implements IMossa {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7969116023392451497L;

	ArrayList<Offerta> at_offerte;

	/**
	 * Costruttore
	 */
	public Mercato() {
		at_offerte = new ArrayList<Offerta>();
	}

	/**
	 * Aggiunge un'{@link Offerta} alla lista
	 * 
	 * @param offerta
	 *            {@link Offerta} che si vuole aggiungere
	 */
	public void aggiungiOfferta(Offerta offerta) {
		at_offerte.add(offerta);
	}

	/**
	 * Aggiugne un elenco di offerte alla lista di offerte
	 * 
	 * @param offerte
	 *            elenco i offerte da aggiungere
	 */
	public void aggiungiOfferte(ArrayList<Offerta> offerte) {
		for (Offerta offerta : offerte) {
			at_offerte.add(new Offerta(offerta.getTerreno(), offerta.getVenditore(), offerta.getPrezzo()));
		}
	}

	/**
	 * Rimuove un'{@link Offerta} dall'elenco di offerte
	 * 
	 * @param offerta
	 *            {@link Offerta} da rimuovere
	 */
	public void rimuoviOfferta(Offerta offerta) {
		at_offerte.remove(offerta);
	}

	/**
	 * Rimuove un'{@link Offerta} dall'elenco di offerte
	 * 
	 * @param indice
	 *            {@link Integer} che rappresenta l'indice dell'offerta da
	 *            rimuovere
	 */
	public void rimuoviOfferta(Integer indice) {
		at_offerte.remove(indice);
	}

	/**
	 * Ritorna un'{@link Offerta} specificandone l'indice
	 * 
	 * @param indice
	 *            {@link Integer} che rappresenta l'indice dell'offerta
	 * @return Ritorna un {@link Offerta}
	 */
	public Offerta getOfferta(Integer indice) {
		return at_offerte.get(indice);
	}

	/**
	 * Ritorna la lista di offerte
	 * 
	 * @return la lista delle offerte
	 */
	public ArrayList<Offerta> getOfferte() {
		return at_offerte;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "";
	}

}
