package model.mosse;

/**
 * 
 */
public class tentativoFallito implements IMossa {

	private static final long serialVersionUID = -4430429831534160390L;
	private String atMessaggio;

	/**
	 * Costruttore
	 * 
	 * @param messaggio
	 *            {@link String} che rappresenta il messaggio da inivare
	 */
	public tentativoFallito(String messaggio) {
		super();
		atMessaggio = messaggio;
	}

	/**
	 * Ritorna il messaggio contenuto nela mossa
	 * 
	 * @return {@link String} che rappresenta il messaggio
	 */
	public String getMessaggio() {
		return atMessaggio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "";
	}
}
