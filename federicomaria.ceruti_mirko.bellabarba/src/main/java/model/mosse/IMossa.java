package model.mosse;

/**
 * Interfaccia che devono esterndere le mosse. Estende {@link IComunicazione}
 * 
 * @author federico
 * 
 */
public interface IMossa extends IComunicazione {

	/**
	 * Ritorna una stringa che rappresenta la proposta stampabile della mossa
	 * 
	 * @return {@link String} che rappresenta la proposta testuale della mossa
	 */
	public String propostaMossa();
}
