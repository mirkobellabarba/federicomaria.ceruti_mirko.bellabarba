package model.mosse;

import java.util.ArrayList;

/**
 * 
 * Classe che fornisce l'implementazione di un messaggio di controllo generico
 * serializzabile da inviare tra client e server<br>
 * <b>Importante</b>: Una volta impostato il messaggio, lo si potra solo leggere
 * e <b>NON</b> si potrà modificare.
 */
public class Messaggio implements IComunicazione {
	private static final long serialVersionUID = 130317495691130953L;
	private StringBuilder atMessaggio;

	/**
	 * Costruttore che imposta il messaggio come messaggio vuoto
	 */
	public Messaggio() {
		atMessaggio = null;
	}

	/**
	 * Ritorna il messaggio contenuto come stringa
	 * 
	 * @return
	 */
	public String getMessaggio() {
		return atMessaggio.toString();
	}

	/**
	 * Ritorna l'intestazione di un messaggio
	 * 
	 * @return un EIntestazioneMessaggio
	 */
	public EIntestazioneMessaggio getIntestazione() {
		String[] wvBlocchi = atMessaggio.toString().split("\\:");
		return EIntestazioneMessaggio.valueOf(wvBlocchi[0]);
	}

	/**
	 * Ritorna i parametri del messagio
	 * 
	 * @return Arrai di {@link String} che rappresent ai parametri dle messaggio
	 */
	public String[] getParametriMessaggio() {
		String[] wvBlocchi = atMessaggio.toString().split("\\:");
		String[] wvParametri = wvBlocchi[1].split("\\,");
		return wvParametri;
	}

	/**
	 * Imposta il mesaggio sul controllo
	 * 
	 * @param idPartita
	 */
	public void idPartita(Integer idPartita) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._IDPARTITA.toString().length() + 8);

		atMessaggio.append(EIntestazioneMessaggio._IDPARTITA.toString());
		atMessaggio.append(":" + idPartita);

	}

	/**
	 * Imposta l'id del giocatore nel messaggio
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 * @throws Operazione_Vietata_Exception
	 */
	public void idGiocatore(Integer idGiocatore) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._IDGIOCATORE.toString().length() + 8);

		atMessaggio.append(EIntestazioneMessaggio._IDGIOCATORE.toString());
		atMessaggio.append(":" + idGiocatore);
	}

	/**
	 * Prepara un messaggio al server per farlo terminare
	 */
	public void uscita() {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._USCITA.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._USCITA);
	}

	/**
	 * Prepara un messaggio di pronto
	 * 
	 * @param nome
	 */
	public void pronto(String nome) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._PRONTO.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._PRONTO);
		atMessaggio.append(":");
		atMessaggio.append(nome);
	}

	/**
	 * Prepara un messaggio di pronto
	 * 
	 * @param nome
	 *            nome giocatore
	 * @param righe
	 *            i paramentri della connessione precedente
	 */
	public void pronto(String nome, ArrayList<String> righe) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._PRONTO.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._PRONTO);
		atMessaggio.append(":");
		atMessaggio.append(nome);
		atMessaggio.append(",");
		atMessaggio.append(righe.get(0));
		atMessaggio.append(",");
		atMessaggio.append(righe.get(1));
	}

	/**
	 * prepara un messaggio di ack
	 */
	public void ack() {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._ACK.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._ACK);
	}

	/**
	 * prepara un messaggio di nack
	 */
	public void nack() {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._NACK.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._NACK);
	}

	/**
	 * Costruisce un messaggio per indicare al server la posizione iniziale
	 * scelta così formattato:<br>
	 * <i>_POSIZIONEINIZIALE:idpastore:posizionescelta</>i
	 * 
	 * @param idPastore
	 *            un integer rappresentatnte l'id da inviare
	 * @param posizioneScelta
	 *            un uinteger rappresentante la posizione della strada scelta
	 */
	public void posizioneIniziale(Integer idPastore, Integer posizioneScelta) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._POSIZIONEINIZIALE.toString().length() + 15);
		atMessaggio.append(EIntestazioneMessaggio._POSIZIONEINIZIALE);
		atMessaggio.append(":");
		atMessaggio.append(idPastore);
		atMessaggio.append(",");
		atMessaggio.append(posizioneScelta);
	}

	/**
	 * Il server deve richiamare questo metodo ogni qualvolta deve inviare gli
	 * id delle strade occupabili. la formattazione è fatta all'interno
	 * 
	 * @param idStradeLibere
	 *            un array di id di strade libere
	 */
	public void stadeLibere(ArrayList<Integer> idStradeLibere) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._IDSTRADE.toString().length() + 25);
		atMessaggio.append(EIntestazioneMessaggio._IDSTRADE);
		atMessaggio.append(":");
		for (int i = 0; i < idStradeLibere.size(); i++) {
			atMessaggio.append(idStradeLibere.get(i));
			if ((i + 1) < idStradeLibere.size()) {
				atMessaggio.append(",");
			}
		}
	}

	/**
	 * Prepara un messaggio serializzabile da poter inviare sulla rete server
	 * per la tessera terreno iniziale
	 * 
	 * @param tesseraTerreno
	 *            è il tipo di terreno che vogliamo far transitare sulla rete
	 */
	public void tesseraTerreno(String tesseraTerreno) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._TESSERATERRENO.toString().length() + 10);
		atMessaggio.append(EIntestazioneMessaggio._TESSERATERRENO);
		atMessaggio.append(":");
		atMessaggio.append(tesseraTerreno);
	}

	/**
	 * Permette di inviare un messaggio per far capire al client che deve
	 * inviare una pedina
	 */
	public void posizionaPedina() {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._POSIZIONAPEDINA.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._POSIZIONAPEDINA);
	}

	public void inizioPartita() {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._AVVIAPARTITA.toString().length());
		atMessaggio.append(EIntestazioneMessaggio._AVVIAPARTITA);
	}

	/**
	 * prepara un messaggio di turno
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 */
	public void turno(Integer idGiocatore) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._TURNOGIOCATORE.toString().length() + 4);
		atMessaggio.append(EIntestazioneMessaggio._TURNOGIOCATORE);
		atMessaggio.append(":");
		atMessaggio.append(idGiocatore);

	}

	/**
	 * Prepara un turno per l'inizio delle offerte
	 * 
	 * @param idGiocatore
	 */
	public void turnoOfferta(Integer idGiocatore) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._TURNOOFFERTA.toString().length() + 8);
		atMessaggio.append(EIntestazioneMessaggio._TURNOOFFERTA);
		atMessaggio.append(":");
		atMessaggio.append(idGiocatore);
	}

	/**
	 * Prepara un messaggio per indicare a chi tocca
	 * 
	 * @param idGiocatore
	 *            l'id del giocatore
	 */
	public void turnoAcquisto(Integer idGiocatore) {
		atMessaggio = new StringBuilder(EIntestazioneMessaggio._TURNOACQUISTO.toString().length() + 8);
		atMessaggio.append(EIntestazioneMessaggio._TURNOACQUISTO);
		atMessaggio.append(":");
		atMessaggio.append(idGiocatore);
	}
}
