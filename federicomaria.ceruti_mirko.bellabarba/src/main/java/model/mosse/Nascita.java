package model.mosse;

import model.Agnello;
import model.Regione;

/**
 * Mossa che imposta la nascita di un agnello
 * 
 * @author federico
 * 
 */
public class Nascita implements IMossa {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8160231620168329177L;
	private Integer atIdRegione, atIdAgnello;

	/**
	 * Costruttore della mossa {@link Nascita}
	 * 
	 * @param idRegione
	 *            {@link Integer} che rappresenta l'id della {@link Regione} in
	 *            cui è nato l'{@link Agnello}
	 * @param idAgnello
	 *            {@link Integer} che rappresenta l'id dell'{@link Agnello} che
	 *            è nato
	 */
	public Nascita(Integer idRegione, Integer idAgnello) {
		this.setIdRegione(idRegione);
		this.setIdAgnello(idAgnello);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return null;
	}

	/**
	 * Ritorna l'id della {@link Regione} in cui è nato l'{@link Agnello}
	 * 
	 * @return {@link Integer} che rappresetna l'id della {@link Regione}
	 */
	public Integer getIdRegione() {
		return this.atIdRegione;
	}

	/**
	 * Imposta l'id della {@link Regione} in cui è nato l'{@link Agnello}
	 * 
	 * @param atIdRegione
	 *            {@link Integer} che rappresetna l'id dell {@link Regione}
	 */
	public void setIdRegione(Integer atIdRegione) {
		this.atIdRegione = atIdRegione;
	}

	/**
	 * Ritorna l'id dell'{@link Agnello} nato
	 * 
	 * @return {@link Integer}
	 */
	public Integer getIdAgnello() {
		return this.atIdAgnello;
	}

	/**
	 * Imposta l'id dell'{@link Agnello} nato
	 * 
	 * @param atIdAgnello
	 *            {@link Integer} id {@link Agnello} nato
	 */
	public void setIdAgnello(Integer atIdAgnello) {
		this.atIdAgnello = atIdAgnello;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String wvStringa = new String();

		wvStringa += "Nella regione " + this.getIdRegione() + " è nato l'agnello " + this.getIdAgnello();
		return wvStringa;
	}
}
