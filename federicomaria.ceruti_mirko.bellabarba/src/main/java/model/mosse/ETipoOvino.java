package model.mosse;

/**
 * Enum che rapresenta il tipo di ovino in cui può evolvere l'agnello
 * 
 * @author federico
 * 
 */
public enum ETipoOvino {
	Pecora, Montone
}
