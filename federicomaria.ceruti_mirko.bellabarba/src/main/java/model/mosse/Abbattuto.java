package model.mosse;

import java.util.ArrayList;

import model.Animale;
import model.Giocatore;

/**
 * Mossa di tipo abbattuto
 * 
 * @author federico
 * 
 */
public class Abbattuto implements IMossa {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2873321555392519984L;

	private Integer at_id_animale_abattuto;
	private Integer at_id_debitore;
	private Integer atIdRegioneAppartenenza;
	private ArrayList<Integer> at_id_creditori;

	/**
	 * Costruttore
	 */
	public Abbattuto() {
		at_id_creditori = new ArrayList<Integer>();
	}

	/**
	 * Ritorna l'Id del gicoatore che deve pagare
	 * 
	 * @return {@link Integer} che rappresenta l'Id del giocatore che deve
	 *         pagare
	 */
	public Integer getIdDebitore() {
		return at_id_debitore;
	}

	/**
	 * Ritorna gli Id dei giocatori da pagare
	 * 
	 * @return {@link ArrayList} di {@link Integer} contenente gli Id dei
	 *         gicoatori da pagare
	 */
	public ArrayList<Integer> getCreditori() {
		return at_id_creditori;
	}

	/**
	 * Ritorna l'id dell'{@link Animale} abbattuto
	 * 
	 * @return {@link Integer} id dell'animale abbattuto
	 */
	public Integer getAnimaleAbbattuto() {
		return at_id_animale_abattuto;
	}

	/**
	 * Aggiunge un creditore alla lista di creditori
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del giocatore creditore
	 */
	public void aggiungiCreditore(Integer idGiocatore) {
		at_id_creditori.add(idGiocatore);
	}

	/**
	 * Imposta il {@link Giocatore} che deve pagare
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del gicoatore che deve
	 *            pagare
	 */
	public void setDebitore(Integer idGiocatore) {
		at_id_debitore = idGiocatore;
	}

	/**
	 * Imposta l'animale che è stato abbattuto
	 * 
	 * @param idAimale
	 *            {@link Integer} che reppresenta l'Id dell'{@link Animale} che
	 *            è stato abbattuto
	 */
	public void setAnimaleAbbattuo(Integer idAimale) {
		at_id_animale_abattuto = idAimale;
	}

	/**
	 * Ritorna la proposta scritta della mossa Questa mossa è sprovvista
	 * dell'implementazione di tale metodo, ritornerà quindi la stringa vuota
	 */
	public String propostaMossa() {
		return "";
	}

	/**
	 * Ritorna un valore che indica l'id della regione dell'abbatimento
	 * 
	 * @return {@link Integer} che rappresenta l'id della regione in cui è stato
	 *         abbattuto
	 */
	public Integer getIdRegioneAppartenenza() {
		return atIdRegioneAppartenenza;
	}

	/**
	 * Imposta la regione in cui è stato effettuato l'abbattimento
	 * 
	 * @param idRegioneAppartenenza
	 *            {@link Integer} che rappresenta l'id della regione
	 */
	public void setIdRegioneAppartenenza(Integer idRegioneAppartenenza) {
		this.atIdRegioneAppartenenza = idRegioneAppartenenza;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String wvString = "Il giocatore " + at_id_debitore + "ha abbattuto l'animale" + at_id_animale_abattuto;
		if (at_id_creditori.size() != 0) {
			wvString += "e deve pagare i giocatori: ";
			for (Integer wvInteger : at_id_creditori) {
				wvString += ("\\n" + wvInteger);
			}
		}
		return wvString;
	}

}
