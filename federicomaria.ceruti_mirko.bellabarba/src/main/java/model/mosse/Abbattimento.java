package model.mosse;

import java.util.ArrayList;

import model.Animale;
import model.Strada;

/**
 * Mossa di tipo Abbattimento
 * 
 * @author federico
 * 
 */
public class Abbattimento implements IMossa {
	private Integer atIdPastore, atIdRegioneColpita, atIdAnimale, atNumeroStradaOccupata;
	private Animale atAnimale;
	private ArrayList<Integer> atIdGiocatoriVicini;

	// ID utile nella serializzazione
	private static final long serialVersionUID = -529016650501791391L;

	/**
	 * Costtruisce una mossa di tipo {@link Abbattimento}
	 * 
	 * @param idPastoreCheAbbatte
	 *            {@link Integer} che rappresenta l'id del Giocatore che
	 *            effettua l'abbattimento
	 * @param idRegioneColpita
	 *            {@link Integer} che rappresenta l'id della regiona in cui
	 *            effettuare l'abbatimento
	 * @param idAnimale
	 *            {@link Integer} che rappresenta l'id dell'animale da abbattere
	 * @param idGiocatoriVicini
	 *            {@link ArrayList} di {@link Integer} dei giocatori vicini
	 */
	public Abbattimento(Integer idPastoreCheAbbatte, Integer idRegioneColpita, Integer idAnimale, ArrayList<Integer> idGiocatoriVicini) {
		this.setIdPastore(idPastoreCheAbbatte);
		this.atIdRegioneColpita = idRegioneColpita;
		this.setIdAnimale(idAnimale);
		this.atIdGiocatoriVicini = idGiocatoriVicini;
	}

	/**
	 * Costruisce una mossa di tipo {@link Abbattimento}
	 * 
	 * @param idPastoreCheAbbatte
	 *            {@link Integer} che rappresenta l'id del Giocatore che
	 *            effettua l'abbattimento
	 * @param idRegioneColpita
	 *            {@link Integer} che rappresenta l'id della regiona in cui
	 *            effettuare l'abbatimento
	 * @param idAnimale
	 *            {@link Integer} che rappresenta l'id dell'animale da abbattere
	 * @param idGiocatoriVicini
	 *            {@link ArrayList} di {@link Integer} dei giocatori vicini
	 * @param numeroStradaOccupata
	 *            {@link Integer} che rappresenta il numero della strada
	 *            occuapata
	 */
	public Abbattimento(Integer idPastoreCheAbbatte, Integer idRegioneColpita, Animale animale, ArrayList<Integer> idGiocatoriVicini,
			Integer numeroStradaOccupata) {
		this.setIdPastore(idPastoreCheAbbatte);
		this.atIdRegioneColpita = idRegioneColpita;
		this.atAnimale = animale;
		this.atIdGiocatoriVicini = idGiocatoriVicini;
		this.atNumeroStradaOccupata = numeroStradaOccupata;
		this.atIdAnimale = animale.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "Puoi abbattare un " + atAnimale.toString() + " nella regione " + atIdRegioneColpita;
	}

	/**
	 * Ritorna l'id del Giocatore che ha effettuato l'abbattimento
	 * 
	 * @return {@link Integer} che rappresenta l'id del giocatore
	 */
	public Integer getIdPastore() {
		return atIdPastore;
	}

	/**
	 * Imposta l'id del giocatore che deve effettuare l'abbattimento
	 * 
	 * @param atIdPastore
	 *            {@link Integer} che rappresenta l'id del gicoatore
	 */
	public void setIdPastore(Integer atIdPastore) {
		this.atIdPastore = atIdPastore;
	}

	/**
	 * Ritorna l'id dell'animale abbattuto
	 * 
	 * @return {@link Integer} che rappresenta l'id dell'animale
	 */
	public Integer getIdAnimale() {
		return atIdAnimale;
	}

	/**
	 * Imposta l'id dell'animale abbattuto
	 * 
	 * @param atIdAnimale
	 *            {@link Integer} che rappresenta l'id dell'animale abbattuto
	 */
	public void setIdAnimale(Integer atIdAnimale) {
		this.atIdAnimale = atIdAnimale;
	}

	/**
	 * Ritorna l'id della regione in cui è stato abbatuto l'animale
	 * 
	 * @return {@link Integer}che rappresena l'id della regione
	 */
	public Integer getIdRegione() {
		return atIdRegioneColpita;
	}

	/**
	 * Ritorna gli id dei gicoatori vicini a chi ha fatto l'abbattimento
	 * 
	 * @return {@link ArrayList} di {@link Integer} che contiene gli id dei
	 *         gicoatori vicini
	 */
	public ArrayList<Integer> getIdGiocatoriVicini() {
		return atIdGiocatoriVicini;
	}

	/**
	 * Ritorna l'animale da abbattere
	 * 
	 * @return {@link Animale} che rappresenta l'animale da abbattere
	 */
	public Animale getAnimale() {
		if (atAnimale == null) {
			return null;
		}
		return atAnimale;
	}

	/**
	 * Ritorna il numero della {@link Strada} occupata
	 * 
	 * @return ritorna un {@link Integer} che rappresenta il numero della
	 *         starada occupata
	 */
	public Integer getNumeroStradaOccupata() {
		return atNumeroStradaOccupata;
	}

}
