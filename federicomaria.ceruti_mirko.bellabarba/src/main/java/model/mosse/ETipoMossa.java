package model.mosse;

/**
 * Elenco model.mosse:
 * 
 * Acquisto: Acquisto dal mercato Abbattimento: Abbatti un animale
 * Accoppiamento: Generi un agnello Investimento: Acquisto di un tipo di terreno
 * da parte di un giocatore Movimento: Movimento degli animalie del pastore
 * Posizionamento: Scelta della strada da cui partire Transazione: Vedita:
 * Vendita nella fase di mercato, mandi i terreni che vuoi vendere
 * 
 */
public enum ETipoMossa {
	ACQUISTO, ABBATTIMENTO, ACCOPPIAMENTO, INVESTIMENTO, MOVIMENTO, POSIZIONAMENTO, TRANSAZIONE, VENDITA;
}
