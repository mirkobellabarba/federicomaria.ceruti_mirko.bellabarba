package model.mosse;

/**
 * Mossa che indica la fine delle mosse preliminari
 */
public class finePreliminari implements IMossa {

	private static final long serialVersionUID = 1775775115018030614L;
	private Integer idGiocatore;

	/**
	 * costruttore della mossa
	 * 
	 * @param idGiocatore
	 *            {@link Integer} che rappresenta l'id del gicatore che dovrà
	 *            giocare il prossimo turno
	 */
	public finePreliminari(Integer idGiocatore) {
		super();
		this.idGiocatore = idGiocatore;
	}

	/**
	 * 
	 * @return {@link Integer} ceh rappresenta l'id del giocatore
	 */
	public Integer getIdGiocatore() {
		return this.idGiocatore;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "";
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Fine fasi preliminari";
	}
}
