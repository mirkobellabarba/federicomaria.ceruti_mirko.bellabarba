package model.mosse;

import model.Agnello;
import model.Animale;
import model.ETipoID;
import model.Montone;
import model.Pecora;
import model.Pecoranera;
import model.PedinaPastore;

/**
 * Mossa movimento, usata per mandare il movimento delle {@link PedinaPastore} e
 * degli {@link Animale}
 * 
 * @author federico
 * 
 */
public class Movimento implements IMossa {

	// ID utile nella serializzazione
	private static final long serialVersionUID = -8980082114989752138L;

	private Integer atIdSoggetto, atIdVecchiaPosizione, atIdNuovaPosizione, atCostoMossa;
	private Boolean atIsAnimale;

	/**
	 * Prepara una mossa serializzabile che rappresenta il movimento di una
	 * persona o di un'animale
	 * 
	 * @param idSoggettoMovimento
	 *            l'id di chi si sta muovendo
	 * @param idVecchiaPosizione
	 *            l'id della strada/regione precedente allo spostamento (quella
	 *            attuale)
	 * @param idNuovaPosizione
	 *            l'id della strada/regione dove voglio spostare il soggetto
	 * @param costoMossa
	 *            un intero che rappresenta il costo della mossa (0 o 1)
	 */
	public Movimento(Integer idSoggettoMovimento, Integer idVecchiaPosizione, Integer idNuovaPosizione, Integer costoMossa) {
		this.setAtIdSoggetto(idSoggettoMovimento);
		this.setAtIdVecchiaPosizione(idVecchiaPosizione);
		this.setAtIdNuovaPosizione(idNuovaPosizione);
		this.setAtIsAnimale(this.checkIsAnimale(idSoggettoMovimento));
		this.setAtCostoMossa(costoMossa);
	}

	/**
	 * Serve per sapere se lo spostamento avviene via regioni o via strade
	 * 
	 * @param idSoggettoMovimento
	 *            l'id di chi si sposta
	 * @return true se è un animale (spostamento tramite regioni), false se è un
	 *         pastore (spostamento tramite strade) null in caso di errore
	 */
	private Boolean checkIsAnimale(Integer idSoggettoMovimento) {
		return (idSoggettoMovimento % 10) == ETipoID.ANIMALE.ordinal() ? true
				: (idSoggettoMovimento % 10) == ETipoID.PEDINA_GIOCATORE.ordinal() ? false : null;
	}

	/**
	 * Ritorna l'id di chi è stato mosso
	 * 
	 * @return {@link Integer} che rappresenta l'id di ciò che è stato mosso.<br>
	 *         Possono essere mosse, con questa mossa, instanze di:<br>
	 *         {@link Agnello}, {@link Montone}, {@link Pecora},
	 *         {@link Pecoranera}, {@link PedinaPastore}
	 */
	public Integer getAtIdSoggetto() {
		return this.atIdSoggetto;
	}

	/**
	 * Imposta l'id del soggetto del movimento
	 * 
	 * @param {@link Integer} che rappresenta l'id del soggetto
	 */
	public void setAtIdSoggetto(Integer atIdSoggetto) {
		this.atIdSoggetto = atIdSoggetto;
	}

	/**
	 * Ritorna l'id della vecchia poszione
	 * 
	 * @return {@link Integer} id della vecchia posizione
	 */
	public Integer getAtIdVecchiaPosizione() {
		return this.atIdVecchiaPosizione;
	}

	/**
	 * Imposta la vecchia posizione occupata del soggetto del movimento
	 * 
	 * @param idVecchiaPosizione
	 *            {@link Integer} id della posizione prima del movimento
	 */
	public void setAtIdVecchiaPosizione(Integer idVecchiaPosizione) {
		this.atIdVecchiaPosizione = idVecchiaPosizione;
	}

	/**
	 * Ritorna la posizione di arrivo del movimento
	 * 
	 * @return {@link Integer} che rappresenta l'id dell'arrivo del movimento
	 */
	public Integer getAtIdNuovaPosizione() {
		return this.atIdNuovaPosizione;
	}

	/**
	 * Ritorna l'id della nuova posizione
	 * 
	 * @param atIdNuovaPosizione
	 *            {@link Integer} he rappresenta l'id della posizione di arrivo
	 */
	public void setAtIdNuovaPosizione(Integer atIdNuovaPosizione) {
		this.atIdNuovaPosizione = atIdNuovaPosizione;
	}

	/**
	 * Ritorna un valore che indica se il soggetto del movimento è un
	 * {@link Animale} oppure no
	 * 
	 * @return {@link Boolean} che indica se il soggetto è un {@link Animale}(
	 *         <code>true</code>) oppure no(<code>false</code>)
	 */
	public Boolean isAnimale() {
		return this.atIsAnimale;
	}

	/**
	 * Imposta un valore che indica se il soggeto è un {@link Animale}
	 * 
	 * @param atIsAnimale
	 *            {@link Boolean}
	 */
	public void setAtIsAnimale(Boolean atIsAnimale) {
		this.atIsAnimale = atIsAnimale;
	}

	/**
	 * Ritorna il costo della mossa
	 * 
	 * @return {@link Integer} che rappresenta il costo del movimento
	 */
	public Integer getAtCostoMossa() {
		return this.atCostoMossa;
	}

	/**
	 * Imposta il costo della mossa
	 * 
	 * @param atCostoMossa
	 *            {@link Integer} che indica il costo della mossa
	 */
	public void setAtCostoMossa(Integer atCostoMossa) {
		this.atCostoMossa = atCostoMossa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		if (this.atIsAnimale) {
			return "Puoi spostare l'animale numero " + this.atIdSoggetto + " dalla regione " + this.atIdVecchiaPosizione + " alla regione "
					+ this.atIdNuovaPosizione;
		} else {
			return "Puoi spostare il pastore numero " + this.atIdSoggetto + " dalla strada " + this.atIdVecchiaPosizione + " alla strada "
					+ this.atIdNuovaPosizione + " con costo: " + this.atCostoMossa;
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String wvString = new String("");
		if (this.atIsAnimale) {
			wvString += "è stato mosso l'animale: " + this.atIdSoggetto + " nella regione: " + this.atIdNuovaPosizione;
		} else {
			wvString += "è stato mossa la pedina pastore: " + this.atIdSoggetto + "nella strada " + this.atIdNuovaPosizione;
		}
		return wvString;
	}
}
