package model.mosse;

/**
 * Mosa di Accopiamento
 * 
 * @author federico
 * 
 */
public class Accoppiamento implements IMossa {

	// ID utile nella serializzazione
	private static final long serialVersionUID = 6312260916540241698L;

	private Integer atRegione;

	/**
	 * Crea una mossa serializzabile dove viene indicata la regione dove si
	 * vuole efferruare un accoppiamento
	 * 
	 * @param idRegioneAccoppiamento
	 *            l'id della regione dove fare l'accoppiamento
	 */
	public Accoppiamento(Integer idRegioneAccoppiamento) {
		this.atRegione = idRegioneAccoppiamento;
	}

	/**
	 * Ritorna la regione dell'accopiamento
	 * 
	 * @return {@link Integer} che rappresenta la regione dove avviene
	 *         l'accoppiamento
	 */
	public Integer getIdRegione() {
		return atRegione;
	}

	/**
	 * @param setta
	 *            la regione dove avviene l'accoppiamento
	 */
	public void setAtRegione(Integer atRegione) {
		this.atRegione = atRegione;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "Puoi effettuare un accoppiamento nella regione " + atRegione;
	}
}
