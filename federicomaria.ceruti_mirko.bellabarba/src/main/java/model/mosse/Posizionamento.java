package model.mosse;

import model.Giocatore;
import model.PedinaPastore;
import model.Strada;

/**
 * Mossa per il posizionamento delle {@link PedinaPastore}
 * 
 * @author federico
 * 
 */
public class Posizionamento implements IMossa {

	// ID utile nella serializzazione
	private static final long serialVersionUID = 1163686937144183388L;

	private Integer atIdStradaScelta;
	private Integer atIdGiocatore;
	private Integer atIdPedina;

	/**
	 * prepara una mossa serializzabile con la quale il client indica in che
	 * posizione vuole iniziare la partita
	 * 
	 * @param idStradaScelta
	 *            l'id della strada scelta
	 */
	public Posizionamento(Integer idStradaScelta) {
		this.setAtIdStradaScelta(idStradaScelta);
	}

	/**
	 * Costruttore
	 * 
	 * @param idGiocatore
	 *            {@link Integer} id del {@link Giocatore} che deve effetuare il
	 *            posizionamento
	 * @param idPedina
	 *            {@link Integer} id della {@link PedinaPastore} da impostare
	 * @param idStrada
	 *            {@link Integer} id della {@link Strada} su cui impostare la
	 *            pedina
	 */
	public Posizionamento(Integer idGiocatore, Integer idPedina, Integer idStrada) {
		this.atIdGiocatore = idGiocatore;
		this.atIdPedina = idPedina;
		this.atIdStradaScelta = idStrada;
	}

	/**
	 * Ritorna l'id della {@link Strada} scelta per il posizionamento
	 * 
	 * @return {@link Integer} che rappresenta l'id dela strada scelta
	 */
	public Integer getIdStradaScelta() {
		return this.atIdStradaScelta;
	}

	/**
	 * Imposta la {@link Strada} iniziale della {@link PedinaPastore}
	 * 
	 * @param atIdStradaScelta
	 *            {@link Integer} id della strada
	 */
	public void setAtIdStradaScelta(Integer atIdStradaScelta) {
		this.atIdStradaScelta = atIdStradaScelta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return null;
	}

	/**
	 * Ritorna l'id del giocatore che deve impostare la posizione della
	 * {@link PedinaPastore}
	 * 
	 * @return {@link Integer} che rappresenta l'id del {@link Giocatore}
	 */
	public Integer getIdGiocatore() {
		return this.atIdGiocatore;
	}

	/**
	 * Ritorna l'id della {@link PedinaPastore} da impostare
	 * 
	 * @return {@link Integer} ch rappresenta l'id della pedina
	 */
	public Integer getIdPedina() {
		return this.atIdPedina;
	}

	/**
	 * Ritorna l'id della {@link Strada} in cui posizionare la
	 * {@link PedinaPastore}
	 * 
	 * @return {@link Integer} l'id della {@link Strada} scelta
	 */
	public Integer getIdStrada() {
		return this.atIdStradaScelta;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String wvStringa = new String();

		wvStringa += "Il gioatore " + this.getIdGiocatore() + " ha posizionato la pedina " + this.getIdPedina() + " nella strada "
				+ this.getIdStradaScelta();
		return wvStringa;
	}
}
