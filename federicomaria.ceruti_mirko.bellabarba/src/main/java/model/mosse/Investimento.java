package model.mosse;

import model.ETipoTerreno;

/**
 * Mossa investimento, server per comprare i terreni
 * 
 * @author federico
 * 
 */
public class Investimento implements IMossa {
	private static final long serialVersionUID = 5692012015412661078L;

	private final ETipoTerreno atTipoTerreno;
	private final Integer atCosto;
	private final Integer atIdGiocatore;

	/**
	 * Una mossa che viene effettuata quando il giocatore investe in un terreno
	 * 
	 * @param idGiocatore
	 *            id del giocatore
	 * @param tipoTerreno
	 *            id del tipo di terreno
	 * @param costo
	 *            il costo dell'investimento
	 */
	public Investimento(Integer idGiocatore, ETipoTerreno tipoTerreno, Integer costo) {
		this.atIdGiocatore = idGiocatore;
		this.atTipoTerreno = tipoTerreno;
		this.atCosto = costo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.mosse.IMossa#propostaMossa()
	 */
	public String propostaMossa() {
		return "Puoi compare un terreno di tipo " + this.atTipoTerreno.toString() + " a costo " + this.atCosto;
	}

	/**
	 * Ritorna il tipo di terreno su cui si è effettuato l'investimento
	 * 
	 * @return {@link ETipoTerreno} che indica il tipo di terreno comprato
	 */
	public ETipoTerreno getTipoTerreno() {
		return this.atTipoTerreno;
	}

	/**
	 * Ritorna il costa dell'acquisto
	 * 
	 * @return {@link Integer} che rappresenta il costo dell'acquisto
	 */
	public Integer getCosto() {
		return this.atCosto;
	}

	/**
	 * Ritorna l'id del giocatore che compra il terreno
	 * 
	 * @return {@link Integer} che rappresenta l'id del giocatore che ha
	 *         effetuato l'acquisto
	 */
	public Integer getIdGiocatore() {
		return this.atIdGiocatore;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Il giocatore: " + this.atIdGiocatore + " ha comprato un/a" + this.atTipoTerreno + "per " + this.atCosto + " danari";
	}
}
