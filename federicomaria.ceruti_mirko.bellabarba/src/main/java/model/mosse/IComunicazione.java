package model.mosse;

import java.io.Serializable;

/**
 * Interfaccia ausiliaria da implementare negli oggetti da scambiare tra client
 * e server
 * 
 */
public interface IComunicazione extends Serializable {

}
