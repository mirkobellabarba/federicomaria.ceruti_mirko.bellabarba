package model.mosse;

/**
 * Enumerativo per identificare il tipo di messaggio di controllo da scambiare
 * tra client e server
 */
public enum EIntestazioneMessaggio {
	_IDPARTITA, _IDGIOCATORE, _USCITA, _PRONTO, _POSIZIONEINIZIALE, _ACK, _NACK, _AVVIAPARTITA, _IDSTRADE, _TESSERATERRENO, _TURNOGIOCATORE, _FINETURNO, _POSIZIONAPEDINA, _TURNOOFFERTA, _TURNOACQUISTO
}