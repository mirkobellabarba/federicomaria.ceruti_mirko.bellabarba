package model;

import java.util.ArrayList;

/**
 * la classifica finale
 * 
 * @author Mirko
 * 
 */
public class Classifica {

	private Mappa atMappa;
	private ArrayList<Giocatore> giocatori;
	private ArrayList<Integer> punteggi;

	/**
	 * Costruttore
	 */
	public Classifica(Mappa mappa) {
		atMappa = mappa;
		giocatori = new ArrayList<Giocatore>();
		punteggi = new ArrayList<Integer>();
	}

	/**
	 * Calcola i punteggi di tutti i giocatori presenti in mappa
	 */
	public void calcolaPunteggi() {
		for (Giocatore giocatore : atMappa.getGiocatori()) {
			int punteggio = 0;

			// Calcolo pecore contenute nei terreni acquistati
			for (ETipoTerreno terrenoAcquistato : giocatore.getTerreniComprati()) {
				if (terrenoAcquistato == null) {
					continue;
				}
				for (Regione regione : atMappa.getRegioni()) {
					if (regione.getTipoTerreno().equals(terrenoAcquistato)) {
						for (Animale animaleInRegione : regione.getAnimali()) {
							if (animaleInRegione instanceof Pecoranera) {
								punteggio += 2;
							} else if (!(animaleInRegione instanceof Lupo)) {
								punteggio += 1;
							}
						}
					}
				}
			}

			punteggio += giocatore.getDanaroResiduo();

			aggiungiRisultato(giocatore, punteggio);
		}
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = "";
		result += "IL VINCITORE E': " + giocatori.get(getIndicePunteggioMassimo()) + "\n______________________________________\n";
		for (Giocatore giocatore : giocatori) {
			result += "Nome giocatore: " + giocatore.getNome() + " punteggio: " + punteggi.get(giocatori.indexOf(giocatore)) + "\n";
		}

		return result;
	}

	/**
	 * Aggiunge il punteggio asssociato al giocatore nella tabella
	 * 
	 * @param giocatore
	 * @param punteggio
	 */
	private void aggiungiRisultato(Giocatore giocatore, int punteggio) {
		giocatori.add(giocatore);
		punteggi.add(punteggio);
	}

	/**
	 * Calcola l'indice del punteggio più alto
	 * 
	 * @return
	 */
	private int getIndicePunteggioMassimo() {
		int wvIndicePunteggioMassimo = -1;
		int wvPunteggioMassimo = -1;
		for (int i = 0; i < punteggi.size(); i++) {
			if (punteggi.get(i) > wvPunteggioMassimo) {
				wvPunteggioMassimo = punteggi.get(i);
				wvIndicePunteggioMassimo = i;
			}
		}
		return wvIndicePunteggioMassimo;
	}

}
