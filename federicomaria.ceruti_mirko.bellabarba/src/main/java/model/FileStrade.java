package model;

/**
 * classe di appoggio che prende i dati delle strade da file
 * 
 * @author Mirko
 * 
 */
public class FileStrade {
	private Integer id;
	private Integer[] regioniLimitrofe;
	private Integer[] stradeLimitrofe;
	private Integer numeroStrada;

	/**
	 * Serve per instanziare un oggetto temporaneo contenente i dati "grezzi" da
	 * cui prelevare di volta in volta le informazioni per popolare le strade
	 * della mappa
	 * 
	 * @param id
	 *            della strada (intero)
	 * @param regioniLimitrofe
	 *            id delle regioni limitrofe (array di interi)
	 * @param stradeLimitrofe
	 *            id delle strade limitrofe (array di interi)
	 * @param numeroStrada
	 *            il numerino della strada (intero)
	 */
	public FileStrade(Integer id, Integer[] regioniLimitrofe, Integer[] stradeLimitrofe, Integer numeroStrada) {
		this.id = id;
		this.setRegioniLimitrofe(regioniLimitrofe);
		this.setStradeLimitrofe(stradeLimitrofe);
		this.numeroStrada = numeroStrada;
	}

	/**
	 * @return l'id univoco della strada (intero)
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return un array di interi contenente gli id delle refioni limitrofe alla
	 *         strada
	 */
	public Integer[] getRegioniLimitrofe() {
		return regioniLimitrofe;
	}

	/**
	 * @param regioniLimitrofe
	 *            un array di id delle regioni limitrofe alla strada
	 */
	public void setRegioniLimitrofe(Integer[] regioniLimitrofe) {
		this.regioniLimitrofe = regioniLimitrofe;
	}

	/**
	 * @return una array di interi con gli id delle strade limitrofe
	 */
	public Integer[] getStradeLimitrofe() {
		return stradeLimitrofe;
	}

	/**
	 * @param stradeLimitrofe
	 *            l'array di strade limitrofe da settare
	 */
	public void setStradeLimitrofe(Integer[] stradeLimitrofe) {
		this.stradeLimitrofe = stradeLimitrofe;
	}

	/**
	 * @return il numerino della strada da visualizzare sulla mappa (1-6)
	 */
	public Integer getNumeroStrada() {
		return numeroStrada;
	}
}
