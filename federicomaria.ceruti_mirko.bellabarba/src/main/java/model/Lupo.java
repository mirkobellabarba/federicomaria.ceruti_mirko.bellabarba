package model;

/**
 * 
 * Rappresenta un'estensione della Classe astratta Animale
 * 
 * @author Federico
 * 
 */
public class Lupo extends Animale {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3834179049939173746L;

	/**
	 * Fornisce il costruttore per l'oggetto Lupo
	 * 
	 * @param id
	 *            Id per l'identifacazione dell'oggetto
	 * @param regioneOccupata
	 *            Regione inizialmente occupata dal Lupo
	 */
	public Lupo(Integer id, Regione regioneOccupata) {
		super(id, regioneOccupata);
	}

}
