package model.swing;

import javax.swing.JFrame;

public class AnimazioneSpostamento extends Thread {
	private Object oggettoDaSpostare;
	private int xIniziale, yIniziale, xFinale, yFinale;
	private JFrame wvFramePrincipale;
	private boolean rimuovereAllafine;
	private long millisecondi = 20;

	public AnimazioneSpostamento(JFrame wvFramePrincipale, Object oggettoDaSpostare, int xIniziale, int yIniziale, int xFinale, int yFinale,
			boolean rimuovereAllaFine) {
		this.oggettoDaSpostare = oggettoDaSpostare;
		this.xIniziale = xIniziale;
		this.yIniziale = yIniziale;
		this.xFinale = xFinale;
		this.yFinale = yFinale;
		this.wvFramePrincipale = wvFramePrincipale;
		this.rimuovereAllafine = rimuovereAllaFine;
	}

	@Override
	public void run() {
		float tempoPassato = 0;
		while (true) {
			tempoPassato += 0.035;
			float xAttuale = xIniziale + tempoPassato * (xFinale - xIniziale);
			float yAttuale = yIniziale + tempoPassato * (yFinale - yIniziale);

			if (oggettoDaSpostare instanceof IconaSpostabile) {
				if ((xAttuale - 10 <= xFinale && xFinale <= xAttuale + 10) && (yAttuale - 10 <= yFinale && yFinale <= yAttuale + 10)) {
					((IconaSpostabile) oggettoDaSpostare).setLocation(xFinale, yFinale);
				} else {
					((IconaSpostabile) oggettoDaSpostare).setLocation((int) xAttuale, (int) yAttuale);
				}

				if (((IconaSpostabile) oggettoDaSpostare).getX() == xFinale && ((IconaSpostabile) oggettoDaSpostare).getY() == yFinale) {
					wvFramePrincipale.repaint();
					wvFramePrincipale.revalidate();
					break;
				}
			}

			wvFramePrincipale.repaint();
			wvFramePrincipale.revalidate();

			long tempoIniziale = System.currentTimeMillis();
			long tempoTrascorso = 0;
			long timer = (long) millisecondi;
			while (tempoTrascorso < timer) {
				tempoTrascorso = System.currentTimeMillis() - tempoIniziale;
			}
		}

		if (rimuovereAllafine) {
			this.wvFramePrincipale.getContentPane().remove((IconaSpostabile) oggettoDaSpostare);
		}
	}
}
