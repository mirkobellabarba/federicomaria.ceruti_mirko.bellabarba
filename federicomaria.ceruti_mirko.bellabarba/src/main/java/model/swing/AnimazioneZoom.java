package model.swing;

import javax.swing.JFrame;

/**
 * Un thread che permette di generare un'animazione di spostamento
 * 
 * @author Mirko
 * 
 */
public class AnimazioneZoom extends Thread {

	private JFrame wvFramePrincipale;
	private int x, y;
	private Object oggettoDaSpostare;
	private long millisecondi = 20;

	/**
	 * Costruttore
	 * 
	 * @param wvFramePrincipale
	 *            il frame principale
	 * @param x
	 *            posiione iniziale x
	 * @param y
	 *            posizione iniziale y
	 * @param oggettoDaSpostare
	 */
	public AnimazioneZoom(JFrame wvFramePrincipale, int x, int y, Object oggettoDaSpostare) {
		this.wvFramePrincipale = wvFramePrincipale;
		this.x = x;
		this.y = y;
		this.oggettoDaSpostare = oggettoDaSpostare;
	}

	int coefficiente = 1;

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		// Se oggetto è spostabile
		if (oggettoDaSpostare instanceof IconaSpostabile) {
			IconaSpostabile iconaSpostabile = (IconaSpostabile) oggettoDaSpostare;
			// lo aggiungo
			wvFramePrincipale.getContentPane().add(iconaSpostabile);
			int width = 0, height = 0;
			while (true) {
				// Lo sposto
				iconaSpostabile.setSize(width, height);

				long tempoIniziale = System.currentTimeMillis();
				long tempoTrascorso = 0;
				long timer = (long) millisecondi;
				while (tempoTrascorso < timer) {
					tempoTrascorso = System.currentTimeMillis() - tempoIniziale;
				}
				width += coefficiente;
				height += coefficiente;
				wvFramePrincipale.repaint();
				wvFramePrincipale.revalidate();

				if (width > 65) {
					wvFramePrincipale.getContentPane().remove(iconaSpostabile);
					wvFramePrincipale.repaint();
					wvFramePrincipale.revalidate();
					break;
					// se la dimensione è cresciuta sopra i 65 pixel esco dal
					// while
				}
			}
		}
	}
}
