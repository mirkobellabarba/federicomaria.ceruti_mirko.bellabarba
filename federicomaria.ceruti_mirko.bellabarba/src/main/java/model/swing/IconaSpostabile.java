package model.swing;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import view.CLCHelper;
import model.EColore;

public class IconaSpostabile extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5623405259099703316L;
	private String urlImmagine;
	private Integer idPedina;
	private EColore colorePedinaColore;

	/**
	 * Costruttore
	 * 
	 * @param text
	 */
	public IconaSpostabile(String text) {
		super(text);
	}

	/**
	 * Costruttore
	 * 
	 * @param image
	 */
	public IconaSpostabile(Icon image) {
		super(image);
	}

	/**
	 * Costruttore
	 * 
	 * @param image
	 * @param idPedina
	 * @param colorePedina
	 */
	public IconaSpostabile(Icon image, Integer idPedina, EColore colorePedina) {
		super(image);
		this.idPedina = idPedina;
		this.colorePedinaColore = colorePedina;
		this.setSize(30, 30);
	}

	/**
	 * Costruttore
	 * 
	 * @param image
	 * @param idPedina
	 */
	public IconaSpostabile(Icon image, Integer idPedina) {
		super(image);
		this.idPedina = idPedina;
		this.setSize(30, 30);
	}

	/**
	 * Costruttore
	 * 
	 * @param text
	 * @param icon
	 * @param horizontalAlignment
	 */
	public IconaSpostabile(String text, Icon icon, int horizontalAlignment) {
		super(text, icon, horizontalAlignment);
		this.setSize(95, 30);
	}

	/**
	 * Costruttore
	 * 
	 * @param text
	 * @param icon
	 * @param horizontalAlignment
	 * @param width
	 */
	public IconaSpostabile(String text, Icon icon, int horizontalAlignment, int width) {
		super(text, icon, horizontalAlignment);
		this.setSize(width, 30);
	}

	/**
	 * ritorna l'id del pastore associato
	 * 
	 * @return
	 */
	public Integer getIdPastore() {
		return idPedina;
	}

}
