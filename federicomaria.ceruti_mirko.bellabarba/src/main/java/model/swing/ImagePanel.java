package model.swing;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JComponent;

/**
 * Definisce un component che contiene un'immagine generica
 * 
 * @author Mirko
 * 
 */
public class ImagePanel extends JComponent {
	private Image atImage;

	/**
	 * costruttore
	 * 
	 * @param image
	 */
	public ImagePanel(Image image) {
		this.atImage = image;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g) {
		g.drawImage(this.atImage, 0, 0, null);
	}

}
