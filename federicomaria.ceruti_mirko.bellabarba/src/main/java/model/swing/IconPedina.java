package model.swing;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.*;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.IconView;

import model.Animale;

/**
 * Classe che permette di creare le pedine dei pastori
 * 
 * @author Mirko
 * 
 */
public class IconPedina extends JPanel {

	private static final long serialVersionUID = 7923303773975750046L;
	private BufferedImage myPicture;
	private Animale atAnimale;
	private Integer atIdAssociato;
	private JLabel label;

	/**
	 * Costruttore
	 * 
	 * @param iconURL
	 *            il path dell'icona
	 */
	public IconPedina(String iconURL, int width, int height, int x, int y) {
		try {
			myPicture = ImageIO.read(new File(iconURL));
		} catch (IOException ex) {
			Logger.getGlobal().log (Level.WARNING, this.getClass().toString(), ex);
		}

		// la label che mostra la pedina
		label = new JLabel(new ImageIcon(myPicture));
		label.setSize(width, height);
		label.setLocation(x, y);
	}

	/**
	 * Ritorna l'icona con le proprie corrdionate e dimenisioni già impostate,
	 * resta da aggiungerla al panel
	 * 
	 * @return
	 */
	public JLabel getIcona() {
		return label;
	}

	/**
	 * Associa un animale all'immagine
	 * 
	 * @param animale
	 * @return
	 */
	public void setAnimale(Animale animale) {
		atAnimale = animale;
	}

	/**
	 * ritorna l'animale
	 * 
	 * @return
	 */
	public Animale getAnimale() {
		return atAnimale;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(myPicture, 0, 0, null);
	}

	/**
	 * Setto l'id associato alla pedina
	 * 
	 * @param id
	 */
	public void setIdAssociato(Integer id) {
		atIdAssociato = id;
	}

	/**
	 * Ritorna l'id associato alla pedina
	 * 
	 * @return
	 */
	public Integer getIdAssociato() {
		return atIdAssociato;
	}

}
