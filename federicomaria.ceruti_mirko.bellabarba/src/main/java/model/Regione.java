package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import view.CLCHelper;

/**
 * la regione
 * 
 * @author Mirko
 * 
 */
public class Regione implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9054153696693346504L;
	private final Integer atID;
	private final ETipoTerreno atTipoTerreno;
	private ArrayList<Animale> atListaAnimali;
	private ArrayList<Strada> atStradeLimitrofe;

	// Eccezioni
	/**
	 * Lanciata se non risultano animali nella Regione
	 */
	public class ListaAnimaliNull extends Exception {
		private static final long serialVersionUID = 1L;

		/**
		 * Costruttore
		 */
		public ListaAnimaliNull() {
			super();
		}

		/**
		 * Costruttore
		 * 
		 * @param s
		 *            {@link String} messaggio dell'eccezione
		 */
		public ListaAnimaliNull(String s) {
			super(s);
		}
	}

	/**
	 * Lanciata se si tenta di modifcare le strade limitrofe alla regione dopo
	 * averle già impostate
	 */
	public class StradeInseriteException extends Exception {
		private static final long serialVersionUID = 1L;

		/**
		 * Costruttore
		 * 
		 * @param Chiamante
		 *            {@link Strada} con inserito il chimante dell'eccezione
		 */
		public StradeInseriteException(String Chiamante) {

			super(Chiamante + ": Strade già inserite");
		}

	}

	/**
	 * Lanciata se l'oggetto Animale risulta non trovato
	 */
	public class AnimaleNonTrovato extends Exception {
		private static final long serialVersionUID = 1L;

		/**
		 * Costruttore
		 */
		public AnimaleNonTrovato() {
			super();
		}

		/**
		 * Costruttore
		 * 
		 * @param s
		 *            {@link String} messaggio dell'eccezione
		 */
		public AnimaleNonTrovato(String s) {
			super(s);
		}
	}

	/**
	 * Fornisce il costruttore dell'oggetto {@link Regione}
	 * 
	 * @param ID
	 *            {@link Integer}, rappresenta l'ID della {@link Regione}
	 * @param Tipo
	 *            {@link ETipoTerreno}, rappresenta il tipo di terrreno della
	 *            {@link Regione}
	 */
	public Regione(Integer ID, ETipoTerreno Tipo) {
		this.atTipoTerreno = Tipo;
		this.atID = ID;
		this.atStradeLimitrofe = null;
	}

	/**
	 * Fornisce il costruttore dell'Oggetto regione
	 * 
	 * @param ID
	 *            Integer, rappresenta l'ID della Regione
	 * @param Tipo
	 *            E_Tipo_Terreno, rappresenta il tipo di terrreno della Regione
	 * @param Lista_Animali
	 *            ArrayList<Animali> rappresenta la lista di animali
	 */
	public Regione(Integer ID, ETipoTerreno Tipo,
			ArrayList<Animale> Lista_Animali) {
		this.atID = ID;
		this.atTipoTerreno = Tipo;
		this.atListaAnimali = Lista_Animali;
		this.atStradeLimitrofe = null;
	}

	/**
	 * Ritorna l'Id della Regione
	 * 
	 * @return ritorna un intero rappresentante l'Id della Regione
	 */
	public Integer getId() {
		return this.atID;
	}

	/**
	 * Ritorna il tipo di terreno della Regione
	 * 
	 * @return ritorna un enumarativo E_Tipo_Terreno che rappresenta il tipo di
	 *         terreno della Regione
	 */
	public ETipoTerreno getTipoTerreno() {
		return this.atTipoTerreno;
	}

	/**
	 * Ritorna la {@link Strada} corrispondente al numero richiesto
	 * 
	 * @param numero
	 * @return Ritorna un oggetto di tipo {@link Strada} che rappresenta la
	 *         strada associata al Numero indicato, se non ve ne sono, ritorna
	 *         <code>null</code>
	 */
	public Strada getStradaNumero(Integer numero) {
		for (Strada wvItem : this.atStradeLimitrofe) {
			if (wvItem.getNumeroStrada().compareTo(numero) == 0) {
				return wvItem;
			}
		}
		return null;
	}

	public ArrayList<Strada> getStradeLimitrofe() {
		return this.atStradeLimitrofe;
	}

	/**
	 * Ritorna un {@link ArrayList} di {@link Strada} che indica rappresenta le
	 * strade limitrofe alla {@link Regione} e che non sono occupate
	 * 
	 * @return Ritorna un {@link ArrayList} di {@link Strada} che rappresenta le
	 *         strade libere
	 */
	public ArrayList<Strada> getStradeLimitrofeLibere() {
		ArrayList<Strada> wvStrdeLibere = new ArrayList<Strada>();
		for (Strada wvItem : this.atStradeLimitrofe) {
			if (!wvItem.Occupata()) {
				// se la strada in esame non è occupata entro qui
				wvStrdeLibere.add(wvItem);
			}
		}
		return wvStrdeLibere;
	}

	/**
	 * Ritorna un {@link ArrayList} di {@link Strada} che indica rappresenta le
	 * strade limitrofe alla {@link Regione} e che sono occupate
	 * 
	 * @return Ritorna un {@link ArrayList} di {@link Strada} che rappresenta le
	 *         strade occupate
	 */
	public ArrayList<Strada> getStradeLimitrofeOccupate() {
		ArrayList<Strada> wvStradeLibere = new ArrayList<Strada>();
		for (Strada wvItem : this.atStradeLimitrofe) {
			if (wvItem.Occupata()) {
				// se la strada in esame è occupata entro qui
				wvStradeLibere.add(wvItem);
			}
		}
		return wvStradeLibere;
	}

	/**
	 * Ritorna un {@link ArrayList} di {@link Strada} che indica rappresenta le
	 * strade limitrofe alla {@link Regione} e che sono occupate da una
	 * {@link PedinaRecinto} o {@link PedinaRecintoFinale}
	 * 
	 * @return Ritorna un {@link ArrayList} di {@link Strada} che rappresenta le
	 *         strade occupate da {@link PedinaRecinto} o
	 *         {@link PedinaRecintoFinale}
	 */
	public ArrayList<Strada> getStradeLimitrofeOccupateRecinto() {
		ArrayList<Strada> wvStradeLibere = new ArrayList<Strada>();
		for (Strada wvItem : this.atStradeLimitrofe) {
			if (wvItem.occupataDaRecinto()) {
				// se la strada in esame è occupata da una Pedina_Recinto o
				// Pedina_Recinto_Finale entro qui
				wvStradeLibere.add(wvItem);
			}
		}
		return wvStradeLibere;
	}

	/**
	 * Ritorna un {@link ArrayList} di {@link Strada} che indica rappresenta le
	 * strade limitrofe alla {@link Regione} e che sono occupate da una
	 * {@link PedinaPastore}
	 * 
	 * @return Ritorna un {@link ArrayList} di {@link Strada} che rappresenta le
	 *         strade occupate dalla {@link PedinaPastore} di un
	 *         {@link Giocatore}
	 */
	public ArrayList<Strada> getStradeLimitrofeOccupatePastore() {
		ArrayList<Strada> wvStradeLibere = new ArrayList<Strada>();
		for (Strada wvItem : this.atStradeLimitrofe) {
			if (wvItem.occupadaDaPastore()) {
				// se la strada in esame è occupata da una Pedina_Pastore entro
				// qui
				wvStradeLibere.add(wvItem);
			}
		}
		return wvStradeLibere;
	}

	public ArrayList<Animale> getAnimali() {
		return this.atListaAnimali;
	}

	/**
	 * Ritorna se la {@link Regione} contiene oggetti di tipo {@link Montone}
	 * e/o {@link Pecora} (ma non {@link Pecoranera}) e/o {@link Agnello}
	 * 
	 * @return <code>true</code> se contiene ovini normali, <code>false</code>
	 *         se non ne contiene
	 */
	public Boolean contieneOviniMangiabili() {
		Boolean wvMontoneAgnelloPecora;
		Boolean wvPecoraNera;
		for (Animale wvAnimale : this.atListaAnimali) {
			// se è un lupo, diventa false
			wvMontoneAgnelloPecora = (wvAnimale instanceof Montone)
					|| (wvAnimale instanceof Agnello)
					|| (wvAnimale instanceof Pecora);
			wvPecoraNera = wvAnimale instanceof Pecoranera;

			if (wvMontoneAgnelloPecora && !wvPecoraNera) {
				return true;
			}

		}
		return false;
	}

	/**
	 * ottiene gli animali in regione, tranne il lupo
	 * 
	 * @author Mirko
	 * @return quanti animali
	 */
	public Integer getNumeroOvini() {
		Integer wvCount = 0;
		for (Animale item : this.atListaAnimali) {
			if (item instanceof Lupo) {
				continue;
			} else {
				wvCount++;
			}
		}
		return wvCount;
	}

	/**
	 * Ritorna il nuemro di ovini che il lupo può mangiare nella regione
	 * 
	 * @return
	 */
	public Integer getNumeroOviniMangiabili() {
		Integer wvCount = 0;
		for (Animale item : this.atListaAnimali) {
			if ((item instanceof Lupo) || (item instanceof Pecoranera)) {
				continue;
			} else {
				wvCount++;
			}
		}
		return wvCount;
	}

	/**
	 * Imposta le starde limitrofe alla regione
	 * 
	 * @param stradeLimitrofe
	 * @throws StradeInseriteException
	 */
	public void setStradeLimitrofe(ArrayList<Strada> stradeLimitrofe)
			throws StradeInseriteException {
		if (this.atStradeLimitrofe != null) {
			throw new StradeInseriteException("Regione.setStradeLimitrofe");
		}

		this.atStradeLimitrofe = stradeLimitrofe;
	}

	/**
	 * aggiunge un'animale all'arraylist degli animali
	 * 
	 * @param animale
	 *            The Animal you want to add
	 */
	public void aggiungiAnimale(Animale animale) {
		if (this.atListaAnimali == null) {
			this.atListaAnimali = new ArrayList<Animale>();
		}
		this.atListaAnimali.add(animale);
	}

	/**
	 * permette di rimuovere un animale dalla lista
	 * 
	 * @author Mirko
	 * @param animale
	 *            l'animale da rimuovere
	 * @throws ListaAnimaliNull
	 *             se non esiste la lista degli animali
	 * 
	 */
	public void rimuoviAnimale(Animale animale) {
		this.atListaAnimali.remove(animale);
	}

	/**
	 * rimuove un animale passato tramite id
	 * 
	 * @author Mirko
	 * @param idAnimale
	 * @throws PecoraNeraPersaException
	 * @throws LupoPersoException
	 * @throws ListaAnimaliNull
	 * 
	 */
	public void rimuoviAnimale(Integer idAnimale)
			throws PecoraNeraPersaException, LupoPersoException {
		Integer index = this.trovaOccorrenza(idAnimale);
		try {
			this.atListaAnimali.remove(this.atListaAnimali.get(index));
		} catch (IndexOutOfBoundsException e) {
			if (idAnimale.equals(1003)) {
				throw new PecoraNeraPersaException();
			} else if (idAnimale.equals(1103)) {
				throw new LupoPersoException();
			}
			Logger.getGlobal().log(Level.WARNING,
					this.getClass().toString() + " reg an id: " + idAnimale, e);
			CLCHelper.stampa("Errore outofbound per id: " + idAnimale);
		}

	}

	/**
	 * Eccezione da lanciare nel caso di errore rimozione pecora Nera
	 * 
	 * @author Mirko
	 * 
	 */
	public class PecoraNeraPersaException extends Exception {
		public PecoraNeraPersaException() {
			super();
		}
	}

	/**
	 * Eccezione da lanciare nel caso di errore rimozione lupo
	 * 
	 * @author Mirko
	 * 
	 */
	public class LupoPersoException extends Exception {
		public LupoPersoException() {
			super();
		}
	}

	/**
	 * Permette di scambiare un {@link Animale} con un altro
	 * 
	 * @param animaleDaSostituire
	 *            {@link Animale} che si vuole sostitutire
	 * @param animaleSostituto
	 *            {@link Animale} che si vuole inserire
	 */
	public void scambiaAnimale(Animale animaleDaSostituire,
			Animale animaleSostituto) {
		this.rimuoviAnimale(animaleDaSostituire);
		this.aggiungiAnimale(animaleSostituto);
	}

	/**
	 * ritorna true se è occupata da animali otherwise
	 * 
	 * @return true o false
	 */
	public Boolean occupataDaAnimali() {
		if (this.atListaAnimali == null) {
			return false;
		}
		if (this.atListaAnimali.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Permette di sapere se nella regione sono presenti animali spostabili
	 * (solo ovini, no lupi)
	 * 
	 * @return true o false
	 */
	public Boolean occupataDaAnimaliSpostabili() {
		if (this.atListaAnimali == null) {
			return false;
		}
		if (this.atListaAnimali.size() > 0) {
			if ((this.atListaAnimali.size() == 1) && this.occupataDaLupo()) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Ritorna una lista di animali spostabili, quindi non lupi ma solo ovini Al
	 * suo interno utilizza il metodo occupataDaAnimaliSpostabili per sapere se
	 * la regione contiene animali spostabili
	 * 
	 * @return Un arraylist di animali
	 */
	public ArrayList<Animale> getAnimaliSpostabili() {
		if (this.occupataDaAnimaliSpostabili()) {
			return this.atListaAnimali;
		} else {
			return null;
		}
	}

	/**
	 * ritorna se la regione è occupata da montoni
	 * 
	 * @return true o false
	 */
	public Boolean occupataDaMontoni() {
		for (Animale item : this.atListaAnimali) {
			if (item instanceof Montone) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ritorna se la regione è occupata da pecore/pecorenere
	 * 
	 * @author Mirko
	 * @return true o false
	 */
	public Boolean occupataDaPecore() {
		for (Animale item : this.atListaAnimali) {
			if (item instanceof Pecora) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ritorna se la regione è occupata da pecore
	 * 
	 * @return
	 */
	public Boolean occupataDaPecoraNonNera() {
		for (Animale item : this.atListaAnimali) {
			if ((item instanceof Pecora) && !(item instanceof Pecoranera)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Ritorna true se la regione è occupata da almeno un animale che non sia
	 * pecora nera o lupo
	 * 
	 * @return
	 */
	public boolean occupataDaOviniNonNeri() {
		for (Animale item : this.atListaAnimali) {
			if ((item instanceof Animale) && !(item instanceof Pecoranera)
					&& !(item instanceof Lupo)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ritorna se la regione è occupata da pecora nera
	 * 
	 * @return
	 */
	public Boolean occupataDaPecoraNera() {
		for (Animale item : this.atListaAnimali) {
			if (item instanceof Pecoranera) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ritorna se la regione è occupata da agnelli
	 * 
	 * @return
	 */
	public Boolean occupataDaAgnello() {
		for (Animale item : this.atListaAnimali) {
			if (item instanceof Agnello) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ritorna se la regione è occupata da lupo
	 * 
	 * @return
	 */
	public Boolean occupataDaLupo() {
		for (Animale item : this.atListaAnimali) {
			if (item instanceof Lupo) {
				return true;
			}
		}
		return false;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.atTipoTerreno.toString();
	}

	// Inizio metodi privati

	/**
	 * trova l'index in array
	 * 
	 * @author Mirko
	 * @param id
	 * @return -1 se non trovato, altrimenti l'id
	 */
	private Integer trovaOccorrenza(Integer id) {
		for (int i = 0; i < this.atListaAnimali.size(); i++) {
			if (this.atListaAnimali.get(i).getId().equals(id)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Ritorna la prima occorrenza di un animale presente nella regione per ogni
	 * tipo abbattibile
	 * 
	 * @return un arrayList di animali
	 */
	public ArrayList<Animale> getAnimaliAbbattibili() {
		ArrayList<Animale> wvAnimaliAbbatibili = new ArrayList<Animale>();
		Boolean wvAgnello = false, wvPecora = false, wvMontoneBoolean = false;
		for (Animale animale : this.atListaAnimali) {
			if ((animale instanceof Agnello) && !wvAgnello) {
				wvAnimaliAbbatibili.add(animale);
			} else if (((animale instanceof Pecora) && !(animale instanceof Pecoranera))
					&& !wvPecora) {
				wvAnimaliAbbatibili.add(animale);
			} else if ((animale instanceof Montone) && !wvMontoneBoolean) {
				wvAnimaliAbbatibili.add(animale);
			}
		}
		return wvAnimaliAbbatibili;
	}

	/**
	 * Ritorna il numero di pecore nella regione
	 * 
	 * @return
	 */
	public int getNumeroPecore() {
		int i = 0;
		for (Animale animale : this.atListaAnimali) {
			if ((animale instanceof Pecora) && !(animale instanceof Pecoranera)) {
				i++;
			}
		}
		return i;
	}

	/**
	 * Ritorna il numero di montoni nella regione
	 * 
	 * @return
	 */
	public int getNumeroMontoni() {
		int i = 0;
		for (Animale animale : this.atListaAnimali) {
			if (animale instanceof Montone) {
				i++;
			}
		}
		return i;
	}

	/**
	 * Ritorna il numero di agnelli nella regione
	 * 
	 * @return
	 */
	public int getNumeroAgnelli() {
		int i = 0;
		for (Animale animale : this.atListaAnimali) {
			if (animale instanceof Agnello) {
				i++;
			}
		}
		return i;
	}

	/**
	 * Ritorna la prima pecora disponibile nella regione
	 * 
	 * @return
	 */
	public Pecora getPecora() {
		if (this.atListaAnimali != null) {
			if (this.occupataDaPecoraNonNera()) {
				for (Animale animale : this.atListaAnimali) {
					if ((animale instanceof Pecora)
							&& !(animale instanceof Pecoranera)) {
						return (Pecora) animale;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Ritorna il primo montone disponib
	 * 
	 * @return
	 */
	public Montone getMontone() {
		if (this.atListaAnimali != null) {
			if (this.occupataDaMontoni()) {
				for (Animale animale : this.atListaAnimali) {
					if (animale instanceof Montone) {
						return (Montone) animale;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Ritorna il primo agnello disponib
	 * 
	 * @return
	 */
	public Agnello getAgnello() {
		if (this.atListaAnimali != null) {
			if (this.occupataDaAgnello()) {
				for (Animale animale : this.atListaAnimali) {
					if (animale instanceof Agnello) {
						return (Agnello) animale;
					}
				}
			}
		}
		return null;
	}
}
