package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * la strada
 * 
 * @author Mirko
 * 
 */
public class Strada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7669682515697696803L;
	/*
	 * Codice ID: ZTxTyNs
	 * 
	 * Z: Tipo ID Tx: Tipo numerico di uno dei 2 terreni adiacenti Ty: Tipo
	 * numerico dell'altro dei 2 terreni adiacenti
	 * 
	 * Ns: Numero strada, compreso tra 1-6
	 * 
	 * (primo in alto sinistra nella mappa di gioco) Z622
	 */
	private final Integer atID;
	private Integer atNumeroStrada;
	private Regione[] atRegioniLimitrofe;
	private ArrayList<Strada> atStradeCollegate;
	private Occupante atOccupante;

	// Eccezioni
	/**
	 * 
	 * @author Mirko
	 * @exception thown
	 *                when a street is enroached by a Recinto object
	 * 
	 */
	public class OccupataDaRecintoException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Costruttore
		 */
		public OccupataDaRecintoException() {
			super();
		}

		/**
		 * Costruttore
		 * 
		 * @param s
		 *            {@link String} che rappresenta il messaggio da allegare
		 *            all'eccezione
		 */
		public OccupataDaRecintoException(String s) {
			super(s);
		}
	}

	/**
	 * 
	 * @author Mirko
	 * @exception thown
	 *                quando una strada è occupata da una PedinaPastore, e si
	 *                cerca di metterci un altro oggetto che implementa
	 *                {@link Occupante}
	 * 
	 */
	public class OccupataDaPastoreException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Costruttore
		 */
		public OccupataDaPastoreException() {
			super();
		}

		/**
		 * Costruttore
		 * 
		 * @param s
		 *            {@link String} che rappresenta il messaggio da allegare
		 *            all'eccezione
		 */
		public OccupataDaPastoreException(String s) {
			super(s);
		}
	}

	/**
	 * Costruttore da richiamare nel momento in bisogna creare la mappa
	 * 
	 * @param ID
	 *            intero che indica l'id della mappa
	 */
	public Strada(Integer ID) {
		this.atID = ID;
	}

	/**
	 * Costruttore da richiamare quando bisogna creare la mappa, setta anchje il
	 * numero della strada oltre all'id univoco
	 * 
	 * @param ID
	 * @param numeroStrada
	 */
	public Strada(Integer ID, Integer numeroStrada) {
		this.atID = ID;
		this.atNumeroStrada = numeroStrada;
	}

	/**
	 * 
	 * @param id
	 * @param regioniLimitrofe
	 */
	public Strada(Integer id, Regione[] regioniLimitrofe, Integer numeroStrada) {
		this.atID = id;
		this.atNumeroStrada = numeroStrada;
		this.atRegioniLimitrofe = regioniLimitrofe;
		this.atOccupante = null;
	}

	/**
	 * 
	 * @param id
	 * @param regioniLimitrofe
	 * @param stradeCollegate
	 */
	public Strada(Integer id, Regione[] regioniLimitrofe, Integer numeroStrada, ArrayList<Strada> stradeCollegate) {
		this.atID = id;
		this.atNumeroStrada = numeroStrada;
		this.atRegioniLimitrofe = regioniLimitrofe;
		this.atStradeCollegate = stradeCollegate;
		this.atOccupante = null;
	}

	/**
	 * ritorna l'id
	 * 
	 * @return
	 */
	public Integer getId() {
		return this.atID;
	}

	/**
	 * ritorna il numero della strada
	 * 
	 * @return
	 */
	public Integer getNumeroStrada() {
		return this.atNumeroStrada;
	}

	/**
	 * Ritorna la {@link Regione} opposta a quella indicata
	 * 
	 * @param regioneIndicata
	 * @return Ritorna un oggetto di tipo {@link Regione} che rappresenta la
	 *         {@link Regione} opposta della stessa strada, se la strada non
	 *         dovesse avere regioni opposte, ritorna quella indicata
	 */
	public Regione getRegioneOpposta(Regione regioneIndicata) {
		Regione wvRegioneOpposta = regioneIndicata;

		for (Regione wvItemRegione : this.atRegioniLimitrofe) {
			if (!wvItemRegione.equals(regioneIndicata)) {
				wvRegioneOpposta = wvItemRegione;
			}
		}

		return wvRegioneOpposta;
	}

	/**
	 * ritorna le regioni limitrofe
	 * 
	 * @return
	 */
	public Regione[] getRegioniLimitrofe() {
		return this.atRegioniLimitrofe;
	}

	/**
	 * ritorna le strade collegate
	 * 
	 * @return
	 */
	public ArrayList<Strada> getStradeCollegate() {
		return this.atStradeCollegate;
	}

	/**
	 * setta le strade collegate
	 * 
	 * @param stradeCollegate
	 */
	public void setStradeCollegate(ArrayList<Strada> stradeCollegate) {
		this.atStradeCollegate = stradeCollegate;
	}

	/**
	 * @return ritorna l'occupante
	 */
	public Occupante getOccupante() {
		if (this.atOccupante == null) {
			return null;
		}
		return this.atOccupante;
	}

	/**
	 * setta l'occupante
	 * 
	 * @param atOccupante
	 */
	public void setOccupante(Occupante occupante) {
		this.atOccupante = occupante;
	}

	/**
	 * Indica se la strada è occupata
	 * 
	 * @return Ritorna un {@link Boolean} che indica se la strada è occupata
	 */
	public Boolean Occupata() {
		if (this.atOccupante != null) {
			return true;
		}
		return false;
	}

	/**
	 * dice se è occupata da un recinto
	 * 
	 * @return
	 */
	public Boolean occupataDaRecinto() {
		if ((this.atOccupante != null) && (this.atOccupante instanceof PedinaRecinto)) {
			return true;
		}
		return false;
	}

	/**
	 * ritorna se è occupata da pastore
	 * 
	 * @return
	 */
	public Boolean occupadaDaPastore() {
		if ((this.atOccupante != null) && (this.atOccupante instanceof PedinaPastore)) {
			return true;
		}
		return false;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String wvRitorno = "ID: ";
		wvRitorno += this.atID;
		wvRitorno += "\nRegioni Limitrofe:\n";

		if (this.atRegioniLimitrofe != null) {
			for (Regione item : this.atRegioniLimitrofe) {
				wvRitorno += (item.toString() + "|");
			}
		} else {
			wvRitorno += "Nessuna";
		}

		wvRitorno += "\nStrade collegate:\n";

		if (this.atStradeCollegate != null) {
			for (Strada item : this.atStradeCollegate) {
				wvRitorno += item.getId();
			}
		} else {
			wvRitorno += "Nessuna";
		}

		return wvRitorno;
	}

	/**
	 * Setta le regioni limitrofe solo la prima volta
	 * 
	 * @param listaRegioni
	 *            la lista delle regioni limitrofe
	 */
	public void setRegioniLimitrofe(Regione[] listaRegioni) {
		if (this.atRegioniLimitrofe == null) {
			this.atRegioniLimitrofe = listaRegioni;
		}
	}

	/**
	 * da richiamare quando il pastore lascia la regione
	 */
	public void pastoreLascia() {
		this.setOccupante(new PedinaRecinto());
	}

	/**
	 * da richiamare quando il pastore lascia la regione durante il turno finale
	 */
	public void pastoreLasciaFinale() {
		this.atOccupante = new PedinaRecintoFinale();
	}

	/**
	 * Ritorna gli id dei pastori vicini
	 * 
	 * @param giocatore
	 * @return
	 */
	public ArrayList<Integer> getPastoriVicini(Giocatore giocatore) {
		ArrayList<Integer> wvIdPastori = new ArrayList<Integer>();
		for (Strada stradaVicina : this.getStradeCollegate()) {
			if (stradaVicina.getOccupante() instanceof PedinaPastore) {
				boolean isMia = false;
				for (PedinaPastore pedinaPastore : giocatore.getPedinePastore()) {
					if (pedinaPastore.getId().equals(stradaVicina.getOccupante().getId())) {
						isMia = true;
					}
				}
				if (isMia) {
					continue;
				}
				wvIdPastori.add(stradaVicina.getOccupante().getId());
			}
		}
		return wvIdPastori;
	}
}
