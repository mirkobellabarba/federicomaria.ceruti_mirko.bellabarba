package model;

/**
 * la classe che rappresenta il recinto
 * 
 * @author Mirko
 * 
 */
public class PedinaRecinto extends Occupante {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4959059348843162147L;

	/**
	 * 
	 * Rappresenta un'estensione della Classe astratta Occupante
	 * 
	 * @author Federico
	 * 
	 */
	public PedinaRecinto(Integer id) {
		super(id);
	}

	/**
	 * costruttore
	 */
	public PedinaRecinto() {
		super();
	}
}
