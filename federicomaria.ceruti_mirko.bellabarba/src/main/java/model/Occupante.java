package model;

import java.io.Serializable;

/**
 * 
 * Fornisce l'astrazione per gli oggetti che possono occupare una Strada
 * 
 * @author Federico
 * 
 */
public abstract class Occupante implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2150974918557029276L;
	private final Integer atID;
	private Strada atStradaOccupata;

	/**
	 * Fornisce il costruttore per l'oggetto Occupante
	 * 
	 * @param id
	 *            Id dell'oggetto Occupante
	 * @param Strada_Occupata
	 *            Strada occupata dall'oggetto occupante
	 */
	public Occupante(Integer id) {
		atID = id;
	}

	public Occupante() {
		atID = null;
	}

	/**
	 * Ritorna L'ID dell'Occupante
	 * 
	 * @return ritorna un intero rappresentante l'ID dell'Occupante
	 */
	public Integer getId() {
		return atID;
	}

	/**
	 * Ritorna la Strada occupata dall' Occupante
	 * 
	 * @return ritorna una Strada rappresentante la Strda occupata dal'Occupante
	 */
	public Strada getStradaOccupata() {
		return atStradaOccupata;
	}

	/**
	 * Cambia l'attuale Strada che occupa l'Occupante con un altra Strada
	 * 
	 * @param stradaArrivo
	 *            Oggetto di tipo Strada che sostituisce l'attuale Strada
	 *            occuapata dall'Occupante
	 */
	public void setStradaOccupata(Strada stradaArrivo) {
		atStradaOccupata = stradaArrivo;
	}
}
