package testHelper;

import helper.FileIOHelper;
import helper.FileIOHelper.FileNonTrovatoException;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.TestCase;

import org.junit.Test;

import view.Start;

public class TestFileIOHelper extends TestCase {

	@Test
	public void testCostruttore() {
		FileIOHelper file = new FileIOHelper("regioni.txt");
		assertEquals("regioni.txt", file.getIndirizzo());
	}

	@Test
	public void testLettura() {
		Start.abilitaLogger = false;
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		}
		// Testo file regioni (1° e ultima riga)
		FileIOHelper file = new FileIOHelper("regioni.txt");
		ArrayList<String> righe = null;
		try {
			righe = file.getFile();
		} catch (FileNonTrovatoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		}
		assertEquals("101|PIANURA|102,502,402", righe.get(0));
		assertEquals("1901|LAGO|3602,3702,4002,4202",
				righe.get(righe.size() - 1));

		// Testo file strade (1° e ultima riga)
		FileIOHelper file2 = new FileIOHelper("strade.txt");
		ArrayList<String> righe2 = null;
		try {
			righe2 = file2.getFile();
		} catch (FileNonTrovatoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		}
		assertEquals("102|101,201|502,602|2", righe2.get(0));
		assertEquals("4202|1801,1901|3602,3902|5",
				righe2.get(righe2.size() - 1));
	}

	@Test
	public void testCompleto() {
		Start.abilitaLogger = false;
		// Scrittura
		FileIOHelper fileIOHelper = new FileIOHelper("testScrittura.txt");
		ArrayList<String> righe = new ArrayList<String>();
		righe.add("hello");
		righe.add("world");
		fileIOHelper.writeFile(righe);

		// Cancellazione
		boolean isCancellato = false;
		isCancellato = fileIOHelper.deleteFile();
		assertEquals(true, isCancellato);
	}

}
