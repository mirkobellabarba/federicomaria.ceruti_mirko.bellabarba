package testHelper;

import static org.junit.Assert.*;
import helper.SwingHelper;

import org.junit.Test;

public class TestSwingHelper {

	@Test
	public void test() {
		SwingHelper swingHelper = new SwingHelper();
		swingHelper.resetId();
		swingHelper.setIdCliccato(46);
		assertEquals(new Integer(46), swingHelper.getIdCliccato());
	}

}
