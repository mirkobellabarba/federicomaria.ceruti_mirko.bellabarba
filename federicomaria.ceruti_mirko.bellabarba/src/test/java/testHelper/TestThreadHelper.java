package testHelper;

import static org.junit.Assert.*;
import helper.ThreadHelper;

import org.junit.Test;

public class TestThreadHelper {

	@Test
	public void test() {
		ThreadHelper threadHelper = new ThreadHelper();
		threadHelper.resetId();
		threadHelper.setLocked(true);
		assertEquals(true, threadHelper.getLocked());
	}

}
