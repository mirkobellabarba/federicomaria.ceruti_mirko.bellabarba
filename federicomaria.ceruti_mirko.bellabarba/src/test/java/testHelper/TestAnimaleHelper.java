package testHelper;

import static org.junit.Assert.*;
import helper.AnimaleHelper;
import model.Animale;
import model.Pecora;

import org.junit.Test;

public class TestAnimaleHelper {

	@Test
	public void test() {
		AnimaleHelper animaleHelper = new AnimaleHelper();
		animaleHelper.resetAnimale();
		Animale animale = new Pecora(0, null);
		animaleHelper.setTipoAnimale(animale);
		Animale animaleRitornato = animaleHelper.getTipoAnimale();
		assertEquals(animale, animaleRitornato);
	}

}
