package testHelper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;
import helper.MapHelper;

import java.util.ArrayList;

import model.EColore;
import model.ETipoTerreno;
import model.Giocatore;
import model.Mappa;
import model.PedinaPastore;
import model.Regione;
import model.Strada;

import org.junit.Test;

public class TestMapHelper {
	private ArrayList<Giocatore> giocatori;

	@Test
	public void testEnum() {
		assertEquals(ETipoTerreno.PIANURA, ETipoTerreno.valueOf("PIANURA"));
	}

	@Test
	public void testCreaMappa4Giocatori() {

		this.giocatori = new ArrayList<Giocatore>();
		PedinaPastore pMirko = new PedinaPastore(103, EColore.ROSSO);
		PedinaPastore[] pedineMirko = new PedinaPastore[1];
		pedineMirko[0] = pMirko;
		Giocatore mirko = new Giocatore(100, "Mirko", 1, pedineMirko,
				ETipoTerreno.COLLINA);

		PedinaPastore pCeru = new PedinaPastore(203, EColore.BLU);
		PedinaPastore[] pedineCeru = new PedinaPastore[1];
		pedineCeru[0] = pCeru;
		Giocatore ceru = new Giocatore(200, "Ceru", 2, pedineCeru,
				ETipoTerreno.PIANURA);

		PedinaPastore pMattia = new PedinaPastore(303, EColore.GIALLO);
		PedinaPastore[] pedineMattia = new PedinaPastore[1];
		pedineMattia[0] = pMattia;
		Giocatore mattia = new Giocatore(300, "Mattia", 3, pedineMattia,
				ETipoTerreno.MONTAGNA);

		PedinaPastore pLorenzo = new PedinaPastore(403, EColore.VERDE);
		PedinaPastore[] pedineLorenzo = new PedinaPastore[1];
		pedineLorenzo[0] = pLorenzo;
		Giocatore lorenzo = new Giocatore(400, "Lorenzo", 4, pedineLorenzo,
				ETipoTerreno.DESERTO);

		this.giocatori.add(mirko);
		this.giocatori.add(ceru);
		this.giocatori.add(mattia);
		this.giocatori.add(lorenzo);

		if (this.giocatori.size() > 2) {
			for (Giocatore giocatore : this.giocatori) {
				giocatore.setDenaroIniziale(20);
			}
		} else {
			for (Giocatore giocatore : this.giocatori) {
				giocatore.setDenaroIniziale(30);
			}
		}

		MapHelper helper = new MapHelper(this.giocatori);

		helper.caricaMappa();
		Mappa mappa = helper.getMappa();
		assertEquals(4, mappa.getGiocatori().size());
		// Faccio il test su sheepsburg
		assertEquals(2, mappa.getRegioni().get(9).getAnimali().size());
		for (Regione regione : mappa.getRegioni()) {
			if (regione.getTipoTerreno().equals(ETipoTerreno.SHEEPSBURG)) {
				assertEquals(2, regione.getAnimali().size());
			} else {
				assertEquals(1, regione.getAnimali().size());
			}
		}
		assertNotEquals(0, helper.getNumeroPecore());
		assertNotEquals(0, helper.getNumeroMontoni());
	}

	@Test
	public void testCreaMappa2Giocatori() {

		this.giocatori = new ArrayList<Giocatore>();
		PedinaPastore pMirko = new PedinaPastore(103, EColore.ROSSO);
		PedinaPastore[] pedineMirko = new PedinaPastore[1];
		pedineMirko[0] = pMirko;
		Giocatore mirko = new Giocatore(100, "Mirko", 1, pedineMirko,
				ETipoTerreno.COLLINA);

		PedinaPastore pCeru = new PedinaPastore(203, EColore.BLU);
		PedinaPastore[] pedineCeru = new PedinaPastore[1];
		pedineCeru[0] = pCeru;
		Giocatore ceru = new Giocatore(200, "Ceru", 2, pedineCeru,
				ETipoTerreno.PIANURA);

		this.giocatori.add(mirko);
		this.giocatori.add(ceru);

		if (this.giocatori.size() > 2) {
			for (Giocatore giocatore : this.giocatori) {
				giocatore.setDenaroIniziale(20);
			}
		} else {
			for (Giocatore giocatore : this.giocatori) {
				giocatore.setDenaroIniziale(30);
			}
		}

		MapHelper helper = new MapHelper(this.giocatori);

		helper.caricaMappa();
		Mappa mappa = helper.getMappa();
		assertEquals(2, mappa.getGiocatori().size());
		assertEquals((Integer) 30, mappa.getGiocatori().get(0)
				.getDanaroResiduo());
		// Faccio il test su sheepsburg
		assertEquals(2, mappa.getRegioni().get(9).getAnimali().size());
	}

	@Test
	public void testStrade() {
		this.giocatori = new ArrayList<Giocatore>();
		PedinaPastore pMirko = new PedinaPastore(103, EColore.ROSSO);
		PedinaPastore[] pedineMirko = new PedinaPastore[1];
		pedineMirko[0] = pMirko;
		Giocatore mirko = new Giocatore(100, "Mirko", 1, pedineMirko,
				ETipoTerreno.COLLINA);

		PedinaPastore pCeru = new PedinaPastore(203, EColore.BLU);
		PedinaPastore[] pedineCeru = new PedinaPastore[1];
		pedineCeru[0] = pCeru;
		Giocatore ceru = new Giocatore(200, "Ceru", 2, pedineCeru,
				ETipoTerreno.PIANURA);

		this.giocatori.add(mirko);
		this.giocatori.add(ceru);

		if (this.giocatori.size() > 2) {
			for (Giocatore giocatore : this.giocatori) {
				giocatore.setDenaroIniziale(20);
			}
		} else {
			for (Giocatore giocatore : this.giocatori) {
				giocatore.setDenaroIniziale(30);
			}
		}

		MapHelper helper = new MapHelper(this.giocatori);
		helper.caricaMappa();
		Mappa mappa = helper.getMappa();

		for (Strada wvStrada : mappa.getStrade()) {
			for (Strada wvStrada2 : wvStrada.getStradeCollegate()) {
				if (wvStrada2.getId().equals(wvStrada.getId())) {
					fail(wvStrada.getId().toString());
				}
			}
		}

	}
}
