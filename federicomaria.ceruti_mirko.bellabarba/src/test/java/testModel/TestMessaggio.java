package testModel;

import static org.junit.Assert.assertEquals;
import model.ETipoTerreno;
import model.mosse.EIntestazioneMessaggio;
import model.mosse.Messaggio;

import org.junit.Test;

import view.CLCHelper;
import view.Start;

public class TestMessaggio {

	Messaggio messaggio;

	/**
	 * Controlla che l'id inserito in messaggio sia esattamente quello
	 * specificato
	 */
	@Test
	public void testIdPartita() {
		Start.abilitaLogger = false;
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			CLCHelper.stampaAncheInTest(e.getMessage());
		}
		this.messaggio = new Messaggio();
		this.messaggio.idPartita(400);
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._IDPARTITA.toString() + ":400");
	}

	/**
	 * Controlla che l'id inserito in messaggio sia esattamente quello
	 * specificato
	 */
	@Test
	public void testIdGiocatore() {
		this.messaggio = new Messaggio();
		this.messaggio.idGiocatore(302);
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._IDGIOCATORE.toString() + ":302");

	}

	/**
	 * Controlla che il messaggio sia di uscita
	 */
	@Test
	public void testUscita() {
		this.messaggio = new Messaggio();
		this.messaggio.uscita();
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._USCITA.toString());
	}

	/**
	 * Controlla che il messaggio sia di ACK
	 */
	@Test
	public void testACK() {
		this.messaggio = new Messaggio();
		this.messaggio.ack();
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._ACK.toString());
	}

	/**
	 * Controlla che il messaggio sia di NACK
	 */
	@Test
	public void testNACK() {
		this.messaggio = new Messaggio();
		this.messaggio.nack();
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._NACK.toString());
	}

	/**
	 * Controlla che il messaggio sia Posizione iniziale
	 */
	@Test
	public void testPosizioneIniziale() {
		this.messaggio = new Messaggio();
		this.messaggio.posizioneIniziale(105, 502);
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._POSIZIONEINIZIALE.toString() + ":105,502");
	}

	/**
	 * Controlla che il messaggio sia di tessera terreno
	 */
	@Test
	public void testTessereTerreno() {
		this.messaggio = new Messaggio();
		this.messaggio.tesseraTerreno(ETipoTerreno.DESERTO.toString());
		assertEquals(this.messaggio.getMessaggio(), EIntestazioneMessaggio._TESSERATERRENO.toString() + ":DESERTO");
	}
}
