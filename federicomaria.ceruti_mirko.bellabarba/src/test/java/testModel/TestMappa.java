package testModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import helper.MapHelper;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Animale;
import model.EColore;
import model.ETipoTerreno;
import model.Giocatore;
import model.Mappa;
import model.Pecora;
import model.PedinaPastore;
import model.Regione;
import model.Regione.LupoPersoException;
import model.Regione.PecoraNeraPersaException;
import model.mosse.Movimento;

import org.junit.Before;
import org.junit.Test;

import view.Start;

public class TestMappa {

	Mappa atMappa;

	/**
	 * indica il numero di partite da effettuare per il test
	 */
	@Before
	public void testPopola() {
		Start.abilitaLogger = false;
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		}
		MapHelper wvHelper = new MapHelper();
		wvHelper.caricaMappa();
		this.atMappa = null;
		this.atMappa = wvHelper.getMappa();
		assertTrue(this.atMappa != null);

		// PartitaServer wvPartitaServer = new PartitaServer(200);

		ArrayList<Giocatore> wv_giocatori = new ArrayList<Giocatore>();
		PedinaPastore[] wv_pedine;

		wv_pedine = new PedinaPastore[1];
		wv_pedine[0] = new PedinaPastore(104, EColore.values()[0]);
		wv_giocatori.add(new Giocatore(106, "N1", 1, wv_pedine,
				ETipoTerreno.BOSCO, 20));

		wv_pedine = new PedinaPastore[1];
		wv_pedine[0] = new PedinaPastore(204, EColore.values()[1]);
		wv_giocatori.add(new Giocatore(206, "N2", 1, wv_pedine,
				ETipoTerreno.COLLINA, 20));

		wv_pedine = new PedinaPastore[1];
		wv_pedine[0] = new PedinaPastore(304, EColore.values()[2]);
		wv_giocatori.add(new Giocatore(306, "N3", 1, wv_pedine,
				ETipoTerreno.PIANURA, 20));

		wv_pedine = new PedinaPastore[1];
		wv_pedine[0] = new PedinaPastore(404, EColore.values()[3]);
		wv_giocatori.add(new Giocatore(406, "N4", 1, wv_pedine,
				ETipoTerreno.DESERTO, 20));

		this.atMappa.setListaGiocatori(wv_giocatori);
		assertTrue(this.atMappa.getGiocatori() != null);
	}

	/**
	 * TEst per controllare la correttezza dei metodi dele regioni
	 */
	@Test
	public void testRegioni() {
		Regione wvRegione = this.atMappa.getRegione(101);
		Regione wvRegione2 = this.atMappa.getRegione(201);
		assertTrue(wvRegione.getAnimali() != null);

		Animale wvAnimale = wvRegione.getAnimali().get(0);
		Integer wvId = wvAnimale.getId();

		try {
			this.atMappa.movimento(new Movimento(wvId, wvRegione.getId(),
					wvRegione2.getId(), 0));
		} catch (PecoraNeraPersaException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		} catch (LupoPersoException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(),
						e);
			}
		}

		assertEquals(0, wvRegione.getAnimali().size());
		assertEquals(2, wvRegione2.getAnimali().size());
		assertEquals(201, wvAnimale.getRegioneOccuapta().getId().intValue());
		Animale wvAnimale2 = new Pecora(1003, wvRegione2);
		this.atMappa.scambiaAnimale(wvAnimale, wvAnimale2);

		assertEquals(wvAnimale2, wvRegione2.getAnimali().get(1));

	}

}
