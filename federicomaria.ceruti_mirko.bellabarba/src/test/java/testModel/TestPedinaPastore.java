package testModel;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import model.EColore;
import model.ETipoTerreno;
import model.PedinaPastore;
import model.Regione;

import org.junit.Test;

import view.Start;

public class TestPedinaPastore {

	/**
	 * testo il costruttore della pedina pastore
	 */
	@Test
	public void testCostruttore() {
		Start.abilitaLogger = false;
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(), e);
			}
		}
		Regione[] Regioni = new Regione[1];
		Regioni[0] = new Regione(5, ETipoTerreno.COLLINA);
		PedinaPastore pastore = new PedinaPastore(5, EColore.ROSSO);
		assertEquals((Integer) 5, pastore.getId());
	}

}
