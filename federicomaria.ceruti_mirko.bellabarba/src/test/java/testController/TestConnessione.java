package testController;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import model.mosse.EIntestazioneMessaggio;
import model.mosse.Messaggio;

import org.junit.Before;
import org.junit.Test;

import view.CLCHelper;
import view.Start;
import controllerClient.ConnessioneSocket;

public class TestConnessione {

	private ServerSocket ss;
	private Socket socketS;

	class accetta extends Thread {
		@Override
		public void run() {
			try {
				System.out.println("Avvio");
				TestConnessione.this.ss = new ServerSocket(4079);
				TestConnessione.this.socketS = TestConnessione.this.ss.accept();

				System.out.println("SERVER: Client Connesso");

				InputStream is = TestConnessione.this.socketS.getInputStream();
				ObjectInputStream oisReady = new ObjectInputStream(is);
				Messaggio pronto = (Messaggio) oisReady.readObject();
				if (pronto.getMessaggio().toString().equals(EIntestazioneMessaggio._PRONTO.toString())) {
					System.out.println("SERVER: Client Pronto (" + pronto.getMessaggio().toString() + ")");
				}
				System.out.println("SERVER: INVIO MESSAGGIO");
				OutputStream os = TestConnessione.this.socketS.getOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(os);
				Messaggio idpartita = new Messaggio();
				idpartita.idPartita(new Integer(100));
				oos.writeObject(idpartita);
				oos.flush();
				System.out.println("SERVER: Inviato: " + idpartita.getMessaggio());

				ObjectInputStream ois = new ObjectInputStream(is);
				Messaggio risposta = (Messaggio) ois.readObject();
				System.out.println(risposta);

				if (risposta.getMessaggio().toString().equals("_USCITA")) {
					System.out.println("SERVER: _USCITA, chiudo tutto");
					ois.close();
					oos.close();
					TestConnessione.this.ss.close();
					TestConnessione.this.socketS.close();
				}
			} catch (IOException e) {
				System.err.println("Errore run socket: " + e.getMessage());
			} catch (ClassNotFoundException e) {
				CLCHelper.stampaEccezione(e);
			}

		}
	}

	@Before
	public void AttivaServer() throws InterruptedException {
		Start.abilitaLogger = false;
		accetta a = new accetta();
		a.start();
		Thread.sleep(500);
	}

	@Test
	public void testRiceviIDPartita() throws IOException, ClassNotFoundException, InterruptedException {
		ConnessioneSocket connessioneClient = new ConnessioneSocket();
		boolean connesso = connessioneClient.connetti("Mirko");
		if (connesso == false) {
			return;
		}

		// System.out.println("CLIENT: Aspetto IDPartita...");

		Integer idPartita = connessioneClient.getIdPartita();
		// System.out.println("CLIENT: Ricevuto: "+idPartita.toString());
		assertEquals((Integer) 100, idPartita);

		// System.out.println("CLIENT: Invio: _USCITA");
		connessioneClient.chiudiSocket();
	}

}
