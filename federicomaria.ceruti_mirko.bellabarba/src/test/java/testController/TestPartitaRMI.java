package testController;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import view.CLCHelper;
import view.ETipoGrafica;
import view.GraficaCLC;
import view.Start;
import controllerCommon.ETipoConnessione;
import controllerServer.GestorePartitaRMI;

public class TestPartitaRMI {

	/**
	 * indica il numero di partite da effettuare per il test
	 */
	public static final Integer NUMERO_PARTITE_TEST = 3;
	private GestorePartitaRMI at_server;

	/**
	 * Prepara l'ambiente per per iniziare i test sulla aprtita RMI
	 */
	@Before
	public void testPrepara() {
		Start.abilitaTest = true;
		Start.abilitaTest2G = false;
		Start.abilitaLogger = false;
		this.at_server = GestorePartitaRMI.istanzia();
	}

	/**
	 * Testa un numero di partite predefinito
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testPartitaRMI() {
		this.at_server.start();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			CLCHelper.stampaAncheInTest(e1.getMessage());
		}
		GraficaCLC wvGestoreGrafica;
		GraficaCLC wvGestoreGrafica2;
		GraficaCLC wvGestoreGrafica3;
		GraficaCLC wvGestoreGrafica4;

		for (int i = 0; i < NUMERO_PARTITE_TEST; i++) {
			CLCHelper.stampa("Avvio partita test: " + i);
			wvGestoreGrafica = GraficaCLC.istanziaTest(ETipoGrafica.CLC, ETipoConnessione.RMI, "Giocatore1");
			wvGestoreGrafica2 = GraficaCLC.istanziaTest(ETipoGrafica.CLC, ETipoConnessione.RMI, "Giocatore2");
			wvGestoreGrafica3 = GraficaCLC.istanziaTest(ETipoGrafica.CLC, ETipoConnessione.RMI, "Giocatore3");
			wvGestoreGrafica4 = GraficaCLC.istanziaTest(ETipoGrafica.CLC, ETipoConnessione.RMI, "Giocatore4");

			try {
				Thread.sleep(500);
				wvGestoreGrafica.start();
				Thread.sleep(10);
				wvGestoreGrafica2.start();
				Thread.sleep(10);
				wvGestoreGrafica3.start();
				Thread.sleep(10);
				wvGestoreGrafica4.start();
				Thread.sleep(5000);

				if (wvGestoreGrafica.isAlive()) {
					wvGestoreGrafica.stop();
				}
				if (wvGestoreGrafica2.isAlive()) {
					wvGestoreGrafica.stop();
				}
				if (wvGestoreGrafica3.isAlive()) {
					wvGestoreGrafica.stop();
				}
				if (wvGestoreGrafica4.isAlive()) {
					wvGestoreGrafica.stop();
				}
				// CLCHelper.stampaAncheInTest("Sono in join");
				// wvGestoreGrafica.join();
				CLCHelper.stampaAncheInTest("Client 1 - fine");
				// wvGestoreGrafica2.join();
				CLCHelper.stampaAncheInTest("Client 2 - fine");
				// wvGestoreGrafica3.join();
				CLCHelper.stampaAncheInTest("Client 3 - fine");
				// wvGestoreGrafica4.join();
				CLCHelper.stampaAncheInTest("Client 4 - fine");
			} catch (InterruptedException e) {
				if (Start.abilitaLogger) {
					Logger.getGlobal().log(Level.INFO, this.getClass().toString(), e);
				}
			}
		}

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			if (Start.abilitaLogger) {
				Logger.getGlobal().log(Level.INFO, this.getClass().toString(), e);
			}
		}

	}

	/**
	 * viene eseguito alla fine del test riporta l'ambiente allo stato
	 * originale, prma dell'avvio del server
	 */
	@After
	public void testFine() {
		Start.abilitaTest = false;
		Start.abilitaTest2G = false;
		CLCHelper.stampaAncheInTest("Fine Test");
		GestorePartitaRMI.termina();
	}
}
